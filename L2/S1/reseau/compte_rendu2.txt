1.1) Il s'agit du protocole Ethernet 2. L'adresse source : 00:1b:fc:23:f0:94, l'adresse de destination 00:07:cb:3e:fd:73. Elles sont codées sur 6 octets. 6 octets = 48 bits, donc il y a (2^48)-1 adresses possibiles.

1.2) Le protocole utilisé est IPv4. L'adresse source : 192.168.0.10, l'adresse destination : 212.27.40.240. Elle est codée sur 4 octets, ce qui donne (2^32)-1 adresses possibles.

1.3) L'adresse de liaison de destination est invariable car il s'agit de l'adresse matérielle de la machine destination, alors que l'acheminement de l'information se fait de proche en proche, donc d'adresse en adresse.

1.4) Cette adresse est un canal de diffusion qui permet l'envoi d'information à toutes les machines connectées aux réseaux disponibles, sans aucune distinction.


2.1)L'adresse mac de ma machine est : 80:c5:f2:b8:fe:bd
	Le constructeur est AzureWave Technology Inc.

2.2) L'adresse IP de ma machine : 192.168.43.91
	Le masque de réseau est 255.255.255.0. Cela signifie que 192.168.43 est ma partie réseau, et 91 ma partie équipement


3.1)	RIPE-NCC (Réseaux IP Européens, créé en 1992) pour l'Europe et le Moyen-Orient ;
    	APNIC (Asia Pacific Network Information Center, créé en 1993) pour l'Asie et le Pacifique ;
    	ARIN (American Registry for Internet Numbers, créé en 1997) pour l'Amérique du Nord (entre 1993 et 1997, ce rôle était attribué à InterNIC) ;
    	LACNIC (Latin America and Caribbean Network Information Center, créé en 1999) pour l'Amérique latine et les îles des Caraïbes ;
    	AfriNIC (African Network Information Center, créé en 2005) pour l'Afrique.

3.2.1) Le préfixe 193 est alloué a RIPE-NCC (Réseaux IP Européens)

3.2.2) 

person:         Tovoherizo RAKOTONAVALONA
address:        TELECOM Lille1
address:        Cite Scientifique, rue Guglielmo Marconi
address:        59653 Villeneuve d'Ascq cedex
phone:          +33 3 20 33 55 63
fax-no:         +33 3 20 33 55 99
nic-hdl:        TR3368-RIPE
mnt-by:         RENATER-MNT
remarks:        changed:        rensvp@renater.fr 20120713
created:        2012-07-13T07:59:19Z
last-modified:  2015-08-07T14:18:58Z
source:         RIPE # Filtered

3.2.3) Le préfixe 64 appartient à l'ARIN (American registry)

OrgName:        MegaPath Networks Inc.
OrgId:          MENT
Address:        6800 Koll Center Parkway
Address:        Suite 200
City:           Pleasanton
StateProv:      CA
PostalCode:     94566
Country:        US
RegDate:        
Updated:        2017-01-28
Ref:            https://rdap.arin.net/registry/entity/MENT

Le préfixe 202 appartient à l'APNIC (African Network Information Center)

person:         Nasir Abdul Rahimy
nic-hdl:        NA62-AP
e-mail:         anasir@ceretechs.com
address:        House # 647 Daramsal Street, Karte Parwan II, Kabul, Afghanistan
phone:          +93-070-274826
fax-no:         +93-079-300275
country:        af
mnt-by:         MAINT-AP-CERETECHS
last-modified:  2008-09-04T07:29:22Z
source:         APNIC

3.3) Le préfixe 130 appartient à l'ARIN (American Registry)

NetRange:       130.120.0.0 - 130.120.255.255
CIDR:           130.120.0.0/16
NetName:        RIPE-ERX-130-120-0-0
NetHandle:      NET-130-120-0-0-1
Parent:         NET130 (NET-130-0-0-0-0)
NetType:        Early Registrations, Transferred to RIPE NCC
OriginAS:       
Organization:   RIPE Network Coordination Centre (RIPE)
RegDate:        2003-11-12
Updated:        2004-03-02
Comment:        These addresses have been further assigned to users in
Comment:        the RIPE NCC region. Contact information can be found in
Comment:        the RIPE database at http://www.ripe.net/whois
Ref:            https://rdap.arin.net/registry/ip/130.120.0.0

Found a referral to whois.ripe.net.

On peut en déduire que la plage d'adresse 130.120.0.0 - 130.120.255.255 appartient en réalité à RIPE mais est redirigé depuis ARIN.

person:         Alexandre GOUVERNEUR
address:        Universite Paul Sabatier
address:        118, route de Narbonne
address:        F-31062 Toulouse CEDEX, France
phone:          +33 5 61 36 60 23
fax-no:         +33 5 61 52 14 58
nic-hdl:        AG15487-RIPE
mnt-by:         RENATER-MNT
remarks:        changed:        rensvp@renater.fr 20121022
created:        2012-10-22T12:29:23Z
last-modified:  2015-08-07T13:55:46Z
source:         RIPE # Filtered
