#!/bin/sh
echo "il y a `ls -a1 | grep ".txt" | wc -l` fichier dont le nom se termine par .txt"
echo "il y a `ls -a1 | grep ".sh" | wc -l` fichier dont le nom se termine par .sh"
echo "il y a `ls -a1d .* | wc -l` fichier ou dossiers cachés"
echo "il y a `ls -1 | grep -v ".sh" | grep -v ".txt" | wc -l` autres fichiers"