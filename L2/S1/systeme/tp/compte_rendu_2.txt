Exercice 8 : 

1) 	#!/bin/sh
	mkdir $1
	cp $2 $1

2) 	#!/bin/sh
	rm $1/*
	rm $1
3)	#!/bin/sh
	mkdir REP
	ls -1 *.sh *.txt > REP/liste.txt
	cp REP/liste.txt REP/`tail -n 1 REP/liste.txt`
	rm REP/liste.txt

Exercice 9 :

#!/bin/sh
echo "il y a `ls -a1 | grep ".txt" | wc -l` fichier dont le nom se termine par .txt"
echo "il y a `ls -a1 | grep ".sh" | wc -l` fichier dont le nom se termine par .sh"
echo "il y a `ls -a1d .* | wc -l` fichier ou dossiers cachés"
echo "il y a `ls -1 | grep -v ".sh" | grep -v ".txt" | wc -l` autres fichiers"

Exercice 10 : 

1) echo `cat listecp.txt | grep "linfg.$" | sort +2 | head -1 | cut -d' ' -f1`

2) echo `cat listecp.txt | grep "linfg[0-9]\{3\}$" liste.txt | sort +2 | tail -1 | cut -d' ' -f1`

3) 	#!/bin/sh
	echo `cat liste.txt | cut -d' ' -f1 | tail -n 6 | grep "[A-Z]\{$1\}" | grep -v "[A-Z]\{$2\}" | wc -l`