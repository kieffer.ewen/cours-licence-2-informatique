donnerJetonAuSuivant() {
	kill -s 18 $1
}
attendreJeton() {
	trap "return" 18 
	while true 
	do 
		sleep 1
	done
}

c=$(( $1 - 1 ))

	./maillon.sh $c $$ &
	c=$(( $c - 1 ))
while [ $c -ne 0 ]
do
	./maillon.sh $c $! &
	c=$(( $c - 1 )) 
done

while true 
do
	attendreJeton;
	echo "Maillon $1/$$ ayant pour successeur $! a le jeton"
	echo "Maillon $1/$$ ayant pour successeur $! donne le jeton"
	donnerJetonAuSuivant $!;
done