#!/bin/sh
 
if [ ! $# -ge 1 ]
then
        exit 0
fi
 
if [ ! -d $1 ] && [ ! -x $1 ]
then
        exit 0
 
 
fi
 
 
c=`ls -pF $1 | grep '/$'| wc -l`
for i in `ls -pF "$1"| grep '/$' | cut -d'/' -f1`
do
    if [ $c -ne 1 ]
    then
        echo "$2 +- $i"      
        ./script_exo18.sh "$1/$i" "$2 | "
    else
    echo "$2 +- $i"   
		./script_exo18.sh "$1/$i" "$2   "
    fi
    c=$(( $c - 1 ))
done