if [ $# -eq 0 ]
then
	echo "inaccessible"
	exit 0
fi

while [ "$1" != "" ]
do
	echo "Lecture du fichier $1"
	echo `cat $1`
	shift
done
echo "fin de la lecture du fichier"