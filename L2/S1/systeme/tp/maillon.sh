donnerJetonAuSuivant() {
	kill -s 18 $1
}
attendreJeton() {
	trap "return" 18 
	while true 
	do 
		sleep 1
	done
}
if [ $1 -eq 1 ]
then
	echo "Maillon $1/$$ ayant pour successeur $2 a le jeton"
	echo "Maillon $1/$$ ayant pour successeur $2 donne le jeton"
	donnerJetonAuSuivant $2;
fi
while true 
do
	attendreJeton;
	echo "Maillon $1/$$ ayant pour successeur $2 a le jeton"
	echo "Maillon $1/$$ ayant pour successeur $2 donne le jeton"
	donnerJetonAuSuivant $2;
done