#!/bin/sh
if [ $# -ne 1 ]&&[ $1 -d ]&&[ ! -x $1]
then 
	echo "Fichier invalide"
	exit 0
fi
for i in `ls -F "$1" | grep '/$' | cut -d'/' -f1`
do
		echo "$i"
		sh script_exo17.sh "$1/$i"
done 