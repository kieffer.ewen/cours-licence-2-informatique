#!/bin/sh

dayy=`date -R | cut -d',' -f1`
monthh=`date -R | cut -d' ' -f3`
number=`date -R | cut -d' ' -f2`
yearr=`date -R | cut -d' ' -f4`
time=`date -R | cut -d' ' -f5`

case $dayy in 
	"Mon")
		dayy="Lundi"
		;;
	"Tue")
		dayy="Mardi"
		;;
	"Wed")
		dayy="Mercredi"
		;;
	"Thu")
		dayy="Jeudi"
		;;
	"Fri")
		dayy="Vendredi"
		;;
	"Sat")
		dayy="Samedi"
		;;
	"Sun")
		dayy="Dimanche"
		;;
esac

case $monthh in 
	"Jan")
		monthh="Janvier"
		;;
	"Feb")
		monthh="Février"
		;;
	"Mar")
		monthh="Mars"
		;;
	"Apr")
		monthh="Avril"
		;;
	"May")
		monthh="Mai"
		;;
	"Jun")
		monthh="Juin"
		;;
	"Jul")
		monthh="Juillet"
		;;
	"Aug")
		monthh="Août"
		;;
	"Sep")
		monthh="Septembre"
		;;
	"Oct")
		monthh="Octobre"
		;;
	"Nov")
		monthh="Novembre"
		;;
	"Dec")
		monthh="Decembre"
		;;
esac

echo "$dayy $number $monthh $yearr $time"
