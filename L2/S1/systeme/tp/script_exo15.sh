if [ $# -gt 2 ]||[ $# -lt 1 ]
then
	echo "Erreur : inverse.sh nom_fichier1 [nom_fichier2]"
	exit 0
fi
if [ $# -eq 1 ]
then 
	echo "Un seul paramètre"
	nblignes=`wc -l $1 | cut -d' ' -f1`
	echo $nblignes
while [ $nblignes -ne 0 ]
do
	`cat $1 | head -n +$nblignes > .inv1`
	`tail -n -1 .inv1 >> .inv2` 
	nblignes=`expr $nblignes - 1`
done
`cat .inv2 > $1`
rm .inv1 .inv2
fi
if [ $# -eq 2 ]
then 
	echo "Deux paramètres"
	`cat $1 > $2`
	sh script_exo15.sh "$2"
fi