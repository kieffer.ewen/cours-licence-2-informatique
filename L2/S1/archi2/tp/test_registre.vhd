LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY testa is 
END ENTITY;

ARCHITECTURE test_reg8 of testa is
	SIGNAL clk, rst, wr : STD_LOGIC;
	SIGNAL I : STD_LOGIC_VECTOR(7 downto 0) := "11111111";
	SIGNAL O : STD_LOGIC_VECTOR(7 downto 0);
BEGIN

	reg : ENTITY work.registre_8b PORT MAP(clk,rst,wr,I,O);
	clk <= '0', '1' after 10 ns, '1' after 20 ns;
	rst <= '0', '1' after 20 ns, '0' after 30 ns;
	wr <= '1';
END ARCHITECTURE test_reg8;

