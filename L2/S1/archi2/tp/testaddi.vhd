LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
 
ENTITY test IS
END ENTITY;
 
ARCHITECTURE arc OF test IS
   
    SIGNAL I2, I1, I0, S, R : STD_LOGIC := '0';
    BEGIN
   
    fdd_full_adder : ENTITY WORK.full_adder PORT MAP(I2, I1, I0, S, R );
   	I2 <= '0', '0' AFTER 10 ns, '1' after 20 ns, '0' AFTER 30 ns;
        I1 <= '0', '0' AFTER 10 ns, '1' after 20 ns, '1' AFTER 30 ns;
        I0 <= '0', '1' AFTER 10 ns, '0' after 20 ns, '1' AFTER 30 ns;
       
END ARCHITECTURE;
 

