LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY registre IS 
	PORT( 	E : IN STD_LOGIC_VECTOR(2 downto 0);
		O : INOUT STD_LOGIC_VECTOR(2 downto 0);
		ESD, ESG, WR, RD, P1, P0, nCLR, nSET : IN STD_LOGIC;
		SSD, SSG : OUT STD_LOGIC);
END ENTITY registre;

ARCHITECTURE comp OF registre IS
	SUBTYPE selecteur IS STD_LOGIC_VECTOR(1 DOWNTO 0);
BEGIN 
	PROCESS(nCLR, nSET, WR, RD)
	BEGIN
		IF nSET = '0' THEN
			O <= (OTHERS => '1');
		ELSIF nCLR = '0' THEN
			O <= (OTHERS => '0');			
		ELSIF RISING_EDGE(WR) THEN
			CASE selecteur'(P1,P0) IS
				WHEN "00" => O <= E;
				WHEN "01" => O <= ESD & O(2 downto 1);
				WHEN "10" => O <= O(1 downto 0) & ESG;
				WHEN OTHERS => O <= O;
			END CASE;
		ELSIF RISING_EDGE(RD) THEN 
			CASE selecteur'(P1,P0) IS
				WHEN "01" => O <= O(0) & O(2 downto 1);
				WHEN "10" => O <= O(1 downto 0) & O(2);
				WHEN OTHERS => O <= O;
			END CASE;		
		END IF;
	END PROCESS;
END ARCHITECTURE comp;
