LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;   

ENTITY bascule_rs IS
PORT( rst, clk, set : IN STD_LOGIC;
	Q : INOUT STD_LOGIC;
	nQ : OUT STD_LOGIC);
END ENTITY;

ARCHITECTURE comp OF bascule_rs IS 
	SUBTYPE selecteur IS STD_LOGIC_VECTOR(1 DOWNTO 0);
BEGIN
	nQ <= NOT Q;
	PROCESS(clk)
	BEGIN
	IF RISING_EDGE(clk) THEN
		CASE selecteur'(rst,set) IS
			WHEN "01" => Q <= '0';
			WHEN "10" => Q <= '1';
			WHEN "11" => Q <= 'X';
			WHEN OTHERS => Q <= Q;
		END CASE;
	--IF rst = '1' THEN
	--	Q <= '0';				
	--ELSIF set = '1' THEN 
	--	Q <= '1';
	--END IF;
	END IF;
	END PROCESS;
END ARCHITECTURE comp;



