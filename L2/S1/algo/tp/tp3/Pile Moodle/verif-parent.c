/*
 * Vérification de parenthèses.
 */

#include <stdio.h>

#include "pile.h"

int main() {
	pile_t p;
	char c;
	int ouvrantes[5];
	int fermantes[5];
	for (int i = 0; i < 5; i++) {
		ouvrantes[i] = 0;
		fermantes[i] = 0;
	}
	init_pile(&p);
	
	scanf("%c", &c);
	switch(c) {
		case('('):
		ouvrantes[0] ++;
		break;
		case(')'):
		fermantes[0] ++;
		break;
		case('['):
		ouvrantes[1] ++;
		break;
		case(']'):
		fermantes[1] ++;
		break;
		case('{'):
		ouvrantes[2] ++;
		break;
		case('}'):
		fermantes[2] ++;
		break;
	}
	while(c != '$') {
		scanf("%c", &c);
	switch(c) {
		case('('):
		ouvrantes[0] ++;
		break;
		case(')'):
		fermantes[0] ++;
		break;
		case('['):
		ouvrantes[1] ++;
		break;
		case(']'):
		fermantes[1] ++;
		break;
		case('{'):
		ouvrantes[2] ++;
		break;
		case('}'):
		fermantes[2] ++;
		break;
	}
	}
	if(ouvrantes[0] != fermantes[0]) {
		printf("erreur ()\n");
	}else{
		prinft("validé ()\n");
	}
	if(ouvrantes[1] != fermantes[1]) {
		printf("erreur []\n");
	}else{
		printf("validé []\n");
	}
	if(ouvrantes[2] != fermantes[2]) {
		printf("erreur {}\n");
	}else{
		prinft("validé {}\n");
	}

	return 0;
}
