#include <stdio.h>
#include <stdlib.h>

typedef struct
{
  int val; /* Valeur de la case (0 si vide) */
  int n_candidats; /* Nombre de candidats possibles. Lorsqu'une valeur est choisie pour une case, n_candidat doit etre mis a 0. */
  int candidats[9]; /* Liste de candidats */
} T_case;

typedef struct
{
  T_case grille[81]; /* Vecteur de 81 cases */
} T_sudoku;

typedef struct
{
  int ligne;
  int colonne;
} T_coordonnees;

int obtenirIndice(T_coordonnees coords){
	return 9*coords.ligne + coords.colonne;
}

T_coordonnees obtenirCoords(int indice){
	T_coordonnees coordonnees;
	coordonnees.ligne = indice/9;
	coordonnees.colonne = indice%9;
	return coordonnees;
}
T_coordonnees debutRegion(int indiceRegion){
	T_coordonnees coordonnees;
	coordonnees.ligne = 3*(indiceRegion/3);
	coordonnees.colonne = 3*(indiceRegion%3);
	return coordonnees;
}

int indiceRegion(T_coordonnees coords){
	return (coords.ligne/3)*3 + coords.colonne/3;
}/* Retourne l'indice de la region a laquelle appartient la case ayant ces coordonnees*/

int estCandidat(T_sudoku s, int candidat, int indice){
	T_coordonnees debut = debutRegion(indiceRegion(obtenirCoords(indice)));
	int debut_col = debut.colonne;
	for(int i = 0; i < 3; i++){
		debut.colonne = debut_col;
		for(int j = 0; j < 3; j++){
			if(candidat == s.grille[obtenirIndice(debut)].val)
				return 1;
			debut.colonne ++;
		}
		debut.ligne ++;
	}
	T_coordonnees coord_case = obtenirCoords(indice);
	T_coordonnees it;
	T_coordonnees at;
	it.ligne = coord_case.ligne;
	it.colonne = 0;
	at.colonne = coord_case.colonne;
	at.ligne = 0;
	for(int i = 1; i < 10; i ++){
		if(candidat == s.grille[obtenirIndice(it)].val)
			return 1;
		it.colonne ++;	

	}
	for(int i = 1; i < 10; i ++){
		if(candidat == s.grille[obtenirIndice(at)].val)
			return 1;
		at.ligne ++;

	}
	return 0;

}
void supprimerValeur(int ival, T_case* pc){
	for (int i = 0; i < 9; i++) {
		if(ival == pc->candidats[i]){
			pc->n_candidats --;
			pc->candidats[i] = 0;
		}
	}
	for (int i = 0; i < 8; i ++) {
		if(pc->candidats[i] == 0) {
			pc->candidats[i] = pc->candidats[i+1];
			pc->candidats[i+1] = 0;
		}
	}
}/* ival est l'indice de la valeur a supprimer, on considere que la valeur a supprimer est presente dans les candidats de la case */

/* Implementation des regles */
int R1_case(T_case* pc){
	if(pc->n_candidats == 1)
	{
		pc->val = pc->candidats[0];
		for (int i = 0; i < 9; i ++)
		 	pc->candidats[i] = 0;
		pc->n_candidats = 0;
		return 1;
	}
	else 
		return 0;
} /* Applique R1 sur la case */

int R1_sudoku(T_sudoku* ps){
	int cpt = 0;
	for(int i = 0; i<81 ;i++)
	{
		if(R1_case(&ps->grille[i])==1)
		{
				int k = ps->grille[i].val;
				T_coordonnees debut = debutRegion(indiceRegion(obtenirCoords(i)));
				int debut_col = debut.colonne;
				T_coordonnees t;
				for(int f = 0; f < 3; f++){
					debut.colonne = debut_col;
					for(int j = 0; j < 3; j++){
						t.ligne = debut.ligne;
						t.colonne = debut.colonne;
						supprimerValeur(k, &ps->grille[obtenirIndice(t)]);										
						debut.colonne ++;
					}
					debut.ligne ++;
				}

				T_coordonnees caze = obtenirCoords(i);
				t.ligne = caze.ligne;
				for (int j = 0; j < 9; j ++) {
					t.colonne = j;
					supprimerValeur(k, &ps->grille[obtenirIndice(t)]);				
				}
				t.colonne = caze.colonne;
				for (int j = 0; j < 9; j ++) {
					t.ligne = j;
					supprimerValeur(k, &ps->grille[obtenirIndice(t)]);					
				}

			cpt++;
		}
	}
	if(cpt > 0)
		return 1;
	return 0;
}
int R2_case(int indCase, T_sudoku* ps){
	int cpt = 0;
	for(int k = 1; k < 10; k ++){
		if(estCandidat(*ps,k,indCase) == 1){
			supprimerValeur(k, &ps->grille[indCase]);
			cpt ++;
		}
	}
	if (cpt > 0){
		return 1;
	}
	return 0;

} /* Applique R2 sur la case. */
int R2_sudoku(T_sudoku* ps){
	int cpt = 0;
	for(int i = 0; i < 81; i++){
		if(R2_case(i, ps) == 1){
			cpt ++;
		}
	}
	if(cpt > 0){
		return 1;
	}
	return 0;
} /* Applique R2 sur toutes les cases du sudoku, renvoie 1 si une modification a eu lieu, 0 sinon */

void afficherSudoku(T_sudoku s){
	T_coordonnees caze;
	printf("\n");
	for (int i = 0; i < 9; i++){
		for (int j = 0; j < 9; j++) {
			caze.ligne = i;
			caze.colonne = j;
			printf("%d  ", s.grille[obtenirIndice(caze)].val);
		}
		printf("\n");
	}
} /* Afficher un sudoku: 9 lignes de 9 entiers separes par des tabulations (\t)*/

T_sudoku lireSudoku(char* chemin){
	FILE* fic=NULL;
	T_sudoku sudoku;
	fic = fopen(chemin, "r");
	if(fic == NULL)
		{
			fprintf(stderr, "Erreur à l'ouverture du fichier : %s\n", chemin);
			exit(1);
		}
	else
	{
		int cpt = 0;
		int caractere;
		while(fscanf(fic,"%d", &caractere) != EOF){
			sudoku.grille[cpt].val = caractere;
			sudoku.grille[cpt].n_candidats = 0;
			cpt++;
		}

	}
	fclose(fic);
	return sudoku;
}
void initialiserSudoku(T_sudoku* s){
	for (int i = 0; i < 81; i ++) {
		if(s->grille[i].val == 0) {
			s->grille[i].n_candidats = 9;
		for(int k = 0; k < 9; k ++){
			s->grille[i].candidats[k] = k + 1;
		}
		}else{
			s->grille[i].n_candidats = 0;
		for(int j = 0; j<9;j++){
			s->grille[i].candidats[j] = 0;
		}

		}
	}
}


int main()
{
T_sudoku s;
s = lireSudoku("sudoku_ex_lecture.txt");
initialiserSudoku(&s);
for(int i = 0; i < 81; i ++) {
	printf("Case %d\n", i);
	for(int j = 0; j < s.grille[i].n_candidats; j++){
		printf("%d ",s.grille[i].candidats[j]);
	}
	printf("\n" );
}
R2_sudoku(&s);
for(int i = 0; i < 81; i ++) {
	printf("Case %d\n", i);
	for(int j = 0; j < s.grille[i].n_candidats; j++){
		printf("%d ",s.grille[i].candidats[j]);
	}
	printf("\n" );
}
R1_sudoku(&s);
R2_sudoku(&s);
for(int i = 0; i < 81; i ++) {
	printf("Case %d\n", i);
	for(int j = 0; j < s.grille[i].n_candidats; j++){
		printf("%d ",s.grille[i].candidats[j]);
	}
	printf("\n" );
}
afficherSudoku(s);
return 0;
}