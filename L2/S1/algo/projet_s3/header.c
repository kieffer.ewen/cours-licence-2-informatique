#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
#include <assert.h>
#include "header.h"

bool sans_zero = 0;

void erreur(char *message){
/// \brief imprime à l'écran un message d'erreur (chaîne de caractères)
/// \brief le message est sensé être composé du nom du sous-programme+" : "+message d'erreur
/// \param[in] message : le message à imprimer
	printf("*** ERREUR : %s ***\n",message);
	exit(1);
}

int random_node(int min,int max){
  /// \brief crée un nombre aléatoire entre min et max
  /// \returns le nombre
	return (rand()%(max-min)) + min;
}

int * creer_tab_int(int dim){
  /// \brief crée un tableau dynamique d'entiers de dim cases
  /// \param[in] dim : la dimension du tableau voulu
  /// \returns adresse du tableau
	return (int *)malloc(dim*sizeof(int));
}

char * creer_tab_char(int dim){
   /// \brief crée un tableau dynamique de char de dim cases
	return (char *)malloc(dim*sizeof(char));
}

void affiche_tab_int(int *tab,int dim, FILE *logfp){
	fprintf(logfp,"[");
	for(int i=0;i<dim-1;i++)
		fprintf(logfp,"%d,",tab[i]);
	fprintf(logfp,"%d]\n",tab[dim-1]);
}

void affiche_tab_char_hex(char *tab,int dim, FILE *logfp){
	for(int i=0;i<dim;i++)
		fprintf(logfp,"%x",tab[i]);
	fprintf(logfp,"\n");
}

void affiche_tab_char(char *tab,int dim, FILE *logfp){
	for(int i=0;i<dim;i++)
		fprintf(logfp,"%c",tab[i]);
	fprintf(logfp,"\n");
}

int ** creer_mat_int(int nbRows,int nbCol){
	int ** tab = (int **)malloc(nbRows*sizeof(int *));
	for(int i=0;i<nbRows;i++)
		tab[i] = (int *) malloc(nbCol*sizeof(int));
	return tab;
}

double ** creer_mat_dist(int dimension){
  /// \brief crée la matrice carrées des distances = tab dynamique triangulaire
  /// \brief chaque ligne a une case de moins que la précédente
	double ** tab = (double **)malloc(dimension*sizeof(double *));
	for(int i=0;i<dimension;i++)
		tab[i] = (double *) malloc(dimension*sizeof(double));
	
	dimension--;
	return tab;
}

void swap (int *tab,int ind1,int ind2)
/// \brief inverse le contenu des cases ind1 et ind2 dans tab
/// \param[in,out] tab : le tableau
/// \param[in] ind1 : premier indice
/// \param[in] ind2 : deuxième indice
/// \post *tab[ind1]=*TAB[ind2] && *tab[ind2]=*TAB[ind1]
{
	int temp;
	temp = tab[ind1];
	tab[ind1] = tab[ind2];
	tab[ind2] = temp;
}




void renverse_tab(int *tab,int prem,int der){
  /// \brief reverse le contenu du tableau entre les indices prem et der
  /// \param[in,out] tab : le tableau à modifier
  /// \param[in] prem : indice de début
  /// \param[in] der : indice de fin
	int ind;
	int dim=der-prem+1;

	for(ind=0;ind<dim/2;ind++)
		swap(tab,prem+ind,der-ind);
}

void analyse_balises(int argc,char ** argv,int tab_arg[NB_BALISES])
{
	/// \brief Analyse les balises en paramètre du programme
	/// \param[in] argc : le nombre de balises
	/// \param[in] argv : indice de début
	/// \param[in, out] tab_arg[NB_BALISES] : tableau des arguments présents ou non (HYPOTHESE) 
	for(int i = 1; i < argc; i++)
	{
		if(strstr(argv[i], "-"))
		{
			if(strcmp(argv[i], "-f")==0)
				tab_arg[0] = i;
			else if(strcmp(argv[i], "-t")==0)
				tab_arg[1] = i;
			else if(strcmp(argv[i], "-v")==0)
				tab_arg[2] = i;
			else if(strcmp(argv[i], "-o")==0)
				tab_arg[3] = i;
			else if(strcmp(argv[i], "-ppv")==0)
				tab_arg[4] = i;
			else if(strcmp(argv[i], "-bf")==0)
				tab_arg[5] = i;
			else if(strcmp(argv[i], "-bfm")==0)
				tab_arg[6] = i;
			else if(strcmp(argv[i], "-rw")==0)
				tab_arg[7] = i;
			else if(strcmp(argv[i], "-2opt")==0)
				tab_arg[8] = i;
			else if(strcmp(argv[i], "-ga")==0)
				tab_arg[9] = i;
			else if(strcmp(argv[i], "-h")==0)
				tab_arg[10] = i;
			else if(strcmp(argv[i], "-nz")==0)
				tab_arg[11] = i;
		}
		
	}
}

bool nextPermutation(int *array,int dim){
	if (dim == 0)
		return false;
	size_t i = dim - 1;
	while (i > 0 && array[i - 1] >= array[i])
		i--;
	if (i == 0)
		return false;
	
	// Find successor to pivot
	size_t j = dim - 1;
	while (array[j] <= array[i - 1])
		j--;
	int temp = array[i - 1];
	array[i - 1] = array[j];
	array[j] = temp;
	
	// Reverse suffix
	j = dim - 1;
	while (i < j) {
		temp = array[i];
		array[i] = array[j];
		array[j] = temp;
		i++;
		j--;
	}
	return true;
}

double brute_force_tsp(instance_t instance, tour_t *tournee, tour_t *pire,bool use_mat){
	double tmp = 0;
	if(use_mat) {
		for(int i = 0; i < instance.dimension; i++){
			for (int j = 0; j < instance.dimension; j++) {
				if(j>i){
					instance.matDist[i][j] = dist_euclidienne(instance,i,j);
				}else{
					instance.matDist[j][i] = dist_euclidienne(instance,i,j);
				}
			}
		}
		while(nextPermutation(instance.tabTour, tournee->dimension)){
			for(int i = 0; i < tournee->dimension - 1; i ++){
				tmp += instance.matDist[instance.tabTour[i]][instance.tabTour[i+1]];
			}
			if(instance.length > tmp || instance.length == 0) {
				tmp = instance.length;
				tournee->tour = instance.tabTour;
				tournee->length = tmp;
			}else {
				if (tmp > pire->length) {
					pire->length = tmp;
					pire->tour = instance.tabTour;
				}
			}
			tmp = 0;
		}
		
	}else {
		while(nextPermutation(instance.tabTour,tournee->dimension)){
			tmp = tour_length(instance, instance.tabTour, false);
			if(instance.length > tmp || instance.length == 0) {
				tmp = instance.length;
				tournee->tour = instance.tabTour;
				tournee->length = tmp;
			}else {
				if (tmp > pire->length) {
				pire->length = tmp;
				pire->tour = instance.tabTour;
					}
			}
		}
	return tournee->length;
	}
}

double tour_length(instance_t instance,int *tabTour,bool sans_zero){
	double tmp = 0;
	for(int i = 0; i < instance.dimension - 1; i ++){
		tmp += dist_euclidienne(instance,tabTour[i], tabTour[i+1]);
	}
	return tmp;
}

void print_help()
{
	/// \brief Affiche le manuel du programme
	printf("\n################################# HELP #################################\n\n\n");
	printf("Usage :  ./tsp -f <file> [-t <tour>] [-v [<file>]] -<méthode> [-h]\n-f <file> : nom du fichier tsp (obligatoire)\n-t <file> : nom du fichier solution (optionnel)\n-v [<file>] : mode verbose (optionnel), écrit dans un fichier si présent\n-o <file> : export des résultats sans un fichier csv\n-h : help, affiche ce texte\n\n<méthode> : méthodes de calcul (cumulables) :\n\n-bf : brute force,\n-bfm : brute force en utilisant la matrice de distance,\n-ppv : plus proche voisin,\n-rw : random walk,\n-2opt : 2 optimisation. Si -ppv ou -rw ne sont pas présentes on utilise -rw,\n-ga <nombre d'individus> <nombre de générations> <taux de mutation> : Algorithme génétique, défaut = 20 individus, 200 générations, 0.3 mutation.\n\n\n");
	printf("#######################################################################\n");
}


void hard_copy_tour(tour_t* t1, tour_t t2)
{

	strcpy(t1->name,t2.name);
	t1->dimension = t2.dimension;
	t1->length = t2.length;
	t1->tour = t2.tour;
}
int lecture_fichier_tour(char* filename, tour_t* tour)
{
	FILE* fic = NULL;
	char str[MAXBUF];
	char* token = NULL;
	fic = fopen(filename, "r");
	int ct = 0;
	int sectionOk = 0;
	if(fic == NULL)
	{
		erreur("Ouverture du fichier impossible");
		return NIL;	
	}else
	{
		int ligne = 0;
		
		while(fgets(str, MAXBUF, fic) != NULL)
		{
			int i = 0;
			char mot[7][MAXBUF] = {"","","","","","",""};
			token = strtok(str, " ");
			while(token)
			{
				strcat(mot[i], token);
				//printf("%2d %s\n", i++, token);
				i++;
				token= strtok(NULL, " ");
			}
			if(strcmp(mot[0], "NAME")==0)
			{	
				strcpy(tour->name, mot[2]);
			}
			else if(strcmp(mot[0], "DIMENSION")==0)
			{	
				tour->dimension = atoi(mot[2]);	
			}
			else if(strcmp(mot[0], "TOUR_SECTION\n")==0)
			{
				sectionOk = 1;	
				tour->tour = creer_tab_int(tour->dimension);
			}
			else if((sectionOk == 1 && atoi(mot[0])!=0) && strcmp(str,"EOF\n"))
			{
				int value = abs(atoi(mot[0]));
				
				//printf("DIMENSION :%d\t tour[%d] vaut :\t %d \n",tour->dimension, ct, value);
				tour->tour[ct] = value;
				ct++;
			}		
			ligne++;
		}
		fclose(fic);
	}
	return 0;
}
int lecture_fichier_inst(char * filename,instance_t *instance)
{
	/// \brief lis un affichier, l'analyse et remplis une instance 
	/// \param[in] filename : Nom du fichier 
	/// \param[in] instance : instance tsp
	/// \returns : Code retour

	FILE* fic = NULL;
	char str[MAXBUF];
	int coordOk = 0;
	int ct=0;
	char* token = NULL;
	fic = fopen(filename,"r");
	if(fic == NULL)
	{
		erreur("Echec de la lecture ***\n*** KO");
		return NIL;
	}else
	{
		int ligne = 0;
		while(fgets(str, MAXBUF, fic) != NULL)
		{
			int i = 0;
			char mot[10][MAXBUF] = {"","","","","",""}; 
			token = strtok(str, " ");
			while(token)
			{
				strcat(mot[i], token);
				//printf("%2d %s\n", i++, token);
				i++;
				token= strtok(NULL, " ");
			}
			if(strcmp(mot[0], "NAME")==0)
			{	
				strcpy(instance->name,mot[2]);
			}
			else if(strcmp(mot[0], "TYPE")==0)
			{
				strcpy(instance->type,mot[2]);
			}
			else if(strcmp(mot[0], "DIMENSION")==0)
			{	
			
				if(sans_zero)
					instance->dimension = atoi(mot[2]);	
				else{
					int temp = atoi(mot[2]);
					temp++;
					instance->dimension = temp;	
				}
			}
			else if(strcmp(mot[0], "EDGE_WEIGHT_TYPE")==0)
			{
				strcpy(instance->EDGE_TYPE,mot[2]);
			}
			else if(strcmp(mot[0], "NODE_COORD_SECTION\n")==0)
			{
				coordOk = 1;	
					instance->tabCoord = creer_mat_int(instance->dimension,4);
				
			}
			else if((coordOk == 1 && atoi(mot[0])>=0) && ct<instance->dimension)
			{
				if(!sans_zero){
					if(ct >= 0)
					{
						int order = atoi(mot[0]);
						int x = atoi(mot[1]);
						int y = atoi(mot[2]);
						instance->tabCoord[order][0] = x;
						instance->tabCoord[order][1] = y;
						instance->tabCoord[order][2] = 0;
						instance->tabCoord[order][3] = order;
					}else{
					instance->tabCoord[0][0] = 0;
						instance->tabCoord[0][1] = 0;
						instance->tabCoord[0][2] = 0;
						instance->tabCoord[0][3] = 0;
					}
				}else{
				int order = atoi(mot[0])-1;
				int x = atoi(mot[1]);
				int y = atoi(mot[2]);
				instance->tabCoord[order][0] = x;
				instance->tabCoord[order][1] = y;
				instance->tabCoord[order][2] = 0;
				instance->tabCoord[order][3] = order+1;
				}
				ct++;
				
			}			
			
			ligne++;
		}
		printf("%d lignes chargées !\n", ligne);
		free(token);
		fclose(fic);
	}
	return 0;
}

void init_instance(instance_t *instance)
{
	/// \brief initialise une instance 
	/// \param[in, out] filename : instance à initialiser 
	
	strcpy(instance->name, "");
	strcpy(instance->type, "");
	instance->dimension = 0;
	strcpy(instance->EDGE_TYPE, "");
	instance->length = 0;
	//A FINIR	
}

double dist_euclidienne(instance_t instance, int v1, int v2){
	/// \brief Calcul la distance euclidienne entre deux points 
	/// \param[in] instance : instance tsp 
	/// \param[in] v1 : position dans un tableau de v1
	/// \param[in] v2 : position dans un tableau de v2
	/// \returns : Distance euclidienne entre v1 et v2
	
	
	return sqrt(pow(instance.tabCoord[v2][0]-instance.tabCoord[v1][0],2)+pow(instance.tabCoord[v2][1]-instance.tabCoord[v1][1],2));
}

void dump_instance(instance_t instance)
{
	
	strcpy(instance.name, "");
	strcpy(instance.type, "");
	instance.dimension = 0;
	strcpy(instance.EDGE_TYPE, "");
	instance.length = 0;
	
	for(int i=0; i<instance.dimension; i++)
	{
		free(instance.tabCoord[i]);
	}
	
	for(int i=0; i<instance.dimension; i++)
	{
		free(instance.matDist[i]);
	}
	
	free(instance.tabTour);
}

int ecrire_fichier(char* filename, char* message)
{	
	FILE* fichier = NULL;
	if(strcmp(filename, "")==0)
	{
		fichier = stdout;
		
		int h, min, s, day, mois, an;
		time_t now;
		
	  	// Renvoie l'heure actuelle
		time(&now);
	  	// Convertir au format heure locale
		struct tm *local = localtime(&now);
		h = local->tm_hour;        
		min = local->tm_min;       
		s = local->tm_sec;       
		day = local->tm_mday;          
		mois = local->tm_mon + 1;     
		an = local->tm_year + 1900;  
		
		char string[MAXBUF];
		sprintf(string,"%d/%d/%d %d:%d:%d\t%s\n", day,mois,an,h,min,s,message);
		fprintf(fichier, string);	
	}
	else{
		fichier = fopen(filename, "a");
		if (fichier != NULL)
		{
			
			int h, min, s, day, mois, an;
			time_t now;
			
	  	// Renvoie l'heure actuelle
			time(&now);
	  	// Convertir au format heure locale
			struct tm *local = localtime(&now);
			h = local->tm_hour;        
			min = local->tm_min;       
			s = local->tm_sec;       
			day = local->tm_mday;          
			mois = local->tm_mon + 1;     
			an = local->tm_year + 1900;  
			
			char string[MAXBUF];
			sprintf(string,"%d/%d/%d %d:%d:%d %s\n", an,mois,day,h,min,s,message);
			fprintf(fichier, string);
			fclose(fichier);
		}else
		{
			erreur("Impossible d'ouvrir le fichier log en mode écriture");
		}
	}
	return 0;

}

int init_tour_random(instance_t instance, int ville_depart, tour_t *tournee){
    bool valide = false;
    int tmp = 0;
    while(!valide){
        tmp = random_node(0, instance.dimension);
        if(instance.tabCoord[tournee->tour[tmp]][2] == 0 && tmp != ville_depart){
            valide = true;
            instance.tabCoord[tournee->tour[tmp]][2] == 1;
			tournee->tour[tournee->dimension] = tmp;
            tournee->dimension ++;
        }
    }
    return tmp;
}

int init_tour_ppv(instance_t instance, int depart, tour_t *tournee){
    bool valide = false;
    double tmp = 0;
    int indice = 0;
    for(int i = 0; i < instance.dimension; i ++){
        if(instance.tabCoord[i][2] == 0 && i != depart)
            if(tmp > dist_euclidienne(instance, depart, i) || tmp == 0){
                tmp = dist_euclidienne(instance, depart, i);
                indice = i;
                tournee->tour[tournee->dimension] = tmp;
            	tournee->dimension ++;
            }
    }
    return indice;
}
double randomwalk(instance_t instance, int depart, tour_t *tournee){
    double tmp = 0;
    tournee->dimension == 1;
    int sommetinitial = depart;
    int sommetSuivant = init_tour_random(instance, depart, tournee);
    for(int i = 0; i < instance.dimension; i ++){
         tmp =+ dist_euclidienne(instance, sommetinitial, sommetSuivant);
         sommetinitial = sommetSuivant;
         sommetSuivant = init_tour_random(instance, sommetinitial, tournee);
    }
    return tmp;
}

double ppv(instance_t instance, int depart, tour_t *tournee){
    double tmp = 0;
	tournee->dimension == 1;
    int sommetinitial = depart;
    int sommetSuivant = init_tour_ppv(instance, depart, tournee);
    for(int i = 0; i < instance.dimension; i ++){
         tmp =+ dist_euclidienne(instance, sommetinitial, sommetSuivant);
         sommetinitial = sommetSuivant;
         sommetSuivant = init_tour_ppv(instance, sommetinitial, tournee);
    }
    return tmp;
}

bool estDansTab(int *tab,int dim, int valeur){
	for(int i = 0; i < dim; i ++){
		if(tab[i] == valeur)
			return true;
	}
	return false;
}

tour_t generateRandomTour(instance_t instance){
	tour_t tournee;
	strcpy(tournee.name,instance.name);
	tournee.dimension = 0;
	tournee.tour = creer_tab_int(instance.dimension);
	int cpt = 0;
	int tmp = 0;
	do{
		tmp = random_node(0, instance.dimension);
		if(!estDansTab(tournee.tour, tournee.dimension, tmp)){
			tournee.tour[tournee.dimension] = tmp;
			tournee.dimension ++;
			cpt ++;
		}
	}while(cpt < instance.dimension);
	return tournee;
}
bool villes_adjacentes(tour_t tournee,int ind1,int ind2){
	for(int i = 0; i < tournee.dimension - 1; i ++){
		if((ind1 == tournee.tour[i] && ind2 == tournee.tour[i+1]) || (ind1 == tournee.tour[i+1] && tournee.tour[i] == ind2))
			return true;
	}
	return false;
}

// double all_2opt(instance_t instance,tour_t *tournee){

// }
tour_t DPX(instance_t instance, tour_t t1, tour_t t2) {
	tour_t t;
	return t;
}


double genetique(instance_t instance, tour_t *tournee, int nbIndividu, double tauxMutation, int nbGeneration){
	int cptGen = 0;
	int cptCroisement= 0;
	int nbCroisement = random_node(0, nbIndividu/2);
	tour_t individus[nbIndividu]; 
	int t1 = 0;
	int t2 = 0;
	tour_t fille;
	for(int i = 0; i < nbIndividu; i ++)
		individus[i] = generateRandomTour(instance);
	do{
		while(cptCroisement < nbCroisement){
			t1 = random_node(0, nbIndividu);
			t2 = random_node(0, nbIndividu);
			while (t2 == t1)
				t2 = random_node(0, nbIndividu);
			fille = DPX(instance, invidivus[t1], individus[t2]);
			double mutation = random_node(tauxMutation*10, 10);
			if(mutation*10 <= tauxMutation*10){
				int swap1 = random_node(0, instance.dimension);
				int swap2 = random_node(0, instance.dimension);
				swap(fille,swap1,swap2);
			}
			int remplacement = random_node(0, nbIndividu);
			invidivus[remplacement] = fille;
		}
	cptGen ++;
	cptCroisement = 0;
	nbCroisement = random_node(0, nbIndividu/2);
	}while(cptGen < nbGeneration);
	double tmp = 0;
	for(int i = 0; i < nbIndividu; i ++){
		if(tmp == 0 || tour_length(instance, individus[i], false) < tmp){
			tmp = tour_length(instance, individus[i], false);
			tournee = individus[i];
			tournee->length = tmp;
		}
	}
	return tmp;

}


int main(int argc, char** argv)
{
	//VARIABLES :
	char* methodName[6]; //Nom des méthodes utilisés
	
	tour_t tourTab[6]; // Tableau contenant les tours modifiés

	double resultTab[6][3]; //Tableau contenant les résultats : Longeur, Temps

	clock_t now, end;// Variable pour le calcul du temps

	int* tab_arg; //Tableau contenant la position des arguments si présent, 0 si non présent	
	opterr = 0; //
	
	int opt;
	
	sans_zero = 0;
	
	verbose_mode = 0;

	char filename[MAXBUF]; //Nom du fichier log
	
	tab_arg = creer_tab_int(NB_BALISES); //Tableau boolo
	
	instance_t instance;
	
	tour_t tour;
	
	char message[MAXBUF];
	
	init_instance(&instance);	
	
	
	//DEBUT DU PROGRAMME
	
	analyse_balises(argc, argv, tab_arg);
	if(tab_arg[BAL_H]>1)
	{
		print_help();
		return 0;
	}
	
	//INFORMATIONS SUR LES BALISES PRESENTES
	/*printf("Tableau des arguments : \n");
	for(int i=0;i<NB_BALISES;i++)
	{
		printf("Position : %d\n", tab_arg[i]);
	}*/
	while ((opt = getopt (argc, argv, "o:ppv:rw:2opt:ga:h:nz:bf:bfm:f:t:v:")) != -1)
	{
		switch (opt)
		{
			case 'v':
			verbose_mode = 1;
				//printf("OPTION POUR -V : %s\n", optarg);
			if(optarg[0]!='-')
			{
				strcpy(filename, optarg);	  
				
			}
			break;
			
			case '?':
			if(optopt == 'v')
			{
				strcpy(filename,"");
				break;
			}
			default: ;
		}
	}
	if(tab_arg[BAL_ZERO]>0)
	{
		sans_zero = 1;
	}
	//VALIDATION OPTION -f <file>
	if(tab_arg[BAL_F] >= 1)
	{
		for(int i = 0; i < argc; i++)
		{
			if(strcmp(argv[i],"-f")==0)
			{
				if(tab_arg[BAL_V] >= 1)
					{	strcpy(message, "Lecture du fichier instance : ");
				strcat(message, argv[i+1]);	
				ecrire_fichier(filename, message);
			}
			
			lecture_fichier_inst(argv[i+1], &instance);
			
			if(tab_arg[BAL_V] >= 1)
			{
				ecrire_fichier(filename, "Fin de la lecture instance : OK");
			}
		}
	}
	
	if(tab_arg[BAL_T] >= 1)
	{
			//Maintenant que le fichier est bien lu est l'instance initialisé. Valider les autres options.
		for(int i = 0; i<argc; i++)
		{
				//Validation [-t <file>]
			if(strcmp(argv[i],"-t")==0)
			{
				if(tab_arg[BAL_V]>=1)
				{ 
					strcpy(message, "Lecture du fichier tour : ");
					strcat(message, argv[i+1]);
					ecrire_fichier(filename, message);
				}
				lecture_fichier_tour(argv[i+1], &tour);
				if(tab_arg[BAL_V] >= 1)
				{
					ecrire_fichier(filename, "Fin de la lecture : OK");
				}
			}
		}		
	}
	
		//Debut des algos
		//####################################################################################################################		
	int ordreOption[6][2];
	int ordreCpt = 0;
	for(int i = 0; i<6;i++)
	{
		ordreOption[i][0] = 0;
		ordreOption[i][1] = 0;
		
	}
	for(int i = BAL_PPV; i<BAL_H;i++)
	{
		if(tab_arg[i]>0)
		{	
			ordreOption[ordreCpt][0] = i;
			ordreOption[ordreCpt][1] = tab_arg[i];
			ordreCpt++;
		}
	}
		//TRI DES ARGUMENTS

	int i,j,temp, tempbis;
	
	for(i=6-1; i>0 ; i--)
	{
		for(j=1;j<=i;j++)
		{
			if(ordreOption[j-1][1]>ordreOption[j][1])
			{
				tempbis=ordreOption[j-1][0];
				temp=ordreOption[j-1][1];
				ordreOption[j-1][1]=ordreOption[j][1];
				ordreOption[j-1][0]=ordreOption[j][0];
				
				ordreOption[j][1]=temp;
				ordreOption[j][0]=tempbis;
				
			}
		}
	}	
		 //POUR DEBUG
		/*for(int i = 0; i<6;i++)
		{
			printf("Argument : %d\t position : %d\n", ordreOption[i][0], ordreOption[i][1]);
		}*/
		for(int i = 0; i<6;i++)
		{
			if(ordreOption[i][0] == 4)
			{
					now = clock(); // depart chrono
					methodName[i] = "PPV";
					hard_copy_tour(&tourTab[i], tour);			
					//TRAITEMENT PPV
					if(sans_zero == 1)
					{
						resultTab[i][0] = ppv(instance, 1, &tourTab[i]);
					}else
					{
						resultTab[i][0] = ppv(instance, 0, &tourTab[i]);
					}
					end =clock();
					//printf("CPU TIME for PPV algorithm : %.2f",(end-now));
					resultTab[i][1]=(end-now)/CLOCKS_PER_SEC;
			}
			if(ordreOption[i][0] == 5)
			{
				tour_t pire;
				hard_copy_tour(&tourTab[i], tour);
				//printf("ON UTILISE BF, %s \t %s\n",tour.name,pire.name);
				strcpy(tourTab[i].name, "tour");
				tourTab[i].dimension = instance.dimension;
				methodName[i] = "BF";
				now = clock(); // depart chrono
				resultTab[i][0] = brute_force_tsp(instance, &tourTab[i], &pire, 0);
				end = clock();
				resultTab[i][1] = (end-now)/CLOCKS_PER_SEC; 
				
			}
			if(ordreOption[i][0]==6)
			{
				tour_t pire;
				hard_copy_tour(&tourTab[i], tour);
				methodName[i]= "BFM";
				strcpy(tourTab[i].name, "tour");
				tourTab[i].dimension = instance.dimension;
				now = clock(); // depart chrono
				resultTab[i][0]= brute_force_tsp(instance, &tourTab[i], &pire, 1);;
				end = clock();
				resultTab[i][2]= 1;
				resultTab[i][1]= (end-now)/CLOCKS_PER_SEC;
			}
			if(ordreOption[i][0]==7)
			{
				methodName[i]= "RW";
				hard_copy_tour(&tourTab[i], tour);
				now = clock(); // depart chrono
				resultTab[i][0]= randomwalk(instance, 1, &tourTab[i]);
				end = clock();
				resultTab[i][1]= (end-now)/CLOCKS_PER_SEC;
			}
			if(ordreOption[i][0]==8)
			{
				hard_copy_tour(&tourTab[i], tour);
				methodName[i]= "2OPT";
				now = clock(); // depart chrono
				resultTab[i][0]= 1;
				end = clock();
				resultTab[i][1]= (end-now)/CLOCKS_PER_SEC;
			}
			if(ordreOption[i][0]==9)
			{
				hard_copy_tour(&tourTab[i], tour);
				methodName[i]= "GA";
				now = clock(); // depart chrono
				resultTab[i][0]= 1;
				end = clock();
				resultTab[i][1]= (end-now)/CLOCKS_PER_SEC;
			}
				
		}
				//Validation export -o <file>
						
						if(1==1)
						{
									
							printf("\n*** Instance ***\n");
							
							printf("Nom de l'instance ; %s", instance.name);
							printf("Nb de ville ; %d\n", instance.dimension);
							
							printf("Type %s\n", instance.type);
							
							printf("Point ; Abs ; Ord\n");
							
							for(int j = 0; j<instance.dimension;j++)
							{
								printf("%d ; %d ; %d\n", instance.tabCoord[j][3],instance.tabCoord[j][0],instance.tabCoord[j][1]);		
							}
							
							printf("Methode ; longueur ; Temps CPU (sec) ; Tour\n");
							for(int j = 0; j<6;j++)
							{
								if(ordreOption[j][0]!=0)
								{
									char string[MAXBUF];
									sprintf(string,"%s ; %.2f ; %.2f ; {", methodName[j],resultTab[j][0],resultTab[j][1]);
								
									for(int e = 0; e<instance.dimension;e++)
									{
										char tourTabStr[MAXBUF];
										sprintf(tourTabStr,"%d,", tourTab[j].tour[e]);
										strcat(string,tourTabStr);	
									}
									strcat(string, "}\n");
									printf(string);
								}	
							}
						}
			if(tab_arg[BAL_O] >= 1)
			{
				for(int i = 0; i<argc; i++)
				{
				//Validation export -o <file>
					if(strcmp(argv[i],"-o")==0)
					{
						
						if(tab_arg[BAL_O]>=1)
						{
							if(tab_arg[BAL_V]>0)
							{
								char msg[MAXBUF];
				
								sprintf(msg,"Export dans dans le fichier : %s\n", argv[argc-1]);
								strcpy(message, msg);
								ecrire_fichier(filename, message);
							}
							FILE* exprt = fopen(argv[argc-1],"w");
							char string[MAXBUF];
							
							fprintf(exprt, "*** Instance ***\n");
							
							sprintf(string,"Nom de l'instance ; %s", instance.name);
							fprintf(exprt, string);
							
							sprintf(string,"Nb de ville ; %d\n", instance.dimension);
							fprintf(exprt, string);
							
							sprintf(string,"Type %s\n", instance.type);
							fprintf(exprt, string);
							
							sprintf(string,"Point ; Abs ; Ord\n");
							fprintf(exprt, string);
							
							for(int j = 0; j<instance.dimension;j++)
							{
								
								sprintf(string,"%d ; %d ; %d\n", instance.tabCoord[j][3],instance.tabCoord[j][0],instance.tabCoord[j][1]);		
								fprintf(exprt, string);
							}
							
							sprintf(string,"Methode ; longueur ; Temps CPU (sec) ; Tour\n");
							fprintf(exprt, string);
							for(int j = 0; j<6;j++)
							{
								if(ordreOption[j][0]!=0)
								{
									sprintf(string,"%s ; %.2f ; %.2f ; {", methodName[j],resultTab[j][0],resultTab[j][1]);
								
									for(int e = 0; e<tour.dimension;e++)
									{
										char* tourTabStr = "";
										sprintf(tourTabStr," %d", tourTab->tour[e]);
										strcat(string,tourTabStr);
										free(tourTabStr);	
									}
									strcat(string, "}\n");
									fprintf(exprt, string);
								}	
							}
							fclose(exprt);
						}
					}
				}
	}				//Fin du programme	
}
else{		
	erreur("Usage :  ./tsp -f <file> [-t <tour>] [-v [<file>]] -<méthode> [-h]");
}

free(tab_arg);
return 0;
}