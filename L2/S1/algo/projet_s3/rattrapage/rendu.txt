Exercice 1:
Q1 : Remplacer n=n/10; par n=n/pow(10,i)+1; 
Q2 : 

Exercice 2:


TAB addtinvt(TABt,int∗s){
/*Precondition:
0≤(∗s)<99∧(∀K:0≤K<(∗s)→0≤t[k]≤9)
*/
TAB R=(TAB)malloc(sizeof(int)∗100);
TAB ret=(TAB)malloc(sizeof(int)∗100);
int i;
i=-1;
ret[i+1]=0;
/*Invariant:
	(i < (∗s) < 99) ∧ (∀K : i < K ≤ (∗s) → 0 ≤ ret[K] ≤ 1) ∧
	(∀K : i < K < (∗s) → 0 ≤ R[K] ≤ 9) ∧
	(∀K : i < K < (∗s) → R[K] = (t[K] + t[(∗s) − K − i] + ret[K + 1]) − 10 ∗ ret[K]
*/
while(i != *s)
{
    if((t[(*s)+i]+t[i])%10 > 0)
    	...
    	...
    i++;
}

/* Postcondition:
(−1<(∗s)<99)∧(∀K:−1<K≤(∗s)→0≤ret[K]≤1)∧
(∀K:−1<K<(∗s)→0≤R[K]≤9)∧
(∀K:−1<K<(∗s)→R[K]=(t[K]+t[(∗s)−K−1]+ret[K+1])−10∗ret[K]
*/

//on ajoute 1 à gauche du nombre si le dernier retenu=1
if(ret[i+1]!=0)
{
for(intk=(∗s)+1;k>0;−−k)
R[k]=R[k−1];
R[0]=1;
++(∗s);
}
free(ret);
return R;
}


Exercice 3:

TAB Rech(TAB t, int *size, int *k)
{
	int symet = 0;
	*k = 0;
	/*Invariant

	*/
	while(k<53 && !symet)
	{
		addinvt(&t, &size);
		*k++;
		symet = simetrique(&t, &size);
	}
	return t;
}
