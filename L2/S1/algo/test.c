#include <stdio.h>

typedef int * TAB;

void xch(int *a, int *b) {
	int t;
	t = *a;
	*a = *b;
	*b = t;
}
void tabxch(int tab[], int taille) {
	int i, j;
	i = 0;
	j = taille -1;
	while (i < j) {
		xch(&tab[j], &tab[i]);
		i++;
		j--;
	}
}

int main() {
	int tab[10] = {0,1,2,3,4,5,6,7,8,9}; 
	int a = 5;
	int b = 6;
	int* pta = &a;
	int* ptb = &b;
	printf("a = %d, b = %d\n", a, b);
	xch(pta, ptb);
	printf("a = %d, b = %d\n", a, b);
	for(int i = 0; i < 10; i++){
		printf("| %d |", tab[i]);
	}
	printf("\n");
	tabxch(tab, 10);
	for(int i = 0; i < 10; i++){
		printf("| %d |", tab[i]);
	}
	printf("\n");
	return 0;
}
