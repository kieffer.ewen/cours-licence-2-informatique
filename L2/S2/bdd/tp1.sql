CREATE TABLE Moniteur (noM char(10 CHAR) PRIMARY KEY NOT NULL, 
		nomM varchar2(20 CHAR), 
		prenomM varchar2(20 CHAR), 
		adresseM varchar2(50 CHAR), 
		dateNaiss date);

CREATE TABLE Adherent (noA char(10 CHAR) PRIMARY KEY NOT NULL, 
		nomA varchar2(20 CHAR), 
		prenomA varchar2(20 CHAR), 
		adresseA varchar2(50 CHAR), 
		telA char(10 CHAR), 
		ageA int,
		constraint ck_age_range check (ageA between 12 and 100));

CREATE TABLE Materiel (codeM char(10 CHAR) PRIMARY KEY NOT NULL, 
		type varchar2(15), 
		marque varchar2(15 CHAR), 
		prix float(2), 
		qteDispo int,
		constraint ck_qtedispo_notnull check (qteDispo > 0),
		constraint ck_prix_notnull check (prix > 0));

CREATE TABLE Specialite (idSpecialite char(10 CHAR) PRIMARY KEY NOT NULL,
		nomS varchar2(20 CHAR),
		constraint ck_specialite check (nomS in ('ski', 'snowboard', 'monoski', 'raquette')));

CREATE TABLE Cours (codeC char(10 CHAR) PRIMARY KEY NOT NULL, 
		niveau varchar2(20 CHAR), 
		nbPlaces int, 
		dateCours date, 
		noM char(10 CHAR), 
		idSpecialite char(10 CHAR), 
		constraint fk_noM FOREIGN KEY (noM) references Moniteur, 
		constraint fk_idSpecialite FOREIGN KEY (idSpecialite) references Specialite, 
		constraint ck_cours_niveau check (niveau in ('débutant', 'moyen', 'confirmé', 'compétition')),
		constraint ck_nbPlaces_notnull check (nbPlaces > 0));

CREATE TABLE Suivre (noA char(10 CHAR), 
		codeC char(10), 
		PRIMARY KEY(noA, codeC), 
		constraint fk_noA_suivre FOREIGN KEY (noA) references Adherent, 
		constraint fk_codeC foreign key (codeC) references Cours);

CREATE TABLE Louer (noA char(10 CHAR), 
		codeM char(10 CHAR), 
		quantite int, 
		PRIMARY KEY(noA, codeM), 
		constraint fk_noA_louer FOREIGN KEY (noA) references Adherent, 
		constraint fk_codeM FOREIGN KEY (codeM) references Moniteur,
		constraint ck_quantite_notnull check (quantite > 0));


Insert into Moniteur(noM, nomM, prenomM, adresseM, dateNaiss) values(1,'Dupond','Jean','12 Rue du Col, Germ', to_date('01011980','DDMMYYYY'));
Insert into Moniteur(noM, nomM, prenomM, adresseM, dateNaiss) values(2,'Martin','Sophie','Route du lac, Loudenville', to_date('13051988','DDMMYYYY'));

Insert into Adherent(noA, nomA, prenomA, adresseA, telA, ageA) values(1,'Lars','Julien','12 rue principale, Muret','0666666666',13);
Insert into Adherent(noA, nomA, prenomA, adresseA, telA, ageA) values(2,'Salma','Louise','route de Tarbes, Pau','0777777777',26);
Insert into Adherent(noA, nomA, prenomA, adresseA, telA, ageA) values(3,'Jardin','Lucien','Route d Espagne, Tarbes','',52);

Insert into Specialite(idSpecialite, nomS) values(1, 'ski');
Insert into Specialite(idSpecialite, nomS) values(2, 'snowboard');
Insert into Specialite(idSpecialite, nomS) values(3, 'raquette');

Insert into Materiel(codeM, type, marque, prix, qtedispo) values(1, 'Ski', 'HyperGliss', 10, 2);
Insert into Materiel(codeM, type, marque, prix, qtedispo) values(2, 'Snowboard', 'HyperGliss', 11, 3);
Insert into Materiel(codeM, type, marque, prix, qtedispo) values(3, 'Raquette', 'Smash', 8, 2);

Insert into Cours(codeC, niveau, nbplaces, datecours, noM, idSpecialite) values(1,'débutant',1,to_date('01022013','DDMMYYYY'),1,2);
Insert into Cours(codeC, niveau, nbplaces, datecours, noM, idSpecialite) values(2,'moyen',1,to_date('02022013','DDMMYYYY'),1,2);
Insert into Cours(codeC, niveau, nbplaces, datecours, noM, idSpecialite) values(3,'débutant',5,to_date('01022013','DDMMYYYY'),2,1);

Insert into Louer(noA, codeM, quantite) values(3,1,1);
Insert into Louer(noA, codeM, quantite) values(2,1,2);

Insert into Suivre(codeC, noA) values(1,1);
Insert into Suivre(codeC, noA) values(1,2);
Insert into Suivre(codeC, noA) values(3,1);
Insert into Suivre(codeC, noA) values(2,3);