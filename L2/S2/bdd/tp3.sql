1.
SELECT COUNT(*) FROM 
gilles_hubert.src1_magasins; (225 rows)
SELECT COUNT(*) FROM 
gilles_hubert.src1_vendre; (4072 rows)
SELECT COUNT(*) FROM 
gilles_hubert.src1_enseignes; (1 row)
SELECT COUNT(*) FROM 
gilles_hubert.src1_produits; (93 rows)
SELECT COUNT(*) FROM 
gilles_hubert.src1_marques; (21 rows)
SELECT COUNT(*) FROM 
gilles_hubert.src1_categories; (25 rows)

2.
Select raisonSoc, tel 
from gilles_hubert.src1_magasins; (225 rows)

3.
Select raisonSoc, adrVille 
from gilles_hubert.src1_magasins
where upper(adrVille) = 'TOULOUSE'; (3 rows)

4.
Select raisonSoc 
from gilles_hubert.src1_magasins
where adrCP = '33000'; (5 rows)

5.
select desigP
from gilles_hubert.src1_produits
where prixU = '1990'; (4 rows)

6.
select desigP
from gilles_hubert.src1_produits
where prixU > 2000; (54 rows)

7.
select desigP
from gilles_hubert.src1_produits
where prixU between 1000 and 2000; (18 rows)

8.
select desigP
from gilles_hubert.src1_produits
where codeC = 2 or codeC = 4; (10 rows)

9. 
select codeP, qte, dateV 
from gilles_hubert.src1_vendre
where to_date(dateV) -to_date('01-MAR-00') < 0; (1295 rows)

10.
select codeP, qte, dateV
from gilles_hubert.src1_vendre
where dateV like '%MAY%'; (660 rows)

11.
select codeP, desigP, desigM 
from gilles_hubert.src1_produits p, gilles_hubert.src1_marques m
where p.codeM = m.codeM; (93 rows)

12.
select distinct codeP
from gilles_hubert.src1_vendre
where to_date(dateV) -to_date('31-MAY-00') > 0; (16 rows)

13.
Select distinct desigM 
from gilles_hubert.src1_marques mar, gilles_hubert.src1_produits p, gilles_hubert.src1_vendre v, gilles_hubert.src1_magasins mag
where upper(adrVille) = 'PAMIERS'
and mar.codeM = p.codeM 
and p.codeP = v.codeP
and v.raisonSoc = mag.raisonSoc; (5 rows)

14.
Select distinct mag.raisonSoc
from gilles_hubert.src1_marques mar, gilles_hubert.src1_produits p, gilles_hubert.src1_vendre v, gilles_hubert.src1_magasins mag
where upper(mar.desigM) = 'SONY'
and mar.codeM = p.codeM 
and p.codeP = v.codeP
and v.raisonSoc = mag.raisonSoc
and p.desigP like '%Téléviseur%'; (14 rows)