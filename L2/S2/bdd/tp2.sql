a. Le  club  ne propose  plus  la location  de  raquettes. 
Donnez  la  requête  permettant  de supprimer les données correspondantes dans la table Matériel et vérifiez le contenu de la table.

delete from materiel 
where upper(type) = 'RAQUETTE'

Select * from materiel

b. L’adhérent 1  ne  veut  plus  suivre  le  cours  3.
 Donnez  la  requête  permettant  de supprimer les données correspondantes dans la table Suivre et vérifiez le contenu de la table.

Delete from suivre
where noa = 1 
and codec = 3;

select * from suivre

c. Le cours 2 est  supprimé. Donnez  la  requête  permettant  de  supprimer  les  données correspondantes   dans   la   table   Cours.   
Pourquoi   selon   vous   rencontrez-vous   un problème ? Que faut-il faire ?

Delete from cours
where codec = 2

#La suppression ne va pas fonctionner car il existe des clés étrangères pointant sur cette clé primaire dans suivre.
#Il faut donc supprimer d'abord toutes les clés étrangères dans suivre, puis réexecuter la requête précédente.

d. L’adhérent Julien  Lars  a  déménagé au ’23 route de Toulouse, Saint-Gaudens’.  
Faites la requête de mise à jour nécessaire et vérifiez le contenu de la table.

update adherent
set adressea = '23 route de Toulouse, Saint-Gaudens'
where upper(prenoma) = 'JULIEN'
and upper(noma) = 'LARS'

select * from adherent


e. Tout  le  matériel  à  la  location  a  augmenté  de  10%.  
Faites la  requête  de mise  à  jour nécessaire et vérifiez le contenu de la table.

update materiel
set prix = prix*1.1

select * from materiel

f. Le club a acheté deux nouvelles paires de ski pour la location. 
Faites la requête de mise à jour nécessaire et vérifiez le contenu de la table

update louer
set quantite = quantite + 2
where codeM = (Select codeM from Materiel where upper(type) = 'SKI')



#Requetes de listing du dictionnaire :

select * from ALL_TABLES where OWNER = 'GILLES_HUBERT' and TABLE_NAME like 'TP2_%'
select * from ALL_TAB_COLUMNS where OWNER = 'GILLES_HUBERT' and TABLE_NAME like 'TP2_%'
select * from ALL_CONSTRAINTS where OWNER = 'GILLES_HUBERT' and TABLE_NAME like 'TP2_%'
select * from ALL_CONS_COLUMNS where OWNER = 'GILLES_HUBERT' and TABLE_NAME like 'TP2_%'

#MLD 
#Utilisant Sublime-texte, le soulignement de texte ne fonctionne pas bien.
#Les noms de colonnes séparé par le caractere '_' correspondent aux clés primaires

Pilote(number_nuPilote,varchar2(20) nomPilote, varchar2(20) prenomPilote, varchar2(20) Nationalite ),
Circuit(number_nuCi, varchar2(20) nomCi, varchar2(20) paysCi, number longueurCi, number nbTours),
Ecurie(number_nuEcurie, varchar2(20) nomEcurie),
Course(number_nuCo, number anneeCo, varchar2(5) meteo, number #nuCi),
EffectuerQualification(number_#nuCo,_number_#nuPilote number classement, number tempsTour ),
Effectuer(number_#nuCo,_number_#nuPilote, number classement, number tempsTour, number temps ),
Appartenir(number_#nuPilote,_number_#nuEcurie, number annee);
