Moniteur (noM, nomM, prenomM, adresse, dateNaiss)

- le nom, le prénom et la date de naissance doivent être non nulle (Même chose pour l'adresse si elle est considérée obligatoire).

Spécialité (idSpecialité, nomS)

Cours (codeC, niveau, nbPlaces, dateCours, #noM, #idSpecialite)

- La date doit etre non nulle.

Adhérent (noA, nomA, prenomA, adresseA, telA, ageA)

- Le nom, le prénom, le telephone et l'age doivent être non nulle. (Même chose pour l'adresse si elle est considérée obligatoire).
- L'age devrait être remplacé par la date de naissance car il s'agit d'une donnée calculable.

Matériel (codeM, type, marque, prix, qteDispo)

- Le type et la marque doivent être forcément précisé (Donc doivent être non nulle)

Suivre (#codeC, #noA)

Louer (#noA, #codeM, quantite)