CREATE TABLE Groupe (nomG varchar2(25) PRIMARY KEY NOT NULL, dateCreationG varchar2(20));
CREATE TABLE Fonction (nomF varchar2(25) PRIMARY KEY NOT NULL);
CREATE TABLE Personne (numP int PRIMARY KEY NOT NULL, nom varchar2(25), prenom varchar2(25));
CREATE TABLE Avis (num int PRIMARY KEY NOT NULL, description varchar2(200), note char(1));
CREATE TABLE horaire(heureDebut varchar2(10) PRIMARY KEY NOT NULL);
CREATE TABLE jour(dateJ varchar2(20) PRIMARY KEY NOT NULL);
CREATE TABLE chaine(nomCh varchar2(25) PRIMARY KEY NOT NULL, dateCreationCh  varchar2(20), nomg varchar2(25), constraint fk_chaine foreign key (nomg) references Groupe);
CREATE TABLE Emission(reference varchar2(3) PRIMARY KEY NOT NULL, titre varchar2(50), resume varchar2(400), type varchar(20), duree int, nomG varchar2(25), num int, constraint fk_emission1 foreign key (nomG) references Groupe, constraint fk_emission2 foreign key (num) references Avis);
CREATE TABLE Intervenir(nomF varchar2(25), reference varchar2(3), numP int, primary key(nomF,reference,numP), constraint fk_intervenir1 foreign key (nomF) references fonction, constraint fk_intervenir2 foreign key (reference) references Emission, constraint fk_intervenir3 foreign key (numP) references Personne);
CREATE TABLE diffuser(nomCh varchar2(25),dateJ  varchar2(20),heureDebut varchar2(10), reference varchar2(3), heureFin varchar2(10), primary key(nomCh,dateJ,heureDebut), constraint fk_diffuser1 foreign key (nomCh) references chaine, constraint fk_diffuser2 foreign key (dateJ) references jour, constraint fk_diffuser3 foreign key (heureDebut) references horaire, constraint fk_diffuser4 foreign key (reference) references Emission);
-- requêtes de creation

drop table diffuser;
drop table intervenir;
drop table emission;
drop table chaine;
drop table horaire;
drop table avis;
drop table personne;
drop table fonction;
drop table groupe;
drop table jour;
-- requêtes de suppression

insert into Groupe(nomG, dateCreationG) select distinct nomg, datecreationg from gilles_hubert.tp4_tv;
insert into Fonction(nomF) select distinct nomf from gilles_hubert.tp4_tv where nomF not in (select nomF from Fonction);
insert into Personne(nump, nom, prenom) select distinct nump, nom, prenom from gilles_hubert.tp4_tv;
insert into Avis(num, description, note) select distinct num, description, note from gilles_hubert.tp4_tv where num is not null;
insert into Horaire(heureDebut) select distinct heureDebut from gilles_hubert.tp4_tv;
insert into Jour(dateJ) select distinct dateD from gilles_hubert.tp4_tv;
insert into Chaine(nomCh, dateCreationCh, nomG) select distinct nomCh, dateCreationCh, nomG from gilles_hubert.tp4_tv;
insert into Emission(reference, titre, resume, type, duree, nomG, num) select distinct reference, titre,resume,type,duree,nomG,num from gilles_hubert.tp4_tv;
insert into Intervenir(nomF, reference, numP) select distinct nomF, reference, numP from gilles_hubert.tp4_tv;
insert into Diffuser(nomCh, dateJ, reference, heureDebut, heureFin) select distinct nomCh, dateD, reference, heureDebut, heureFin from gilles_hubert.tp4_tv;
-- requêtes d'insertion

1. Select * from personne;
2. Select nomCh, nomG from chaine;
3. select * from personne where upper(prenom) = upper('valérie');
4. select * from avis where note >= 4;
5. Select * from emission where num is not null;
6. select titre from diffuser, emission where dateJ = '15-JAN-16' and diffuser.reference = emission.reference;
7. select distinct emission.reference, emission.titre, personne.nom, personne.prenom, fonction.nomf from emission,intervenir, personne, fonction where intervenir.reference = emission.reference and intervenir.nomF = fonction.nomF and intervenir.numP = personne.numP;
8. select nom, prenom 
from intervenir, personne, emission 
where upper(type)=upper('cinéma') 
and intervenir.reference = emission.reference
and intervenir.nump = personne.nump
and upper(intervenir.nomf)=upper('réalisateur');
9.
