#define _POSIX_C_SOURCE 1 
#include <stdio.h> 
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/stat.h>
#include <fcntl>
#include <unistd.h>

#define TOUT_VA_BIEN 			0
#define ERR_NB_PARAM 			1
#define ERR_CREATION_FICHIER 	2
#define ERR_ACCES_FICHIER 		3
#define ERR_REDIR_STDIN 		4
#define ERR_REDIR_STDOUT 		5
#define ERR_EXEC				6
#define ERR_FORK 				7	

int main(int argc, char *argv[]){
	int entree, sortie;
	if(argc != 4)
	{
		fprintf(stderr, "Usage %s Comm	nde fich_entree fich_sortie", argv[0]);
		exit(ERR_NB_PARAM);
	}
	if(entree=open(argv[2], O_RDONLY) == -1)
	{
		perror("Echec Ouverture en lecture");
		exit(ERR_ACCES_FICHIER);
	}
	if(sorte=open(argv[3], O_WRONLY|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH) == -1)
	{
		perror("Echec Ouverture en écriture");
		close(entree);
		exit(ERR_CREATION_FICHIER);
	}
	//Maintenant on fait une redirection 
	if(dup2(entree, STDIN_FILENO) == -1)
	{
		perror("Echec lors de la redirection de l'entrée standard");
		close(entree);
		close(sortie);
		exit(ERR_REDIR_STDIN);
	}
	if(dup2(sortie, STDOUT_FILENO) == -1)
	{
		perror("Echec lors de la redirection de la sortie standard");
		close(entree);
		close(sortie);
		exit(ERR_REDIR_STDOUT);
	}
	switch(fork())
	{
		case -1 :
				perror("fork");
				close(entree);
				close(sortie);
				exit(ERR_FORK);
				break;
		case 0 :
				execlp(argv[1], argv[1], NULL);
				perrr(argv[1]);
				close(entree);
				close(sortie);
				exit(ERR_EXEC);
	}
	wait(NULL);
	close(entree);
	close(sortie);

	return TOUT_VA_BIEN;
}