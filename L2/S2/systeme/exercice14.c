#define _POSIX_C_SOURCE 1 
#include <stdio.h> 
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>

#define ERR_OUVERTURE_REP 1
#define ERR_ARG 2

int main(int argc, char** argv)
{
	if(argc != 2)
	{
		fprintf(stderr, "Usage command %s Directory", argv[0]);
		exit(ERR_ARG);
	}

    DIR* rep = NULL;
    long taille = 0;
    struct dirent* fichierLu = NULL; 
    struct stat infos;
    rep = opendir(argv[1]);
 
    if (rep == NULL)  {
        exit(ERR_OUVERTURE_REP);
   }
 
    while ((fichierLu = readdir(rep)) != NULL) {
    	if(stat(fichierLu->d_name, &infos) == 0)
    	{
    		if(!S_ISDIR(infos.st_mode))
    		{
    			taille += infos.st_size;
    		}
   			else
   			{
   				perror(fichierLu->d_name);
   			}
    	}
    }
 	printf("Taille du répertoire %ld Octets\n", taille);
    if (closedir(rep) == -1) {
        exit(-1);
    }
 
    return 0;
}