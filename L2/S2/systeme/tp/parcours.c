#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
void afficherinfos(char* file,struct stat infos){
	char* type = malloc(255*sizeof(char));
	if(S_ISDIR(infos.st_mode) != 0)
		strcpy(type,"repertoire");
	if(S_ISREG(infos.st_mode) != 0)
		strcpy(type,"fichier");
	printf("%-40s:\t %s\t %8ld octets %s", file,type , infos.st_size, ctime(&infos.st_atime));
	free(type);
}

int main(int argc, char* argv[]){
	if(argc < 2){
		DIR* currentDir; 
		struct dirent* dir;
		struct stat infos;

		if((currentDir = opendir(".")) == NULL){
			printf("Problème a la récupération des informations des fichiers du répertoire courant\n");
		}
		while((dir = readdir(currentDir)) != NULL){
			if(stat(dir->d_name, &infos) == -1)
				printf("Probleme à la récupération des informations du fichier suivant : %s\n", dir->d_name);
			afficherinfos(dir->d_name,infos);
		}
		closedir(currentDir);
	}else {
	struct stat infos;
	for(int i = 1; i < argc; i ++){
		if(stat(argv[i], &infos) == -1){
			printf("Probleme à la récupération des informations du fichier suivant : %s\n", argv[i]);
			return 1;
		}
		afficherinfos(argv[i],infos);
	}
	return 0;
	}
}