#define _POSIX_C_SOURCE 1
#define N 10
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>

void informations(){
	printf("processus fils de pid %d et de pid père %d et d'uid %d et de gid %d et d'utilisateur %s\n", getpid(), getppid(), getuid(), getgid(), getlogin());
}

int main(int argc, char** argv, char** envp){
	int pid = 0;
	switch(pid = fork()){
		
		case -1 : // On est dans le père et ça s'est mal passé
			printf("erreur\n");
			exit(EXIT_FAILURE);
		case 0 : // On est dans le fils
			informations();
			exit(0);
		default : // On est dans le père 
			informations();
			wait(NULL);
	}
	return EXIT_SUCCESS;
}