#define _POSIX_C_SOURCE 1
#define N 10
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#define NBMOTSMAX 20

int decoupe(char Chaine[], char *pMots[])
{
  char *p;
  int NbMots=0;
 
  p=Chaine; /* On commence par le d�but */
  /* Tant que la fin de la cha�ne n'est pas atteinte et qu'on ne d�borde pas */
  while ((*p)!='\0' && NbMots<NBMOTSMAX)
  {
    while ((*p)==' ' && (*p)!='\0') p++; /* Recherche du d�but du mot */
    if ((*p)=='\0') break; /* Fin de cha�ne atteinte */
    pMots[NbMots++]=p; /* Rangement de l'adresse du 1er caract�re du mot */
    while ((*p)!=' ' && (*p)!='\0') p++; /* Recherche de la fin du mot */
    if ((*p)=='\0') break; /* Fin de cha�ne atteinte */
    *p='\0'; /* Marquage de la fin du mot */
    p++; /* Passage au caract�re suivant */
  }
  pMots[NbMots]=NULL; /* Derni�re adresse */
  return NbMots;
}
void informations(){
	printf("processus fils de pid %d et de pid père %d\n", getpid(), getppid());
}

int main(int argc, char** argv, char** envp){
	int pid = 0;
	char* args[2];
	char *pmots[NBMOTSMAX +1];
	for(int i = 1; i < argc; i ++){
	switch(pid = fork()){

		case -1 : // On est dans le père et ça s'est mal passé
			printf("erreur\n");
			exit(EXIT_FAILURE);
		case 0 : // On est dans le fils
			printf("[%d] Je lance %s :\n",getpid(), argv[i]);
			decoupe(argv[i], pmots);
			execvp(pmots[0], pmots);
			perror(pmots[0]);
			exit(0);
		default : // On est dans le père 
			printf("[%d] J'ai délégué %s à %d. J'attends sa fin ...\n", getpid(), argv[i], pid);
			wait(NULL);
			printf("[%d] %d fini \n",getpid(), pid);

	}
}
	printf("J'ai fini\n");
}