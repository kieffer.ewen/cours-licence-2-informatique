#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
void afficherinfos(char* file,struct stat infos){
	char* type = malloc(255*sizeof(char));
	if(S_ISDIR(infos.st_mode) != 0)
		strcpy(type,"repertoire");
	if(S_ISREG(infos.st_mode) != 0)
		strcpy(type,"fichier");
	printf("%-40s:\t %s\t %8ld octets %s", file,type , infos.st_size, ctime(&infos.st_atime));
	free(type);
}

int affNomfic(char nomrep[])
{
  typedef struct dirent dirent;
  struct stat info;
  dirent* param;
  char designation[512];
  size_t dirSize = 0;
  DIR* d = NULL;
  if((d = opendir(nomrep))==NULL)
  {
    perror(nomrep);
    exit(2);
  }
  param = readdir(d);
  while(param!=NULL)
  {
      if(strcmp(param->d_name,".")!=0)
      { 
       if(strcmp(param->d_name,"..")!=0)
        {
          sprintf(designation,"%s/%s", nomrep, param->d_name);
          if(stat(designation, &info)!=-1)
          {
            dirSize += info.st_size;
            if(S_ISDIR(info.st_mode))
            {
             dirSize += affNomfic(designation);
            }
            printf("%-40s: %8ld octets\n", designation, info.st_size);
          }
          else
          { 
            perror(designation);
          }
        }
      }
        param = readdir(d);
}
  closedir(d);
  return dirSize;
}



/*Parcourir(Rep){
	Pour tout Element de Rep
	Si Element est différent de "." et de ".."
		Si Rep/Element est un répertoire
			Parcourir(Rep/Element);
		Sinon
			Traiter(Rep/Element);
}
*/
int main(int argc, char* argv[]){
		if(argc < 2){
			printf("Pas de répertoire spécifié\n");
			return 1;
		}
		if(argc > 2){
			printf("Trop de répertoires spécifiés\n");
			return 2;
		}
		  printf("REPERTOIRE : %s\n", argv[1]);
		  int size = affNomfic(argv[1]);
		  printf("TOTAL %s:\t%8ld octets\n", argv[1], size);
		}
