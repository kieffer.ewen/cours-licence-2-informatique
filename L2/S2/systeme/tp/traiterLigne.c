
#include "matrice.h"
#include "traiterLigne.h"

/* retourne 0: aucun trouve, 1: V1 trouve, 2: v2 trouve et 3: v1 et v2 trouve */
unsigned traiterLigne(  int uneLigne[NB_COLONNES_MAX], unsigned c,
			unsigned v1, unsigned v2)
{
    int cptv1 = 0;
	int cptv2 = 0;
	for(int i = 0; i < c; i++){
		if(uneLigne[i] == v1)
			cptv1 ++;
		if(uneLigne[i] == v2)
			cptv2 ++;
	}
	if(cptv1 > 0 && cptv2 > 0)
		return 3;
	if(cptv2 > 0)
		return 2;
	if(cptv1 > 0)
		return 1;
	return 0;

}
