#define _POSIX_C_SOURCE 199309L
#include <sys/termios.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int Term_non_canonique (){
	struct termios term;
	tcgetattr( fileno(stdin), &term);
	/* lit les flags du terminal dans term */
	term.c_lflag &= ~ICANON;         
	/* mode non-canonique */
	term.c_lflag &= ~ECHO;           
	/* supprime l’écho */
	term.c_cc[VMIN]  = 1;
	/* nombre min de caractères */
	term.c_cc[VTIME] = 1;
	/* latence (timeout) 1/10e de seconde (0: pas de latence) */
	if (tcsetattr( fileno(stdin), TCSANOW, &term) < 0) /* écrit les flags depuis term */{
		perror("Term_non_canonique: problème d’initialisation ");
		return 0;}
	return 1;
}

int Term_canonique (){
	struct termios term;
	/* retour en mode ligne */
	tcgetattr( fileno(stdin), &term);
	/* lit les flags du terminal dans term */
	term.c_lflag |= ICANON;          
	/* mode canonique */
	term.c_lflag |= ECHO;
	/* rétablit l’écho */
	if (tcsetattr( fileno(stdin), TCSANOW, &term) < 0) /* écrit les flags depuis term */{
		perror("Term_canonique: problème d’initialisation ");
		return 0;}
	return 1;
}

void touche(){
	char ch;
	while(read(STDIN_FILENO, &ch, 1)){
		if(ch == 'f'){
			break;
		}else{
			printf("%d %c\n",getpid(),ch);
			fflush(stdout);
		}
	}
}

void clear(){
	printf("\033[H\033[J");
	fflush(stdout);
}

void joueur(char my_char, int ligne, int distance){
	char ch;
	int cpt = 0;
	struct timespec micro_pause;micro_pause.tv_sec = 0;       /* secondes */
	micro_pause.tv_nsec = 1000;   /* nanosecondes */
	printf ("\033[%d;%dH%c",ligne,cpt,my_char);
	fflush (stdout);
	while(read(STDIN_FILENO, &ch, 1)){
		if(ch == my_char){
			printf ("\033[%d;%dH ",ligne,cpt);
			fflush (stdout);
			cpt++;
			printf ("\033[%d;%dH%c",ligne,cpt,my_char);
			fflush (stdout);
			nanosleep( &micro_pause ,NULL);
		}
		if(cpt == distance){
			clear();
			printf("Joueur %c gagne !\n", my_char);
			break;
		}
	}
}

void joueurBin(char my_char, int ligne){
	char ch;
	int cpt = 0;
	struct timespec micro_pause;micro_pause.tv_sec = 0;       /* secondes */
	micro_pause.tv_nsec = 1000;   /* nanosecondes */
	while(read(STDIN_FILENO, &ch, 1)){
		if(ch == my_char){
			cpt++;
			nanosleep( &micro_pause ,NULL);
		}
	}

}
void monitor(){

}
int main(int argc, char** argv){
	// open('monitor.bin',O_RDONLY | O_CREAT, S_IRWXU);
	clear();
	Term_non_canonique();
	int pid;
	int cpt = 2;
	int distance = atoi(argv[2]);
	for(int i = 0; i < atoi(argv[1]); i ++){
		switch(pid=fork()){
			case -1 :
				perror("Erreur a la creation des fils"); 
			case 0 :
				joueur(97+i,i+1,distance);
			default:
			break;
		}
	}
	while(cpt>1){
		wait(NULL);
		cpt --;
	}
	Term_canonique();
	return 0;
}
	// printf ("\033[%d;%dHa", 1, 0);
	// fflush (stdout);
	// printf ("\033[%d;%dHb", 2, 0);
	// fflush(stdout);
