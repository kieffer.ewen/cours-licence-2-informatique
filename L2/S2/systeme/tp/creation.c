#define PB_LECTURE		7
#define POS_INACCESS	6
#define PAS_ASSEZ_PARAM	5
#define TROP_DE_PARAM	4
#define PB_ECRITURE		3
#define PB_FERMETURE	2
#define PB_OUVERTURE	1
#define TOUTVABIEN		0
#define LONG_MAX_NOM	20
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
typedef struct{
	char nom[LONG_MAX_NOM+1];
	int age;
	int nbEnfants;
	} Infos;

int creation(char nomFichier[]){
	int indiceFichier;
	if((indiceFichier = open(nomFichier,O_CREAT|O_RDWR|O_APPEND, S_IRWXU)) == -1)
		return PB_OUVERTURE;
	Infos file;
	printf("Nom ?\n");
	scanf("%s",&file.nom);
	printf("Age ? \n");
	scanf("%d",&file.age);
	printf("nbEnfants ?\n");
	scanf("%d",&file.nbEnfants);
	if(write(indiceFichier, &file, sizeof(Infos)) == -1)
		return PB_ECRITURE;
	if(close(indiceFichier) == -1)
		return PB_FERMETURE;
	return TOUTVABIEN;
}

int consultation(char nomFichier[]){
	int indiceFichier;
	if((indiceFichier = open(nomFichier,O_RDONLY)) == -1)
		return PB_OUVERTURE;
	int retour;
	Infos i;
	int fiches = 1;
	while(read(indiceFichier,&i,sizeof(Infos))){
		printf("Fiche N°%d : %s\n", fiches, i.nom);
		fiches ++;
	}
	int continuer = 1;
	int fichecourante;
	while(continuer){
		printf("Fiche a consulter ?\n");
		scanf("%d",&fichecourante);
		if(lseek(indiceFichier,(fichecourante-1)*sizeof(Infos),SEEK_SET) == -1)
			return POS_INACCESS;
		if(read(indiceFichier,&i,sizeof(Infos)) == -1)
			return PB_LECTURE;
		printf("Nom : %s\n",i.nom);
		printf("Age : %d\n",i.age);
		printf("Nombre d'enfants : %d\n", i.nbEnfants);
		printf("\n");
		printf("Continuer la consultation ? 0 : Non | 1 : Oui\n");
		scanf("%d",&continuer);
	}
}

int main(int argc, char argv[]){
	if(argc>1){
		printf("nombre de parametres trop grand : %d\n", argc - 1);
		return TROP_DE_PARAM;
	}
	int arg;
	int continuer = 1;
	char* file;
	while(1){
		printf("0 : Quitter l'application\n");
		printf("1 : Création d'un fichier binaire\n");
		printf("2 : Consultation d'un fichier binaire\n");
		scanf("%d",&arg);
		switch(arg){
			case 0 : 
				return 0;
			break;
			case 1 : 
				printf("Nom fichier ?\n");
				scanf("%s",file);
				while(continuer){
					switch(creation(file)){
						case 3 :
							printf("Problème à l'écriture du fichier\n");
							free(file);
							return 3;
							break;
						case 2 :
							printf("Problème à la fermeture du fichier\n");
							free(file);
							return 2;
						break;
						case 1 :
							printf("Problème à l'ouverture du fichier\n");
							free(file);
							return 1;
						break;
						case 0 :
							printf("Fiche créé avec succès. Continuer ? (0 : Non  || 1 : Oui\n");
							scanf("%d",&continuer);
							continue;
						break;
					}
				free(file);

				}
			break;
			case 2 :
				printf("Nom fichier ?\n");
				scanf("%s",file);
				switch(consultation(file)){
					case 6 : 
						printf("Fiche introuvable\n");
						break;
					case 7 : 
						printf("Problème à la lecture du fichier\n");
						break;
				}
			break;
	}
	}
}