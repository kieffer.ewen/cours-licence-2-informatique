#define _POSIX_C_SOURCE 1
#define N 10
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

void informations(){
	printf("processus fils de pid %d et de pid père %d et d'uid %d et de gid %d et d'utilisateur %s\n", getpid(), getppid(), getuid(), getgid(), getlogin());

}
int main(int argc, char** argv, char** envp){
	informations();
	return 0;
}