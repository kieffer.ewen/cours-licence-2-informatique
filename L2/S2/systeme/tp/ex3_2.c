#define _POSIX_C_SOURCE 1
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include "matrice.h"
#include "traiterLigne.h"
#include <time.h>
//VERSION 2
int main(int argc, char** argv, char** envp)
{
 
    pid_t pid;
    int circonstance;
    int nbLig = NB_LIGNES_MAX;
    int nbCol = NB_COLONNES_MAX;
    int cpt = nbLig;
    int m[NB_LIGNES_MAX][NB_COLONNES_MAX];
    initialiserMatrice(m,nbLig,nbCol);
    int nbl_aucun = 0;
    int nbl_v1 = 0;
    int nbl_v2 = 0;
    int nbl_v1_v2 = 0;
    int r = 0;
    srand(time(NULL));
    int v1 = rand() % NB_COLONNES_MAX;
    int v2 = rand() % NB_COLONNES_MAX;
    printf("v1 : %d\t v2 : %d\n", v1, v2);
   
    for(int i = 0; i<nbLig; i++)
    {
        switch(pid = fork())
        {
        case -1:
            printf("Une erreur s'est produite ! PID du pere = %d\n", getpid());
            exit(EXIT_FAILURE);
            break;
        case 0:
            exit(traiterLigne(m[i], nbCol, v1, v2));
           
            break;
        default :
           
        break;
        }
    }
    while(cpt>0)
    {
    int finpid = wait(&circonstance);
    switch(WEXITSTATUS(circonstance))
    {
                case 0 :
                    nbl_aucun++;
                    printf("Ligne %d : Non trouvé\n", abs(getpid()-finpid));
                    break;
                case 1 :
                    nbl_v1++;
                    printf("Ligne %d : v1 trouvé\n", abs(getpid()-finpid));
                    break;
                case 2 :
                    nbl_v2++;
                    printf("Ligne %d : v2 trouvé\n", abs(getpid()-finpid));
                    break;
                case 3 :
                    nbl_v1_v2++;
                    printf("Ligne %d : v1 et v2 trouvés\n", abs(getpid()-finpid));
                    break;
    }
    cpt--;
    }
    printf("Total : non trouvées = %d ; v1 trouvée = %d ; v2 trouvée = %d ; les deux trouvées = %d\n", nbl_aucun, nbl_v1, nbl_v2, nbl_v1_v2);

    return EXIT_SUCCESS;
}