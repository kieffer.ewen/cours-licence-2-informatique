#include "matrice.h"
#include "traiterLigne.h"
#include <stdio.h>
#include <time.h>

int main(){
	int matrice[NB_LIGNES_MAX][NB_COLONNES_MAX];
	unsigned nbLignes = NB_LIGNES_MAX;
	unsigned nbColonnes = NB_COLONNES_MAX;
	unsigned v1, v2;
	srand(NB_COLONNES_MAX);
	srand(time(NULL));
	v1 = rand() % NB_COLONNES_MAX;
	v2 = rand() % NB_COLONNES_MAX;
	int cptv1 = 0;
	int cptv2 = 0;
	int cptv1v2 = 0;
	int cpt0 = 0;
	printf("Valeur 1 : %d\n", v1);
	printf("Valeur 2 : %d\n", v2);
	initialiserMatrice(matrice, nbLignes, nbColonnes);
	afficherMatrice(matrice, nbLignes, nbColonnes);
	for(int i = 0; i < nbLignes; i ++){
		switch(traiterLigne(matrice[i], nbColonnes,v1, v2)) {
			case 3 :
			printf("Ligne %d: les deux valeurs ont été trouvées\n", i);
			cptv1v2 ++;
			break;
			case 2 :
			printf("Ligne %d: la deuxième valeur a été trouvée\n", i);
			cptv2 ++;
			break;
			case 1 :
			printf("Ligne %d: la premère valeur a été trouvée\n", i);
			cptv1 ++;
			break;
			default :
			printf("Ligne %d: Aucune des valeurs n'a été trouvée\n", i);
			cpt0 ++;
			break;
		}
	}
	printf("Total : non trouvées = %d ; v1 trouvée = %d ; v2 trouvée = %d ; les deux trouvées = %d\n", cpt0, cptv1,cptv2,cptv1v2);
	return 0;
}