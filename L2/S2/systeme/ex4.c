#define _POSIX_C_SOURCE 1
#define N 10
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char *argv[]){
	int retour;
	int successeur;
	for(int i = 0; i < 4; i ++){
	switch(fork()){

		case -1: //père
			printf("erreur\n");
			break;
		case 0: //fils
			if(i == 3){
				successeur = -3;
			}else {
				successeur = 1;
			}
			for(int j = 0; j < N; j ++)
				printf("processus fils de pid %d et de père %d et de successeur %d\n", getpid(), getppid(), getpid() + successeur);
			exit(0);
		default:
			printf("processus père %d\n", getpid());
			wait(&retour);
	}
}
	return 0;
}