#include <stdio.h>
#include "couche_transport.h"
#include "services_reseau.h"

/* ************************************** */
/* Fonctions utilitaires couche transport */
/* ************************************** */

/* Ajouter ici vos fonctions utilitaires
 * (generer_controle, verifier_controle...)
 */

uint8_t generer_controle(paquet_t data){
	uint8_t tmp = data.type^data.num_seq^data.lg_info;
	for(int i = 0; i < data.lg_info; i ++){
		tmp ^= data.info[i];
	}
	return tmp;
}

int verifier_controle(paquet_t data){
	if(data.somme_ctrl == generer_controle(data))
		return 1;
	return 0;
}

/* ======================================================================= */
/* =================== Fenêtre d'anticipation ============================ */
/* ======================================================================= */

/*--------------------------------------*/
/* Fonction d'inclusion dans la fenetre */
/*--------------------------------------*/
int dans_fenetre(unsigned int inf, unsigned int pointeur, int taille) {

    unsigned int sup = (inf+taille-1) % SEQ_NUM_SIZE;

    return
        /* inf <= pointeur <= sup */
        ( inf <= sup && pointeur >= inf && pointeur <= sup ) ||
        /* sup < inf <= pointeur */
        ( sup < inf && pointeur >= inf) ||
        /* pointeur <= sup < inf */
        ( sup < inf && pointeur <= sup);
}
