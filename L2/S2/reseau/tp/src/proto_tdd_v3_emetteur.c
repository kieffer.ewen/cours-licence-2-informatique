/*************************************************************
* proto_tdd_v2 -  émetteur                                   *
* TRANSFERT DE DONNEES  v3                                   *
*                                                            *
* Protocole sans contrôle de flux, sans reprise sur erreurs  *
*                                                            *
* E. Lavinal - Univ. de Toulouse III - Paul Sabatier         *
**************************************************************/

#include <stdio.h>
#include "application.h"
#include "couche_transport.h"
#include "services_reseau.h"

/* =============================== */
/* Programme principal - émetteur  */
/* =============================== */


int main(int argc, char* argv[])
{
    unsigned char message[MAX_INFO]; /* message de l'application */
    int taille_msg; /* taille du message */
    paquet_t paquet[atoi(argv[1])]; /*Fenetre d'emission*/
    paquet_t retour; //paquet reçu
    
    init_reseau(EMISSION);


    printf("[TRP] Initialisation reseau : OK.\n");
    printf("[TRP] Debut execution protocole transport.\n");

    /* lecture de donnees provenant de la couche application */
    for(int i = O; i < atoi(argv[1]); i ++){
        de_application(message, &taille_msg);
        while( taille_msg != 0) {
            for(int j = 0; j < taille_msg; j++){
            paquet[i].info[j] = message[j];
        }
        paquet[i].lg_info = taille_msg;
        paquet[i].type = DATA;
        paquet.somme_ctrl = generer_controle(paquet);
        paquet.num_seq = i;
        }
    /* tant que l'émetteur a des données à envoyer */
    while ( taille_msg != 0 ) {

        /* construction paquet */
        for (int i=0; i<taille_msg; i++) {
            paquet.info[i] = message[i];
        }
        paquet.lg_info = taille_msg;
        paquet.type = DATA;
        paquet.somme_ctrl = generer_controle(paquet);
        /* remise à la couche reseau */
        do{
        depart_temporisateur(0, 2000);
        vers_reseau(&paquet);
        }while(attendre() != -1);
        de_reseau(&retour);
        if(retour.type == ACK)
            arreter_temporisateur(0);

        /* lecture des donnees suivantes de la couche application */
        de_application(message, &taille_msg);
    }

    printf("[TRP] Fin execution protocole transfert de donnees (TDD).\n");
    return 0;
}
