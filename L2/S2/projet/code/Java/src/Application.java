import java.util.List;
import java.awt.desktop.SystemEventListener;
import java.util.Date;

public class Application {

	public static void main(String args[]) {
		Date d = new Date();
		Block block = new Block(0, Integer.toString(d.getDate()), null, 0, null, null);
		block.mine(5);
		System.out.println(block.getHash());
		System.out.println(block.getNonce());
	}
}
