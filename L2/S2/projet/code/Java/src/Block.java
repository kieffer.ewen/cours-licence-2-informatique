import java.util.List;

public class Block {

	private int index;
	private String timestamp;
	private String previewHash;
	private int nbTransactions;
	private List<Transaction> transactions;
	private String hashRoot;
	private String hash;
	private int nonce;

	public Block(int index, String timestamp, String previewHash, int nbTransactions, List<Transaction> transactions,
			String hashRoot) {
		super();
		this.index = index;
		this.timestamp = timestamp;
		this.previewHash = previewHash;
		this.nbTransactions = nbTransactions;
		this.transactions = transactions;
		this.hashRoot = hashRoot;
		this.nonce = 0;
		this.hash = Integer.toString(this.hashCode());
	}

	public void mine(int difficulty) {
		this.hash = HashUtil.applySha256(this.toString());
		StringBuffer s = new StringBuffer();
		for (int i = 0; i < difficulty; i++)
			s.append(0);
		// créer une chaine de difficulty '0'
		// difficulty = 4 -> '0000'
		while (!this.hash.startsWith(s.toString())) {
			this.nonce ++;
			this.hash = HashUtil.applySha256(this.toString());
		}
	}

	public int getNonce() {
		return this.nonce;
	}
	public String getHash() {
		return this.hash;
	}
	
	@Override
	public String toString() {
		return "Block [index=" + index + ", timestamp=" + timestamp + ", previewHash=" + previewHash
				+ ", nbTransactions=" + nbTransactions + ", transactions=" + transactions + ", hashRoot=" + hashRoot
				+ ", hash=" + hash + ", nonce=" + nonce + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hash == null) ? 0 : hash.hashCode());
		result = prime * result + ((hashRoot == null) ? 0 : hashRoot.hashCode());
		result = prime * result + index;
		result = prime * result + nbTransactions;
		result = prime * result + nonce;
		result = prime * result + ((previewHash == null) ? 0 : previewHash.hashCode());
		result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
		result = prime * result + ((transactions == null) ? 0 : transactions.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Block other = (Block) obj;
		if (hash == null) {
			if (other.hash != null)
				return false;
		} else if (!hash.equals(other.hash))
			return false;
		if (hashRoot == null) {
			if (other.hashRoot != null)
				return false;
		} else if (!hashRoot.equals(other.hashRoot))
			return false;
		if (index != other.index)
			return false;
		if (nbTransactions != other.nbTransactions)
			return false;
		if (nonce != other.nonce)
			return false;
		if (previewHash == null) {
			if (other.previewHash != null)
				return false;
		} else if (!previewHash.equals(other.previewHash))
			return false;
		if (timestamp == null) {
			if (other.timestamp != null)
				return false;
		} else if (!timestamp.equals(other.timestamp))
			return false;
		if (transactions == null) {
			if (other.transactions != null)
				return false;
		} else if (!transactions.equals(other.transactions))
			return false;
		return true;
	}


}
