#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//Le code compile normalement

long puissance_mod_n (long a, long e, long n) {
  /// /brief puissance modulaire, calcule a^e mod n
  /// a*a peut dépasser la capacité d'un long
  // https://www.labri.fr/perso/betrema/deug/poly/exp-rapide.html
  long p;
  for (p = 1; e > 0; e = e / 2) {
    if (e % 2 != 0)
      p = (p * a) % n;
    a = (a * a) % n;
  }
  return p;
}

int proto_diffie_Helman(int nbAlice, int nbBob) 
{
    int p;
    int g;
    printf("choisissez p : ");
    scanf("%d", &p);
    printf("choisissez g : ");
    scanf("%d", &g);
    printf("Alice envoie une valeur à Bob...\n");
    long res_msgA = puissance_mod_n(g, nbAlice, p);
    printf("Bob envoie une valeur à Alice...\n");
    long res_msgB = puissance_mod_n(g, nbBob, p);
    printf("calcul des clés...\n");
    long cleA = puissance_mod_n(res_msgB, nbAlice, p);
    long cleB = puissance_mod_n(res_msgA, nbBob, p);
    if(cleA == cleB)
    {
        printf("Clé a : %ld\tclé b: %ld\nÉchange Validé\n", cleA, cleB);   
        return cleA;
    }
    else 
    {
        fprintf(stderr, "Erreur dans l'échange des clés, elles ne sont pas égales", NULL);
        return 1;
    }
    return 0;
}

char* chiffrageXor(const char* msg, const char* key)
{
    int taille_msg = strlen(msg);
    int taille_cle = strlen(key);
    char* buff = (char*)malloc((taille_msg+1)*sizeof(char));
    for(int i = 0; i <taille_msg; i++)
    {
        buff[i] = msg[i] ^ key[i%taille_cle];
    }

    return buff;
}

int main(void)
{
    //Ex1
    //int cle = proto_diffie_Helman(6, 15);
    //printf("le retour de la clé est %d\n", cle);
    //Ex2
    char* phrase = "Les carottes sont cuites"; 
    char* retour;
    retour = chiffrageXor(phrase, "rutabaga");
    for(int i=0; i<strlen(phrase); ++i)
    {
         printf(" %c  ", phrase[i]);
    }
    printf("\n");
    for(int i=0; i<strlen(phrase); ++i)
    {
         printf(" %02x ", retour[i]);
    }
    printf("\n");
    free(retour);
    ////////////////////////////////////////////////
    return 0;
}