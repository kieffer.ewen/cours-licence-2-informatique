
#include "rsa_header.h"
const int batch_length = 4;
FILE *logfp;

int main(){
  logfp = stdout;
  
  rsaKey_t privKey = {4279,5141};
  rsaKey_t pubKey = {7,5141};

  int nbCar;
  printf("Chiffement du message...\n");
  RSAcryptFile("msg.txt","res.txt",pubKey,&nbCar);
  printf("Fini, %d caractères lus à la dernière opération\n",nbCar);
  printf("Déchiffement du message...\n");
  RSAunCryptFile("res.txt","msg_decrypt.txt",privKey,nbCar);
  printf("Fini, %d caractères lus à la dernière opération\n",nbCar);
  return 0;
}