//#include "rsa_header.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <malloc.h>

#define COMMAND_STR_SIZE 20
#define MAX_STR_SIZE 255
#define ERROR_OPEN_DIR 1


void list_files(char* dir_param, char* opt_param)
{

    char* dir;
    int n;
    DIR* ls_dir;
    struct dirent** ls_struct;
    if(strcmp(dir_param, "") == 0)
    {
        dir = (char*)malloc(3*sizeof(char));
        strcpy(dir, "./");
    }else
    {
            strcpy(dir, dir_param);
    }

    printf("DIR : %s\n", dir);
    
    if((ls_dir = opendir(dir))==NULL)
    {
        fprintf(stderr, "ERROR OCCURING WHILE TRYING TO OPEN %s DIRECTORY.", dir);
        exit(ERROR_OPEN_DIR);
    }
    n = scandir(dir, &ls_struct, NULL, alphasort);
    if (n == -1) {
        perror("scandir");
        exit(EXIT_FAILURE);
    }

    int i = 0;
    while(i<n)
    {
        printf("%s\n", ls_struct[i]->d_name);
        free(ls_struct[i]);
        i++;
    }

    free(ls_struct);
    free(dir);
    closedir(ls_dir);
}

int main(void){
printf("########## COMMAND INTERPRETER ############\n");

while(1)
{ 

    char cmd[COMMAND_STR_SIZE];
    char list_arg[12][255];
    char* pch;
    char* arg;
    char ligneDeCommande[MAX_STR_SIZE];
    printf("$: ");
    fgets(ligneDeCommande, MAX_STR_SIZE, stdin);
    printf("%s\n", ligneDeCommande);
      sscanf (ligneDeCommande,"%s %[^\n]",cmd,arg);
    if(strcmp("quit", cmd)==0)
    {
        printf("Closing the command interpreter...\n\n");
        return 0;
    }   
    int i = 0;
    pch = strtok (arg," ,.-");
    while (pch != NULL)
    {
        strcpy(list_arg[i], pch);
        printf ("%s\n",pch);
        pch = strtok (NULL, " ,.-");
        i++;
    }
    for(int i = 0; i < 10; i++)
    {
        printf("list_arg[%d] = %s\n", i, list_arg[i]);
    }
}
return 0;

}