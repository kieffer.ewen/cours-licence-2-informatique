
#include "rsa_header.h"
const int batch_length = 4;
FILE *logfp;

int main(){
  logfp = stdout;
  
    rsaKey_t pubKeyb = {23,8889895013};
  rsaKey_t privKeyb = {3865086887,8889895013};

  int nbCar;
 printf("Chiffrement du message...\n");
 RSAfile_crypt("msg.txt","res.txt",pubKeyb);
  printf("Déchiffrement du message...\n");
  RSAfile_decrypt("res.txt","res2.txt",privKeyb);
  return 0;
}