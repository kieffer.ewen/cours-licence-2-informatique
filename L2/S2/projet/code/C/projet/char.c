
#include "rsa_header.h"
#include "string.h"
#include <fcntl.h>
#include <unistd.h>
//Pour Compiler : gcc bezout.c rsa_tools.c crypt_decrypt_rsa.c char.c test_base64.c int2char.c other_base64.c -o main

void  RSAcryptFile(char  *inFilename,  char  *outFilename,  rsaKey_t  pubKey, int* output_length)
{

	FILE* inFile = NULL;
	FILE* outFile = NULL;
    char buffer;
	inFile = fopen(inFilename, "r");
	outFile = fopen(outFilename, "w+ "); 

	if(inFile == NULL || outFile == NULL)
	{
		exit(-1);
	}
    while(!feof(inFile))
    {
        buffer = fgetc(inFile);
        if(buffer == EOF)
            break;
        uint64 cryptedChar = puissance_mod_n(buffer, pubKey.E, pubKey.N);
        char* strB64 = base64_encode(&cryptedChar, sizeof(uint64), output_length);
        fprintf(outFile, strB64);
        free(strB64);
    }
		fclose(inFile);
		fclose(outFile);
}

void RSAunCryptFile(char *inFilename,char *outFilename,rsaKey_t privKey,int length)
{
    FILE* inFile = NULL;
    FILE* outFile = NULL;
    int* output_length;
    char *blockB64 = malloc(length*sizeof(char));
    
    inFile = fopen(inFilename, "r");
	outFile = fopen(outFilename, "w+"); 

	if(inFile == NULL || outFile == NULL)
	{
		exit(-1);
	}
    while(!feof(inFile))
    {
        fread(blockB64, sizeof(char), length, inFile);
        if(strcmp(blockB64, "")!=0){
        uint64* intBlock = base64_decode(blockB64,length, &output_length);
        uint64 cUnCrypt = puissance_mod_n(*intBlock,privKey.E,privKey.N);
        fprintf(outFile, "%c", cUnCrypt);
        strcpy(blockB64, "\0");
        free(intBlock);
        }
    }
    free(blockB64);
    free(output_length);
    fclose(inFile);
    fclose(outFile);
}
