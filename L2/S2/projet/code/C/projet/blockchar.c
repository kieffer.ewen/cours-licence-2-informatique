#include "string.h"
#include <fcntl.h>
#include <unistd.h>
#include "rsa_header.h"

uint64 RSAcrypt1BlockGmp(uint64 blockInt, rsaKey_t pubKey)
{
  mpz_t calc;
  mpz_init (calc);
  puissance_mod_n_gmp(calc,blockInt, pubKey.E, pubKey.N);
  uint tmp = mpz_get_ui(calc);
  return tmp;
}

uint64 RSAdecrypt1BlockGmp(uint64 blockInt, rsaKey_t privKey)
{
  mpz_t calc;
  mpz_init (calc);
  puissance_mod_n_gmp(calc,blockInt, privKey.E, privKey.N);
  uint64 tmp = mpz_get_ui(calc);
  return tmp;
}


void pdata(const unsigned char* data)
{
  printf("DATA VAUT : %d\n", *data);
}

void RSAfile_crypt(char* inFilename, char* outFilename, rsaKey_t pubKey)
{
  FILE* inFile = NULL;
	FILE* outFile = NULL;
	inFile = fopen(inFilename, "r");
	outFile = fopen(outFilename, "w+ "); 
	if(inFile == NULL || outFile == NULL)
	{
		exit(-1);
	}
    while(!feof(inFile))
    {
      block_t blockB64;
      fread(blockB64, sizeof(uchar), 4, inFile);
      printf("Block : %s\n", blockB64);
      uint64 convB64 = convert_4byte2int(blockB64);
      uint64 rsaB64 = RSAcrypt1BlockGmp(convB64, pubKey);
      printf("ENCODE : %ld\n", convB64);
      size_t output_length;
      output_length = BLOCK_SIZE;
      char* strB64 = base64_encode(&rsaB64, sizeof(rsaB64), &output_length);
      printf("OL : %d\n", output_length);
      fprintf(outFile, strB64);
      free(strB64);
   }
    fclose(inFile);
   fclose(outFile);
}

void RSAfile_decrypt(char* filename, char* outFilename, rsaKey_t privKey)
{
  FILE* inFile = NULL;
  FILE* outFile = NULL;
  int output_length;
  output_length = BLOCK_SIZE;
  inFile = fopen(filename, "r");
	outFile = fopen(outFilename, "w+"); 
  char* blockB64 = malloc(12*sizeof(char));
	
  if(inFile == NULL || outFile == NULL)
	{
		exit(-1);
	}
    while(!feof(inFile))
    {
      fread(blockB64, sizeof(char), 12, inFile);
      if(strcmp(blockB64, "")!=0){
        uint64* intBlock = base64_decode(blockB64,12, &output_length);
        uint64 cUnCrypt = RSAdecrypt1BlockGmp(*intBlock, privKey);
        //block_t* block = malloc(sizeof(block_t))
        uchar* tab4bytes = malloc(5*sizeof(uchar));
        convertInt2uchar(cUnCrypt,tab4bytes);
        printf("0 => %c 1 => %c 2 => %c 3 => %c\n", tab4bytes[0], tab4bytes[1],tab4bytes[2], tab4bytes[3]);
        free(intBlock);
        free(tab4bytes);
        strcpy(blockB64, "");
      }
    }
    free(blockB64);
    fclose(inFile);
    fclose(outFile);
}