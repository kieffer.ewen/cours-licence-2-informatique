#define ELEM int;

type_def struct queue_t marabout;

void createQueue(marabout* q);

void push(marabout* q, ELEM e);

int top(marabout* q);

int isEmpty(const marabout* q);

void pop(marabout* q);

