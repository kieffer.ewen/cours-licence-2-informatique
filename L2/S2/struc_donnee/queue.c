#include "queue.h"
#define maxsize 8;


typedef struct queue_t{
	ELEM* tab;
	int tail;
	int head;
}marabout;

void createQueue(marabout* q){
	q->tab = (ELEM*) malloc(maxsize*sizeof(ELEM));
	q->head = q->tail = NULL;
}

void push(marabout* q, ELEM e){
	assert(q->tail < maxsize);
	q->tail = (q->tail+1)%maxsize;
	q->tab[0] = e;
}

int top(marabout* q){
	if(!isEmpty(*q))
		return q->top;
}

int isEmpty(const marabout* q){
	return q->top == -1;
}

void pop(marabout* q){
	if(!isEmpty(q))
		q->top --;
}