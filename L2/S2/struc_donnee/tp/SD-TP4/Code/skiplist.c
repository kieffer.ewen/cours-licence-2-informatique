#include <stdlib.h>
#include <assert.h>

#include "skiplist.h"
#include "rng.h"


struct s_node {
	int value;
	int level;
	struct s_node **previous;
	struct s_node **next;
};

 struct s_SkipList{
	struct s_node *sentinel;
	int size;
	int nblevel;
	RNG rng_level;
};


SkipList skiplist_create(int nblevels) {
	SkipList s = malloc(sizeof(SkipList));
	s->sentinel = malloc(sizeof(Node));
	s->sentinel->level = 1;
	s->size = 0;
	s->nblevel = nblevels;
	s->rng_level = rng_initialize(0);
	for(int i = 0; i < nblevels; i ++){
		s->sentinel->next[i] = s->sentinel->previous[i] = s->sentinel;
	}
	return s;
}

void skiplist_delete(SkipList d) {
	Node *itr = d->sentinel->next[0];
	while(d->sentinel->next[0] != d->sentinel->previous[0]){
		itr->previous[0] = itr;
		itr = itr->next[0];
		d->sentinel->next[0] = itr;
		free(itr->previous[0]);
	}
	free(d->sentinel);
	free(itr);
	free(d);
	}

SkipList skiplist_insert(SkipList d, int value) {
	Node *itr;
	Node *e = malloc(sizeof(Node));
	int trouve;
	e->level = rng_get_value(&(d->rng_level), d->nblevel);
	e->value = value;
	if(d->size == 0){
		for(int i = 0; i < e->level; i ++){
			e->next[i] = d->sentinel;
			e->previous[i] = d->sentinel;
			d->sentinel->next[i] = e;
			d->sentinel->previous[i] = e;
		}
	}else {
		for(int i = 0; i < e->level; i ++){
			itr = d->sentinel->next[i];
			trouve = 1;
			while(trouve){
				if(itr->level < e->level){
					itr = itr->next[i];
				}else{
					 if(value < itr->value){
						itr = itr->next[i];
					 }else{
						trouve = 0;
						e->next[i] = itr;
						e->previous[i] = itr->previous[i];
						e->next[i]->previous[i] = e;
						e->previous[i]->next[i] = e;
					}
				}
			}				
		}
	}
	return d;
}
unsigned int skiplist_size(SkipList d){
	return d->size;
}

int skiplist_ith(SkipList d, unsigned int i){
	assert(d->size != 0);
	Node *itr = d->sentinel->next[0];
	while (i--) itr = itr->next[0];
	return itr->value;
}
void skiplist_map(SkipList d, ScanOperator f, void *user_data){
	for(Node *e = d->sentinel->next[0]; e != d->sentinel; e = e->next[0]){
		f(e->value, user_data);
	}

}