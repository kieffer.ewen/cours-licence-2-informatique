/*-----------------------------------------------------------------*/
/*
 Licence Informatique - Structures de données
 Mathias Paulin (Mathias.Paulin@irit.fr)
 
 Implantation du TAD List vu en cours.
 */
/*-----------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "list.h"

typedef struct s_LinkedElement {
	int value;
	struct s_LinkedElement *previous;
	struct s_LinkedElement *next;
} LinkedElement;

/* Use of a sentinel for implementing the list :
 The sentinel is a LinkedElement * whose next pointer refer always to the head of the list and previous pointer to the tail of the list
 */
struct s_List {
	LinkedElement *sentinel;
	int size;
};

struct s_SubList {
	LinkedElement *head;
	LinkedElement *tail;
	int size;
};

/*-----------------------------------------------------------------*/

List *list_create() {
	List *list = malloc(sizeof(struct s_List));
	list->sentinel = malloc(sizeof(LinkedElement));
	list->size = 0;
	list->sentinel->next = list->sentinel->previous = list->sentinel;
	return list;
}

/*-----------------------------------------------------------------*/

List *list_push_back(List *list, int v) {
	LinkedElement *e = malloc(sizeof(LinkedElement));
	e->value = v;
	e->next = list->sentinel;
	e->previous = list->sentinel->previous;
	e->previous->next = e;
	list->sentinel->previous = e;
	(list->size)++;
	return list;
}

SubList list_split(SubList *l){
	LinkedElement *itr = l->head->previous;
	SubList *tmp = malloc(sizeof(SubList));
	for(int i = 0; i < l->size/2; i ++)
		itr = itr->next;
	tmp->head = itr;
	tmp->tail = itr->next;
	return *tmp;
}
SubList list_merge(SubList *leftlist, SubList *rightlist,OrderFunctor f){
	SubList *l = malloc(sizeof(SubList));
	if(f(leftlist->head->value,rightlist->head->value)){
		l->head = leftlist->head;
		l->tail = rightlist->tail;
	}else{
		l->head = rightlist->tail;
		l->tail = leftlist->head;
	}
	return *l;
}
/*-----------------------------------------------------------------*/

void list_delete(ptrList *list) {
	LinkedElement *itr = (*list)->sentinel->next;
	while((*list)->sentinel->next != (*list)->sentinel->previous){
		itr->previous = itr;
		itr = itr->next;
		(*list)->sentinel->next = itr;
		free(itr->previous);
	}
	free((*list)->sentinel);
	free(itr);
	free(*list);
	*list=NULL;
}

/*-----------------------------------------------------------------*/

List *list_push_front(List *list, int v) {
	LinkedElement *e = malloc(sizeof(LinkedElement));
	e->value = v;
	e->previous = list->sentinel;
	e->next = list->sentinel->next;
	e->next->previous = e;
	list->sentinel->next = e;
	(list->size)++;
	return list;
}

/*-----------------------------------------------------------------*/

int list_front(List *list) {
	assert(list->size != 0);
	return list->sentinel->previous->value;

}

/*-----------------------------------------------------------------*/

int list_back(List *list) {
	assert(list->size != 0);
	return list->sentinel->next->value;
}

/*-----------------------------------------------------------------*/


List *list_pop_front(List *list) {
	LinkedElement *e;
	e = list->sentinel->next;
	list->sentinel->next = e->next;
	list->sentinel->next->previous = list->sentinel; 
	(list->size)--;
	free(e);
	return list;
}

/*-----------------------------------------------------------------*/

List *list_pop_back(List *list){
	LinkedElement *e;
	e = list->sentinel->previous;
	list->sentinel->previous = e->previous;
	list->sentinel->previous->next = list->sentinel;
	(list->size)--;
	free(e);
	return list; 
}

/*-----------------------------------------------------------------*/

List *list_insert_at(List *list, int p, int v) {
	assert(list->size != 0 && p < list->size);
	LinkedElement *e = malloc(sizeof(LinkedElement));
	e->value = v;
	LinkedElement *itr = list->sentinel->next;
	while(p--) itr = itr->next;
	e->next = itr;
	e->previous =itr->previous;
	e->next->previous = e;
	e->previous->next = e;
	++(list->size);
	return list;
}

/*-----------------------------------------------------------------*/

List *list_remove_at(List *list, int p) {
	assert(list->size != 0 && p < list->size);
	LinkedElement *itr = list->sentinel->next;
	while(p--) itr = itr->next;
	itr->previous->next = itr->next;
	itr->next->previous = itr->previous;
	free(itr);
	--(list->size);
	return list;
}

/*-----------------------------------------------------------------*/

int list_at(List *list, int p) {
	assert(list->size != 0);
	LinkedElement *itr = list->sentinel->next;
	while(p--) itr = itr->next;
	return itr->value;
}

/*-----------------------------------------------------------------*/

bool list_is_empty(List *list) {
	return list->size == 0;
}

/*-----------------------------------------------------------------*/

int list_size(List *list) {
	return list->size;
}

/*-----------------------------------------------------------------*/

List * list_map(List *list, SimpleFunctor f) {
	for(LinkedElement *itr = list->sentinel->next; itr!= list->sentinel; itr = itr->next)
		itr->value = f(itr->value);	
	return list;
}


List *list_reduce(List *list, ReduceFunctor f, void *userData) {
	for(LinkedElement *e = list->sentinel->next; e != list->sentinel; e = e->next){
		f(e->value, userData);
	}
	return list;
}

/*-----------------------------------------------------------------*/

List *list_sort(List *l, OrderFunctor f) {
	(void)f;
	return l;
}

