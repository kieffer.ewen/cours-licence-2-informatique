#include <stdio.h>
#include "couche_transport.h"
#include "services_reseau.h"

/* ************************************** */
/* Fonctions utilitaires couche transport */
/* ************************************** */

/* Ajouter ici vos fonctions utilitaires
 * (generer_controle, verifier_controle...)
 */

/*

typedef struct paquet_s {
    uint8_t type;        
    uint8_t num_seq;     
    uint8_t lg_info;     
    uint8_t somme_ctrl;   
    unsigned char info[MAX_INFO];
} paquet_t;
*/

uint8_t generer_controle(paquet_t paquet)
{
	uint8_t checksum;
	checksum = paquet.type^paquet.num_seq^paquet.lg_info;
	for(int i = 0; i<paquet.lg_info;i++)
	{
		checksum ^= paquet.info[i];
	}
	return checksum;
}
int verifier_controle(uint8_t checksum, paquet_t paquet)
{
	uint8_t tmp = generer_controle(paquet);
	if(checksum == tmp)
		return 1;
	return 0;

}


/* ======================================================================= */
/* =================== Fenêtre d'anticipation ============================ */
/* ======================================================================= */

/*--------------------------------------*/
/* Fonction d'inclusion dans la fenetre */
/*--------------------------------------*/
int dans_fenetre(unsigned int inf, unsigned int pointeur, int taille) {

    unsigned int sup = (inf+taille-1) % SEQ_NUM_SIZE;

    return
        /* inf <= pointeur <= sup */
        ( inf <= sup && pointeur >= inf && pointeur <= sup ) ||
        /* sup < inf <= pointeur */
        ( sup < inf && pointeur >= inf) ||
        /* pointeur <= sup < inf */
        ( sup < inf && pointeur <= sup);
}
