/*************************************************************
* proto_tdd_v3 -  récepteur                                  *
* TRANSFERT DE DONNEES  v3                                   *
*                                                            *
* Protocole sans contrôle de flux, sans reprise sur erreurs  *
*                                                            *
* E. Lavinal - Univ. de Toulouse III - Paul Sabatier         *
**************************************************************/

#include <stdio.h>
#include "application.h"
#include "couche_transport.h"
#include "services_reseau.h"

/* =============================== */
/* Programme principal - récepteur */
/* =============================== */
int main(int argc, char* argv[])
{
    unsigned char message[MAX_INFO]; /* message pour l'application */
    paquet_t paquet; /* paquet utilisé par le protocole */
    paquet_t retour;
    paquet_t tmp;
    int curseur = 0;
    int fin = 0; /* condition d'arrêt */

    init_reseau(RECEPTION);

    printf("[TRP] Initialisation reseau : OK.\n");
    printf("[TRP] Debut execution protocole transport.\n");

    /* tant que le récepteur reçoit des données */
    while (!fin)
    {
        // attendre(); /* optionnel ici car de_reseau() fct bloquante */
        de_reseau(&paquet);
        depart_temporisateur(2,3000);
        while(!verifier_controle(paquet.somme_ctrl, paquet)){
            attendre();
            depart_temporisateur(2,3000);    
            de_reseau(&paquet);
        }
        arreter_temporisateur(2);
        if(dans_fenetre(curseur,paquet.num_seq, 1))
        {
            tmp = paquet;
            retour.num_seq = curseur;
            retour.type = ACK;
            curseur++;
            for (int i=0; i<paquet.lg_info; i++)
            {
                    message[i] = paquet.info[i];
            }
            /* remise des données à la couche application */
            fin = vers_application(message, paquet.lg_info);  
        }else
        {
            retour.num_seq = curseur;
            retour.type = ACK;
        }
        vers_reseau(&retour);
    }
    printf("[TRP] Fin execution protocole transport.\n");
    return 0;
}