/*************************************************************
* proto_tdd_v3 -  émetteur                                   *
* TRANSFERT DE DONNEES  v3                                   *
*                                                            *
* Protocole sans contrôle de flux, sans reprise sur erreurs  *
*                                                            *
* E. Lavinal - Univ. de Toulouse III - Paul Sabatier         *
**************************************************************/

#include <stdio.h>
#include "application.h"
#include "couche_transport.h"
#include "services_reseau.h"

/* =============================== */
/* Programme principal - émetteur  */
/* =============================== */


int main(int argc, char* argv[])
{
    unsigned char message[MAX_INFO]; /* message de l'application */
    paquet_t tab_p[8]; /*Fenetre d'emission*/
    paquet_t pack; //paquet reçu
    int borne_inf = 0;
    int curseur = 0;
    int taille = 4;
    int taille_message;
    init_reseau(EMISSION);
    printf("[TRP] Initialisation reseau : OK.\n");
    printf("[TRP] Debut execution protocole transport.\n");
    while(1)
    {
        if(dans_fenetre(borne_inf, curseur, taille))
        {
            de_application(message, &taille_message);
            tab_p[curseur].num_seq = curseur;
            tab_p[curseur].type = DATA;
            for (int i=0; i<taille_message; i++)
            {
                tab_p[curseur].info[i] = message[i];
            }
            tab_p[curseur].somme_ctrl = generer_controle(tab_p[curseur]);
            vers_reseau(&tab_p[curseur]);
        
            if(curseur == borne_inf)
                depart_temporisateur(0, 3000);
            curseur++;
        }
        else
        {
            int evt = attendre();
            if(evt == PAQUET_RECU)
            {
                de_reseau(&pack);
                if(verifier_controle(pack.somme_ctrl, pack) &&  dans_fenetre(borne_inf,pack.num_seq, taille))
                {
                    int tmp = pack.num_seq + 1;
                    borne_inf = tmp;
                    arreter_temporisateur(0);
                    if(borne_inf != curseur)
                        depart_temporisateur(0,3000);

                }
                else
                {
                    int i = borne_inf;
                    while(i != curseur)
                    {
                        vers_reseau(&tab_p[i]);
                        i++;
                    }
                }
            }
        }
    }
    printf("[TRP] Fin execution protocole transfert de donnees (TDD).\n");
    return 0;
}
