le nombre place à un cours ne peut dépasser 10

DELETE from materiel where codem = 3;

DELETE FROM suivre WHERE codec = 3 AND noa = 1;


DELETE FROM cours WHERE codec = 2;
-- Une erreur est déclencé pour ne pas rompre le lien avec la table suivre ou un enregistrement avec ce cours est présent.
-- ON FAIT DONC:
DELETE FROM suivre WHERE codec = 2;
DELETE FROM cours WHERE codec = 2;
-- Et la ça fonctionne.

UPDATE adherent SET adressea = '23 route de Toulouse, Saint-Gaudens' WHERE prenoma = 'Julien' AND noma = 'Lars';

UPDATE materiel SET prix = prix*1.1;

update materiel set qtedispo = qtedispo+2 where materiel.type = 'Ski';

-- PARTIE 2
SELECT table_name from ALL_TABLES WHERE OWNER='GILLES_HUBERT' and table_name like 'TP2_%';
--debut
SET ServerOutput ON;
Create or Replace Procedure selectfromtable (pParam Varchar2)  as
 TYPE         t_Char IS TABLE OF Varchar2(128);
 TYPE         t_Num IS TABLE OF Number;
 l_Name        t_char;
Begin
 
SELECT table_name BULK COLLECT INTO l_Name from ALL_TABLES WHERE OWNER='GILLES_HUBERT' and table_name like 'TP2_%';
 
 if l_Name.FIRST is not null then
  FOR i IN l_Name.FIRST..l_Name.LAST Loop 
    dbms_output.Put_line ('Table Name = '||l_Name(i));
    select * from GILLES_HUBERT.l_Name;
  End LOOP;
 End If;
END; 
 --fin
