
#include "matrice.h"
#include "traiterLigne.h"

/* retourne 0: aucun trouve, 1: V1 trouve, 2: v2 trouve et 3: v1 et v2 trouve */
unsigned traiterLigne(  int uneLigne[NB_COLONNES_MAX], unsigned c,
			unsigned v1, unsigned v2)
{
	int bv1 = 0;
	int bv2 = 0;
	for(int i = 0;i<c;i++)
	{
		if(uneLigne[i] == v1)
			bv1=1;
		if(uneLigne[i] == v2)
			bv2=1;
	}
	if(bv1==1 && bv2 == 1)
		return 3;
	else if(bv1==0 && bv2 == 1)
		return 2;
	else if(bv1==1 && bv2 == 0)
		return 1;
	else if(bv1==0 && bv2 == 0)
		return 0;
			
}
