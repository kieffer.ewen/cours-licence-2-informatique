#define _POSIX_C_SOURCE 1
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>	
#include "matrice.h"
#include "traiterLigne.h"
#include <time.h>
//VERSION 1

int main(int argc, char** argv, char** envp)
{
	int nbLig = NB_LIGNES_MAX;
	int nbCol = NB_COLONNES_MAX;
	int m[NB_LIGNES_MAX][NB_COLONNES_MAX];
	initialiserMatrice(m,nbLig,nbCol);
	afficherMatrice(m, nbLig,nbCol);
	int nbl_aucun = 0;
	int nbl_v1 = 0; 
	int nbl_v2 = 0; 
	int nbl_v1_v2 = 0;
	srand(time(NULL));
	int v1 = rand() % NB_COLONNES_MAX;
	int v2 = rand() % NB_COLONNES_MAX;
	printf("v1 : %d\t v2 : %d\n", v1, v2);
 	for(int i = 0; i<nbLig;i++)
	{
		int r = traiterLigne(m[i], nbCol, v1, v2);
		switch(r)
		{
			case 0 :
				nbl_aucun++;
				printf("Ligne %d : Non trouvé\n", i);
				break;
			case 1 :
				nbl_v1++;
				printf("Ligne %d : v1 trouvé\n", i);
				break;
			case 2 :
				nbl_v2++;
				printf("Ligne %d : v2 trouvé\n", i);
				break;
			case 3 :
				nbl_v1_v2++;
				printf("Ligne %d : v1 et v2 trouvés\n", i);
				break;
		}
	}
	printf("#### BILAN ####\n");
	printf("nbl_aucun = %d\tnbl_v1 = %d\tnbl_v2 = %d\tnbl_v1_v2 = %d\n", nbl_aucun, nbl_v1, nbl_v2, nbl_v1_v2);
	return 0;
}
/* ### RÉSULTATS ####
0:  1  3  0  1
1:  1  1  2  3
2:  3  1  0  0
3:  0  0  0  2
Ligne 0 : v1 trouvé
Ligne 1 : v1 et v2 trouvés
Ligne 2 : v1 trouvé
Ligne 3 : v2 trouvé
#### BILAN ####
nbl_aucun = 0   nbl_v1 = 2      nbl_v2 = 1      nbl_v1_v2 = 1
*/
