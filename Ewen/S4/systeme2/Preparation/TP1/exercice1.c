#define _POSIX_C_SOURCE 1

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

//Definition des constantes
#define EXIT_OK 0
#define EXIT_CHILD_OK 5
#define EXIT_FAIL -1

typedef struct process_info
{
	pid_t pid;
	pid_t ppid;
	pid_t pgrp;
	char* login;
	uid_t uid;
	gid_t gid;
	struct process_info* parent;
}process_info;

typedef struct process_info* ptrproc_info;

void create_processInfo(ptrproc_info p)
{
	p->pid = getpid();
	p->ppid = getppid();
	p->pgrp = getpgrp();
	p->login = getlogin();
	p->uid = getuid();
	p->gid = getgid();
}

void getInformation(process_info p)
{
	printf("INFO :\t[%d]\n\tPropriétaire : %s\n\tGID : %d\n\tPGRP : %d\n\tPPID : %d\n",p.pid, p.login, p.gid, p.pgrp, p.ppid);
}

/*int main(void)
{
	process_info p;
	create_processInfo(&p);
	getInformation(p);
	return EXIT_OK;
}*/

int main(int argc, char** argv, char** envp)
{
	if(argc < 2)
	{
		fprintf(stderr, "Usage :\t %s <Nombre_de_Processus>\n", argv[0]);
		return EXIT_FAIL;
	}
	pid_t pid;
	int nb = atoi(argv[1]);
	int circonstance;
	process_info p[nb];
	int cpt = nb;
	create_processInfo(&p[0]);
	for(int i = 1; i<nb;i++)
	{
	switch(pid = fork())
	{
		case -1 :
				fprintf(stderr, "Erreur lors de la creation du processus voir information suivante");
				getInformation(p[0]);
				exit(EXIT_FAIL);
				break;
		case 0 :
			printf("[FILS] ");
			create_processInfo(&p[i]);
			getInformation(p[i]);
			exit(EXIT_CHILD_OK);
			break;
		default :
			printf("[Parent] ");
			create_processInfo(&p[0]);
			getInformation(p[0]);
			exit(EXIT_OK);
			break;
	}
	while(cpt>0)
	{
	int finpid = wait(&circonstance);
	printf("%d terminé\n",finpid);
	cpt--;
	}
	printf("J'ai fini\n");
	}
	return EXIT_OK;
}

/*int main(voi
{
	pid_t pid;
	int circonstance;
	process_info p[2];
	create_processInfo(&p[0]);
	switch(pid = fork())
	{
		case -1:
				fprintf(stderr, "Erreur lors de la creation du processus voir information suivante");
				getInformation(p[0]);
				return EXIT_FAIL;
		case 0:
			printf("[FILS] ");
			create_processInfo(&p[1]);
			getInformation(p[1]);
			exit(EXIT_OK);
		default :
			printf("[Parent] ");
			create_processInfo(&p[0]);
			getInformation(p[0]);
			exit(EXIT_OK);
	}
	wait(&circonstance);
	printf("CIRCONSTANCE DE FIN  : %d", WEXITSTATUS(circonstance));
	return EXIT_OK;
}*/