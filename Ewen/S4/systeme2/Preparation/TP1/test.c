#define POSIX_C_SOURCE 1

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>

//Definition des constantes
#define OK 0
#define FAIL -1

void afficher_repertoire(const char* repo)
{
	DIR* d;
	struct dirent* dir_stat;
	struct stat* file_stat;
	if((d=opendir(repo)) == NULL)
	{	
		perror(repo);
		exit(FAIL);
	}
	while((dir_stat = readdir(d))!=NULL)
	{
		if(strcmp("..", dir_stat->d_name)!=0 && strcmp(".", dir_stat->d_name) != 0)
		{
			stat(dir_stat->d_name, file_stat);
			printf("\t%s\n", dir_stat->d_name);
		}
	}
	closedir(d);
}

int main(int argc, const char** argv, const char** envp)
{

	pid_t pid;
	if(argc < 2)
	{
			fprintf(stderr, "Usage : %s [<repo>]\n", argv[0]);
			return -1; 
	}
	for(int i = 1;i<argc-1;i++)
	{
		switch(pid=fork())
		{
			case -1: 
				fprintf(stderr, "ERROR : Cannot create child ==> fork()", getpid());
				return FAIL;
				break;
			case 0:
					afficher_repertoire(argv[i]);
					break;
			default:
				printf("Affichage du dossier : %s\n", argv[i]);
				break;
		}
	}
	return OK;
}