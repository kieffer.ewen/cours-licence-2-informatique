#define _POSIX_C_SOURCE 1
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char** argv, char** envp)
{
pid_t pid;

	
	switch(pid = fork())
	{

		case -1:
			printf("Une erreur s'est produite ! PID du pere = %d\n", getpid());
			exit(EXIT_FAILURE);
			break;
		case 0: 
			printf("Je suis le fils et mon père est %d. mon PID est %d\n", getppid(), getpid());
			break;
		default :
			printf("Je suis le pere mon PID est %d\n", getpid());
			wait(NULL);
			break;
	}	
	return EXIT_SUCCESS;

}
