#define _POSIX_C_SOURCE 1
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#define N 2

int main(int argc, char** argv, char** envp)
{
	pid_t pid[4];
	printf("##### DEBUT #####\n");
	for(int j = 0; j <4;j++)
	{

		pid_t fils;
		int status;
		switch(pid[j] = fork())
		{

			case -1:
				printf("Une erreur s'est produite ! PID du pere = %d\n", getpid());
				exit(EXIT_FAILURE);
				break;
			case 0: 
				for(int i = 0;i<N; i++)
					printf("Je suis le fils %d\n", getpid());
				if(j == 3)
					exit(0);
				exit(j+1);
				break;
			default :
				waitpid(pid[j], &status, 0);
				printf("Le fils %d a terminé avec le compte rendu %d\n", pid[j], WEXITSTATUS(status));
				break;
		}	
	}

	return EXIT_SUCCESS;

}
