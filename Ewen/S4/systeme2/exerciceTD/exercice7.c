#define _POSIX_C_SOURCE 1
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char** argv, char** envp)
{
pid_t pid;

	switch(pid = fork())
	{

		case -1:
			printf("Une erreur s'est produite ! PID du pere = %d\n", getpid());
			exit(EXIT_FAILURE);
			break;
		case 0: 
			execl("/bin/ls","ls","-l",NULL);
			break;
		default :
			wait(NULL);

			execl("/bin/pwd", "pwd",NULL, NULL);
			break;
	}	
	return EXIT_SUCCESS;

}
