#define _POSIX_C_SOURCE 1
#include <stdio.h>

int main(int argc, char** argv, char** envp)
{
	for(int i = 0;i <argc;i++)
		printf("Argv[%d] = %s\n", i, argv[i]);
		
	printf("%d arguments on été trouvés !\n", argc);
	int j = 0;
	while(envp[j]!=NULL)
		{
			printf("envp[%d] = %s\n", j, envp[j]);
			j++;
		}
	printf("%d variables d'environnement on été trouvés !\n", j);
		
	return 0;
}
