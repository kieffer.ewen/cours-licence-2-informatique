#define _POSIX_C_SOURCE 1
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
int main(int argc, char** argv, char** envp)
{

	int fdR = open(argv[1],  O_RDONLY);
	int fdW = open(argv[2], O_WRONLY, O_CREAT);
	void* p;
	ssize_t r = read(fdR, p, 512);	
	ssize_t w = write(fdW, p, 512);
	close(fdR);
	close(fdW);
	return EXIT_SUCCESS;

}
