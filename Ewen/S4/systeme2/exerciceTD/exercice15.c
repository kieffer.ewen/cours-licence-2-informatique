#define _POSIX_C_SOURCE 1
#define NBMOTSMAX 20
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <dirent.h>
//VERSION 1
int affNomfic(char nomrep[])
{
    typedef struct dirent dirent;
  struct stat info;
  dirent* param;
  char designation[512];
  
  DIR* d = NULL;
  if((d = opendir(nomrep))==NULL)
  {
    perror(nomrep);
    exit(2);
  }
  param = readdir(d);
  while(param!=NULL)
  {
      if(strcmp(param->d_name,".")!=0 && !strcmp(param->d_name,"..")!=0)
      {
          sprintf(designation,"%s/%s", nomrep, param->d_name);
          if(stat(designation, &info)==0)
          {
            if(S_ISDIR(info.st_mode))
            {
                            printf("%s\n", designation);

             affNomfic(designation);
            }
            else
            {
              printf("%s\n", designation);
            }
          }
          else
          { 
            perror(designation);
          }
        }
        param = readdir(d);
}

  closedir(d);
}

int main(int argc, char** argv, char** envp)
{
  if(argc != 2)
  {
    fprintf(stderr, "Usage : %s <Directory>\n", argv[0]);
    exit(1);
  }
  printf("REPERTOIRE : %s\n", argv[1]);
  affNomfic(argv[1]);
  
  return 0;
}