#define _POSIX_C_SOURCE 1

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>

//DEFINITION CONSTANTES
#define EXIT_OK 0
#define EXIT_FAIL -1

//Chiffrage par methode XOR
int chiffrageXOR(char designation_fichier_source[], char designation_fichier_cible[], char clef[])
{
	int fs; //FICHIER SOURCE
	int fc;	//FICHIER CIBLE
	int taille_clef = strlen(clef); // TAILLE DE LA CLEF DE CHIFFRAGE
	char charlu[taille_clef]; //A ECRIRE DANS LA CIBLE
	
	if((fs = open(designation_fichier_source, O_RDONLY))==-1)
	{
		fprintf((stderr), "%s\n", "ERROR CANNOT OPEN SOURCE FILE");
		return EXIT_FAIL;
	}

	if((fc = open(designation_fichier_cible, O_CREAT|O_WRONLY|O_TRUNC, S_IRWXU))==-1)
	{	
		close(fs);
		fprintf(stderr, "%s\n", "ERROR CANNOT CREATE OR OPEN WITH WRITE RIGHTS DESTINATION FILE");
		return EXIT_FAIL;
	}



	while(read(fs, charlu, taille_clef)>0)
	{
		//CHIFFRAGE PAR XOR D'UNE PHRASE DE TAILLE taille_clef
		for(int i = 0; i<taille_clef; i++)
		{
			if(charlu[i] != 0)
			{	
				charlu[i] ^= clef[i];
			}
		}
		write(fc, charlu, taille_clef);
	}
	close(fc);
	close(fs);
	return EXIT_OK;
}


//chiffrage repertoire
int chiffrage_rep(char repertoire_dest[], char clef[])
{
	struct stat* pInfos;
	struct dirent* dir_infos;
	DIR* rep;
	if((rep = opendir(repertoire_dest)) == NULL)
	{
		fprintf(stderr, "%s\n", "Error while opening the repertory");
		return EXIT_FAIL;
	}
	while((dir_infos = readdir(rep)) != NULL)
	{
		stat(dir_infos->d_name, pInfos);
		if(S_ISDIR(pInfos->st_mode)!=0)
		{
			if(strcmp(dir_infos->d_name, ".") != 0 && strcmp(dir_infos->d_name, "..") != 0)
			{
				chiffrage_rep(dir_infos->d_name, clef);
			}
		}else
		{
			char prefixe[9] = "crypted_";
			int taille_finale = strlen(prefixe)+strlen(dir_infos->d_name);
			char nom[taille_finale];
			strcat(nom, prefixe);
			strcat(nom,dir_infos->d_name);
			if(chiffrageXOR(dir_infos->d_name, nom, clef)!=0)
			{
				fprintf(stderr, "le fichier %s n'as pas pu être chiffré.", dir_infos->d_name);
			}
		}
	}
	closedir(rep);
	return EXIT_OK;
}

int main(void)
{
	if(chiffrage_rep("./","abcdefg")!=0)
	{
		fprintf((stderr), "%s\n", "ERROR -1 : CHIFFRAGEXOR() FUNC NOT WORKING");
		return 	EXIT_FAIL;
	}

	return EXIT_OK;
}
