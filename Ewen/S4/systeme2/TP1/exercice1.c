#define _POSIX_C_SOURCE 1
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

void getInformation(){
	printf("INFO : [%d]\tPropriétaire : %s(%d)\t GID : %d,PGRP : %d PPID : %d\n",getpid(),getlogin(),getuid(), getgid(), getpgrp(),getppid());
}

//Version 1
// int main(int argc, char** argv, char** envp)
// {
// 	getInformation();
// }

//Version 2
int main(int argc, char** argv, char** envp)
{
pid_t pid;
int circonstance;
	
	switch(pid = fork())
	{

		case -1:
			printf("Une erreur s'est produite ! PID du pere = %d\n", getpid());
			exit(EXIT_FAILURE);
			break;
		case 0: 
			printf("[%d]Fils\n", getpid());
			getInformation();
			exit(1);
		default :
			printf("[%d]Pere\n", getpid());
			getInformation();
			wait(&circonstance);
			printf("Code retour du fils : %d\n", WEXITSTATUS(circonstance));
			break;
	}	
	return EXIT_SUCCESS;

}