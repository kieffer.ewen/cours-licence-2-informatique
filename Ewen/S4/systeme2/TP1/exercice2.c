#define _POSIX_C_SOURCE 1
#define NBMOTSMAX 20
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
int decoupe(char Chaine[], char *pMots[])
{
  char *p;
  int NbMots=0;

  p=Chaine; /* On commence par le d�but */
  /* Tant que la fin de la cha�ne n'est pas atteinte et qu'on ne d�borde pas */
  while ((*p)!='\0' && NbMots<NBMOTSMAX)
  {
    while ((*p)==' ' && (*p)!='\0') p++; /* Recherche du d�but du mot */
    if ((*p)=='\0') break; /* Fin de cha�ne atteinte */
    pMots[NbMots++]=p; /* Rangement de l'adresse du 1er caract�re du mot */
    while ((*p)!=' ' && (*p)!='\0') p++; /* Recherche de la fin du mot */
    if ((*p)=='\0') break; /* Fin de cha�ne atteinte */
    *p='\0'; /* Marquage de la fin du mot */
    p++; /* Passage au caract�re suivant */
  }
  pMots[NbMots]=NULL; /* Derni�re adresse */
  return NbMots;
}

//VERSION 1
/*int main(int argc, char** argv, char** envp)
{

	pid_t pid;
	char* pMots[NBMOTSMAX+1];
	for(int i = 1; i<argc; i++)
	{
		switch(pid = fork())
		{
		case -1:
			printf("Une erreur s'est produite ! PID du pere = %d\n", getpid());
			exit(EXIT_FAILURE);
			break;
		case 0:
			printf("[%d]\tJe lance %s\n",getpid(), argv[i]);
			decoupe(argv[i], pMots); 
			execvp(pMots[0], pMots);
			perror(pMots[0]);
			exit(0);
		default :
			printf("[%d]\tJ'ai délégué %s à %d\n",getpid(), argv[i], pid);
			wait(NULL);
			printf("%d terminé\n", pid);
			break;
		}
	}
	printf("J'ai fini\n");
	return EXIT_SUCCESS;
}*/

//VERSION 2 
int main(int argc, char** argv, char** envp)
{

	pid_t pid;
	int circonstance;
	char* pMots[NBMOTSMAX+1];
	int cpt = argc-1;
	for(int i = 1; i<argc; i++)
	{
		switch(pid = fork())
		{
		case -1:
			printf("Une erreur s'est produite ! PID du pere = %d\n", getpid());
			exit(EXIT_FAILURE);
			break;
		case 0:
			printf("[%d]\tJe lance %s:\n",getpid(), argv[i]);
			decoupe(argv[i], pMots); 
			execvp(pMots[0], pMots);
			perror(pMots[0]);
			exit(56);
		default :
			printf("[%d]\tJ'ai délégué %s à %d. J'attends sa fin...\n",getpid(), argv[i], pid);
			break;
		}
	}
	while(cpt>0)
	{
	int finpid = wait(&circonstance);
	printf("%d terminé\n",finpid);
	cpt--;
	}
	printf("J'ai fini\n");
	return EXIT_SUCCESS;
}