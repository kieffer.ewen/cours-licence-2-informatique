#define _POSIX_C_SOURCE 1
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>	
#include <fcntl.h>

//Error definition
#define ERR_OP		1
#define ERR_WR		2
#define ERR_RD		3
#define ERR_CL		4
#define ERR_POS		5
#define ERR_ARG 	6
#define ERR_CREA	7
#define ERR_CONS	8

//END Error definition

#define LONG_MAX_NOM      20

typedef struct{
	char Nom[LONG_MAX_NOM+1];
	int Age;
	int NbEnfants;
}Infos;


int creation(char* nomFichier)
{
	int fd;
	if((fd = open(nomFichier, O_WRONLY|O_APPEND|O_CREAT, S_IRWXU)) == -1)
	{
		fprintf(stderr,"ERROR : Ouverture de %s impossible\n", nomFichier);
		return ERR_OP;
	}
	Infos i;
	printf("\n\nEntrez le nom de la personne : ");
	scanf("%s", i.Nom);
	printf("\nEntrez l'age de la personne : ");
	scanf("%d", &i.Age);
	printf("\nEntrez le nombre d'enfant de la personne : ");
	scanf("%d", &i.NbEnfants);
	if(write(fd, &i, sizeof(i))==-1)
	{
		fprintf(stderr, "Erreur d'écriture dans le fichier");
		return ERR_WR;
	}
	if(close(fd)==-1)
	{
		fprintf(stderr, "Erreur lors de la fermeture du descripteur de fichier\n");
		return ERR_CL;
	}
	return 0;
}


int consulter(char* nomFichier)
{
	int fd;
	int cpt=0;
	int consult;
	Infos i;

	if((fd = open(nomFichier, O_RDONLY))==-1)
	{
		fprintf(stderr,"ERROR : Ouverture de %s impossible\n", nomFichier);
		return ERR_OP;
	}
	while(read(fd, &i, sizeof(i)))
	{
		printf("Fiche n°%d : %s\n", cpt+1, i.Nom);
		cpt++;
	}
	printf("\nQuel fiche consulter ?\n");
	scanf("%d", &consult);

	if(lseek(fd, (consult-1)*sizeof(i), SEEK_SET) == -1)
	{
		fprintf(stderr, "Erreur positionnement dans le fichier\n");
		return ERR_POS;
	}
	if(read(fd, &i, sizeof(i))==-1)
	{
		fprintf(stderr, "Erreur de lecture du fichier\n");
		return ERR_RD;
	}

	printf("\n\n##)## Fiche : %s ####\n", i.Nom);
	printf("Age : %d\n", i.Age);
	printf("Nombre d'enfant : %d\n\n", i.NbEnfants);
	if(close(fd)==-1)
	{
		fprintf(stderr, "Erreur lors de la fermeture du descripteur de fichier\n");
		return ERR_CL;
	}
	return 0;
}

int main(int argc, char** argv)
{	int code = 0;
	if(argc != 2)
	{
		fprintf(stderr, "ERROR : usage %s <filename>\n",argv[0]);
		return ERR_ARG;
	}
	system("clear");
	while(1)
	{
		int choice;
		int cont = 0;
		printf("########### MENU ###########\n");
		printf("1 : Creation\n");
		printf("2 : Consulter\n");
		printf("3 : Quitter\n");
		printf("Votre choix : ");
		scanf("%d", &choice);
		switch(choice){
			case 1 :
					do
					{
					if(creation(argv[1])!=0)
					{
						fprintf(stderr, "ERREUR LORS DE LA CREATION\n");
						return ERR_CREA;
					}
					printf("continuer l'écriture ?(0 : Oui | 1: Non)\n");
					scanf("%d", &cont);
					printf("\n\n");
					}while(cont == 0);
					break;
			case 2 :
					do
					{
					if(consulter(argv[1])!=0)
					{
						fprintf(stderr, "Erreur lors de la consultation\n");
						return ERR_CONS;
					}
					printf("continuer la consultation ?(0 : Oui | 1: Non)\n");
					scanf("%d", &cont);
					printf("\n\n");
					}while(cont == 0);
					break;
			case 3 :
					printf("Sortie du programme !\n");
					return 0;
			default : 
				break;
		}
	}	
}