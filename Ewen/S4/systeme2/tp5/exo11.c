#define _POSIX_C_SOURCE 1
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <dirent.h>

int affInfofic(char* infoFic)
{
  struct stat info;
  char type[255] = "NULL";
  //tp12ex1.c            : fichier        1093 octets Thu May 27 16:50:34 200
  stat(infoFic, &info);
  if(S_ISDIR(info.st_mode))
  {
    strcpy(type,"Répertoire");
  }
  if(S_ISREG(info.st_mode))
  {
    strcpy(type,"Fichier");
  }

  printf("%-20s: %-20s %8ld octets %s", infoFic,type , info.st_size, ctime(&info.st_atime));
}

int main(int argc, char** argv, char** envp)
{
  exelc("ls","ls","-l",NULL);
  
  return 0;
}