#define _POSIX_C_SOURCE 1
#define NBMOTSMAX 20
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <dirent.h>
//VERSION 1
int affNomfic(char nomrep[])
{
  typedef struct dirent dirent;
  struct stat info;
  dirent* param;
  char designation[512];
  size_t dirSize = 0;
  DIR* d = NULL;
  if((d = opendir(nomrep))==NULL)
  {
    perror(nomrep);
    exit(2);
  }
  param = readdir(d);
  while(param!=NULL)
  {
      if(strcmp(param->d_name,".")!=0)
      { 
       if(strcmp(param->d_name,"..")!=0)
        {
          sprintf(designation,"%s/%s", nomrep, param->d_name);
          if(stat(designation, &info)!=-1)
          {
            dirSize += info.st_size;
            if(S_ISDIR(info.st_mode))
            {
             dirSize += affNomfic(designation);
            }
            printf("%-40s: %8ld octets\n", designation, info.st_size);
          }
          else
          { 
            perror(designation);
          }
        }
      }
        param = readdir(d);
}
  closedir(d);
  return dirSize;
}

int main(int argc, char** argv, char** envp)
{
  if(argc != 2)
  {
    fprintf(stderr, "Usage : %s <Directory>\n", argv[0]);
    exit(1);
  }
  printf("REPERTOIRE : %s\n", argv[1]);
  int size = affNomfic(argv[1]);
  printf("TOTAL %s:\t%8ld octets\n", argv[1], size);
  
  return 0;
}