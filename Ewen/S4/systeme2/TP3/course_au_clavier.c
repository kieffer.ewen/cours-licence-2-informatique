#define _POSIX_C_SOURCE 199309L /* Norme POSIX 199309 : IEEE Std 1003.1b-1993*/
#include <sys/termios.h>
#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/* le terminal non canonique */
/* Term_non_canonique =================================
// Permet de lire le clavier touche par touche, sans
// écho.
//===================================================*/
int Term_non_canonique()
{
struct termios
term;
tcgetattr( fileno(stdin), &term);/* lit les flags du terminal dans term */
term.c_lflag &= ~ICANON;
/* mode non-canonique */
term.c_lflag &= ~ECHO;
/* supprime l’écho */
term.c_cc[VMIN] = 1;
/* nombre min de caractères */
term.c_cc[VTIME] = 1;
/* latence (timeout) 1/10e de seconde (0: pas de latence) */
if (tcsetattr( fileno(stdin), TCSANOW, &term) < 0) /* écrit les flags depuis term */
{
perror("Term_non_canonique: problème d’initialisation ");
return 0;
}
return 1;
}
/* Term_canonique =====================================
// Mode normal du clavier: lecture par ligne et écho.
//===================================================*/
int Term_canonique()
{
struct termios
term;
/* retour en mode ligne */
tcgetattr( fileno(stdin), &term);/* lit les flags du terminal dans term */
term.c_lflag |= ICANON;
/* mode canonique */
term.c_lflag |= ECHO;
/* rétablit l’écho */
if (tcsetattr( fileno(stdin), TCSANOW, &term) < 0) /* écrit les flags depuis term */
{
perror("Term_canonique: problème d’initialisation ");
return 0;
}
return 1;
}

void joueur(char my_char, int ligne, int distance)
{
	int f;
	struct timespec micro_pause;
	micro_pause.tv_sec = 0;
	/* secondes */
	micro_pause.tv_nsec = 10;
	/* nanosecondes */
	int cpt = 0;
	char c;
	char ch;
	do
	{
		if(ch == my_char)
		{	
			//WORK IN PROGRESS
			cpt++;
			c = cpt +'0';
			lseek(f, ligne, 0);
			write(f, &cpt, 1);
		}
		if(ch == 'f')
		{
			break;
		}
		nanosleep( &micro_pause ,NULL);
	}while(read(STDIN_FILENO, &ch, 1)>0 && cpt < distance);
}

void touche()
{
char ch;
while(read(STDIN_FILENO, &ch, 1)>0)
	{
		printf("[%d] %c\n", getpid(), ch);
		if(ch == 'f')
		{
			break;
		}
	}
}

void clear()
{
		printf ("\033[H\033[J");
		fflush (stdout);
}

int main(int argc, char** argv)
{
	pid_t pid;
	int circonstance;
	char debut = '1';
	if(argc != 3)
	{
		fprintf(stderr, "Usage : ./%s, NbJoueur Distance", argv[0]);
		return 2;
	}
	int f;
	f = open("position", O_RDWR | O_CREAT | O_TRUNC, S_IRWXU);
	
	int cpt = atoi(argv[1]);
	int distance = atoi(argv[2]);
	for(int i = 0; i<cpt; i++)
	{
		write(f, &debut, 1);
	}

	if(Term_non_canonique())
	{
		clear();
		for(int i = 0; i<cpt;i++)
		{
			switch(pid = fork())
			{

				case -1:
					printf("Une erreur s'est produite ! PID du pere = %d\n", getpid());
					exit(EXIT_FAILURE);
					break;
				case 0: 
					joueur((97+i), i+1, distance);
					exit(i);
				default :
					break;
			}
		}
		while(cpt>0)
		{
			int finpid = wait(&circonstance);
			printf("%d terminé\n",finpid);
			cpt--;
		}
		printf("J'ai fini\n");
		Term_canonique();
	}
	else{
		close(f);
		return 1;
	}
	close(f);
	return 0;
}