.global _start
_start:
      mov		         r5, #48
      mov		         r6, #18

  bcl1:
        cmp		       r6, #0
        beq		       fin1
        mov		       r1, r5
  bcl2:
        cmp		       r1, r6
        blt		       fin2
        sub		       r1, r1, r6
        b		         bcl2
  fin2 :
      mov		       r5, r6
      mov		       r6, r1
      b		         bcl1
  fin1:
_exit:
      mov       r0, r0
