.global _start

.equ PIOB_BASE, 0xFFFFF600
.equ PIO_PER, 0
.equ PIO_OER, 0x10
.equ PIO_ODR, 0x14
.equ PIO_SODR, 0x30
.equ PIO_CODR, 0x34
.equ PIO_PDSR, 0x3C
.EQU LED1, 1<<27
.EQU LED2, 1<<28
.EQU LED3, 1<<29
.EQU LED4, 1<<30
.EQU LEDS, LED1|LED2|LED3|LED4
.EQU SW1, 1<<24
.EQU SW2, 1<<25
.EQU BOUTONS, SW1|SW2

_start:
bl init
ldr r12,=PIOB_BASE
mov r1,#LED1
while1:
mov r6, #0x200000
ldr r2,[r12,#PIO_PDSR]
tst r2, #SW1
beq _exit
tst r2,#LED1
streq r1,[r12,#PIO_SODR] @ allume
strne r1,[r12,#PIO_CODR] @ eteint
while2:cmp r6, #0x0
        subne r6, r6, #1
        bne while2
b while1

_exit:
b _exit


init:
stmfd sp!,{r12,lr}
ldr r12,=PIOB_BASE
mov lr,#LED1|SW1
str lr,[r12,#PIO_PER] @ activer broches 24 et 27
mov lr,#LED1
str lr,[r12,#PIO_OER] @ LED est une sortie
mov lr,#SW1
str lr,[r12,#PIO_ODR] @ BOUTON est une entrée
ldmfd sp!,{r12,pc}
