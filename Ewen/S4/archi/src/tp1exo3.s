.global _start
_start :
                mov             r4, #0
                mov             r1, #0
                mov             r2, #0
                mov             r3, #0
                mov             r6, #0
bcl:
                mov             r6, r4
                cmp             r4, #8
                bhi             fin
                movs            r6, r6, lsr #1 ; [0,0,0,0] C = bit poid faible
                bcs             suite
alors1:
                add             r1, r1, r4
                movs            r6, r6, lsr #1
                bcc             suite
alors2:
                add             r2, r2, r4
                movs            r6, r6, lsr #1
                bcc             suite

alors3:
                add             r3,r3, r4
suite:
                add             r4, r4, #1
                b               bcl
fin:
_exit: mov r0, r0
