.global _start

MAT1 :  .byte 0, 1, 2, 3
        .byte 4, 5, 6, 7
MAT2 :  .int 0, 1, 2, 3
        .int 4, 5, 6, 7


_start:
mov r0, #1 @i
mov r1, #2 @j

mov r2, #0
mov r7, #4 @ Taille de la matrice
adr r3, MAT1 @Ou MAT2 pour des int
mla r5, r7, r0, r1
strb r2 , [r3, r5]
@Pour les .int :
@str r2, [r3, r5, LSL#2]


_exit:
b     _exit
