.global _start

tab: .int 9, -4, 27592, 0, -27592, 9, -4, 27592, 0, -27592


_start:
mov r1, #0 @Compteur
mov r5, #0 @Position de la valeur
adr r2, tab
ldr r3, [r2, r1, lsl#2] @Première valeur

bcl:
    cmp r1, #9 @ 9 = taille du tableau
    beq _exit
    add r1, r1, #1
    ldr r4, [r2, r1, lsl#2]
cond: cmp r2, r4
      movgt r2, r4
      movgt r5, r1
      b bcl
_exit:
  b _exit
