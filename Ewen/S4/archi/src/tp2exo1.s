.global _start
chaine: .ascii "*"
.align

_start:
  mov r1, #2
  mov r2, #2
  ldrb r4, chaine
cmp r4, #43
  addeq r3, r1, r2
  beq _exit
cmp r4, #42
  muleq r3, r1, r2
  beq _exit
cmp r4, #45
  subeq r3, r1, r2
  beq _exit

_exit: mov r0, r0
        b _exit
