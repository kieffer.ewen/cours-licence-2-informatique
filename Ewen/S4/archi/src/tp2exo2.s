.global _start

chaine: .asciz "#ABZ]aez}"
.align


_start:
        mov     r1, #0
        adr     r5, chaine
        ldrb    r4, [r5, r1]

cond0:
        cmp     r4, #0
        beq     _exit
cond1 :
        cmp     r4, #123
        bhi     _avancer
cond2 :
        cmp      r4, #96
        blt     _avancer
        sub     r4, r4, #32
        strb    r4, [r5, #0]
        b       _avancer

_exit:
        b       _exit

_avancer:
        add     r5, r5, #1
        ldrb    r4, [r5, r1]
        b       cond0
