; version 1
adr r5, tab
mov r1, fintab
mov r2, #0
tq : cmp r5, r1
    bhs fin
    strb r2, [r5], #1
    add r2, r2, #1
    b tq
fin:d
tab: .fill 10, 1
fintab: .align

; version 2
tab: .fill 10, 1
adr r5, tab
mov r1, #0
tq : cmp r1, #10
    bhs fin
    strb r1, [r5, r1]
    add r1, r1, #1
    b tq
fin:

;maximum

main :
adr r5, tab
mov r1, #0
mov r2, #0
tq : cmp r1, #10 
        bhs fin
        ldrsh r3, [r5, r1, lsl#2]
        cmp r2, r3
        movgt r2, r3
        add r1, r1, #1
        b tq
fin:

tab: .fill 10, 10
fintab: .align
