/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <iostream>
#include <QApplication>

#include "MainWindow.hpp"

using namespace std;
using namespace bsim;

int main (int argc,char *argv[]) {
	QApplication app(argc,argv);
	
	// Check args for xml of elf 
	QStringList args;
	args = QCoreApplication::arguments();
	if(args.size() > 3 || args.size() == 2) {	// You have to provide both elf and xml, or nothing
		cerr << "Usage : " << argv[0] << " [board file] [executable file]\n";
		exit(1);
	}

	// run the application
	MainWindow mw;
	if(args.size() > 2) {
		mw.loadBoard(args.at(1));
		if(mw.isBoardReady())
			mw.loadExec(args.at(2));
	}
	mw.show();
	return app.exec();
}
