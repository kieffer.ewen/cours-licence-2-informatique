/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <FileSource.hpp>
#include <Highlighter.hpp>
#include <Program.hpp>

namespace bsim {

/**
 * @class FileSourceException
 */


/**
 * @class FileSource
 * This ISource implementation allows to handle source from a textual file.
 */


/**
 * Build a file source.
 * @param name		Name of the source as found in the debugging information.
 * @param path		Path of the file.
 * @param program	Program the source is for.
 */
FileSource::FileSource(QString name, Path path, Program& program)
: _name(name), _path(path), prog(program), high(0) {
	QFile file(path.toQString());
	if(!file.open(QIODevice::ReadOnly))
		throw FileSourceException(QString("cannot open %1: %2").arg(path.toQString()).arg(file.errorString()));
	QTextStream stream(&file);
	text = stream.readAll();
}


/**
 */
QString FileSource::name(void) {
	return _name;
}


/**
 */
QString FileSource::getText(void) {
	return text;
}


/**
 */
int FileSource::getLine(uint32_t address) {
	QString file;
	int line;
	prog.lineOf(address, file, line);
	if(line < 0 || file != _name)
		return -1;
	else
		return line;
}


/**
 */
uint32_t FileSource::getAddress(int line) {
	return prog.addressOf(_name, line);
}


/**
 */
bool FileSource::isAddressable(int line) {
	return prog.addressOf(_name, line) != 0;
}


/**
 */
Highlighter *FileSource::highlighter(void) {
	if(!high) {
		QString ext = _path.extension();
		high = Highlighter::getForExt(ext);
	}
	return high;
}


}	// bsim
