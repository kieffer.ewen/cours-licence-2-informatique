/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <Display.hpp>
#include <QGraphicsRectItem>
#include <QPen>
#include <QBrush>
#include <Pin.hpp>
#include <IComponent.hpp>
#include <Board.hpp>

namespace bsim {

/**
 * @class Visual
 * Base class of visual items. Provides useful types and generic facilities.
 */


/**
 * @fn bool Visual::isHorizontal(dir_t dir);
 * Test if a direction is horizontal or not.
 * @param dir	Direction to test.
 * @return		True if horizontal, false.
 */


/**
 * @fn bool Visual::isVertical(dir_t ori);
 * Test if a direction is vertical or not.
 * @param dir	Direction to test.
 * @return		True if vertical, false.
 */


/**
 * Parse a direction as text.
 * @param text	Text to parse.
 * @param def	Default value if the text does not match.
 * @return		Parsed direction or the default value.
 */
Visual::dir_t Visual::parseDir(QString text, dir_t def) {
	static struct { QString name; Visual::dir_t dir; } dirs[] = {
		{ "north", Visual::NORTH },
		{ "east", Visual::EAST },
		{ "south", Visual::SOUTH },
		{ "east", Visual::EAST },
		{ }
	};
	if(!text.isNull())
		for(int i = 0; !dirs[i].name.isNull(); i++)
			if(text == dirs[i].name)
				return dirs[i].dir;
	return def;
}


Visual::dir_t Visual::reverseDir(dir_t dir) {
	static dir_t t[] = { NO_DIR, SOUTH, WEST, NORTH, EAST };
	return t[dir];
}


static const int edge_pad = 5, edge_around = 100;

static QGraphicsItem *makeBack(int x1, int y1, int x2, int y2, Visual::dir_t ori) {
	
	// prepare coordinates
	if(((ori == Visual::NORTH || ori == Visual::SOUTH) && y1 > y2)
	|| ((ori == Visual::EAST || ori == Visual::WEST) && x1 > x2)) {
		int t = y1;
		y1 = y2;
		y2 = t;
		t = x1;
		x1 = x2;
		x2 = t;
	}
	int dx, dy, adx, ady;
	switch(ori) {
	case Visual::NORTH:
		dx = 0;
		dy = -edge_pad;
		adx = edge_around;
		ady = -edge_pad;
		break;
	case Visual::EAST:
		dx = edge_pad;
		dy = 0;
		adx = edge_pad;
		ady = edge_around;
		break;
	case Visual::SOUTH:
		dx = 0;
		dy = edge_pad;
		adx = edge_around;
		ady = edge_pad;
		break;
	case Visual::WEST:
		dx = -edge_pad;
		dy = 0;
		adx = -edge_pad;
		ady = edge_around;
		break;
	}

	// make the polygon
	QPainterPath *path = new QPainterPath;
	path->moveTo(x1, y1);
	path->lineTo(x1 + dx, y1 + dy);
	path->lineTo(x1 + adx, y1 + ady);
	path->lineTo(x2 + adx, y2 + ady);
	path->lineTo(x2 + dx, y2 + dy);
	path->lineTo(x2, y2);
	return new QGraphicsPathItem(*path);
}


static QGraphicsItem *makeStraight(int x1, int y1, int x2, int y2, Visual::dir_t ori) {
	QPainterPath *path = new QPainterPath;
	path->moveTo(x1, y1);
	switch(ori) {
	case Visual::NORTH:
	case Visual::SOUTH:
		if(y1 != y2) {
			path->lineTo(x1, (y1 + y2) / 2);
			path->lineTo(x2, (y1 + y2) / 2);
		}
		break;
	case Visual::EAST:
	case Visual::WEST:
		if(x1 != x2) {
			path->lineTo((x1 + x2) / 2, y1);
			path->lineTo((x1 + x2) / 2, y2);
		}
		break;
	}
	path->lineTo(x2, y2);
	return new QGraphicsPathItem(*path);
}


static QGraphicsItem *makeL(int x1, int y1, int x2, int y2, Visual::dir_t ori) {
	QPainterPath *path = new QPainterPath;
	path->moveTo(x1, y1);
	switch(ori) {
	case Visual::NORTH:	path->lineTo(x1, y2); break;
	case Visual::EAST:	path->lineTo(x2, y1); break;
	case Visual::SOUTH:	path->lineTo(x1, y2); break;
	case Visual::WEST:	path->lineTo(x2, y1); break;
	}
	path->lineTo(x2, y2);
	return new QGraphicsPathItem(*path);
}


/**
 * Make an edge between two pins.
 * @param x1	Pin 1 x.
 * @param y1	Pin 1 y.
 * @param or1	Pin 1 direction.
 * @param x2	Pin 2 x.
 * @param y2	Pin 2 y.
 * @param or2	Pin 2 direction.
 */
QGraphicsItem *Visual::makeEdge(int x1, int y1, dir_t or1, int x2, int y2, dir_t or2) {

	// same orientation
	if(or1 == or2)
		return makeBack(x1, y1, x2, y2, or1);
	
	// same direction but opposed orientation
	if((isHorizontal(or1) == isHorizontal(or2))
	|| (isVertical(or1) == isVertical(or2)))
		return makeStraight(x1, y1, x2, y2, or1);
	
	// it is a L
	else
		return makeL(x1, y1, x2, y2, or1);
} 


/**
 * @class VisualPin
 * A pin inside a visual component.
 */


/**
 * Build a visual pin.
 * @param component		Visual component the pin is in.
 * @param pin			Pin model.
 * @param dir			Direction of the pin.
 * @param offset		Offset of the pin in its side.
 * @param with_label	Does the label need to be displayed (true as a default).
 */
VisualPin::VisualPin(VisualComponent& component, Pin *pin, dir_t dir, int offset, bool with_label)
:	_comp(component), _pin(pin), _dir(dir), _off(offset), _wlab(with_label)
{
	
}


/**
 * @fn VisualComponent& VisualPin::component(void) const;
 * Get the owner visual component.
 * @return	Owner visual component.
 */


/**
 * @fn Pin *VisualPin::pin(void) const;
 * Get the model pin.
 * @return	Model pin.
 */


/**
 * @fn VisualPin::dir_t VisualPin::dir(void) const;
 * Get the direction of the pin.
 * @return Pin direction.
 */


/**
 * @fn int VisualPin::offset(void) const;
 * Get the pin offset.
 * @return	Pin offset.
 */


/**
 * @fn bool VisualPin::withLabel(void) const;
 * Get the label display property.
 * @return	Label display property.
 */


/**
 * @fn QString VisualPin::label(void) const;
 * Get the label of the pin.
 * @return	Pin label.
 */


/**
 * Parse the configuration of a pin from the given XML element.
 * @param element	XML element to parse.
 */
void VisualPin::parse(QDomElement element) {
	
	// get the direction
	_dir = parseDir(element.attribute("dir"), _dir);
	
	// get the offset
	if(element.hasAttribute("offset"))
		_off = element.attribute("offset").toInt();
	
	// with-label ?
	if(element.attribute("with-label") == "no")
		_wlab = false;
}


/**
 * Get the X of the pin on the component visual.
 * @return	Pin X.
 */
int VisualPin::x(void) {
	switch(_dir) {
	case NORTH:	return _comp.x + _off;
	case EAST:	return _comp.x + _comp.w;
	case SOUTH:	return _comp.x + _off;
	case WEST:	return _comp.x;
	}
}


/**
 * Get the Y of the pin on the component visual.
 * @return	Pin Y.
 */
int VisualPin::y(void) {
	switch(_dir) {
	case NORTH:	return _comp.y;
	case EAST:	return _comp.y + _off;
	case SOUTH:	return _comp.y + _comp.h;
	case WEST:	return _comp.y + _off;
	}
}


/**
 * @class VisualComponent
 * The visual representation of a model component.
 */


/**
 * Build a visual component.
 * @param component		Model component.
 */
VisualComponent::VisualComponent(IComponent *component)
:	_comp(component),
	displayed(false),
	x(0), y(0), w(10), h(10),
	lab_xpad(2), lab_ypad(2),
	lab_dir(HORIZONTAL), lab_xpos(CENTER), lab_ypos(CENTER),
	fill_color(QColor("white"))
{
	parse(component->configuration());
}


/**
 * @fn IComponent *VisualComponent::component(void) const;
 * Get the model component.
 * @return	Model component.
 */


/**
 * @fn const QMap<Pin *, VisualPin *>& VisualComponent::pins(void) const;
 * Get the map of pins.
 * @return	Map of pins.
 */


/**
 */
void VisualComponent::parse(QDomElement element) {

	// set position
	if(element.hasAttribute("x")) {
		x = element.attribute("x").toInt();
		displayed = true;
	}
	if(element.hasAttribute("y")) {
		y = element.attribute("y").toInt();
		displayed = true;
	}
	if(element.hasAttribute("w")) {
		w = element.attribute("w").toInt();
		displayed = true;
	}
	if(element.hasAttribute("h")) {
		h = element.attribute("h").toInt();
		displayed = true;
	}
	
	// look for directrion
	QString v = element.attribute("lab-dir");
	if(v == "vertical") {
		lab_dir = VERTICAL;
		displayed = true;
	}
	else if(v == "horizontal") {
		lab_dir = HORIZONTAL;
		displayed = true;
	}
	
	// look for horizontal position
	v = element.attribute("lab-xpos");
	if(!v.isNull()) {
		static struct { QString name; int pos; } t[] = {
			{ "before", BEFORE },
			{ "left", LEFT },
			{ "center", CENTER },
			{ "right", RIGHT },
			{ "after", AFTER },
			{ }
		};
		for(int i = 0; !t[i].name.isNull(); i++)
			if(t[i].name == v) {
				lab_xpos = t[i].pos;
				displayed = true;
				break;
			}
	}

	// look for vertical position
	v = element.attribute("lab-ypos");
	if(!v.isNull()) {
		static struct { QString name; int pos; } t[] = {
			{ "above", ABOVE },
			{ "top", TOP },
			{ "center", CENTER },
			{ "bottom", BOTTOM },
			{ "below", BELOW },
			{ }
		};
		for(int i = 0; !t[i].name.isNull(); i++)
			if(t[i].name == v) {
				lab_ypos = t[i].pos;
				displayed = true;
				break;
			}
	}
	
	// parse colors
	v = element.attribute("fill");
	if(!v.isNull())
		fill_color = QColor(v);
	v = element.attribute("text");
	if(!v.isNull())
		text_color = QColor(v);
	v = element.attribute("border");
	if(!v.isNull())
		border_color = QColor(v);

	// label pad
	if(element.hasAttribute("lab-xpad")) {
		lab_xpad = element.attribute("lab-xpad").toInt();
		displayed = true;
	}
	if(element.hasAttribute("lab-ypad")) {
		lab_ypad = element.attribute("lab-ypad").toInt();
		displayed = true;
	}

	// parse the pins
	QDomNodeList nodes = element.elementsByTagName("pin");
	for(int i = 0; i < nodes.count(); i++) {
		QDomElement child = nodes.at(i).toElement();
		
		// get the name
		QString name = child.attribute("name");
		if(name.isNull()) {
			qDebug() << "no name in \"pin\" element";
			continue;
		}
		
		// find the pin
		Pin *pin = _comp->getPin(name);
		if(!pin) {
			qDebug() << "no pin named " << name;
			continue; 
		}
		
		// the display
		VisualPin *display = _pins.value(pin, 0);
		if(!display) {
			display = new VisualPin(*this, pin, EAST, 10);
			add(display);
		}
		
		// parse the configuration
		display->parse(child);
	}
}


/**
 * Add a visual pin to the visual component.
 * @param pin	Visual pin to add.
 */
void VisualComponent::add(VisualPin *pin) {
	_pins.insert(pin->pin(), pin);
}


/**
 * Remove a visual pin.
 * @param pin	Pin to remove.
 */
void VisualComponent::remove(VisualPin *pin) {
	_pins.remove(pin->pin());
}


/**
 * Build a visual component in a standard way.
 */
void VisualComponent::make(QString name) {
	makeBox();
	makeLabel(name);
}


/**
 * Make a label with the given name.
 * @param name	Name of the label.
 */
void VisualComponent::makeLabel(QString name) {
	
	// build the text
	QGraphicsTextItem *text = new QGraphicsTextItem(name);
	text->setDefaultTextColor(text_color);
	QRectF r = text->boundingRect();
	int tw = r.width(), th = r.height();
	
	// set the direction
	if(lab_dir == VERTICAL) {
		text->setRotation(90);
		int x = tw;
		tw = th;
		th = x;
	}
	
	// set the x position
	int lx;
	switch(lab_xpos) {
	case BEFORE:
		lx = (lab_dir == VERTICAL) ? (x - lab_xpad) : (x - tw - lab_xpad);
		break;
	case LEFT:
		lx = (lab_dir == VERTICAL) ? (x + lab_xpad + tw) : (x + tw);
		break;
	case CENTER:
		lx = (lab_dir == VERTICAL) ? (x + w - (w - tw) / 2) : (x + (w - tw) / 2);
		break;
	case RIGHT:
		lx = (lab_dir == VERTICAL) ? (x + w - lab_ypad) : (x + w - tw - lab_ypad);
		break;
	case AFTER:
		lx = (lab_dir == VERTICAL) ? (x + w + lab_ypad + tw) : (x + w + lab_ypad);
		break;
	}
	
	// set the y position
	int ly;
	switch(lab_ypos) {
	case ABOVE:
		ly = y - lab_ypad - th;
		break;
	case TOP:
		ly = y + lab_ypad;
		break;
	case CENTER:
		ly = y + (h - th) / 2;
		break;
	case BOTTOM:
		ly = y + h - lab_ypad - th;
		break;
	case BELOW:
		ly = y + h + lab_ypad;
		break;
	}
	
	// move and return the text
	text->setPos(lx, ly);
	addToGroup(text);
}


/**
 * Make the box of the component.
 */
void VisualComponent::makeBox(void) {
	QGraphicsRectItem *rect = new QGraphicsRectItem(x, y, w, h);
	rect->setPen(QPen(border_color));
	rect->setBrush(QBrush(fill_color));
	addToGroup(rect);
}


/**
 * Make the pins of the component.
 */
void VisualComponent::makePins(void) {
	for(QMap<Pin *, VisualPin *>::const_iterator pin = _pins.begin();
	pin != _pins.end(); ++pin)
		makePin(*pin);
}


/**
 * Make a pin of the component.
 * @param pin	Pin to display/
 */
void VisualComponent::makePin(VisualPin *pin) {
	if(pin->withLabel()) {
		QGraphicsTextItem *text = new QGraphicsTextItem(pin->label());
		QFont font = text->font();
		font.setPointSize(font.pointSize() * 3 / 4);
		text->setFont(font);
		addToGroup(text);
		QRectF r = text->boundingRect();
		int offset = pin->offset();
		switch(pin->dir()) {
		case NORTH :
			text->setRotation(90);
			text->setPos(x + offset - r.width() / 2, y + lab_ypad);
			break;
		case EAST:
			text->setPos(x + w - r.width() - lab_xpad, y + offset - r.height() / 2);
			break;
		case SOUTH:
			text->setRotation(90);
			text->setPos(x + offset - r.width() / 2, y + h - lab_ypad - r.height());
			break;
		case WEST:
			text->setPos(x + lab_xpad, y + offset - r.height() / 2);
			break;
		}
	}
}


/**
 * @class VisualGraph
 * A graph to represent the board, its components and its connections.
 */


/**
 * Build the visual graph.
 * @param board		Owner board.
 * @param scene		Container scene.
 */
VisualGraph::VisualGraph(Board& board, QGraphicsScene& scene)
: _board(board), _scene(scene) {
	
	// collect all components
	const QMap< QString, IComponent *> &comps = _board.getComponents();
	for(QMap< QString, IComponent *>::const_iterator comp = comps.begin(); comp != comps.end(); ++comp)
		(*comp)->makeVisual(*this);
	
	// add edges
	QSet<VisualPin *> done;
	for(QMap<Pin *, VisualPin *>::const_iterator pin = pins.begin(); pin != pins.end(); ++pin)
		if(!done.contains(*pin) && (*pin)->pin()->connection()) {
			VisualPin *tpin = pins.value((*pin)->pin()->connection(), 0);
			if(tpin)
				_scene.addItem(makeEdge((*pin)->x(), (*pin)->y(), (*pin)->dir(), tpin->x(), tpin->y(), tpin->dir()));
		}
}


/**
 * @fn Board& VisualGraph::board(void) const;
 * Get the represented board.
 * @return	Represented board.
 */


/**
 * @fn const QList<VisualComponent *>& VisualGraph::components(void) const;
 * Get the list of components.
 * @return	List of components.
 */


/**
 */
void VisualGraph::add(VisualComponent *component) {
	_comps.push_back(component);
	_scene.addItem(component);
	const QMap<Pin *, VisualPin *>& pins = component->pins();
	for(QMap<Pin *, VisualPin *>::const_iterator pin = pins.begin(); pin != pins.end(); ++pin)
		this->pins.insert((*pin)->pin(), *pin);
}


/**
 */
void VisualGraph::add(IComponent *component) {
	add(new VisualComponent(component));
}


/**
 * Clear all visual components.
 */
void VisualGraph::clear(void) {
	for(QList<VisualComponent *>::const_iterator i = _comps.begin(); i != _comps.end(); i++)
		_scene.removeItem(*i);		
	_comps.clear();
	pins.clear();
}

} // bsim
