/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <QMap>

#include <CHighlighter.hpp>

namespace bsim {

inline bool isBin(QChar c)		{ return c == '0' || c == '1'; }
inline bool isOctal(QChar c)	{ return c >= '0' && c <= '7'; }
inline bool isDecimal(QChar c)	{ return c >= '0' && c <= '9'; }
inline bool isHex(QChar c) { return isDecimal(c) || (c >= 'a' && c <= 'f') || (c >= 'A' || c <= 'F'); }
inline bool isFirstID(QChar c) { return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '.' || c == '_'; }
inline bool isID(QChar c) { return isFirstID(c) || (c >= '0' && c <= '9'); }
inline bool isSpace(QChar c)	{ return c == ' ' || c == '\t'; }

static bool parseID(const QString& text, int& p) {
	if(p >= text.length())
		return false;
	if(!isFirstID(text[p]))
		return false;
	p++;
	while(p < text.length() && isID(text[p]))
		p++;
	return true;
}

static bool parseDecimal(const QString& text, int& p) {
	if(p >= text.length())
		return false;
	if(!isDecimal(text[p]))
		return false;
	p++;
	while(p < text.length() && isDecimal(text[p]))
		p++;
	return true;
}

static bool parseConstant(const QString& text, int& p) {
	if(p >= text.length())
		return false;
		
	// other
	if(text[p] == '0') {
		if(p >= text.length())
			return true;
		
		// binary
		if(text[p] == 'b' || text[p] == 'B') {
			p++;
			while(p < text.length() && isBin(text[p]))
				p++;
		}
		
		// hexadecimal
		else if(text[p] == 'x' || text[p] == 'X') {
			p++;
			while(p < text.length() && isHex(text[p]))
				p++;
		}
		
		// octal
		else
			while(p < text.length() && isOctal(text[p]))
				p++;
		return true;
	}
	
	// decimal
	else
		return parseDecimal(text, p);
}


/**
 */
CSyntax::CSyntax(const CHighlighter& _high, QTextDocument *document)
: QSyntaxHighlighter(document), high(_high)
{
	// keyword map initialization
	struct {
		const char *name;
		const TokenStyle *style;
	} styles[] = {
		{ "and",			&high.op },
		{ "asm",			&high.keyword },
		{ "auto",			&high.type },
		{ "bool",			&high.type },
		{ "break", 			&high.keyword },
		{ "case", 			&high.keyword },
		{ "catch", 			&high.keyword },
		{ "char",			&high.type },
		{ "class",			&high.type },
		{ "const",			&high.type },
		{ "const_cast",		&high.op },
		{ "constexpr",		&high.type },
		{ "continue",		&high.keyword },
		{ "decltype", 		&high.op },
		{ "default", 		&high.keyword },
		{ "delete", 		&high.keyword },
		{ "do", 			&high.keyword },
		{ "double",			&high.type },
		{ "dynamic_cast",	&high.op },
		{ "else",			&high.keyword },
		{ "enum",			&high.type },
		{ "explicit",		&high.type },
		{ "export",			&high.type },
		{ "extern",			&high.type },
		{ "false",			&high.number },
		{ "float",			&high.type },
		{ "friend",			&high.preproc },
		{ "for",			&high.keyword },
		{ "go", 			&high.keyword },
		{ "if", 			&high.keyword },
		{ "inline",			&high.type },
		{ "int",			&high.type },
		{ "int8_t",			&high.type },
		{ "int16_t",		&high.type },
		{ "int32_t",		&high.type },
		{ "int64_t",		&high.type },
		{ "long",			&high.type },
		{ "mutable",		&high.type },
		{ "namespace",		&high.preproc },
		{ "new",			&high.keyword },
		{ "noexcept",		&high.type },
		{ "not",			&high.op },
		{ "nullptr",		&high.number },
		{ "operator",		&high.op },
		{ "or",				&high.op },
		{ "private",		&high.preproc },
		{ "protected",		&high.preproc },
		{ "public",			&high.preproc },
		{ "register", 		&high.type },
		{ "reinterpret_cast", 	&high.op },
		{ "return", 		&high.keyword },
		{ "short",			&high.type },
		{ "signed",			&high.type },
		{ "sizeof",			&high.op },
		{ "static",			&high.type },
		{ "static_cast",	&high.op },
		{ "struct",			&high.type },
		{ "switch", 		&high.keyword },
		{ "template",		&high.type },
		{ "throw",			&high.keyword },
		{ "this",			&high.number },
		{ "true",			&high.number },
		{ "typedef",		&high.type },
		{ "try",			&high.keyword },
		{ "typename",		&high.type },
		{ "typeid",			&high.op },
		{ "uint8_t",		&high.type },
		{ "uint16_t",		&high.type },
		{ "uint32_t",		&high.type },
		{ "uint64_t",		&high.type },
		{ "union",			&high.type },
		{ "unsigned",		&high.type },
		{ "using",			&high.preproc },
		{ "virtual",		&high.type },
		{ "void",			&high.type },
		{ "volatile",		&high.type },
		{ "while", 			&high.keyword },
		{ 0, 0 }
	};
	
	for(int i = 0; styles[i].name; i++)
		kws.insert(styles[i].name, styles[i].style);
}


/**
 */
void CSyntax::highlightBlock(const QString &text) {
	int p = 0;

	// continuing a comment
	if(previousBlockState() == in_comment) {
		p = text.indexOf("*/");
		
		// continuing comment
		if(p < 0) {
			setCurrentBlockState(in_comment);
			setFormat(0, text.length() - p, high.comment.format);
			return;
		}
		
		// end of comment
		else {
			p += 2;
			setFormat(0, p, high.comment.format);
		}
	}
	setCurrentBlockState(out_comment);
	
	while(p < text.length()) {
		
		// comment
		if(text[p] == '/' && text.length() >= p + 1 && text[p + 1] == '*') {
			int q = p;
			while(p < text.length() - 1 && (text[p] != '*' || text[p + 1] != '/'))
				p++;
			if(p == text.length() - 1) {
				setCurrentBlockState(in_comment);
				setFormat(q, text.length() - q, high.comment.format);
				return;
			}
			else {
				p += 2;
				setFormat(q, p - q, high.comment.format);
				continue;
			}
		}

		// identifier case
		if(isFirstID(text[p])) {
			int q = p;
			parseID(text, p);
			QString id = text.mid(q, p - q);
			QMap<QString, const TokenStyle *>::iterator i = kws.find(id);
			if(i != kws.end())
				setFormat(q, p - q, (*i)->format);
			continue;
		}
		
		// number case
		if(isDecimal(text[p])) {
			int q = p;
			if(parseConstant(text, p))
				setFormat(q, p - q, high.number.format);
		}
		
		// other characters
		switch(text[p].cell()) {
		case '+':
		case '-':
		case '*':
		case '/':
		case '%':
		case '<':
		case '>':
		case '=':
		case '!':
		case '&':
		case '|':
		case '~':
		case '^':
			setFormat(p, 1, high.op.format);
			break;
		case '(':
		case ')':
		case '{':
		case '}':
		case '[':
		case ']':
			setFormat(p, 1, high.braces.format);
			break;
		case '"': {
				int q = p;
				p = text.indexOf('"', q + 1);
				if(p < 0)
					p = text.length();
				setFormat(q, p - q + 1, high.number.format);
			}
			break;
		case '\'': {
				int q = p;
				p = text.indexOf('\'', q + 1);
				if(p < 0)
					p = text.length();
				setFormat(q, p - q + 1, high.number.format);
			}
			break;
		case '#':
			setFormat(p, text.length() - p, high.preproc.format);
			return;
		default:
			break;
		}
		p++;
	}
	
}


/**
 * @class AssemblyHighlighter
 * Highlighter for assembly code (usually .s or .i extensions).
 */
CHighlighter::CHighlighter(void):
	Highlighter("C"),
	comment("comment"),
	keyword("keyword"),
	type("type"),
	number("number"),
	string("string"),
	op("operator"),
	preproc("preproc"),
	braces("braces")
{
	// configure the styles
	comment.format.setForeground(QColor("green"));
	comment.format.setFontItalic(true);
	keyword.format.setForeground(QColor("red"));
	keyword.format.setFontWeight(QFont::Bold);
	type.format.setForeground(QColor("purple"));
	number.format.setForeground(QColor("blue"));
	op.format.setFontWeight(QFont::Bold);
	braces.format.setFontWeight(QFont::Bold);
	preproc.format.setFontWeight(QFont::Bold);
	preproc.format.setForeground(QColor("darkcyan"));

	// add to the highlighter
	add(&comment);
	add(&keyword);
	add(&type);
	add(&number);
	add(&string);
	add(&op);
	add(&preproc);
	add(&braces);
}


/**
 */
QSyntaxHighlighter *CHighlighter::make(QTextDocument *document) const {
	return new CSyntax(*this, document);
}

}	// bsim
