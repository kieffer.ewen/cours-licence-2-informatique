/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include "RegisterDockWidget.hpp"
#include <IComponent.hpp>
#include <Board.hpp>
#include <IRegister.hpp>
#include <QDebug>
#include <QSettings>
#include <QClipboard>
#include "MainWindow.hpp"

namespace bsim {

/**
 * Abstract class to implements node actions.
 */
class AbstractRegisterNode: public QTreeWidgetItem {
public:
	virtual QString name(void) = 0;
	AbstractRegisterNode(QTreeWidget *tree): QTreeWidgetItem(tree) { }
	AbstractRegisterNode(QTreeWidgetItem *parent): QTreeWidgetItem(parent) { }
	virtual void onSetDisplay(int display) = 0;
	virtual void update(void) = 0;
	virtual void load(QSettings& settings) = 0;
	virtual void save(QSettings& settings) = 0;
	virtual bool supportsCopy(void) { return false; }
	virtual QString copy(void) { return ""; }
};



/**
 * Node parent of other nodes.
 */
class ParentNode: public AbstractRegisterNode {
public:
	ParentNode(QTreeWidget *tree): AbstractRegisterNode(tree) { }
	ParentNode(QTreeWidgetItem *parent): AbstractRegisterNode(parent) { }

	virtual void onSetDisplay(int display) {
		for(int i  = 0; i < childCount(); i++)
			static_cast<AbstractRegisterNode *>(child(i))->onSetDisplay(display);
	}

	virtual void update(void) {
		for(int i  = 0; i < childCount(); i++)
			static_cast<AbstractRegisterNode *>(child(i))->update();
	}

	virtual void load(QSettings& settings) {
		loadExpand(settings);
		for(int i  = 0; i < childCount(); i++) {
			AbstractRegisterNode *r = static_cast<AbstractRegisterNode *>(child(i));
			settings.beginGroup(r->name());
			r->load(settings);
			settings.endGroup();
		}
	}
	
	void loadExpand(QSettings& settings) {
		QVariant expand = settings.value("expand");
		if(!expand.isNull())
			setExpanded(expand.toBool());
	}
	
	virtual void save(QSettings& settings) {
		saveExpand(settings);
		for(int i  = 0; i < childCount(); i++) {
			AbstractRegisterNode *r = static_cast<AbstractRegisterNode *>(child(i));
			settings.beginGroup(r->name());
			r->save(settings);
			settings.endGroup();
		}
	}
	
	void saveExpand(QSettings& settings) {
		bool expand = isExpanded();
		if((expand && !settings.contains("expand"))
		|| (settings.contains("expand") && settings.value("expand") != expand))
			settings.setValue("expand", expand);
	}

};


/**
 * Node for the component.
 */
class ComponentNode: public ParentNode {
public:
	ComponentNode(QTreeWidget *tree, IComponent *component);
	virtual QString name(void) { return comp->name(); }

private:
	IComponent *comp;	
};


/**
 * Node for a bank of registers.
 */
class BankNode: public ParentNode {
public:
	BankNode(AbstractRegisterNode *parent, IRegister *_reg);
	virtual QString name(void) { return reg->displayName(); }

	virtual void load(QSettings& settings) {
		loadExpand(settings);
		settings.beginReadArray("bank");
		for(int i  = 0; i < childCount(); i++) {
			AbstractRegisterNode *r = static_cast<AbstractRegisterNode *>(child(i));
			settings.setArrayIndex(i);
			r->load(settings);
		}
		settings.endArray();
	}
	
	virtual void save(QSettings& settings) {
		saveExpand(settings);
		settings.beginReadArray("bank");
		for(int i  = 0; i < childCount(); i++) {
			AbstractRegisterNode *r = static_cast<AbstractRegisterNode *>(child(i));
			settings.setArrayIndex(i);
			r->save(settings);
		}
		settings.endArray();
	}

private:
	IRegister *reg;
};


/**
 * Node for a simple register.
 */
class RegisterNode: public AbstractRegisterNode {
public:
	RegisterNode(AbstractRegisterNode *parent, IRegister *_reg, int _n)
	: 	AbstractRegisterNode(parent),
		reg(_reg),
		display(IRegister::DEF),
		n(_n)
	{
		setText(0, reg->displayName(n));
	}

	virtual QString name(void) { return reg->displayName(n); }

	virtual void onSetDisplay(int _display) {
		if(_display == display)
			return;
		display = _display;
		show();
	}

	virtual void update(void) {
		if(reg->isReadable() && !isHidden()) {
			QString t = reg->format(n, display);
			if(text(1) != t) {
				setText(1, t);				
				setTextColor(1, QColor("red"));
			}
			else
				setTextColor(1, QColor("black"));
		}
	}

	virtual void load(QSettings& settings) {
		QVariant v = settings.value("display");
		if(!v.isNull()) {
			display = v.toInt();
		}
	}
	
	virtual void save(QSettings& settings) {
		if(display != IRegister::DEF)
			settings.setValue("display", display);
	}

	virtual bool supportsCopy(void) {
		return true;
	}
	
	virtual QString copy(void) {
		return text(1);
	}

private:
	void show(void) {
		if(reg->isReadable())
			setText(1, reg->format(n, display));
	}

	IRegister *reg;
	int display;
	int n;
};


ComponentNode::ComponentNode(QTreeWidget *tree, IComponent *component)
:	ParentNode(tree), comp(component) {
		
	// initialize itself
	setText(0, comp->name());
		
	// fill its registers
	const QVector<IRegister *>& regs = comp->getRegisters();
	for(QVector<IRegister *>::const_iterator reg = regs.begin(); reg != regs.end(); ++reg) {
		if((*reg)->count() == 1)
			new RegisterNode(this, *reg, -1);
		else
			new BankNode(this, *reg);
	}
}


BankNode::BankNode(AbstractRegisterNode *parent, IRegister *_reg)
: ParentNode(parent), reg(_reg) {
	setText(0, reg->displayName());
	for(int i = 0; i < reg->count(); i++)
		new RegisterNode(this, reg, i);
}


/**
 * Build the register dock widget.
 * @param name		Name of the dock.
 * @param parent	Parent widget.
 */
RegisterDockWidget::RegisterDockWidget(MainWindow& window): _window(window) {
	setObjectName("register-view");
	setAllowedAreas(Qt::AllDockWidgetAreas) ;
	setWindowTitle("Registers");
	tw = new QTreeWidget();
	tw->setColumnCount(2);
	QList<QString>* hlist = new QList<QString>() ;
	hlist->append("Register");
	hlist->append("Value");
	tw->setHeaderLabels(*hlist);
	tw->setColumnWidth(0, 150);
	setWidget(tw);
	
	// initialize the contextual menu
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(startContextualMenu(const QPoint &)));
	
	// prepare the timer
	timer.setInterval(250);
	QObject::connect(&timer, SIGNAL(timeout()), this, SLOT(timeout()));

	// prepare the contextual menu
	menu.addAction(tr("default display"), this, SLOT(setDef()));
	menu.addAction(tr("binary display"), this, SLOT(setBin()));
	menu.addAction(tr("decimal display"), this, SLOT(setDec()));
	menu.addAction(tr("hexadecimal display"), this, SLOT(setHex()));
	menu.addSeparator();
	copy_action = menu.addAction(tr("copy"), this, SLOT(copy()));

	// add to the window
	window.addDockWidget(Qt::LeftDockWidgetArea, this) ;
	window.getViewMenu()->addAction(toggleViewAction());
}


/**
 * Start the contextual menu.
 */
void RegisterDockWidget::startContextualMenu(const QPoint& pt) {
	current = static_cast<AbstractRegisterNode *>(tw->currentItem());
	copy_action->setEnabled(current->supportsCopy());
	if(current)
		menu.exec(mapToGlobal(pt));
}

void RegisterDockWidget::setDef(void) {
	current->onSetDisplay(IRegister::DEF);
}

void RegisterDockWidget::setDec(void) {
	current->onSetDisplay(IRegister::DEC);
}

void RegisterDockWidget::setHex(void) {
	current->onSetDisplay(IRegister::HEX);
}

void RegisterDockWidget::setBin(void) {
	current->onSetDisplay(IRegister::BIN);
}


/**
 * Set the current board.
 */
void RegisterDockWidget::onBoardLoad(void) {
	_board = &_window.board();
	QMap<QString, IComponent *>& comps = _board->getComponents();
	for(QMap<QString, IComponent *>::const_iterator comp = comps.begin(); comp != comps.end(); comp++) {
		const QVector<IRegister *>& regs = (*comp)->getRegisters();
		if(!regs.empty())
			new ComponentNode(tw, *comp);
	}
}


/**
 */
void RegisterDockWidget::onClear(void) {
	while(tw->topLevelItemCount() > 0) {
		QTreeWidgetItem *item = tw->takeTopLevelItem(0);
		delete item;
	}
}


/**
 * Update the values of the registers.
 */
void RegisterDockWidget::update() {
	for(int i = 0; i < tw->topLevelItemCount(); i++)
		static_cast<AbstractRegisterNode *>(tw->topLevelItem(i))->update();
}


/**
 * Called each time registers must be refreshed  by the timer.
 */
void RegisterDockWidget::timeout(void) {
	update();
}


/**
 * Start the timer to refresh periodically the register display.
 */
void RegisterDockWidget::onRun(void) {
	timer.start();
}


/**
 * Start the timer to refresh periodically the register display.
 */
void RegisterDockWidget::onBreak(void) {
	timer.stop();
}


/**
 */
void RegisterDockWidget::loadSettings(QSettings& settings) {
	settings.beginGroup("registers");
	for(int i = 0; i < tw->topLevelItemCount(); i++) {
		AbstractRegisterNode *node = static_cast<AbstractRegisterNode *>(tw->topLevelItem(i));
		settings.beginGroup(node->name());
		node->load(settings);
		settings.endGroup();
	}
	settings.endGroup();
}


/**
 */
void RegisterDockWidget::saveSettings(QSettings& settings) {
	settings.beginGroup("registers");
	for(int i = 0; i < tw->topLevelItemCount(); i++) {
		AbstractRegisterNode *node = static_cast<AbstractRegisterNode *>(tw->topLevelItem(i));
		settings.beginGroup(node->name());
		node->save(settings);
		settings.endGroup();
	}
	settings.endGroup();
}


/**
 * Copy current register value to clipboard.
 */
void RegisterDockWidget::copy(void) {
	QClipboard *cb = QApplication::clipboard();
	cb->setText(current->copy());
}

} // bsim

