/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
 
#include <QFileInfo>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <Path.hpp>

namespace bsim  {

/**
 * @class Path
 * Represents a path in the host file system.
 */


/**
 * Test if the path is absolute.
 * @return	True if it is absolute, false else.
 */
bool Path::isAbsolute(void) const {
	return str.startsWith(":");
}


/**
 * Test if the path is relative.
 * @return	True if it is relative, false else.
 */
bool Path::isRelative(void) const {
	return !str.startsWith(":");
}


/**
 * Convert path to a C string.
 * @return	Matching C string.
 */
const char *Path::toCString(void) const {
	bytes = str.toLocal8Bit(); 
	return bytes.data();
}


/**
 * Test if the file matching the current path exists.
 * @return	True if the file exists, false else.
 */
bool Path::exists(void) const {
	struct stat st;
	return stat(toCString(), &st) == 0;
}


/**
 * Test if a file exists and is readable.
 * @return	True if file exists and is readable, false else.
 */
bool Path::isReadable(void) const {
	return access(toCString(), F_OK | R_OK) == 0;
}


/**
 * Get the parent path. If the path is the root path, return the root path.
 * @return	Parent path.
 */	
Path Path::parent(void) const {
	
	// normalize the path
	QString res = str;
	int p = res.lastIndexOf('/');
	while(p == res.length() - 1) {
		res = res.left(p);
		p = res.lastIndexOf('/');
	}
	
	// process the path
	if(res == "")
		return "/";
	else if(res.endsWith("/.."))
		return res + "/..";
	else if(p < 0)
		return ".";
	else
		return res.left(p);
}


/**
 * Join two paths. If the given path is absolute, return it.
 * Else join both paths.
 * @param path		Path to join.
 * @return			Result of path join.
 */
Path Path::join(const Path& path) const {
	if(path.isAbsolute())
		return path;
	else if(str.endsWith("/"))
		return str + path.str;
	else
		return str + "/" + path.str;
}


/**
 * Assign the given path to the current.
 * @param path	Path to assign.
 */
void Path::set(const Path& path) {
	str = path.str;
}


/**
 * Compute the extension of the file.
 * @return	File extension.
 */
QString Path::extension(void) const {
	int p = str.lastIndexOf('.');
	if(p < 0)
		return QString();
	else
		return str.mid(p + 1);
}


/**
 * Return the last modification time.
 */
QDateTime Path::lastModif() {
	QFileInfo f(str);
	return f.lastModified();
}

}	// bsim
