/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <TimerScheduler.hpp>
#include <Board.hpp>
#include <Core.hpp>

namespace bsim {

/**
 * @class TimerScheduler
 * This driver uses timer from the UI to execute periodically sets
 * of instructions. The period of execution is 1 second / number of slices
 * configured in this driver and the number of instructions executed
 * by a core at each activation is frequency of the core / number of slices.
 */

/**
 */
TimerScheduler::TimerScheduler(Board& board): Scheduler(board), _slices(default_slices) {
	timer.setInterval(1000 / _slices);
	connect(&timer, SIGNAL(timeout(void)), this, SLOT(timeout(void)));
}


/**
 * @fn TimerScheduler::slices(void) const;
 * Get the current number of slices.
 */


/**
 * Set the current number of slices.
 * @param slices	New number of slices.
 */
void TimerScheduler::setSlices(int slices) {
	_slices = slices;
	timer.setInterval(1000 / _slices);
}


/**
 */
void TimerScheduler::start(void) {
	Scheduler::start();
	paused = board().cores();
	for(QList<Core *>::const_iterator core = paused.begin(); core != paused.end(); ++core)
		(*core)->start();
}


/**
 */
void TimerScheduler::stop(void) {
	timer.stop();
	paused.append(running);
	for(QList<Core *>::const_iterator core = paused.begin(); core != paused.end(); ++core)
		(*core)->stop();
	paused.clear();
	running.clear();
}


/**
 */
void TimerScheduler::pause(Core *core) {
	paused.push_back(core);
	handler().onDone(core);
	running.removeOne(core);
	if(running.empty())
		timer.stop();
}


/**
 */
void TimerScheduler::resume(Core *core) {
	if(running.empty())
		timer.start();
	running.push_back(core);
	paused.removeOne(core);
}


/**
 */
void TimerScheduler::pauseAll(void) {
	if(!running.empty())
		timer.stop();
	for(QList<Core *>::const_iterator core = running.begin(); core != running.end(); ++core)
		handler().onDone(*core);
	paused.append(running);
	running.clear();
}


/**
 */
void TimerScheduler::resumeAll(void) {
	if(running.empty())
		timer.start();
	running.append(paused);
	paused.clear();
}


/**
 * Handle a timeout event.
 */
void TimerScheduler::timeout(void) {
	if(running.empty()) {
		timer.stop();
		return;
	}
	todo = running;
	for(QList<Core *>::const_iterator core = todo.begin(); core != todo.end(); ++core) {
		int n = (*core)->board().mck() / _slices;
		for(int i = 0; i < n; i++) {
			processEvents();
			(*core)->step();
			increaseTime();
			if(handler().stops(*core)) {
				pause(*core);
				break;
			}
		}
	}
}


/**
 */
void TimerScheduler::step(void) {
	for(QList<Core *>::const_iterator core = paused.begin(); core != paused.end(); ++core) {
		processEvents();
		(*core)->step();
		increaseTime();
		handler().onDone(*core);
	}
}

}	// bsim
