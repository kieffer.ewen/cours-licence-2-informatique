/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <DisasmSource.hpp>
#include <Board.hpp>

namespace bsim {

/**
 * @class DisasmSource
 * ISource implementation for disassembled code.
 */


/**
 * Build a disassembly source.
 * @param prog	Program to disassemble.
 */
DisasmSource::DisasmSource(Program *prog): _prog(prog) {
}


/**
 * Called to update the displayed program.
 * @param prog	New program.
 */
void DisasmSource::update(Program *prog) {
	_prog = prog;
	text = QString();
	addr_map.clear();
	line_map.clear();
	init();
}


/**
 */
QString DisasmSource::name(void) {
	return "Disassembly";
}


/**
 */
QString DisasmSource::getText(void) {
	init();
	return text;
}


/**
 */
uint32_t DisasmSource::getAddress(int line) {
	init();
	return addr_map.value(line, 0);
}


/**
 */
bool DisasmSource::isAddressable(int line) {
	init();
	return addr_map.contains(line);
}


/**
 */
int DisasmSource::getLine(uint32_t address) {
	init();
	return line_map.value(address, -1);
}


/**
 * Initialize the data of the source.
 */
void DisasmSource::init(void) {
	if(!text.isNull())
		return;
	text = "";
	int line = 1;
	
	// visit all sections
	QList<Program::section_t> sections;
	_prog->getSections(sections);
	for(QList<Program::section_t>::const_iterator sect = sections.begin(); sect != sections.end(); sect++) {
		
		// disassemble all instructions of the section
		for(uint32_t addr = (*sect).base; addr < (*sect).base + (*sect).size; addr += _prog->size(addr), line++) {
			QString dis = _prog->disasm(addr);
			text = text + mkAddr(addr) + "    " + dis + "\n";
			addr_map.insert(line, addr);
			line_map.insert(addr, line);
			//qDebug() << mkAddr(addr) << " -> " << line;
			//qDebug() << mkAddr(addr) << " in [" << mkAddr((*sect).base) << ", " << mkAddr((*sect).base + (*sect).size) << "]";
		}
	}
}

}	// bsim
