#include "PrintValue.hpp"

QString PrintValue::makeQStringFromInt(int v) {
  QString ret ;
  if (binary){
    QString val=changeBase(v,2);
    ret = QString("0b%1").arg(val) ;
  } else if (hexa) {
    QString val=changeBase(v,16);
    ret = QString("0x%1").arg(val);
  } else {
    ret = QString("%1").arg(v);
  }
  return ret ;
}

void PrintValue::setHexa() {
  hexa = true ;
  binary = false ;
}

void PrintValue::setBinary() {
  hexa = false ;
  binary = true ;
}

void PrintValue::setDecimal() {
  hexa = false ;
  binary = false ;
}

QString PrintValue::changeBase(int n, int base){
  int reste,cpt=0;
  QString baseN="";
  if (n<0){n=(-n)+pow(2,31);}
  if (n==0){return QString::number(0);}
  while (n>0){
    reste=n%base;
    n=n/base;
    if (base==16){
      if(reste>=0 && reste<10){
        baseN=QString::number(reste)+baseN;
      }
      if(reste==10){baseN="A"+baseN;}
      if(reste==11){baseN="B"+baseN;}
      if(reste==12){baseN="C"+baseN;}
      if(reste==13){baseN="D"+baseN;}
      if(reste==14){baseN="E"+baseN;}
      if(reste==15){baseN="F"+baseN;}
    }else{
      if (((cpt%4) == 0) && (n!=0)){
        baseN="  "+baseN;
      }
      cpt++;
      baseN=QString::number(reste)+baseN;
    }
  }
  return baseN;
}
