/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <Pin.hpp>
#include <IComponent.hpp>
#include <Board.hpp>
#include <Scheduler.hpp>
#include <QDebug>

//#define TRACE_PIN
#ifdef TRACE_PIN
#	define TRACE(c)		c
#else
#	define TRACE(c)
#endif

namespace bsim {

/**
 * @class Pin
 * A pin is inserted in a port to communicate with it.
 * 
 * According to the type of communication, the pin provides different
 * interfaces. Notice that are communication method has for first argument
 * the time of the event causing the change on the wire to allow
 * either to mimic propagation time on the wire, or to let the target component
 * to react at a convenient time.
 * 
 * For usual signal communication (0 or 1), the signal interface is used.
 * Each change is passed to the Pin::change() method to be propagated
 * on the wired and received by overloading the method Pin::onChange().
 * 
 * To avoid to slowdown the simulation, more complex communication like
 * serial one must be processed using the serial interface. The main idea
 * is that word are exchanged between components but the exchange is only
 * successful if both components are configured in the same way.
 * Therefore, the methond Pin::configure() and Pin::onConfigure() allows
 * to send and to receive a configuration while the method Pin::send()
 * and Pin::onReceive() allows to exchange words. Take a look at the
 * SerialPin class that provides an automatic handling of the configuration
 * problem.
 * 
 * The electric interface is used to connect together components supporting
 * voltage and current values. Notice that this interface is not supposed
 * to implement a complete electric circuit simulation but, rather, to
 * represent the delivery of power from electric source to electric consumer.
 * It shouldn't be used to simulate low-level component of electric circuits
 * as resistors or capacitors.
 */


/**
 * Build a PIN.
 * @param component	Owner component.
 * @param name		Name of the pin.
 */
Pin::Pin(IComponent *component, QString name)
: _name(name), _pin(0), _comp(component) {
}


/**
 * @fn QString Port::name(void) const;
 * Get the name of the port.
 * @return Port name.
 */


/**
 * This method is called each the port signal connected to the pin changes.
 * As a default, do nothing.
 * @param time	Time of the change.
 * @param value	New pin value.
 */
void Pin::onChange(uint64_t time, bool value) {
}


/**
 * Used to get the name of the pin is the pin is used to issue
 * an interrupt.
 * @param Interrupt name.
 */
QString Pin::asIRQ(void) {
	return component()->name();
}


/**
 * Connect a pin to the port.
 * @param pin	Pin to connect.
 */
void Pin::connect(Pin *pin) {
	if(_pin)
		_pin->_pin = 0;
	_pin = pin;
	if(_pin)
		pin->_pin = this;
}


/**
 * @fn Pin *Pin::connection(void) const;
 * Get the currently connected pin.
 * @return Connected pin.
 */


/**
 * Cause a change signal to be sent to the pin (if any).
 * @param time	Time of the signal.
 * @param value	Value of the signal.
 */
void Pin::change(uint64_t time, bool value) {
	if(_pin)
		_pin->onChange(time, value);
}


/**
 * Mode attribute of the serial communication: one of NORMAL, RS285,
 * HARDWARE_HANDSHAKING, MODEM, ISO7816_NT, IDO7816_T or IRDA.
 */
QString Pin::MODE = "mode";


/**
 * Attribute describing word length in bits.
 */
QString Pin::WORDL = "wordl";


/**
 * Boolean attribute describing wether the communication is synchronous or not.
 */
QString Pin::SYNC = "sync";


/** 
 * Attribute describing the parity check mode: one of NOPAR, ODD,
 * EVEN, FORCED_1,FORCED_0 or MULTIDROP.
 */
QString Pin::PARITY = "parity";


/**
 * Attribute describing the number of stop bits (in half bits).
 */
QString Pin::NBSTOP = "nbstop";


/**
 * Attribute describing if the Most Significant Bit is in the first position (MSB_FIRST)
 * or in the last position (MSB_LAST).
 */
QString Pin::MSB = "msb";


/**
 * Attribute describing the baud rate of the communication (in bauds).
 */
QString Pin::RATE = "rate";


/**
 * Attribute describing the channel mode: one of NORMAL_CH, ECHO, LOCAL_LOOPBACK or REMOTE_LOOPBACK.
 */
QString Pin::CHMODE = "chmode";


/**
 * This method is part of the serial interface and is called to emit
 * a word on the wire.
 * @param time		Time of emission.
 * @param word		Send word.
 */
void Pin::send(uint64_t time, uint32_t word) {
	TRACE(qDebug() << "DEBUG: send(" << (char)word << ")";)
	if(_pin)
		_pin->onReceive(time, word);
}


/**
 * This method is part of the serial interface and is called
 * each time a word is emitted on the wire connected to the pin.
 * As a default, no nothing.
 * @param time		Time of emission.
 * @param word		Emitted word.
 */
void Pin::onReceive(uint64_t time, uint32_t word) {
}


/**
 * This method is part of the serial interface and allows to pass
 * the current configuration of the pin to the wired pin.
 * The configuration allows to check if the word tranmission perform well
 * or not.
 * @param time				Time of configuration.
 * @param configuration	New configuration.
 */
void Pin::configure(uint64_t time, const QMap<QString, QVariant>& configuration) {
	if(_pin)
		_pin->onConfigure(time, configuration);
}


/**
 * This method is part of the serial interface and allows to be aware
 * of a configuration changed from the linked pin. As a default, do nothing.
 * @param time				Time of configuration.
 * @param configuration	New configuration.
 */
void Pin::onConfigure(uint64_t time, const QMap<QString, QVariant>& configuration) {
}


/**
 * This method is part of the analog interface of a pin: it allows
 * to exchange voltage on a line, for example, between a sensor and an ADC.
 * This function allows to inform the linked pin the change in the voltage.
 * The receiveAnalog() method of the linked pin, if any, is automatically called.
 * @param time		Time of the event.
 * @param volt		New voltage on the line.
 */
void Pin::sendAnalog(uint64_t time, double volt) {
	if(_pin)
		_pin->receiveAnalog(time, volt);
}


/**
 * This method is part of the analog interface of a pin: it allows
 * to exchange voltage on a line, for example, between a sensor and an ADC.
 * This functions allows to receive a change on the analog state of the line
 * and must be overrident to handle the event.
 * @param time		Time of the event.
 * @param volt		New voltage on the line.
 */
void Pin::receiveAnalog(uint64_t time, double volt) {
}


/**
 * This method is part of the electric interface and allows to
 * set the voltage and the current delivered on the pin.
 * @param t		Time of change.
 * @param U		New voltage.
 * @param I		New current.
 */
void Pin::setElectric(time_t t, double U, double I) {
	if(_pin)
		_pin->getElectric(t, U, I);
}


/**
 * This function is part of the electric interface.
 * It is called by the pin that is linked to this one when
 * the voltage and the current are changed. The default implementation
 * does nothing.
 * @param t		Time of change.
 * @param U		New voltage.
 * @param I		New current.
 */
void Pin::getElectric(time_t t, double U, double I) {
}


/**
 * @typedef Pin::unit_t;
 * Type used in the physics interface of a @ref Pin to represent the type
 * of transported value.
 */

/**
 * @var Pin::NO_UNIT;
 * No unit.
 */

/**
 * @var Pin::HEAT;
 * Heat unit: Celsius degree.
 */

/**
 * Called to change the physics value over the given pin (part of the
 * physics interface).
 * @param time	Change time.
 * @param value	Changed value.
 * @param unit	Changed unit.
 */	
void Pin::setPhysics(time_t time, double value, unit_t unit) {
	if(_pin)
		_pin->getPhysics(time, value, unit);
}

/**
 * This function must be ovveriden to receive information in the physics
 * interface: it is called each time the physics value of the connected
 * pin is changed. It is part of the physics interface. The default
 * implementation does nothing.
 * @param time	Change time.
 * @param value	Changed value.
 * @param unit	Changed unit.
 */
void Pin::getPhysics(time_t time, double value, unit_t unit) {
}


/**
 * @class SerialPin
 * A serial pin provides an automatic management of a serial communication.
 * 
 * The configuration of the line is done using the SerialPin::get() and
 * SerialPin::set() methods (ensuring the transmission
 * of the configuration).
 * 
 * To send a word, the usual method Pin::send() is used but, for the reception,
 * two methods may be overload:
 * @li SerialPin::onWord() - called when a successful word is received,
 * @li SerialPin::onError() - called when a failed word is recevied.
 * All work of configuration compatibility checking is performed by this
 * class and applied on the word reception.
 */


/**
 */
void SerialPin::onReceive(uint64_t time, uint32_t word) {
	TRACE(qDebug() << "SerialPin::onReceive(" << char(word) << ")";)
	
	if(error_id.isNull())
		onWord(time, word);
	else
		onError(time, error_id);
}


/**
 */
void SerialPin::onConfigure(uint64_t time, const config_t& configuration) {
	error_id = QString::null;
	for(config_t::const_iterator item = my_conf.begin(); item != my_conf.end(); ++item) {
		if(item.key() == RATE) {
			int rate1 = item.value().toInt();
			int rate2 = configuration[item.key()].toInt();
			if(abs(rate1 - rate2) > rate1 / 10) {
				error_id = item.key();
				return;
			}
		}
		else if(item.value() != configuration[item.key()]) {
			error_id = item.key();
			return;
		}
	}
}


/**
 * @fn void SerialPin::onWord(uint64_t time, uint32_t word);
 * Method to overload to get a successfully transported word.
 * @param time		Time of communication.
 * @param word		Transmitted word.
 */


/**
 * @fn void SerialPin::onError(uint64_t time, uint64_t time, QString id);
 * Method to overload to be alerted of an unsuccessful transportation of a word.
 * @param time		Time of communication.
 * @param id		Identifier of the attribute causing the communication failure.
 */


/**
 * @fn QVariant SerialPin::get(QString id) const;
 * Get the value of a configuration attribute.
 * @param id	Attribute Identifier.
 * @return		Attribute value.
 */


/**
 * This method changes an attribute of the communication configuration
 * used by this pin.
 * @param id		Attribute identifier.
 * @param value	Value of the attribute.
 */
void SerialPin::set(QString id, QVariant value) {                    //
	if(value != my_conf[id]) {
		my_conf[id] = value;
		configure(component()->board().scheduler()->time(), my_conf);
	}
}




}	// bsim
