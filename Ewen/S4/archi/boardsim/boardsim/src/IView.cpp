/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <IView.hpp>

namespace bsim {

/**
 * @class IView
 * This class provides a common interface to all views of BoardSim.
 */


///
IView::~IView() {
}


/**
 * Called when the current configuration is cleared. After this call,
 * the board and executable information are released.
 */
void IView::onClear(void) {
}


/**
 * Called to let the view load its settings.
 * As a default, do nothing.
 * @param settings	Object to get settings from.
 */
void IView::loadSettings(QSettings& settings) {
}


/**
 * Called to let the view save its settings.
 * As a default, do nothing.
 * @param settings	Object to set settings from.
 */
void IView::saveSettings(QSettings& settings) {
}


/**
 * Called each time a new board has been loaded.
 * As a default, do nothing.
 */
void IView::onBoardLoad(void) {
}


/**
 * Called each time a new executable is loaded.
 * As a default, do nothing.
 */
void IView::onExecLoad(void) {
}


/**
 * Called to let the view update itself after a program execution.
 * As a default, do nothing.
 */
void IView::update(void) {
}


/**
 * Called each time the program is run (not when a step is performed).
 */
void IView::onRun(void) {
}

/**
 * Called each time a break is reaised (either breakpoint, or user break).
 */
void IView::onBreak(void) {
}


/**
 * Called when the simulator is started.
 */
void IView::onStart(void) {
}


/**
 * Called when the simulator is stopped.
 */
void IView::onStop(void) {
}

}	// bsim
