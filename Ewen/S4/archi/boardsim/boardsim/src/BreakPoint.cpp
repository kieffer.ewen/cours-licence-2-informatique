/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>

#include <BreakPoint.hpp>
#include <Program.hpp>

namespace bsim {

/**
 * @class BreakPointManager
 * Manager for break points.
 */

/**
 */
BreakPointManager::BreakPointManager(void): _prog(0), _ready(false) {
}

/**
 * Set the current program.
 */
void BreakPointManager::setProgram(Program *prog) {
	if(_ready) {
		_enabled.clear();
		_ready = false;
	}
	_prog = prog;
}


/**
 * Remove and delete the given breakpoint.
 */
void BreakPointManager::remove(BreakPoint *bp) {
	if(bp->isEnabled() && _ready)
		_enabled.remove(bp->address());
	_bpts.removeOne(bp);
	for(QList<BreakPointListener *>::const_iterator l = _lists.begin(); l != _lists.end(); l++)
		(*l)->onRemove(bp);
	delete bp;
}

/**
 * Add the given rbeak point.
 * @param bp	Added break point.
 */
void BreakPointManager::add(BreakPoint *bp) {
	_bpts.push_front(bp);
	for(QList<BreakPointListener *>::const_iterator l = _lists.begin(); l != _lists.end(); l++)
		(*l)->onAdd(bp);
	enable(bp);
}

/**
 * @fn const QList<BreakPoint *> BreakPointManager::breakPoints(void) const;
 * Get the list of breakpoints.
 * @return	Break point list.
 */


/**
 * Test if there is an enabled break at the given address.
 * @param addr	Address to test.
 * @return		True if there is an active break, false else.
 */
bool BreakPointManager::breaksAt(addr_t addr) {
	if(!_prog)
		return false;
	if(!_ready) {
		_ready = true;
		for(QList<BreakPoint *>::const_iterator bp = _bpts.begin(); bp != _bpts.end(); bp++)
			if((*bp)->isEnabled())
				_enabled.insert((*bp)->address());
	}
	return _enabled.contains(addr);
}


/**
 * Get the break point at the given address.
 * @parm addr	Address to look  a breakpoint for.
 * @return		Found break point or null.
 */
BreakPoint *BreakPointManager::breakPointAt(addr_t addr) const {
	for(QList<BreakPoint *>::const_iterator bp = _bpts.begin(); bp != _bpts.end(); bp++)
		if((*bp)->address() == addr)
			return *bp;
	return 0;
}


/**
 * Find the break point at the given position.
 * @param file	File containing the breakpoint.
 * @param line	Line containing the breakpoint.
 * @return		Found break point or null.
 */
BreakPoint *BreakPointManager::breakPointAt(QString file, int line) const {
	for(QList<BreakPoint *>::const_iterator bp = _bpts.begin(); bp != _bpts.end(); bp++) {
		addr_t addr = (*bp)->address();
		QString bfile;
		int bline;
		_prog->lineOf(addr, bfile, bline);
		if(file == bfile && line == bline)
			return *bp;
	}
	return 0;
}


/**
 * Enable a breakpoint.
 * @param bp	Enabled break point.
 */
void BreakPointManager::enable(BreakPoint *bp) {
	bp->_enabled = true;
	if(_ready)
		_enabled.insert(bp->address());
	for(QList<BreakPointListener *>::const_iterator l = _lists.begin(); l != _lists.end(); l++)
		(*l)->onEnable(bp);
}

/**
 * Disable the break point.
 * @param bp	Disabled breakpoint.
 */
void BreakPointManager::disable(BreakPoint *bp) {
	bp->_enabled = false;
	if(_ready)
		_enabled.remove(bp->address());
	for(QList<BreakPointListener *>::const_iterator l = _lists.begin(); l != _lists.end(); l++)
		(*l)->onDisable(bp);
}

/**
 * Add a new listener.
 * @param list	Added listener.
 */
void BreakPointManager::add(BreakPointListener *list) {
	_lists.push_front(list);
}

/**
 * Remove a listener.
 * @param list	Removed listener.
 */
void BreakPointManager::remove(BreakPointListener *list) {
	_lists.removeOne(list);
}

/**
 * Load break points from the given settings.
 * @param settings	To load from.
 */
void BreakPointManager::loadSettings(QSettings& settings) {
	
	// load header
	settings.beginGroup("breakpoints");
	int n = settings.value("count", 0).toUInt();
	settings.endGroup();
	
	// load the break points
	for(int i = 0; i < n; i++) {
		settings.beginGroup(QString("break_point_%1").arg(i));
		BreakPoint *bp = BreakPoint::load(settings);
		if(bp)
			add(bp);
		settings.endGroup();
	}
}

/**
 * Save break points to the given settings.
 * @param settings	To save to.
 */
void BreakPointManager::saveSettings(QSettings& settings) {
	
	// save header
	settings.beginGroup("breakpoints");
	settings.setValue("count", _bpts.size());
	settings.endGroup();
	
	// save breakpoints
	int i = 0;
	for(QList<BreakPoint *>::const_iterator bp = _bpts.begin(); bp != _bpts.end(); bp++, i++) {
		settings.beginGroup(QString("break_point_%1").arg(i));
		(*bp)->saveSettings(settings);
		settings.endGroup();
	}
}


/**
 * @class BreakPoint
 * Base class of breakpoints.
 */

/**
 */
BreakPoint::BreakPoint(void): _enabled(false), _man(0) {
}

/**
 */
BreakPoint::BreakPoint(QSettings& settings) {
	_enabled = settings.value("enabled", true).toBool();
}

/**
 */
BreakPoint::~BreakPoint(void) {
}

/**
 * Load a break point from a settings put on a section.
 * @param settings	Current settings.
 * @return			Created break point or null.
 */
BreakPoint *BreakPoint::load(QSettings& settings) {
	QString type = settings.value("type", "").toString();
	if(type == "address")
		return new AddressBreakPoint(settings);
	else if(type == "sourc")
		return new SourceBreakPoint(settings);
	else
		return 0;
}

/**
 * Save a breakpoint to settings.
 * @param settings	Settings to save to.
 */
void BreakPoint::saveSettings(QSettings &settings) {
	settings.setValue("type", type());
	settings.setValue("enabled", _enabled);
}


/**
 * @fn bool BreakPoint::isEnabled(void) const;
 * Test if the break point is enabled.
 * @return	True if enabled, false else.
 */

/**
 * @fn bool BreakPoint::isDisabled(void) const;
 * Test if the break point is disabled.
 * @return	True if disabled, false else.
 */

/**
 * Enable the breakpoint.
 */
void BreakPoint::enable(void) {
	if(_man)
		_man->enable(this);
	else
		_enabled = true;
}

/**
 * Disable the breakpoint.
 */
void BreakPoint::disable(void) {
	if(_man)
		_man->disable(this);
	else
		_enabled = false;
}

/**
 * @fn QString BreakPoint::name(void);
 * Get the break point name for user display.
 * @return Break point name.
 */

/**
 * @fn addr_t BreakPoint::address(Program& prog);
 * Compute the actual address of the breakpoint.
 * @param prog	Current program.
 * @return		Break point address.
 */


/**
 * @class AddressBreakPoint
 * A break point for an absolute address.
 */

/**
 * Build an address break point.
 * @param man	Owner manager.
 * @param addr	Breakpoint address.
 */
AddressBreakPoint::AddressBreakPoint(addr_t addr): _addr(addr) {
}

/**
 */
QString AddressBreakPoint::name(void) {
	return QString("%1").arg(_addr, 8, 16, QChar('0'));
}

/**
 */
addr_t AddressBreakPoint::address(void) {
	return _addr;
}

/**
 */
QString AddressBreakPoint::type(void) {
	return "address";
}

/**
 */
AddressBreakPoint::AddressBreakPoint(QSettings &settings)
: BreakPoint(settings) {
	_addr = settings.value("address", 0).toUInt();
}

/**
 */
void AddressBreakPoint::saveSettings(QSettings &settings) {
	BreakPoint::saveSettings(settings);
	settings.setValue("address",  _addr);
}


/**
 * @class SourceBreakPoint
 * A break point on a source line.
 */

/**
 * Build a source breakpoint.
 * @param man	Breakpoint manager.
 * @param file	Source file.
 * @param line	Source line.
 */
SourceBreakPoint::SourceBreakPoint(QString file, int line): _file(file), _line(line) {
}

/**
 */
QString SourceBreakPoint::name(void) {
	return QString("%1:%2").arg(_file).arg(_line);
}

/**
 */
addr_t SourceBreakPoint::address(void) {
	if(!_man)
		return 0;
	else
		return _man->program()->addressOf(_file, _line);
}

/**
 */
QString SourceBreakPoint::type(void) {
	return "source";
}

/**
 */
SourceBreakPoint::SourceBreakPoint(QSettings &settings)
: BreakPoint(settings) {
	_file = settings.value("file", "").toString();
	_line = settings.value("line", 0).toUInt();
}

/**
 */
void SourceBreakPoint::saveSettings(QSettings &settings) {
	BreakPoint::saveSettings(settings);
	settings.setValue("file",  _file);
	settings.setValue("line",  _line);
}


}	// bsim
