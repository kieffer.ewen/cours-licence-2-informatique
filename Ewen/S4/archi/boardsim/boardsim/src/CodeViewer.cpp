/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QPainter>
#include <QTextBlock>
#include <QDebug>
#include <QSyntaxHighlighter>

#include "CodeViewer.hpp"
#include "Highlighter.hpp"
#include "MainWindow.hpp"

#define BREAK_PAD	8
#define LINES_PAD	4

namespace bsim {

/**
 * Highlighter for assembly language.
 */


/**
 * Build the line number area.
 * @param viewer	Owner viewer.
 */
CodeViewer::LineNumberArea::LineNumberArea(CodeViewer *viewer): QWidget(viewer) {
	_viewer = viewer;
}

/**
 */
QSize CodeViewer::LineNumberArea::sizeHint() const {
	return QSize(_viewer->lineNumberAreaWidth(), 0);
}

/**
 */
void CodeViewer::LineNumberArea::paintEvent(QPaintEvent *event) {
	_viewer->lineNumberAreaPaintEvent(event);
}
 

/**
 * Build the line break area.
 * @param viewer	Parent viewer.
 */
CodeViewer::BreakArea::BreakArea(CodeViewer *viewer): QWidget(viewer) {
	_viewer = viewer;
}

/**
 */
QSize CodeViewer::BreakArea::sizeHint(void) const {
	return QSize(_viewer->breakAreaWidth(), 0);
}

/**
 */
void CodeViewer::BreakArea::paintEvent(QPaintEvent *event) {
	_viewer->breakAreaPaintEvent(event);
}


/**
 */
void CodeViewer::BreakArea::mouseReleaseEvent(QMouseEvent *event) {
	_viewer->setBreak(event->x(), event->y());
}
 

/**
 * Build a code viewer.
 * @param parent	Parent widget.
 */
CodeViewer::CodeViewer(MainWindow& main_window, QWidget *parent)
: main(main_window) {
	main.getBreakPointManager().add(this);

	// initialization
	use_break = true;
	use_lines = true;
	_source = 0;

	// configure the editor
	setReadOnly(true) ;
	toogleAddresses() ;
	toogleAddresses() ;
	lineColor = QColor(Qt::yellow).lighter(160);
	setLineWrapMode(QPlainTextEdit::NoWrap);
	
	// set font
	QFont font = QFont ("Monospace");
	font.setPointSize(10);
	font.setFixedPitch(true);
	setFont(font);
	QPainter painter(this);
	painter.setFont(font);
	QFontMetrics fm = painter.fontMetrics();
	setTabStopWidth(fm.width("0000"));

	// configure the areas
	if(use_break)
		break_area = new BreakArea(this);
	if(use_lines)
		lineNumberArea = new LineNumberArea(this);
	connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateAreaWidth(int)));
	connect(this, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateArea(QRect,int)));
	connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(highlightCurrentLine()));
	updateAreaWidth(0);
	//highlightCurrentLine();
}


/**
 */
CodeViewer::~CodeViewer() {
	main.getBreakPointManager().remove(this);
}


/**
 * ???
 */
void CodeViewer::toogleAddresses() {
}


/**
 * Highlight the given.
 * @param line	Line to highlight.
 */
void CodeViewer::highlight(int line) {

	// remove highlight
	if(line <= 0) {
		QList<QTextEdit::ExtraSelection> extraSelections;
		setExtraSelections(extraSelections) ;
	}
	
	// highlight the current line
	else {
		
		// select the selection line
		QList<QTextEdit::ExtraSelection> extraSelections;
		QTextEdit::ExtraSelection selection;
		selection.cursor = textCursor();
		selection.cursor.movePosition(QTextCursor::Start) ;
		for (int i = 1 ; i < line ; i++ )
			selection.cursor.movePosition(QTextCursor::Down);
		selection.format.setBackground(lineColor);
		selection.format.setProperty(QTextFormat::FullWidthSelection, true);
		extraSelections.append(selection);
		setTextCursor(selection.cursor);
		setExtraSelections(extraSelections);

		// ensure it is visible
		QRect r = cursorRect(selection.cursor);
		if(r.bottom() > height())
			ensureCursorVisible();
	}
}


/**
 * Toggle WRAP mode.
 */
void CodeViewer::wrap() {
	  if (lineWrapMode())
		setLineWrapMode(QPlainTextEdit::NoWrap) ;
	  else
		setLineWrapMode(QPlainTextEdit::WidgetWidth) ;
}


/**
 * Compute the line number area width.
 * @return	Line number area width.
 */
int CodeViewer::lineNumberAreaWidth() {
	int digits = 1;
	int max = qMax(1, blockCount());
	while (max >= 10) {
		max /= 10;
		++digits;
	}
	int space = LINES_PAD + fontMetrics().width(QLatin1Char('9')) * digits;
	return space;
}


/**
 * Compute the break area width.
 * @return	Line number area width.
 */
int CodeViewer::breakAreaWidth() {
	return BREAK_PAD + fontMetrics().height();
}


/**
 * Update the margin width of the areas.
 * @param newBlockCount		Blockc ount.
 */
void CodeViewer::updateAreaWidth(int newBlockCount) {
	int width = 0;
	if(use_break)
		width += breakAreaWidth();
	if(use_lines)
		width += lineNumberAreaWidth();
	setViewportMargins(width, 0, 0, 0);
}


/**
 * Update the display of the areas.
 * @param rect	Rectangle to update.
 * @param dy	Vertical offset.
 */
void CodeViewer::updateArea(const QRect &rect, int dy) {
	if (dy) {
		if(use_break)
			lineNumberArea->scroll(0, dy);
		if(use_lines)
			break_area->scroll(0, dy);
	}
	else {
		int x = 0;
		if(use_break) {
			break_area->update(x, rect.y(), break_area->width(), rect.height());
			x += break_area->width();
		}
		if(use_lines) {
			lineNumberArea->update(x, rect.y(), lineNumberArea->width(), rect.height());
			x += lineNumberArea->width();
		}
	}
	if(rect.contains(viewport()->rect()))
		updateAreaWidth(0);
}


/**
 * Handle resize event.
 * @param e	Event descriptor.
 */
void CodeViewer::resizeEvent(QResizeEvent *e) {
	QPlainTextEdit::resizeEvent(e);
	QRect cr = contentsRect();
	int x = cr.left();
	if(use_break) {
		break_area->setGeometry(QRect(x, cr.top(), breakAreaWidth(), cr.height()));
		x += break_area->width();
	}
	if(use_lines) {
		lineNumberArea->setGeometry(QRect(x, cr.top(), lineNumberAreaWidth(), cr.height()));
		x += lineNumberArea->width();
	}
}


/**
 * Highlight the current line.
 */
void CodeViewer::highlightCurrentLine() {
	QList<QTextEdit::ExtraSelection> extraSelections;
	QTextEdit::ExtraSelection selection;
	QColor lineColor = lineColor;
	selection.format.setBackground(lineColor);
	selection.format.setProperty(QTextFormat::FullWidthSelection, true);
	selection.cursor = textCursor();
	selection.cursor.clearSelection();
	extraSelections.append(selection);
	setExtraSelections(extraSelections);
}


/**
 * Display the line number area.
 * @param event		Repaint event.
 */
void CodeViewer::lineNumberAreaPaintEvent(QPaintEvent *event) {
	QPainter painter(lineNumberArea);
	painter.fillRect(event->rect(), Qt::lightGray);
     
	QTextBlock block = firstVisibleBlock();
	int blockNumber = block.blockNumber();
	int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
	int bottom = top + (int) blockBoundingRect(block).height();

	while(block.isValid() && top <= event->rect().bottom()) {
		if(block.isVisible() && bottom >= event->rect().top()) {
			QString number = QString::number(blockNumber + 1);
			painter.setPen(Qt::black);
			painter.drawText(0, top, lineNumberArea->width() - LINES_PAD, fontMetrics().height(), Qt::AlignRight, number);
		}

		block = block.next();
		top = bottom;
		bottom = top + (int) blockBoundingRect(block).height();
		++blockNumber;
	}
}

/**
 * Display the break area.
 * @param event		Repaint event.
 */
void CodeViewer::breakAreaPaintEvent(QPaintEvent *event) {
	QPainter painter(break_area);
	painter.fillRect(event->rect(), Qt::lightGray);
     
	QTextBlock block = firstVisibleBlock();
	int blockNumber = block.blockNumber();
	int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
	int bottom = top + (int) blockBoundingRect(block).height();

	while(block.isValid() && top <= event->rect().bottom()) {
		if(block.isVisible() && bottom >= event->rect().top()) {
			if(break_lines.contains(blockNumber + 1)) {
				QBrush brush(Qt::red);
				painter.setBrush(brush);
				painter.setPen(Qt::NoPen);
				painter.drawEllipse(BREAK_PAD / 2, top, fontMetrics().height() - 1, fontMetrics().height() - 1);
				painter.setBrush(Qt::NoBrush);
			}
		}

		block = block.next();
		top = bottom;
		bottom = top + (int) blockBoundingRect(block).height();
		++blockNumber;
	}
}


/**
 * Set a break at the given position.
 * @param x	X coordinate.
 * @param y	Y coordinate.
 */
void CodeViewer::setBreak(int x, int y) {
	if(_source) {
		QTextCursor	cursor = cursorForPosition(QPoint(x, y));
		int line = cursor.blockNumber() + 1;
		if(_source->isAddressable(line)) {
			
			// handle the breakpoint
			BreakPoint *bp = main.getBreakPointManager().breakPointAt(_source->name(), line);
			if(!bp)
				main.getBreakPointManager().add(new AddressBreakPoint(_source->getAddress(line)));
			else
				main.getBreakPointManager().remove(bp);
			
			// update the display
			int y = (int)blockBoundingGeometry(cursor.block()).translated(contentOffset()).top();
			int height = (int)blockBoundingRect(cursor.block()).height();
			break_area->update(0, y, break_area->width(), fontMetrics().height());
		}
	}
}


/**
 * Set the break display.
 * @param set	If true, display breaks.
 */
void CodeViewer::setBreaks(bool set) {
	if(set) {
		if(!break_area) {
			break_area = new BreakArea(this);
			updateAreaWidth(0);
		}
	}
	else {
		if(break_area) {
			delete break_area;
			break_area = 0;
			updateAreaWidth(0);
		}
	}
}


/**
 * Set the lines display.
 * @param set	If true, display breaks.
 */
void CodeViewer::setLines(bool set) {
	if(set) {
		if(!lineNumberArea) {
			lineNumberArea = new LineNumberArea(this);
			updateAreaWidth(0);
		}
	}
	else {
		if(lineNumberArea) {
			delete lineNumberArea;
			lineNumberArea = 0;
			updateAreaWidth(0);
		}
	}
}


/**
 * Clear anything displayed in the source view.
 */
void CodeViewer::clearSource(void) {
	setPlainText("");
	break_lines.clear();
	_source = 0;
}


/**
 * Set the current source.
 * @param source	New source or null for no source.
 */
void CodeViewer::setSource(ISource *source) {
	
	// cleanup
	clearSource();
	
	// set the new text if needed
	_source = source;
	if(source) {
		setPlainText(source->getText());
		//new MyHighlighter(document());
		Highlighter *high = source->highlighter();
		if(high)
			high->make(document());
		for(QList<BreakPoint *>::const_iterator bp = main.getBreakPointManager().breakPoints().begin();
		bp != main.getBreakPointManager().breakPoints().end(); ++bp) {
			int line = _source->getLine((*bp)->address());
			if(line > 0)
				break_lines.insert(line);
		}
	}
	
	// update display
	updateAreaWidth(0);
}


/**
 * @dn const QSet<uint32_t>& CodeViewer::getBreakPoints(void) const;
 * Get the break points.
 * @return	Set of breakpoints.
 */


/**
 * Highlight the instruction at the given address.
 * @param addr	Address to address.
 */
void CodeViewer::highlightAddress(uint32_t addr) {
	int line = _source->getLine(addr);
	highlight(line);
}

/**
 */
void CodeViewer::onAdd(BreakPoint *bp) {
	if(!_source)
		return;
	addr_t addr = bp->address();
	if(addr) {
		int line = _source->getLine(addr);
		if(line)
			break_lines.insert(line);
	}
}

/**
 */
void CodeViewer::onRemove(BreakPoint *bp) {
	if(!_source)
		return;
	addr_t addr = bp->address();
	if(addr) {
		int line = _source->getLine(addr);
		if(line)
			break_lines.remove(line);
	}
}

/**
 */
void CodeViewer::onEnable(BreakPoint *bp) {
}

/**
 */
void CodeViewer::onDisable(BreakPoint *bp) {
}

}	// bsim
