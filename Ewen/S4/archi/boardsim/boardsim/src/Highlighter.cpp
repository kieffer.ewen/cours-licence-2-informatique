/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <Highlighter.hpp>

namespace bsim {

/**
 * @class Tokentyle
 * Describe the style for a token type.
 */


/**
 * @fn TokenStyle::TokenStyle(QString name, QString description);
 * Build a token style.
 * @param name			Name of the token style (for human user).
 * @param description	Description of the token style (for human user).
 */


/**
 * @fn QString TokenStyle::name(void) const;
 * Name of the token style.
 * @return	Name.
 */


/**
 * @fn QString TokenStyle::description(void) const;
 * Description of the token style.
 * @return Description.
 */


/**
 * @class Highlighter
 * 
 * This class is a factory of QSyntaxHighlighter allowing also
 * to centralize the used token styles. It provides also a simple
 * way to build preferences window and save or load preferences.
 * Finally, it contains a map for registering highlighters based
 * on file extension and then retrieve them.
 * 
 * This class must be inherited in order to get an actual highlighter
 * and the method overloaded to return it.
 */


/**
 * Register a new highighter based on the given extensions.
 * @param ext			Matching extension.
 * @param highlighter	Highlighter to register.
 */
void Highlighter::registerForExt(QString ext, Highlighter *highlighter) {
	exts.insert(ext, highlighter);
}


/**
 * Unregister a new highighter based on the given extensions.
 * @param ext			Matching extension.
 * @param highlighter	Highlighter to register.
 */
void Highlighter::unregisterForExt(QString ext) {
	exts.remove(ext);
}


/**
 * Get an highlighter matching the given extension.
 * @param ext	Looked extension.
 * @return		Found highlighter or null.
 */
Highlighter *Highlighter::getForExt(QString ext) {
	return exts.value(ext, 0);
}


/**
 * @fn Highlighter::Highlighter(QString name);
 * Build an highlighter.
 * @param name	Name of the highlighter.
 */


/**
 * @fn QString Highlighter::name(void) const;
 * Get the name of the highlighter.
 * @return name
 */


/**
 * @fn int Highlighter::count(void) const;
 * Get the count of token styles.
 * @return	Token style count.
 */


/**
 * @fn TokenStyle *Highlighter::style(int i) const;
 * Get the style with the given index.
 * @param i		Index of the requested style.
 */


/**
 * QSyntaxHighlighter *Highlighter::make(QTextDocument *document) const;
 * Called to build the actual highlighter that may be passed to Qt text editor.
 * @param	document	Document to apply to.
 * @return				Built highlighter.
 */


/**
 * Add a token style.
 * @param style	Style to add.
 */
void Highlighter::add(TokenStyle *style) {
	_styles.push_back(style);
}


/**
 */
QMap<QString, Highlighter *> Highlighter::exts;

}	// bsim
