/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <AssemblyHighlighter.hpp>

namespace bsim {

inline bool isComment(QChar c) { return c == '@' || c == '#'; }
inline bool isSpace(QChar c) { return c == ' ' || c == '\t'; }
inline bool isBin(QChar c) { return c == '0' || c == '1'; }
inline bool isOctal(QChar c) { return c >= '0' && c <= '7'; }
inline bool isDecimal(QChar c) { return c >= '0' && c <= '9'; }
inline bool isHex(QChar c) { return isDecimal(c) || (c >= 'a' && c <= 'f') || (c >= 'A' || c <= 'F'); }
inline bool isFirstID(QChar c) { return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '.' || c == '_' || c == '$'; }
inline bool isID(QChar c) { return isFirstID(c) || (c >= '0' && c <= '9'); }

bool parseID(const QString& text, int& p) {
	if(p >= text.length())
		return false;
	if(!isFirstID(text[p]))
		return false;
	p++;
	while(p < text.length() && isID(text[p]))
		p++;
	return true;
}

static void skipSpaces(const QString& text, int& p) {
	while(p < text.length() && isSpace(text[p]))
		p++;
}

static bool swallow(const QString& text, int& p, char c) {
	if(p >= text.length())
		return false;
	if(text[p] == c) {
		p++;
		return true;
	}
	else
		return false;
}

static bool parseDecimal(const QString& text, int& p) {
	if(p >= text.length())
		return false;
	if(!isDecimal(text[p]))
		return false;
	p++;
	while(p < text.length() && isDecimal(text[p]))
		p++;
	return true;
}

static bool parseConstant(const QString& text, int& p) {
	if(p >= text.length())
		return false;
		
	// other
	if(text[p] == '0') {
		if(p >= text.length())
			return true;
		
		// binary
		if(text[p] == 'b' || text[p] == 'B') {
			p++;
			while(p < text.length() && isBin(text[p]))
				p++;
		}
		
		// hexadecimal
		else if(text[p] == 'x' || text[p] == 'X') {
			p++;
			while(p < text.length() && isHex(text[p]))
				p++;
		}
		
		// octal
		else
			while(p < text.length() && isOctal(text[p]))
				p++;
		return true;
	}
	
	// decimal
	else
		return parseDecimal(text, p);
}


/**
 */
AssemblySyntax::AssemblySyntax(const AssemblyHighlighter& _high, QTextDocument *document):
	QSyntaxHighlighter(document), high(_high)
{ }


/**
 */
void AssemblySyntax::highlightBlock(const QString &text) {
	int p = 0;
	
	// whole line comment
	if(swallow(text, p, '@') || swallow(text, p, '#')) {
		setFormat(0, text.length(), high[AssemblyHighlighter::LINE_COMMENT]->format);
		return;
	}
	
	// label
	if(parseID(text, p)) {
		skipSpaces(text, p);
		if(swallow(text, p, ':'))
			setFormat(0, p, high[AssemblyHighlighter::LABEL]->format);
			
	}
	
	// skip spaces
	skipSpaces(text, p);
	
	// look for a directive
	int q = p;
	if(swallow(text, p, '.')) {
		if(parseID(text, p))
			setFormat(q, p - q, high[AssemblyHighlighter::DIRECTIVE]->format);
	}
	
	// look for an instruction
	else if(parseID(text, p))
		setFormat(p, p - q, high[AssemblyHighlighter::INST]->format);
	
	// parse arguments
	while(p < text.length()) {
		q = p;
		if(swallow(text, p , 'r') || swallow(text, p , 'R')) {
			if(parseDecimal(text, p))
				continue;
			p = q;
		}
		if(parseID(text, p))
			setFormat(q, p - q, high[AssemblyHighlighter::NAMED]->format);
		else if(parseConstant(text, p))
			setFormat(q, p - q, high[AssemblyHighlighter::CONSTANT]->format);
		else
			p++;
	}
}


/**
 * @class AssemblyHighlighter
 * Highlighter for assembly code (usually .s or .i extensions).
 */
AssemblyHighlighter::AssemblyHighlighter(void):
	Highlighter("assembly"),
	label("label"),
	directive("directive"),
	inst("instruction"),
	constant("constant"),
	comment("comment"),
	line_comment("line comment"),
	named("named constant")
{
	// configure the styles
	directive.format.setForeground(QColor("red"));
	directive.format.setFontWeight(QFont::Bold);
	constant.format.setForeground(QColor("blue"));
	comment.format.setForeground(QColor("lime"));
	line_comment.format.setForeground(QColor("green"));
	line_comment.format.setFontItalic(true);
	label.format.setForeground(QColor("purple"));
	label.format.setFontWeight(QFont::Bold);
	named.format.setForeground(QColor("teal"));

	// add to the highlighter
	add(&label);
	add(&directive);
	add(&inst);
	add(&constant);
	add(&comment);
	add(&line_comment);
	add(&named);
}


/**
 */
QSyntaxHighlighter *AssemblyHighlighter::make(QTextDocument *document) const {
	return new AssemblySyntax(*this, document);
}

}	// bsim
