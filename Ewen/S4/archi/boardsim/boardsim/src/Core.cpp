/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <Core.hpp>
#include <Board.hpp>

namespace bsim {

/**
 * @defgroup components Components
 * 
 * This group describes the component built-in in the BoardSim distribution.
 * The components are linked to BoardSim using a plugin linked from
 * directory PREFIX/lib/bsim and from the XML description of a board.
 * More components may be added simply by putting them in this directory
 * and by implementing the plugin protocol of BoardSim.
 * 
 * @section board Board Description Format
 * 
 * A board description is a file containing XML description as below:
 * @code
 * <?xml version="1.0" encoding="..." standalone="yes" ?>
 * <board name="NAME" version="0.1">
 * 	COMPONENTS
 * </board>
 * @endcode
 * 
 * The version is the version of the format, currently 0.1. The NAME
 * is the board as displayed to the user and the COMPONENTS is a list
 * of component element as below:
 * @code
 * <component clas="CLASS" name="NAME" COMPONENT_ATTRIBUTES>
 *  BASE?
 *	LINKS
 *	COMPONENT_ELEMENTS
 * </component>
 * @endcode
 * 
 * The CLASS definition allows to select the plugin that will implement
 * the component while the NAME is the name of the component displayed to the user.
 * According to the class of the component, different configuration component
 * attributes and elements may be defined: takes a look at the configuration documentation
 * of the component.
 * 
 * Finally, the components may be linked together with LINK elements:
 * @code
 *	<link pin="PIN" to="COMPONENT" port="PORT" />
 * @endcode
 * 
 * A link elements allows to connect together the ports of the different components
 * to let signals transit from one component to the other one. The PIN is the port
 * on the component containing the link, the PORT is the pin of the target COMPONENT
 * to link to. The pins of source and target components can be viewed in the
 * documentation of each component.
 * 
 * Optionaly, a base address may be provided if the component has hardware IO registers.
 * In this case, the register addresses in memory may be relatively to a base address
 * defined as below:
 * @code
 * <base value="ADDRESS"/>
 * @endcode
 * 
 * The ADDRESS is any integer using the C syntax (possibly in hexadecimal with prefix @c 0x ).
 * 
 * @section plugin Adding a Component Plugin
 * 
 * Creating a new component is relatively simple. Lets take an example the LED component.
 * A component must inherit from the IComponent class that provides a lot of read-to-use facilities
 * and provide one or several pins to let it connect with other components and/or provide
 * also registers to let the processor communicate with it.
 * 
 * First, it has to declare a class header file, in our case, LED.hpp:
 * @code
 * #include "IComponent.hpp"
 *
 * namespace bsim {
 * class LED: public IComponent {
 * public :
 *	LED(Board& board, QDomElement element, QString name);
 *	~LED(void);
 *	virtual void installUI(MainWindow& window);
 *	virtual void uninstallUI(MainWindow& window);
 * private:
 *	bool val;
 * 
 *	class InPin: public Pin {
 *	public:
 *		InPin(LED& led);
 *		virtual void onChange(uint64_t time, bool value);
 *	private:
 *		LED& _led;
 * 	};
 * };
 * @endcode
 * 
 * Its constructor must as parameters the owner board, the XML element
 * defining the component (to let the component get its own configuration)
 * and its name on the board. The constructor has to initialize its attributes
 * and to launch the configuration analysis (that extracts automatically its name).
 * @code
 * LED::LED(Board& board, QDomElement element, QString n): IComponent(board) {
 *	val = false;
 *	configure(element);
 *	add(new InPin(*this));
 *	update();
 * @endcode
 * 
 * The configure() call allows to perform the automatic configuration
 * provided by IComponent on the given element. For each sub-element that
 * IComponent does not know, a call to method configureOther() to let
 * the component scan its own configuration.
 * 
 * The add() allows to the pins declared by the component. Such an object
 * must inherit from the Pin class as shown in the body of the LED class.
 * Mainly, this class records a backlink to the original LED object
 * and override the onChange() method to react to signal change on this pin.
 * 
 * Finally, as the LED is provided an UI, the methods installUI() and
 * uninstallUI() must be overriden to install or uninstall the UI widgets
 * of the component (notice that the Qt UI environment is used). This display
 * is updated by a call to update().
 * 
 * The main work of the component is performed in the InPin::onChange()
 * method that allows the class to change its display according the signal
 * on the pin.
 * @code
 * LED::InPin::InPin(LED& led): Pin(&led, "in"), _led(led) {
 * }
 * 
 * void LED::InPin::onChange(uint64_t time, bool value) {
 *	_led.val = value;
 *	_led.update();
 * }
 * @endcode
 * 
 * Basically, it records the new value of the LED in the @c val attribute
 * and then update the display accordingly.
 * 
 * Finally, we have to add code to let the plugin creates a new component:
 * @code
 * extern "C" IComponent* init(Board& board, QDomElement element, QString name) {
 *	return new LED(board, element, name) ;
 * }
 * @endcode
 * 
 * To compile and install the component, the easier way is to use a CMake script as below
 * (the sources are stored in the LED.cpp file):
 * @code
 * find_package(Qt4 REQUIRED)
 * SET(QT_USE_QTGUI TRUE)
 * SET(QT_USE_QTXML TRUE)
 * INCLUDE(${QT_USE_FILE})
 *
 * add_library(LED SHARED LED.cpp)
 * target_link_libraries(LED sim ${QT_LIBRARIES})
 * install(TARGETS LED LIBRARY DESTINATION "lib/bsim")
 * @endcode
 * 
 * Then you have to configure with CMake, compile and install:
 * @code
 * cmake .
 * make
 * make install
 * @endcode
 */


/**
 * @class Core
 * Represents a core of execution in the current board.
 * Provides the ability to execute a program. Basically, it provides
 * as many pins as interrupt exception in the architecture description
 * (with the same name). The program is retrieved from the memory
 * of the board.
 * @ingroup components
 */


/**
 * Build the core.
 * @param board	The board the core is on.
 */
Core::Core(Board& board, QDomElement configuration)
:	IComponent(board, configuration), freq(default_freq) {
}


/**
 * @fn Program *Core::load(QString path);
 * Load the program which path is given and return it.
 * 
 * @warning This method must be implemented by the core to let BoardSim
 * manage executable files corresponding to the core. In addition, this
 * means that the core has to provide its own version of the Program
 * class.
 * 
 * @param path		Path of the executable to load.
 * @return			Loaded program or null.
 */


/**
 * @fn void Core::start(void);
 * Prepare the simulator to run.
 */


/**
 * @fn void Core::stop(void);
 * Stop the simulation.
 */


/**
 * @fn Core::step(void);
 * Perform a step of simulation.
 */


/**
 * @fn uint32_t Core::pc(void);
 * Get the current value of the PC.
 * @return	PC value.
 */


/**
 */
Core *Core::toCore(void) {
	return this;
}


/**
 * @fn QList<section_t> Core::sections(void);
 * Get the list of code sections.
 * @return	List of sections descriptors.
 */


/**
 * @fn uint32_t Core::size(uint32_t address);
 * Get the size of an instruction.
 * @param address	Address of the instruction.
 * @return			Size of the instruction in bytes.
 */


/**
 * @fn QString Core::disasm(uint32_t address);
 * Disassemble the instruction at the given address.
 * @param address	Address of the instruction.
 * @return			Disassembled form of the instruction.
 */

}	// bsim
