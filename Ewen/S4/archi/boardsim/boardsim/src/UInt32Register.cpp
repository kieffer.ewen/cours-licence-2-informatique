/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <UInt32Register.hpp>

namespace bsim {

/**
 * @class UInt32
 * Base class of ref UInt32Register. Provides basic work of customizable
 * get and set by memort-mapped hardware register.
 */

/**
 * Build the register.
 * @param comp		Owner component.
 * @param make		Make descriptor obtained by a call to static make() method and customized.
 */
UInt32::UInt32(IComponent *comp, const Make& make): IHardRegister(comp, make) {
	_get = make._get;
	_set = make._set;
}

/**
 */
UInt32::~UInt32(void) {
	if(_get != &null_accessor)
		delete _get;
	if(_set != &null_accessor)
		delete _set;
}

/**
 */
void UInt32::read(addr_t addr, void *data, int size) {
	_get->access(addr, size, data);
}

/**
 */
void UInt32::write(addr_t addr, void *data, int size) {
	_set->access(addr, size, data);
}


/**
 * @class UInt32ControlRegister
 * Manage a 32-bit register receiving commands, that is, only react to
 * written commands but that does not store any bits. Getting / setting
 * this register from UI has not effect. If not getter is specifically
 * defined, it is considered as write-only.
 */
 
/**
 */
UInt32ControlRegister::UInt32ControlRegister(IComponent *comp, const Make& make)
: UInt32(comp, make) {
	if(_get == &null_accessor)
		_access &= ~READ;
}

/**
 */
QVariant UInt32ControlRegister::get(int i) {
	return QVariant();
}

/**
 */
void UInt32ControlRegister::set(QVariant value, int i) {
}


/**
 * @class UInt32ControlRegister::Bit
 * Provides a simple way to test a control bit and record the bit
 * for API generation.
 */

UInt32ControlRegister::Bit::Bit(UInt32ControlRegister& reg, QString name, int i): Field(reg, name, i) {
}

/**
 * @fn bool UInt32ControlRegister::Bit::bit(uint32_t v);
 * Test the bit on the given control word.
 * @param v		Control word to test.
 * @return		True if bit is 1, false else.
 */

/**
 * @fn bool UInt32ControlRegister::Bit::operator()(uint32_t v);
 * Shortcut to bit().
 */


/**
 * @class UInt32Register
 * This class is an helper to implement an hardware register 
 * storing an uint32_t value. As a default, the CPU access modify the
 * stored integer but the access may be also customized using method
 * pointers.
 * 
 * In addition, this class provides subclasses to record internal bits
 * and bit field and to make easier the access of the current bits.
 */

/**
 * Build the register.
 * @param comp		Owner component.
 * @param make		Make descriptor obtained by a call to static make() method and customized.
 */
UInt32Register::UInt32Register(IComponent *comp, const Make& make): UInt32(comp, make) {

	// set the getter
	if(make._get == &null_accessor && !isOnlyReadable())
		_get = new Getter<UInt32Register>(*this, &UInt32Register::get_reg);

	// set the setter
	if(make._set == &null_accessor && !isOnlyWritable())
		_set = new Setter<UInt32Register>(*this, &UInt32Register::set_reg);
}

/**
 */
uint32_t UInt32Register::get_reg(void) {
	return _v;
}

/**
 */
void UInt32Register::set_reg(uint32_t v) {
	_v = v;
}

/**
 */
QVariant UInt32Register::get(int i) {
	return _v;
}

/**
 */
void UInt32Register::set(QVariant value, int i) {
	_v = value.toUInt();
}

/**
 * @class UInt32Register::Bit
 * Provides access to a particular bit inside a UInt32 register.
 * It can read or written as usual C++ member variable. It provides
 * also the identification of this bit to generate the API.
 */


/**
 * @class UInt32Register::BitField
 * Provides access to a particular bit field inside a UInt32 register.
 * It can read or written as usual C++ member variable. It provides
 * also the identification of these bits to generate the API.
 */


/**
 * @class UInt32Register::BitArray
 * Field supporting access to bit as a bit array.
 * It can read or written as usual C++ member variable array. It provides
 * also the identification of this bit to generate the API.
 */

/**
 * @class UInt32Register::BitEnum
 * Provides access to a particular bit field inside a UInt32 register
 * containing enumerated values.
 * It can read or written as usual C++ member variable. It provides
 * also the identification of these bits to generate the API and 
 * provides the enumerated values in the API.
 */


/**
 * @class UInt32Array
 * Provides implementation (and API generation) for an array of 32-bits unsigned integer
 * array of registers.
 */

/**
 */
UInt32Array::UInt32Array(IComponent *comp, const Make& make)
: IHardRegister(comp, make), _v(new uint32_t[count()]), _get(make._get), _set(make._set) {
	if(_get == &null_accessor && isReadable())
		_get = new Getter<UInt32Array>(*this, *this, &UInt32Array::get_reg);
	if(_set == &null_accessor && isWritable())
		_set = new Setter<UInt32Array>(*this, *this, &UInt32Array::set_reg);
}

/**
 */
UInt32Array::~UInt32Array(void) {
	delete [] _v;
}

/**
 */
QVariant UInt32Array::get(int i) {
	if(isReadable())
		return _v[i];
	else
		return QVariant();
}

/**
 */
void UInt32Array::set(QVariant value, int i) {
	if(isWritable())
		_v[i] = value.toUInt();
}

/**
 */
void UInt32Array::read(addr_t addr, void *data, int size) {
	_get->access(addr, size, data);
}

/**
 */
void UInt32Array::write(addr_t addr, void *data, int size) {
	_set->access(addr, size, data);
}

/**
 */
uint32_t UInt32Array::get_reg(int i) {
	return _v[i];
}

/**
 */
void UInt32Array::set_reg(int i, uint32_t v) {
	_v[i] = v;
}

}	// bsim
