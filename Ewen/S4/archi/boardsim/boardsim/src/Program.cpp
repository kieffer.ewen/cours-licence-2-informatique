/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <gel/gel_elf.h>
#include <QDebug>
#include <Board.hpp>
#include <Program.hpp>
#include <FileSource.hpp>

namespace bsim {

/**
 * @class Program
 * Represents a simulated program and provides all facilities for the program:
 * executable information, debugging, etc.
 */


/**
 * Build the program from the given path.
 * @param path	Path of the program.
 * @param mon	Monitor for openging.
 */
Program::Program(Path path, IMonitor& mon): _path(path), _source_done(false) {
	src_paths.push_back(".");
	src_paths.push_back(path.parent());
}


/**
 */
Program::~Program(void) {
	for(QMap<QString, FileSource *>::const_iterator file = _sources.begin(); file != _sources.end(); ++file)
		delete *file;
}


/**
 * Reset the program memory to the initial state.
 * Default implementation does nothing.
 */
void Program::reset() {
}


/**
 * Get the list of sources in this program (only available sources).
 */
void Program::getSources(QList<FileSource *>& sources) {
	if(!_source_done) {
		_source_done = true;
		fillSources(_sources);
	}
	for(QMap<QString, FileSource *>::const_iterator s = _sources.begin();
	s != _sources.end(); s++) {
		sources.push_front(*s);
	}
}


/**
 * @fn void Program::listen(addr_t addr);
 * Record listening the given address.
 * @param addr	Address  to listen to.
 */
 
 
 /**
  * Called when  a read is performed.
  * @param addr	Read address.
  * @param data	Pointer to store the read data to.
  * @param size	Read size.
  */
void Program::onRead(addr_t addr, void *data, int size) {
	QMap<addr_t, IHardRegister *>::iterator it = callbacks.find(addr);
	if(it != callbacks.end())
		(*it)->read(addr, data, size);
	else
		qDebug() << "WARNING: unknown read to address 0x" << mkAddr(addr);
}


 /**
  * Called when a write is performed.
  * @param addr	Written address.
  * @param data	Pointer to get the data from.
  * @param size	Written size.
  */
void Program::onWrite(addr_t addr, void *data, int size) {
	QMap<addr_t, IHardRegister *>::iterator it = callbacks.find(addr) ;
	if(it != callbacks.end())
		(*it)->write(addr, data, size);
	else
		qDebug() << "WARNING: unknown write to address 0x" << mkAddr(addr);
}

/**
 * Record a new hardware register. After this call, it may receive
 * read/write actions.
 * @param reg	Recorded register.
 */
void Program::record(IHardRegister *reg) {
	for(int i = 0; i < reg->count(); i++) {
		addr_t addr = reg->address() + i * reg->stride();
		//qDebug() << "install " << reg->name(-1) << " of " << reg->component()->name() << " at " << (void *)addr;
		callbacks.insert(addr, reg);
		listen(addr);
	}
}


/**
 * Release a new hardware register. After this call, it will no more
 * receive read/write actions.
 */
void Program::release(IHardRegister *reg) {
	for(int i = 0; i < reg->count(); i++) {
		addr_t addr = reg->address() + i * reg->size() / 8;
		callbacks.erase(callbacks.find(addr));
	}
}


/**
 * @fn void Program::get(addr_t addr, uint32_t& val);
 * Get the word at the given address.
 * @param addr	Address to look to.
 * @param val	To return the value.
 */

/**
 * @fn void Program::lineOf(addr_t addr, QString& file, int& line);
 * Get source line and source file matching the given address.
 * @param addr	Looked address.
 * @param file	To store source file name.
 * @param line	To store source line number or -1 if the source line is not found.
 */

/**
 * @fn addr_t Program::addressOf(QString file, int line);
 * Get the address matching the given source file and line.
 * @param file	Looked source file.
 * @param line	Looked source line.
 * @return		Found address or null.
 */

/**
 * Get the source object representing the given file.
 * @param file	Looked file.
 * @return		Source file object or null if not found.
 */
FileSource *Program::sourceFor(QString file) {
	
	// init sources if requiredd
	if(!_source_done) {
		_source_done = true;
		fillSources(_sources);
	}
	
	// look for source
	FileSource *source = _sources.value(file, 0);
	if(!source) {
		Path p = file;
		if(p.isAbsolute()) {
			try {
				source = new FileSource(file, p, *this);
			}
			catch(FileSourceException& e) {
				return 0;
			}
		}
		else {
			for(QList<Path>::const_iterator path = src_paths.begin(); path != src_paths.end(); ++path)
				try {
					Path source_path = (*path).join(file);
					source = new FileSource(file, source_path, *this);
					break;
				}
				catch(FileSourceException& e) {
				}
			}
	}
	return source;	
}

}	// bsim
