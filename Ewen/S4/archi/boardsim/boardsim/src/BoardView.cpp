/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>

#include <BoardView.hpp>
#include <MainWindow.hpp>
#include <Display.hpp>

namespace bsim {

/**
 * @class BoardView
 * A view to display as precisely as possible the board.
 */


/**
 * Build a board view.
 * @param window	Owner window.
 */
BoardView::BoardView(MainWindow& window)
:	_window(window),
	view(&scene),
	vgraph(0)
{

	// initialize the content
	setAllowedAreas(Qt::AllDockWidgetAreas) ;
	setWindowTitle("Board View");
	setWidget(&view);
	setObjectName("board-view");

	// add to the window
	window.addDockWidget(Qt::RightDockWidgetArea, this) ;
	window.getViewMenu()->addAction(toggleViewAction());
}


/**
 */
BoardView::~BoardView() {
}


/**
 */
void BoardView::loadSettings(QSettings& settings) {
}


/**
 */
void BoardView::saveSettings(QSettings& settings) {
}


/**
 */
void BoardView::onClear(void) {
	if(vgraph)
		vgraph->clear();
}


/**
 */
void BoardView::onBoardLoad(void) {
	
	// set a preferred size
	const QSize size = _window.board().size();
	if(size.width() && size.height())
		scene.setSceneRect(0, 0, size.width(), size.height());
	
	// install the components
	vgraph = new VisualGraph(_window.board(), scene);
}


/**
 */
void BoardView::update(void) {
}


/**
 */
void BoardView::onRun(void) {
}


/**
 */
void BoardView::onBreak(void) {
}

}	// bsim
