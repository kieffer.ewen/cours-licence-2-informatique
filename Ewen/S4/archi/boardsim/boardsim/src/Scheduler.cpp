/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <Scheduler.hpp>
#include <Board.hpp>
#include <Core.hpp>
#include <Program.hpp>

//#define TRACE_SCH
#ifdef TRACE_SCH
#	define TRACE(c)		c
#else
#	define TRACE(c)
#endif

namespace bsim {


/**
 * @class Scheduler::Event
 * Base class of events to process by the scheduler.
 * Any component asking for a time activation must create a class
 * inheriting from this class and define the run() method. At the construction
 * of the object, the activation time must be passed to the constructor.
 * 
 * Finally, the object must be added to the scheduler using
 * Scheduler::addEvent().
 */


/**
 * Build an event that will be activated at the given time.
 * @param time			Activation time (optional).
 * @param name			Name of the event (optional).
 */
Scheduler::Event::Event(Board& board, time_t time, QString name)
: _board(board), _time(time), _name(name) {
#	ifdef TRACE_SCH
		if(_name.isNull())
			_name = QString::number((uint64_t)this, 16);
#	endif
}


/**
 */
Scheduler::Event::~Event() {

}


/**
 * @fn time_t Scheduler::Event::time(void) const;
 * Get the activation time.
 * @return	Activation time.
 */


/**
 * @fn void Scheduler::Event::run(void);
 * Called by the scheduler when the time has come.
 * Must overrident to customize the vent.
 */


/**
 * Get the current scheduler (if any).
 * @return	Current scheduler.
 */
Scheduler *Scheduler::Event::scheduler(void) const {
	return _board.scheduler();
}


/**
 * Cancel the event.
 */
void Scheduler::Event::cancel(void) {
	TRACE(qDebug() << "Event Cancelled" << endl;)
	_board.scheduler()->events.removeOne(this);
}


/**
 * Re-schedule the event for a different time.
 * @param time	New trigger time.
 */
void Scheduler::Event::reschedule(time_t time) {
	_board.scheduler()->events.removeOne(this);	
	_time = time;
	_board.scheduler()->add(this);
}




/**
 * @class Scheduler
 * This interface provides facilities to perform simulation.
 * It is currently implemented by the class TimerDriver.
 */


/**
 * @class Scheduler::Handler
 * This interface is used to get back running information
 * in an asynchrone context.
 */


/**
 * @fn void Scheduler::Handler::onPause(Core *core);
 * Called when a core is paused.
 */


/**
 * @fn bool Scheduler::Handler::stops(Core *core);
 * Called to test if the core must stop.
 * @param core	Core to test.
 * @return		True if the core must stop, false else.
 */


/**
 * @fn Scheduler::Scheduler(Board& board);
 * Build the run driver.
 * @param board		Current board.
 */


/**
 * Build a scheduler.
 * @param board		Board the scheduler will work with.
 */
Scheduler::Scheduler(Board& board): _board(board), _handler(&FOREVER) {
	board.setScheduler(this);
}


/**
 */
Scheduler::~Scheduler(void) {
}


/**
 * Singleton of a handler that let's the application run forever.
 */
Scheduler::ForeverHandler Scheduler::FOREVER;


/**
 * @fn void Scheduler::setHandler(Handler& handler);
 * Set the current handler.
 * @param handler	Set handler.
 */

/**
 * Start the simulation. After this call, all cores are paused.
 * To start really the simulation, they must be resumed.
 */
void Scheduler::start(void) {
	_time = 0;
}


/**
 * @fn void Scheduler::stop(void);
 * Stop the simulation.
 */


/**
 * @fn void Scheduler::pause(Core *core);
 * Pause the current core or the given one.
 * @param core	Core to pause.
 */


/**
 * @fn void Scheduler::pauseAll(void);
 * Pause all cores.
 */


/**
 * @fn void Scheduler::resume(Core *core);
 * Resume the run of the given core.
 * @param core	Resumed core.
 */


/**
 * @fn  void Scheduler::step(void);
 * Perform a step of simulation on the paused cores.
 */


/**
 * @fn void Scheduler::resumeAll(void);
 * Resume all drivers.
 */


/**
 * @class Scheduler::ForeverHandler
 * A run driver handler that never stops.
 */

/**
 * @class RunDriver::CompHandler
 * Basic class of handlers that may be chained. To allow chaining,
 * the stops() method of this call must be called each time
 * an overload version is called.
 */


/**
 * @class Scheduler::BreakPointsHandler
 * Handler that stops on break points.
 */


/**
 * Constructor.
 * @param breaks	List of break point addresses.
 * @param next		Next handler in the chain.
 */
Scheduler::BreakPointsHandler::BreakPointsHandler(QList<uint32_t> breaks, Handler& next)
:	CompHandler(next), _breaks(breaks) {
}


/**
 */
bool Scheduler::BreakPointsHandler::stops(Core *core) {
	uint32_t pc = core->pc();
	for(QList<uint32_t>::const_iterator brk = _breaks.begin();
	brk != _breaks.end(); ++brk)
		if(pc == *brk)
			return true;
	CompHandler::stops(core);
}


/**
 * @class Scheduler::NextHandler
 * Handler that stops when a different source line is executed.
 */


/**
 * Build a new next handler.
 * @param file	Current file.
 * @param line	Current line.
 * @param next	Next handler to apply.
 */
Scheduler::NextHandler::NextHandler(QString file, int line, Handler& next)
: CompHandler(next), _file(file), _line(line) {
}


/**
 */
bool Scheduler::NextHandler::stops(Core *core) {
	Program *prog = core->board().program();
	QString cfile;
	int cline;
	prog->lineOf(core->pc(), cfile, cline);
	return cline != _line || cfile != _file;
}


/**
 * @fn time_t Scheduler::time(void) const;
 * Get the current time.
 * @return	Current time (in master clock cycle).
 */


/**
 * Add an event to the scheduler. If the current time is greater than
 * the event time, the event will be activated on the next event processing cycle.
 * @param event	Event to add.
 */
void Scheduler::add(Event *event) {
	TRACE(qDebug() << "Scheduler: added event " <<  event->name() << " for " << event->time();)
	
	if(event->time() < nextEventTime())
		events.push_front(event);
	else {
		QList<Event *>::iterator cur = events.begin();
		for(cur++; cur != events.end(); ++cur)
			if((*cur)->time() > event->time())
				break;
		events.insert(cur, event);
	}
	TRACE(
		qDebug() << "Scheduler: events =";
		for(QList<Event *>::iterator cur = events.begin(); cur != events.end(); ++cur)
			qDebug() << "Scheduler:	" << (*cur)->name() << " at " << (*cur)->time();
	)
}


/**
 * @fn void Scheduler::increaseTime(time_t delay = 1);
 * Increase the current time of the given amount. Only callable
 * by the scheduler itself (this class and its descendent).
 * @param delay		Delay to increase the time.
 */


/**
 * @fn time_t Scheduler::nextEventTime(void) const;
 * Get the time of the next event.
 * @return	Next event time.
 */


/**
 * Process events for the current time (including the current time).
 */
void Scheduler::processEvents(void) {
	while(nextEventTime() <= _time) {
		
		Event *event = events.takeFirst();
		event->run();
		//TRACE(qDebug() << "Run called at " <<  event->time() <<endl;)
	}
}

} // bsim

