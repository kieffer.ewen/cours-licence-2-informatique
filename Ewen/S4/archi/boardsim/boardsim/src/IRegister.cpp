/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <assert.h>
#include <QDebug>
#include <IRegister.hpp>
#include <Board.hpp>
#include <Core.hpp>

namespace bsim {

/**
 * @class IRegister
 * Base of registers or bank of registers and of hardware or core registers.
 */
 
/**
 * @var IRegister::NONE
 * Type unknown for a register.
 */

/**
 * @var IRegister::INT
 * Type integer for a register.
 */

/**
 * @var IRegister::BITS
 * Type bit for a register.
 */

/**
 * @var IRegister::ADDR
 * Type address for a register.
 */

/**
 * @var IRegister::FLOAT
 * Type float for a register.
 */

/**
 * @var IRegister::READ
 * Bit mask to mark a register is readable.
 */
 
/**
 * @var IRegister::WRITE
 * Bit mask to mark a register as writable.
 */

/**
 * @var IRegister::BOTH
 * Bit mask to mark a register as both readable and writable.
 */

/**
 * @fn QString IRegister::name(int i);
 * Get the name of the register. If the register is a bank (composed of
 * multiple registers), this produces the name of register of index i.
 * If i is -1, returns the name of the bank.
 * @param i		Index of the register.
 * @return		Register name.
 */

/**
 * @fn int IRegister::count(void);
 * Get the count of register in the bank (1 for a lonely register).
 * @return	Count of registers.
 */

/**
 * @fn int IRegister::size(void);
 * Get the size in bits of the register.
 * @return	Size in bits.
 */

/**
 * @fn int IRegister::type(void);
 * Get the type of the register, one of IRegister::NONE, IRegister::INT,
 * IRegister::BITS or IRegister::ADDR.
 * @return	Register type.
 */

/**
 * @fn IRegister::QVariant get(int i);
 * Get the register value. If the register of only one register,
 * the index i may be ignored.
 * @param i	Index of the register.
 * @return	Value of the register.
 */

/**
 * @fn void IRegister::set(QVariant value, int i = 0);
 * Set the value of a register. If the register is composed of only one,
 * ignore the index i.
 * @param value		Value to set.
 * @param i			Index of the register.
 */

/**
 * @fn int IRegister::access(void);
 * Get the access type of the register, a combination of mask
 * IRegister::READ and IRegister::WRITE.
 * @return	Access type of the register.
 */

/**
 * @fn bool IRegister::isReadable(void);
 * Test if the register is readable.
 * @return	True if the register is readable, false else.
 */

/**
 * @fn bool IRegister::isWritable(void);
 * Test if the register is writable.
 * @return	True if the register is writable, false else.
 */

/**
 * @fn bool IRegister::isReadWrite(void);
 * Test if the register is readable and writable.
 * @return	True if the register is readable and writable, false else.
 */

/**
 * @fn bool IRegister::isOnlyReadable(void);
 * Test if the register is only readable.
 * @return	True if the register is only readable, false else.
 */

/**
 * @fn bool IRegister::isOnlyWritable(void);
 * Test if the register is only writable.
 * @return	True if the register is only writable, false else.
 */

/**
 * @fn HardRegister *IRegister::toHard(void);
 * Convert the register to HardRegister if it can be done, return null else.
 * @return	Conversion to hardware register.
 */

/**
 * @fn CoreRegister *IRegister::toCore(void);
 * Convert the register to CoreRegister if it can be done, return null else.
 * @return	Conversion to core register.
 */


/**
 * Format the integer value according to the required format.
 * @param i		Index of the register.
 * @param fmt	Format to display the register value (one of IRegister::DEF
 *  * 				IRegister::BIN, IRegister::DEC or IRegister::HEX).
 * @return		String displaying the register value.
 */
QString IRegister::format(int i, int fmt) {
	
	// determine the format
	int f = fmt;
	if(f == DEF)
		switch(type()) {
		case BITS:
		case ADDR:	f = HEX; break;
		case INT:	
		default:	f = DEC; break;
		}
	
	// build the format output
	QVariant v = get(i);
	int base = 10;
	switch(f) {
	case DEC: return QString::number(v.toInt());
	case BIN: return QString("0b%1").arg(uint32_t(v.toInt()), 32, 2, QChar('0'));
	case HEX: return QString("0x%1").arg(uint32_t(v.toInt()), 8, 16, QChar('0'));
	}

}


/**
 * Build the qualified name of the register. As default, it is the
 * concatenation of the component name, underscore, register name and,
 * if not negative, of the index.
 * @param component	Parent component.
 * @param index		Register index (ignored if negative).
 * @return			Qualified name.
 */
QString IRegister::qualifiedName(IComponent *component, int index) {
	if(index < 0)
		return component->name() + "_" + name();
	else
		return component->name() + "_" + name() + QString::number(index);
}


/**
 * Get the register name as it will be displayed to the human user.
 * @param index		Register index if any (optional).
 */
QString IRegister::displayName(int index) {
	if(index < 0)
		return name();
	else
		return name(index);
} 


/**
 * Generator for enumeration list.
 * To create an enumeration list, one has to provide a definition as:
 * 
 * IRegister::Field f("CLOCK", 1, 0,
 * 		_ & val("CLK0", 0b00) &  val("CLK1, 0b01") & val("CLK2", 0b10) & val("CLK3", 0b11));
 */
IRegister::EnumSet IRegister::_;


/**
 * @class IRegister::Field
 * Fields represents named parts of a register. They are defined by their
 * name, up bit number and low bit number. They can also be defined
 * with an enumeration list.
 */

/**
 * Build a single bit field.
 * @param reg	Owner register.
 * @param name	Register name.
 * @param bit	Bit number.
 */
IRegister::Field::Field(IRegister& reg, QString name, int bit)
: _name(name), _up(bit), _lo(bit), _vals(0) {
	reg._fields.append(this);
}

/**
 * Build a bit field with enumerated value.
 * @param reg	Owner register.
 * @param name	Register name.
 * @param up	Upper bit number.
 * @param lo	Lower bit number.
 * @param vals	List of enumerated values (default to null).
 * @param type	Type of field (default to VAL).
 */
IRegister::Field::Field(IRegister& reg, QString name, int up, int lo, QVector<EnumValue> *vals, field_t type)
: _name(name), _up(up), _lo(lo), _vals(vals), _type(type) {
	reg._fields.append(this);
}

/**
 */
IRegister::Field::~Field(void) {
	if(_vals)
		delete _vals;
}

/**
 * Accessor that does nothing.
 */
IRegister::AbstractAccessor IRegister::null_accessor;


/**
 * @class IRegister::AbstractAccessor
 * this class provides uniform access to the content of register
 * from the domain of ISS whatever the size, the type or the address
 * of the accessed register.
 * 
 * It is usally subclassed to provide an actual implementation of the
 * access.
 */

/**
 */
IRegister::AbstractAccessor::~AbstractAccessor(void) {
}

/**
 * Provide access to the data of the register.
 * The default implementation does nothing.
 * @param a		Accessed address.
 * @param s		Size of accessed data.
 * @param d		Buffer containing the written data or to store the read data.
 */
void IRegister::AbstractAccessor::access(addr_t a, int s, void *d) {
}


// CoreRegister32 class
#if 0
class CoreRegister32: public ICoreRegister {
public:
	CoreRegister32(Core& core, register_bank_t *bank): ICoreRegister(core, bank) { }
	
	virtual QVariant get(int i) {
		/*if(_core.state()) {
			register_value_t v = arm_get_register(_core.state(), _bank->id, i);
			return v.iv;
		}
		else*/
			return QVariant();
	}
	
	virtual void set(QVariant value, int i) {
		/*if(_core.state()) {
			register_value_t v;
			v.iv = value.toInt();
			arm_set_register(_core.state(), _bank->id, i, v);
		}*/
	}
};


/**
 * @class ICoreRegister
 * Represents the register of the processor core. Obtained from
 * the GLISS simulator. This class is not instanciable as is, use
 * the ICoregister::make() builder function.
 */

ICoreRegister *ICoreRegister::make(Core& core, register_bank_t *bank) {
	if(bank->type != RTYPE_FLOAT && bank->tsize <= 32)
		return new CoreRegister32(core, bank);
	else {
		qDebug() << "unsupported core register: " << bank->name;
		assert(false);
	}
}


/**
 * Build a core register from a GLISS descriptor.
 * @param core	Owner core of this register.
 * @param bank	GLISS bank descriptor.
 */
ICoreRegister::ICoreRegister(Core& core, register_bank_t *bank): _core(core), _bank(bank) {
}

/**
 */
QString ICoreRegister::name(int i) {
	if(i == -1)
		return _bank->name;
	else
		return QString(_bank->format).replace("%d", QString::number(i));
}

/**
 */
int ICoreRegister::count(void) {
	return _bank->size;
}

/**
 */
int ICoreRegister::size(void) {
	return _bank->tsize;
}

/**
 */
int ICoreRegister::type(void) {
	switch(_bank->type) {
	case RTYPE_NONE:	return NONE;
	case RTYPE_BITS:	return BITS;
	case RTYPE_ADDR:	return ADDR;
	case RTYPE_INT:		return INT;
	case RTYPE_FLOAT:	return FLOAT;
	}
}

/**
 */
int ICoreRegister::access(void) {
	return BOTH;
}

/**
 */
IHardRegister *ICoreRegister::toHard(void) {
	return 0;
}

/**
 */
ICoreRegister *ICoreRegister::toCore(void) {
	return this;
}
#endif


/**
 * @class IHardRegister
 * Basic class of hardware register. To create one, takes a look at classes:
 * @li HardRegister32
 */


/**
 * Protected constructor.
 * @param board	Owner board.
 * @param count	Number of registers.
 */
IHardRegister::IHardRegister(Board& board, int count)
:	_comp(0),
	_count(count),
	_size(32),
	_stride(4),
	_type(INT),
	_access(0),
	_address(0)
{
}


/**
 * Build a hard register using a Make class.
 * @param comp	Owner component.
 * @param make	Make to initialize the class.
 */
IHardRegister::IHardRegister(IComponent *comp, const Make& make)
:	_comp(comp),
	_count(make._count),
	_size(make._size),
	_stride(make._stride),
	_type(make._type),
	_access(make._access),
	_address(make._offset),
	_name(make._name),
	_format(make._format)
{
	if(!_stride)
		_stride = (_size + 7) / 8;
	comp->add(this);
}

/**
 */
QString IHardRegister::name(int i) {
	if(i == -1)
		return _name;
	else
		return QString(_format).replace("%d", QString::number(i));
}

/**
 */
int IHardRegister::count(void) {
	return _count;
}

/**
 */
int IHardRegister::size(void) {
	return _size;
}

/**
 * @fun int IRegister::stride(void) const;
 * When the items of a register array are separated by an offset bigger
 * than the register size, the stride is used to get this offset.
 * As a default, is equal to the register size (in bytes).
 */

/**
 */
int IHardRegister::type(void) {
	return _type;
}

/**
 */
int IHardRegister::access(void) {
	return _access;
}

/**
 */
IHardRegister *IHardRegister::toHard(void) {
	return this;
}

/**
 */
ICoreRegister *IHardRegister::toCore(void) {
	return 0;
}

/**
 * @fn void IHardRegister::read(addr_t addr, void *data, int size);
 * Called to read the register.
 * @param addr	Accessed address.
 * @param data	Pointer to store data to (pointed location must be of the register size).
 * @param size	Read size.
 */
 
/**
 * @fn void IHardRegister::write(addr_t addr, void *data, int size);
 * Called to write data to the register.
 * @param addr	Accessed address.
 * @param data	Pointer to data to write (pointed location must be of the register size).
 * @param size	Written size.
 */

/**
 * Get the address of the register.
 * @return	Register address.
 */
uint32_t IHardRegister::address(void) const {
	if(_comp)
		return _comp->address() + _address;
	else
		return _address;
}


/**
 * Get the monitor of the current component.
 * @return	Current monitor.
 */
IMonitor& IHardRegister::monitor() const {
	return _comp->mon;
}


/**
 * @class HardRegister32
 * Implements a lonely 32-bits hardware register.
 * Build it with HardRegister32::make() and uses the Maker building primitives.
 */

/**
 * Build an hardware 32-bits registers.
 * @param board	Owner board.
 */
HardRegister32::HardRegister32(Board& board): IHardRegister(board), _getter(nullptr), _setter(nullptr) {
}

/**
 */
QVariant HardRegister32::get(int i) {
	return _getter->get();
}

/**
 */
void HardRegister32::set(QVariant value, int i) {
	_setter->set(value.toInt());
}

/**
 */
void HardRegister32::read(addr_t addr, void *data, int size) {
	if(size != 4) {
		monitor().error(QString("invalid read size %1 at 0x%2").arg(size).arg(addr, 8, 16));
		*(uint32_t *)data = -1;
	}
	else if(_getter == nullptr) {
		monitor().error(QString("forbidden read at 0x%1").arg(addr, 8, 16));
		*(uint32_t *)data = -1;
	}
	else
		_getter->exec(addr, size, data);
}

/**
 */
void HardRegister32::write(addr_t addr, void *data, int size) {
	if(size != 4)
		monitor().error(QString("invalid write of size %1 at 0x%2").arg(size).arg(addr, 8, 16));
	else if(_setter == nullptr)
		monitor().error(QString("forbidden write at 0x%2").arg(addr, 8, 16));
	else
		_setter->exec(addr, size, data);
}

/**
 */
void HardRegister32::observe(addr_t addr, void *data, int size) {
	if(size != 4) {
		qDebug() << "invalid observed size " << size << " at " << QString("%1").arg(addr, 8, 16);
		return;
	}
	*static_cast<uint32_t *>(data) = _getter->get();
}

/**
 * Set the getter delegate.
 * @param getter	Getter to set.
 */
void HardRegister32::setGetter(Getter<int32_t> *getter) {
	_getter = getter;
	_access |= READ;
}

/**
 * Set the setter delegate.
 * @param setter	Setter to set.
 */
void HardRegister32::setSetter(Setter<int32_t> *setter) {
	_setter = setter;
	_access |= WRITE;
}


/**
 * @class ArrayHardRegister32
 * Implements an array of 32-bits hardware register.
 * Build it with ArrayHardRegister32::make() and uses the Maker building primitives.
 * 
 * Below an example of creating the SVR registers of an ARM AIC:
 * @code
 * class AIC {
 * private:
 *  uint32_t svr[32];
 * 	int32_t getSVR(int i) { return svr[i]; }
 * 	void setSVR(int i, int32_t v) { svr[i] = v; }
 * 	IHardRegister *svr_reg;
 *
 * public:
 *	AIC(Board& board) {
 *		svr_reg = ArrayHardRegister32::make(board, 32)
 *			.name("SVR")
 *			.format("SVR%d")
 *			.address(0xffff1000)
 *			.getter(t, &AIC::getSVR)
 *			.setter(t, &AIC::setSVR);
 *	}
 * };
 * @endcode
 */


/**
 * Build an hardware 32-bits register array.
 * @param board	Owner board.
 * @param size		Number of registers.
 */
ArrayHardRegister32::ArrayHardRegister32(Board& board, int size)
:	IHardRegister(board, size),
	_getter(new Getter<int32_t> *[size]),
	_setter(new Setter<int32_t> *[size])
{
}

/**
 */
QVariant ArrayHardRegister32::get(int i) {
	assert(i >= 0 && i < size());
	return _getter[i]->get();
}

/**
 */
void ArrayHardRegister32::set(QVariant value, int i) {
	assert(i >= 0 && i < size());
	_setter[i]->set(value.toInt());
}

/**
 */
void ArrayHardRegister32::read(addr_t addr, void *data, int size) {
	if(size != 4) {
		qDebug() << "invalid read size " << size << " at " << QString("%1").arg(addr, 8, 16);
		return;
	}
	_getter[(addr - address()) / 4]->exec(addr, size, data);
}

/**
 */
void ArrayHardRegister32::write(addr_t addr, void *data, int size) {
	if(size != 4) {
		qDebug() << "invalid read size " << size << " at " << QString("%1").arg(addr, 8, 16);
		return;
	}
	_setter[(addr - address()) / 4]->exec(addr, size, data);
}

/**
 */
void ArrayHardRegister32::observe(addr_t addr, void *data, int size) {
	if(size != 4) {
		qDebug() << "invalid read size " << size << " at " << QString("%1").arg(addr, 8, 16);
		return;
	}
	// TODO
}

/**
 * Add  a getter for a register.
 * @param i			Register index.
 * @param getter	Getter to use.
 */
void ArrayHardRegister32::setGetter(int i, Getter<int32_t> *getter) {
	_getter[i] = getter;
	_access|= READ;
}

/**
 * Add  a setter for a register.
 * @param i			Register index.
 * @param setter	Setter to use.
 */
void ArrayHardRegister32::setSetter(int i, Setter<int32_t> *setter) {
	_setter[i] = setter;
	_access|= WRITE;
}


/**
 * @class HardRegister32WithSoftIO
 * Build an hardware registers supporting software triggered IO,
 * that is, the access to a registers triggers different function
 * according to the source, software from the machine simulation
 * or from the user interface.
 * 
 * The getter and setter method of the HardRegister32WithSoftIO::Maker()
 * supports two method, the first for the UI access, the second for
 * the simulated software access.
 * 
 * As an example, below, the implementation of EOICR of the ARM's AIC
 * that acknowledge tan interrupt when this register is written but
 * do nothing for other operations.
 * @code
 * class AIC {
 * public:
 * 	int32_t getEOICR(void) { return 0; }
 * 	int32_t getSoftEOICR(void) { return 0; }
 * 	void setEOICR(int32_t v) { }
 * 	void setSoftEOICR(int32_t v) { /* acknowledge the IT * / }
 * 	
 * 	AIC(Board& board) {
 * 		eoicr_r = HardRegister32WithSoftIO::make(board)
 * 			.name("EOICR")
 * 			.address(0xffff0100)
 * 			.getter(*this, &AIC::getEOICR, &AIC::getSoftEOICR)
 * 			.setter(*this, &AIC::setEOICR, &AIC::setSoftEOICR);
 * 	}
 * 
 * private:
 * 	HardRegister32WithSoftIO *eoicr_r;
 * 	int32_t eoicr;
 * };
 * @endcode
 */

/**
 * Build an hardware registers supporting software triggered IO.
 * @param board	Owner board.
 */
HardRegister32WithSoftIO::HardRegister32WithSoftIO(Board& board): HardRegister32(board) {
}

}	// bsim
