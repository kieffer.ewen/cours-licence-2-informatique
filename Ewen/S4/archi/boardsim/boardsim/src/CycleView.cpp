/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <QStatusBar>

#include <CycleView.hpp>
#include <MainWindow.hpp>

namespace bsim {

/**
 * @class CycleView
 * CycleView class
 */


/**
 * Default displayed string.
 */
const QString CycleView::none = "--- --- ---";


/**
 * Build the cycle view for the given window.
 * @param window	Window cycle view work in.
 */
CycleView::CycleView(MainWindow& window): QLineEdit /*QLabel*/ (none), win(window) {
	setObjectName("cycle-view");
	setReadOnly(true);
	setMaxLength(11);
	setFocusPolicy(Qt::NoFocus);
	setVisible(true);
	win.statusBar()->addPermanentWidget(this);
	timer.setInterval(250);
	connect(&timer, SIGNAL(timeout()), this, SLOT(timeout(void)));
	setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
	setToolTip(tr("Simulation time (in master clock cycle)."));
}


/**
 */
QSize CycleView::sizeHint(void) const {
	QSize size = QWidget::sizeHint();
	QFontMetrics metrics(QApplication::font());
	QMargins margins = textMargins();
	size.setWidth(metrics.width("8888 888 888") + minimumWidth ());
	return size;
}


/**
 */
CycleView::~CycleView(void) {
}


/**
 */
void CycleView::onRun(void) {
	update();
	timer.start();
}


/**
 */
void CycleView::onBreak(void) {
	timer.stop();
	update();
}


/**
 */
void CycleView::update(void) {
	QString s = QString("%1").arg(win.board().scheduler()->time(), 9, 10, QChar('0'));
	setText(s.left(3) + " " + s.mid(3, 3) + " " + s.right(3));
}


/**
 */
void CycleView::timeout(void) {
	update();
}


/**
 */
void CycleView::onStart(void) {
}


/**
 */
void CycleView::onStop(void) {
	timer.stop();
	setText(none);
}

}	// bsim
