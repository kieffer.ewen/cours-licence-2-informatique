/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <iostream>

#include <QDebug>
#include <QGraphicsItemGroup>

#include <Board.hpp>
#include <Display.hpp>
#include <IComponent.hpp>
#include <Scheduler.hpp>

using namespace std;

namespace bsim {

/**
 * Parse a C formatted integer from a string.
 * @param str	String to parse.
 * @param ok	Set to false if there is an error, true else.
 * @return		Read integer.
 */
int parseUInt(QString str, bool& ok) {
	int base = 10;
	int val;
	if(str.length() > 2) {
		if(str.startsWith("0x") or str.startsWith("0X")) {
			base = 16;
			str = str.mid(2);
		}
		else if(str.startsWith("0b") or str.startsWith("0B")) {
			base = 2;
			str = str.mid(2);
		}
		else if(str.startsWith("0o") or str.startsWith("0O")) {
			base = 8;
			str = str.mid(2);
		}
	}
	return str.toUInt(&ok, base);
}


/**
 * @class ComponentException
 * Exception thrown by a component during its build.
 */

/**
 * Build a component exception.
 * @param name		Component name.
 * @param type		Component type.
 * @param message	Exception message.
 */
ComponentException::ComponentException(QString name, QString type, QString message)
: Exception(QString("%1 (%2): %3").arg(name).arg(type).arg(message)) {
}


/**
 * @class IMonitor
 * This class provides a way of communication between component simulation
 * and the UI.
 */

/**
 * @fn void IMonitor::warn(QString message);
 * Display a warning.
 * @param message	Message to display.
 */

/**
 * @fn void IMonitor::error(QString message);
 * Display an error.
 * @param message	Message to display.
 */

/**
 * @fn void IMonitor::info(QString message);
 * Display an information.
 * @param message	Message to display.
 */

class DefaultMonitor: public IMonitor {
public:
	void warn(QString message) 	{ cerr << "WARNING: " << message.toStdString() << endl; }
	void error(QString message) { cerr << "ERROR: " << message.toStdString() << endl; }
	void info(QString message) 	{ cerr << "INFO: " << message.toStdString() << endl; }
};
static DefaultMonitor def_mon;
IMonitor *IMonitor::mon = &def_mon;


/**
 * @class Constant
 * Constant to generate automatically definitions for programming the component.
 * A constant has a name and an 32-bits integer value.
 */
 
 
/**
 * @class IComponent
 * Represents a component of the card.
 * 
 * @par Default Configuration
 * 
 * The following attributes are automatically handled by this class:
 * @li @c x
 * @li @c y
 * @li @c w
 * @li @c h
 * @li @c name
 * 
 * The following configuration elements are also supported:
 * @li @c base
 * @li @c link
 */

/**
 */
IComponent::~IComponent(void) {
}


/**
 * @fn const QVector<Pin *>& IComponent::pins(void) const;
 * Get the pins of the component.
 * @return	Vector of pins.
 */


/**
 * Get a pin by its name.
 * @param name	Name of the port.
 * @return		Found pin or null.
 */
Pin *IComponent::getPin(QString name) {
	const QVector<Pin *>& ps = pins();
	for(QVector<Pin *>::const_iterator pin = ps.begin(); pin != ps.end(); ++pin)
		if((*pin)->name() == name)
			return *pin;
	return 0;
}


/**
 * Add a pin.
 * @param pin	Pin to add.
 */
void IComponent::add(Pin *pin) {
	_pins.push_back(pin);
}


/**
 * This method may be overriden by classes implementing an IComponent.
 * It is called to let a component install its own widget.
 * @param parent	Parent widget of the installed one.
 * @deprecated
 */
void IComponent::installUI(MainWindow& window) {
}

/**
 * This method may be overriden by classes implementing an IComponent.
 * It is called to ask a component to remove tits widgets.
 * @param parent	Parent widget of the installed one.
 * @deprecated
 */
void IComponent::uninstallUI(MainWindow& window) {
}


/**
 * @fn QString IComponent::name(void) const;
 * Get the name of the component.
 * @return	Component name.
 */


/**
 * Default empty builder.
 * @param board		Owner board.
 * @param monitor	Moniyot to use.
 */
IComponent::IComponent(Board& board, QDomElement configuration, IMonitor& monitor)
: conf(configuration), _board(board), mon(monitor), disp(false), _address(0) {
}

/**
 * Get the prefix to use when an API is generated relative to the component.
 * As a default, return the name.
 * @return	Component relative prefix.
 */
QString IComponent::namePrefix(void) {
	return name();
}

/**
 * Get the prefix relative to the type of the component.
 * As a default, return the type.
 * @return	Type relative prefix.
 */
QString IComponent::typePrefix(void) {
	return type();
}


/**
 * Build the configuration from the given XML element.
 * Uses the following items:
 * @li name attribute of the component,
 * @li link sub-elements.
 * @param node	Top-node of the configuration.
 */
void IComponent::configure(QDomElement node) {

	// parse the name
	_name = node.attribute("name");

	// examine children elements
	QDomElement element = node.firstChildElement() ;
	while(!element.isNull()) {
		if(element.tagName() == "link")
			configureLink(element);
		else if(element.tagName() == "base") {
			if(element.hasAttribute("value")) {
				bool ok = true;
				int val = parseUInt(element.attribute("value"), ok);
				if(ok)
					_address = val;
			}
		}
		else
			configureOther(element);
		element = element.nextSiblingElement() ;
	}
}


/**
 * Called to generate custom code in header generation.
 * As a default, do nothing.
 * @param out	Stream to output to.
 */
void IComponent::genHeader(QTextStream& out) {
}


/**
 * Perform configuration from a link element.
 * @param element	XML element containing the configuration.
 */
void IComponent::configureLink(QDomElement element) {
	
	// get the target component
	if(!element.hasAttribute("to")) {
		error("no \"to\" attribute in \"link\" element.");
		return;
	}
	IComponent *to = _board.getComponent(element.attribute("to"));
	if(!to) {
		error(QString("no \"%1\" component").arg(element.attribute("to")));
		return;
	}
	
	// get the port
	if(!element.hasAttribute("port")) {
		error("no \"port\" attribute in \"link\" element.");
		return;
	}
	Pin *port = to->getPin(element.attribute("port"));
	if(!port) {
		error(QString("no pin \"%1\" in the \"%2\" component").arg(element.attribute("port")).arg(to->name()));
		return;
	}
	
	// get the pin
	if(!element.hasAttribute("pin")) {
		error("no \"pin\" attribute in \"link\" element.");
		return;
	}
	Pin *pin = getPin(element.attribute("pin"));
	if(!pin) {
		error(QString("no pin \"%1\" in the \"%2\" component").arg(element.attribute("pin")).arg(name()));
		return;
	}

	// make the link !
	port->connect(pin);
}


/**
 * Called when a configuration element is not known.
 * As a default, ignore the element.
 * @param element	Unknown element.
 */
void IComponent::configureOther(QDomElement element) {
}


/**
 * Identify the component and display a warning.
 * @param message	Message to display.
 */
void IComponent::warn(QString message) {
	mon.warn(QString("%1: %2").arg(name()).arg(message));
}

/**
 * Identify the component and display an error.
 * @param message	Message to display.
 */
void IComponent::error(QString message) {
	mon.error(QString("%1: %2").arg(name()).arg(message));
}

/**
 * Identify the component and display an information.
 * @param message	Message to display.
 */
void IComponent::info(QString message) {
	mon.info(QString("%1: %2").arg(name()).arg(message));
}

/**
 * Add a register to the register list of the component.
 * @param reg	Register to add.
 */
void IComponent::add(IRegister *reg) {
	regs.push_back(reg);
}


/**
 * Add an hardware register to the register list of the component.
 * It is important to call this function with hardware register to ensure
 * the back link to be done between the register and the component.
 * @param reg	Added hardware register.
 */
void IComponent::add(IHardRegister *reg) {
	regs.push_back(reg);
	reg->_comp = this;
	//qDebug() << "DEBUG: adding " << reg->name(-1) << " (" <<(void *)reg->address() << ") to " << type();
}


/**
 * Convert component to a core if it is a core, return null else.
 * @return	Core pointer or null.
 */
Core *IComponent::toCore(void) {
	return 0;
}


/**
 * Get constants that are independent of an instance of the component.
 * @param csts	To store the static constants.
 */
void  IComponent::getStaticConstants(QVector<Constant>& csts) const {
}


/**
 * Get constants that are dependent on a specific instance of the component.
 * @param csts	To store the dynamic constants.
 */
void IComponent::getDynamicConstants(QVector<Constant>& csts) const {
}


/**
 * Call to let the component install its own view in the scene.
 * If the @ref gitem attribute is set, add this item.
 * @param scene		Scene to display on.
 * @deprecated
 */
void IComponent::install(QGraphicsScene& scene) {
}


/**
 * Call to let the component uninstall its own view in the scene.
 * If the @ref gitem attribute is set, remove this item (but do not free it).
 * @param scene		Scene to display on.
 * @deprecated
 */
void IComponent::uninstall(QGraphicsScene& scene) {
}


/**
 * @fn bool IComponent::displayed(void) const;
 * Test if the component is displayed or not.
 * @return	True if it is displayed, false else.
 */


/**
 * Build the visual of a component.
 * As a default, do nothing.
 * @param graph		Graph to make visual in.
 */
void IComponent::makeVisual(VisualGraph& graph) {
}


/**
 * Called when the simulation is started to let the component
 * do its action at startup.
 * 
 * As a default do nothing.
 */
void IComponent::start(void) {
}


/**
 * Called when the simulation is stopped to let the component
 * do its action at this time.
 * 
 * As a default do nothing.
 */
void IComponent::stop(void) {
}


/**
 * Get the current time.
 * @return	Return time in cycles.
 */
time_t IComponent::time(void) {
	return board().scheduler()->time();
}


/**
 * Provide a log stream for debugging purpose. Each time this function
 * is called, the date and the current component are displayed as prefixes.
 * Other display can be appended using the "<<" usual operator for outputting.
 */
QTextStream& IComponent::log(void) {
	static QTextStream  stream(stderr);
	stream << name() << ": " << qSetFieldWidth(10) << time() << qSetFieldWidth(0) << ": ";
	return stream;
}


/**
 * Generate code for header file using the component.
 * @param lang	Language to generate for.
 * @param out	Stream to output to.
 */
void IComponent::genHeader(lang_t lang, QTextStream& out) {
}

/**
 * This function is called each time a component need to be reconfigured
 * (typically just after the creation). The default implementation
 * does nothing.
 * @param conf		Current configuration item.
 * @param monitor	Monitor to display errors.
 */
void IComponent::configure(Configuration& conf, IMonitor& monitor) {
}


/**
 * @class Configuration
 * This class is abstraction of a configuration storage for a component.
 * It provides facilities to access easily the configuration information.
 */

/**
 */
Configuration::Configuration(IComponent *comp, QDomElement& elt, IMonitor& mon)
: _comp(comp), _elt(elt), _mon(mon) {
}

/**
 * Test if a configuration item is defined.
 * @param name	Configuration item name.
 * @return		True if it is defined, false else.
 */
bool Configuration::isDefined(QString name) {
	return _elt.hasAttribute(name);
}

/**
 * Get a boolean configuration item.
 * @param name	Information name.
 * @param def	Default value if the information is not available.
 * @return		Information value.
 */
bool Configuration::get(QString name, bool def) {
	if(_elt.hasAttribute(name)) {
		QString val = _elt.attribute(name);
		if(val == "on" || val == "yes" || val == "true")
			return true;
		else if(val == "off" || val == "no" || val == "false")
			return false;
		else
			_mon.warn(QString("bad boolean value in attribute %1 for %2").arg(name).arg(_comp->name()));
	}
	return def;
}

/**
 * Get a string configuration item.
 * @param name	Information name.
 * @param def	Default value if the information is not available.
 * @return		Information value.
 */
QString Configuration::get(QString name, QString def) {
	if(_elt.hasAttribute(name))
		return _elt.attribute(name);
	else
		return def;
}

/**
 * Get an integer configuration item.
 * @param name	Information name.
 * @param def	Default value if the information is not available.
 * @return		Information value.
 */
int Configuration::get(QString name, int def) {
	if(_elt.hasAttribute(name)) {
		bool ok = true;
		int val = parseUInt(_elt.attribute(name), ok);
		if(ok)
			return val;
	}
	return def;
}

/**
 * Get a double configuration item.
 * @param name	Information name.
 * @param def	Default value if the information is not available.
 * @return		Information value.
 */
double Configuration::get(QString name, double def) {
	if(_elt.hasAttribute(name)) {
		bool ok = true;
		double val = _elt.attribute(name).toDouble(&ok);
		if(ok)
			return val;
	}
	return def;
}

/**
 * Get a color configuration item.
 * @param name	Information name.
 * @param def	Default value if the information is not available.
 * @return		Information value.
 */
QColor Configuration::get(QString name, QColor def) {
	if(_elt.hasAttribute(name)) {
		QColor color(_elt.attribute(name));
		if(color.isValid())
			return color;
	}
	return def;
}

}	// bsim
