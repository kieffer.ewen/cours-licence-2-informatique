/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include "ISource.hpp"

namespace bsim {

/**
 * @class ISource
 * This class allows to display and exploits a source view.
 * It will be inherited from disassembly viewer or real soruce viewer.
 * Sources are accessed on a line basis and all operation (like break
 * management) uses the line numbering. Line numbers starts at 1 and
 * 0 is used as a null line number.
 */


///
ISource::~ISource() {
}


/**
 * @fn void ISource::update();
 * Called to update the source.
 */

/**
 * @fn QString ISource::getText(void);
 * Get the text of the source.
 * @return Source text.
 */
 

/**
 * @fn bool ISource::setBreak(int line);
 * Set a break on the given line.
 * @param line	Line to put the break to.
 * @return		True if the break has been put, false else.
 */


/**
 * @fn bool ISource::isBreak(int line);
 * Test if the current line contains a break.
 * @param line	Used line.
 * @return		True if there is a break, false else.
 */


/**
 * @fn int ISource::getLine(uint32_t address);
 * Get the line matching the given address.
 * @param address	Address to get a line for.
 * @return			Address in the source, 0 if none found.
 */


/**
 * @fn const ISource::QSet<uint32_t>& breaks(void);
 * Get the set breaks.
 * @return	Set of breaks.
 */


/**
 * Get the highlighter for the source.
 * @return	Matching highlighter or null (if there is no highlighter).
 */
Highlighter *ISource::highlighter(void) {
	return 0;
}

} // bsim
