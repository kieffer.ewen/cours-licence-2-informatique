/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <QSet>
#include <QSettings>

#include "BoardView.hpp"
#include "CHighlighter.hpp"
#include "CodeViewer.hpp"
#include "Core.hpp"
#include "CycleView.hpp"
#include "DisasmSource.hpp"
#include "FileSource.hpp"
#include "IComponent.hpp"
#include "MainWindow.hpp"
#include "MemoryDockWidget.hpp"
#include "Program.hpp"
#include "TimerScheduler.hpp"
#include "AssemblyHighlighter.hpp"

#include "config.hpp"

#define USE_DRIVER

namespace bsim {

const QString MainWindow::PROG_VERSION = VERSION;
const QString MainWindow::PROG_NAME = PROJECT;


/**
 * @class LogMonitor;
 * A monitor that accumulates error in a string.
 */

/**
 * Build a log monitor.
 * @param max	Maximum numr of kept messages (-1 for all) (default to 1024).
 */
LogMonitor::LogMonitor(int max): _size(0), _max(max) {
}

/**
 * Reset the log.
 */
void LogMonitor::reset() {
	_log.clear();
}

/**
 * @fn int LogMonitor::max() const;
 * Get the maximum number of message kept in the monitor.
 * @return	Maximum number of messages.
 */

/**
 * @fn const QLinkedList<QString>& LogMonitor::log() const;
 */

///
void LogMonitor::warn(QString message) {
	append("WARNING: " + message);
}

///
void LogMonitor::error(QString message) {
	append("ERROR: " + message);
}

///
void LogMonitor::info(QString message) {
	append(message);
}

/**
 * Append a message and ensures it doesn't the number of message
 * grow out of the maximum.
 * @param m		Message to add.
 */
void LogMonitor::append(QString m) {
	if(_size == _max) {
		_log.removeFirst();
		cleanFirst();
	}
	else
		_size++;
	_log.append(m);
	newMessage(m);
}


/**
 * Monitor for an action.
 */
class ActionMonitor: public IMonitor {
public:
	ActionMonitor(MainWindow& window, QString action)
	: _win(window), error_cnt(0), warn_cnt(0), buf(""), _action(action) {
	}

	virtual void warn(QString message) {
		buf = buf + "WARNING: " + message + "\n";
		warn_cnt++;
	}
	
	virtual void error(QString message) {
		buf = buf + "ERROR: " + message + "\n";
		error_cnt++;
	}
	
	virtual void info(QString message) {
		buf = buf + "INFO: " + message + "\n";
	}
	
	/**
	 * Handle an error action (must be called after an action using the monitor).
	 * @return		True if the action is successful, false else.
	 */
	bool handleError(void) {
		if(error_cnt + warn_cnt != 0) {
			QMessageBox *dialog = new QMessageBox(
				error_cnt ? QMessageBox::Warning : QMessageBox::Warning,
				_action + ": " + (error_cnt ? "errors" : "warnings"),
				buf,
				QMessageBox::Ok,
				&_win,
				Qt::Popup
			);
			dialog->exec();
		}
		return error_cnt == 0;
	}

	/**
	 * Reset the monitor.
	 */
	void reset(void) {
		error_cnt = 0;
		warn_cnt = 0;
		buf = "";
	}
	
	/**
	 * Test if there are errors.
	 * @return	True if there is an error, false else.
	 */
	inline bool hasError(void) {
		return error_cnt;
	}

private:
	MainWindow& _win;
	int error_cnt, warn_cnt;
	QString buf;
	QString _action;
};


/**
 * Build a window with the given board and executable file.
 * @param xml		Board description file.
 * @param elf		Executable file.
 */
MainWindow::MainWindow(QString xml, QString elf)
:	//dmem("Memory", this),
	board_ready(false),
	exec_ready(false),
	code_viewer(new CodeViewer(*this, this)),
	driver(new TimerScheduler(_board)),
	current(0),
	break_handler(*this),
	next_handler(*this),
	over_handler(*this),
	prog(0),
	dis(0),
	cur_source(0),
	banner(0),
	open_exec_icon(0),
	open_board_icon(0),
	quit_icon(0),
	step_icon(0),
	start_icon(0),
	stop_icon(0),
	run_icon(0),
	break_icon(0),
	window_icon(0),
	start_at_main(true),
	dis_selected(false)
{
	init() ;
	loadBoard(xml);
	if(board_ready)
		loadExec(elf);
	updateActions();
}


/**
 * Build the main window.
 */
MainWindow::MainWindow(void)
:	board_ready(false),
	exec_ready(false),
	code_viewer(new CodeViewer(*this, this)),
	driver(new TimerScheduler(_board)),
	current(0),
	break_handler(*this),
	next_handler(*this),
	over_handler(*this),
	prog(0),
	dis(0),
	cur_source(0),
	banner(0),
	open_exec_icon(0),
	open_board_icon(0),
	quit_icon(0),
	step_icon(0),
	start_icon(0),
	stop_icon(0),
	run_icon(0),
	break_icon(0),
	window_icon(0),
	start_at_main(true),
	dis_selected(false)
{
	init();
	updateActions();
}



/**
 * Update the current executable (typically after a compilation).
 */
void MainWindow::updateExec() {
	exec_ready = false;
	updateActions();

	// load the executable
	ActionMonitor mon(*this, "load an executable");
	prog = _board.mainCore()->load(exec_path, mon);
	_board.setProgram(prog);
	_bpts.setProgram(prog);

	// re-populate the source box
	dis->update(prog);
	QString current_source;
	if(source_box->currentIndex() != 0)
		current_source = source_box->currentText();
	QList<FileSource *> sources;
	prog->getSources(sources);
	QSet<QString> new_sources;
	for(const auto s: sources) {
		int i;
		for(i = 1; i < source_box->count(); i++)
			if(source_box->itemText(i) == s->name())
				break;
		if(i >= source_box->count())
			source_box->addItem(s->name());
		new_sources.insert(s->name());
	}
	for(int i = 1; i < source_box->count();) {
		if(new_sources.contains(source_box->itemText(i))) {
			if(current_source == source_box->itemText(i))
				changeSource(i);
			i++;
		}
		else
			source_box->removeItem(i);
	}
	if(source_box->currentIndex() == 0)
		changeSource(0);

	// re-enable all
	exec_ready = true;
	updateActions();
}


/**
 * Load the executable.
 * @param path		Path to the executable.
 */
void MainWindow::loadExec(QString path) {
	exec_ready = false;
	updateActions();
	
	// reset all
	while(source_box->count())
		source_box->removeItem(0);
	if(dis)
		delete dis;
	
	// load the executable
	ActionMonitor mon(*this, "load an executable");
	assert(_board.mainCore());
	prog = _board.mainCore()->load(path, mon);
	_board.setProgram(prog);
	_bpts.setProgram(prog);
	
	// compute the settings path
	if(path.endsWith(".elf"))
		settings_path = path.left(path.size() - 4) + ".bsim";
	else
		settings_path = path + ".bsim";
	loadSettings();

	// populate source box
	bool old_dis = dis_selected;
	dis = new DisasmSource(_board.program());
	source_box->addItem(dis->name());
	QList<FileSource *> sources;
	prog->getSources(sources);
	for(QList<FileSource *>::const_iterator source = sources.begin(); source != sources.end(); source++)
		source_box->addItem((*source)->name());
	dis_selected = old_dis;

	// show the main source
	addr_t a = prog->addressOf("main");
	if(a != 0)
		showInSource(a);

	// if a board is loaded, show it
	exec_ready = true;
	if(exec_path != path) {
		if(exec_path != "")
			watcher.removePath(QFileInfo(exec_path).path());
		watcher.addPath(QFileInfo(path).path());
		exec_path = path;
		exec_date = QFileInfo(path).lastModified();
	}
	updateActions();
}


/**
 * Load the board.
 * @param path	Path to the file.
 */
void MainWindow::loadBoard(QString path) {
	board_ready = false;
	exec_ready = false;
	updateActions();
	IMonitor::setDef(log);
	
	// cleanup all
	for(QList<IView *>::const_iterator view = views.begin(); view != views.end(); ++ view)
		(*view)->onClear();
	
	// load the board
	try {
		ActionMonitor mon(*this, "load a board");
		_board.load(path, mon);
		if(!mon.handleError())
			return;
		if(!_board.mainCore()) {
			error("No execution core provided on the board!");
			return;
		}
	}
	catch(LoadException& exn) {
		error(exn.message());
		return;
	}
	
	// initialize the UI
	for(QList<IView *>::const_iterator view = views.begin(); view != views.end(); ++ view)
		(*view)->onBoardLoad();
	QMap<QString, IComponent*> comps = _board.getComponents();
	for(QMap<QString, IComponent*>::const_iterator comp = comps.begin(); comp != comps.end(); ++comp)
		(*comp)->installUI(*this);

	// if available, load the settings
	if(!settings_path.isNull()) {
		QSettings settings(settings_path, QSettings::NativeFormat);
		for(QList<IView *>::const_iterator view = views.begin(); view != views.end(); ++ view)
			(*view)->loadSettings(settings);
	}
	
	// set the current core
	current = _board.mainCore();
	board_ready = true;
	updateActions();
}


/**
 * Initialize the GUI.
 */
void MainWindow::init() {
	
	// prepare window
	setWindowTitle(PROG_NAME + " " + PROG_VERSION) ;
	setUnifiedTitleAndToolBarOnMac(true) ;
	setCentralWidget(code_viewer) ;
	driver->setHandler(break_handler);

	// initialize the highlighters
	static AssemblyHighlighter assembly;
	Highlighter::registerForExt("s", &assembly);
	Highlighter::registerForExt("i", &assembly);
	static CHighlighter c;
	Highlighter::registerForExt("c", &c);
	Highlighter::registerForExt("h", &c);
	Highlighter::registerForExt("cpp", &c);
	Highlighter::registerForExt("hpp", &c);
	Highlighter::registerForExt("H", &c);
	Highlighter::registerForExt("C", &c);
	Highlighter::registerForExt("hxx", &c);
	Highlighter::registerForExt("cxx", &c);

	// compute the pix path
	pix_path = QApplication::applicationDirPath();
	pix_path = pix_path + "/../share/bsim/pix/";

	// set the window icon
	window_icon = new QIcon(pix_path + "boardsim.png");
	setWindowIcon(*window_icon);

	// Create actions
	openELFAct = new QAction(tr("&Open program"),this);
	openELFAct->setShortcuts(QKeySequence::Open);
	openELFAct->setStatusTip(tr("Load an ARM ELF file"));
	connect(openELFAct, SIGNAL(triggered()), this, SLOT(openELF()));
	open_exec_icon = new QIcon(pix_path + "open-exec.png");
	openELFAct->setIcon(*open_exec_icon);

	openXMLAct = new QAction(tr("&Open board"),this);
	openXMLAct->setShortcut( QKeySequence(tr("Ctrl+l"))) ;
	openXMLAct->setStatusTip(tr("Load a board definition"));
	connect(openXMLAct, SIGNAL(triggered()), this, SLOT(openXML()));
	open_board_icon = new QIcon(pix_path + "open-board.png");
	openXMLAct->setIcon(*open_board_icon);

	quit_action = new QAction(tr("&Quit"),this);
	quit_action->setShortcut( QKeySequence(tr("Ctrl+Q"))) ;
	quit_action->setStatusTip(tr("Quit BoardSim"));
	connect(quit_action, SIGNAL(triggered()), this, SLOT(close()));
	quit_icon = new QIcon(pix_path + "quit.png");
	quit_action->setIcon(*quit_icon);

	aboutAct = new QAction(tr("&About"), this);
	aboutAct->setStatusTip(tr("Show the application's About box"));
	connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

	aboutQtAct = new QAction(tr("About &Qt"), this);
	aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
	connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));

	arm_startAct = new QAction(tr("&Start"), this) ;
	arm_startAct->setStatusTip(tr("Start a simulation"));
	arm_startAct->setShortcut( QKeySequence(tr("Ctrl+s"))) ;
	start_icon = new QIcon(pix_path + "start.png");
	arm_startAct->setIcon(*start_icon);
	connect(arm_startAct, SIGNAL(triggered()), this, SLOT(arm_start())) ;
	arm_startAct->setEnabled(false);

	arm_stepAct = new QAction(tr("&Step"), this) ;
	arm_stepAct->setShortcut( QKeySequence(tr("Ctrl+i"))) ;
	arm_stepAct->setStatusTip(tr("Step on the next instruction"));
	arm_stepAct->setDisabled(true);
	step_icon = new QIcon(pix_path + "step.png");
	arm_stepAct->setIcon(*step_icon);
	connect(arm_stepAct, SIGNAL(triggered()), this, SLOT(arm_step())) ;

	arm_nextAct = new QAction(tr("&Next"), this) ;
	arm_nextAct->setShortcut( QKeySequence(tr("Ctrl+n"))) ;
	arm_nextAct->setStatusTip(tr("Execute to next line"));
	arm_nextAct->setDisabled(true);
	next_icon = new QIcon(pix_path + "next.png");
	arm_nextAct->setIcon(*next_icon);
	connect(arm_nextAct, SIGNAL(triggered()), this, SLOT(arm_next())) ;

	arm_overAct = new QAction(tr("&Over"), this) ;
	arm_overAct->setShortcut( QKeySequence(tr("Ctrl+o"))) ;
	arm_overAct->setStatusTip(tr("Execute to next line, jump over function calls"));
	arm_overAct->setDisabled(true);
	over_icon = new QIcon(pix_path + "over.png");
	arm_overAct->setIcon(*over_icon);
	connect(arm_overAct, SIGNAL(triggered()), this, SLOT(arm_over())) ;

	arm_runAct = new QAction(tr("&Continue"), this) ;
	arm_runAct->setDisabled(true);
	arm_runAct->setShortcut(QKeySequence(tr("Ctrl+c"))) ;
	run_icon = new QIcon(pix_path + "run.png");
	arm_runAct->setIcon(*run_icon);
	connect(arm_runAct, SIGNAL(triggered()), this, SLOT(arm_run())) ;

	arm_breakAct = new QAction(tr("&Break"), this) ;
	arm_breakAct->setDisabled(true);
	arm_breakAct->setStatusTip(tr("Break the simulation"));
	arm_breakAct->setShortcut( QKeySequence(tr("Ctrl+b"))) ;
	break_icon = new QIcon(pix_path + "break.png");
	arm_breakAct->setIcon(*break_icon);
	connect(arm_breakAct, SIGNAL(triggered()), this, SLOT(arm_break())) ;

	arm_stopAct = new QAction(tr("&Stop"), this) ;
	arm_stopAct->setDisabled(true);
	arm_stopAct->setStatusTip(tr("Stop current simulation"));
	arm_stopAct->setShortcut( QKeySequence(tr("Ctrl+x"))) ;
	stop_icon = new QIcon(pix_path + "stop.png");
	arm_stopAct->setIcon(*stop_icon);
	connect(arm_stopAct, SIGNAL(triggered()), this, SLOT(arm_stop())) ;

	// Menus
	fileMenu = this->menuBar()->addMenu(tr("File")) ;
	fileMenu->addAction(openELFAct) ;
	fileMenu->addAction(openXMLAct) ;
	fileMenu->addSeparator();
	fileMenu->addAction(quit_action);
	viewMenu = this->menuBar()->addMenu(tr("View")) ;
	simulationMenu = this->menuBar()->addMenu(tr("Simulation")) ;
	simulationMenu->addAction(arm_startAct) ;
	simulationMenu->addAction(arm_stepAct) ;
	simulationMenu->addAction(arm_nextAct) ;
	simulationMenu->addAction(arm_overAct) ;
	simulationMenu->addAction(arm_runAct) ;
	simulationMenu->addAction(arm_breakAct) ;
	simulationMenu->addAction(arm_stopAct) ;
	help_menu = this->menuBar()->addMenu(tr("Help")) ;
	help_menu->addAction(aboutQtAct) ;
	help_menu->addAction(aboutAct) ;

	// source combo
	source_box = new QComboBox(this);
	source_box->setObjectName("source-box");
	connect(source_box, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSource(int)));	

	// Toolbar
	file_tb = addToolBar(tr("File"));
	file_tb->setObjectName("file-toolbar");
	file_tb->addAction(openELFAct);
	file_tb->addAction(openXMLAct);
	file_tb->addAction(quit_action);
	tb = this->addToolBar(tr("Simulation")) ;
	tb->setObjectName("simulation-toolbar");
	tb->addAction(arm_startAct);
	tb->addAction(arm_nextAct);
	tb->addAction(arm_overAct);
	tb->addAction(arm_stepAct);
	tb->addAction(arm_runAct);
	tb->addAction(arm_breakAct);
	tb->addAction(arm_stopAct);
	tb->addWidget(source_box);

	// add register dock
	views.push_back(new RegisterDockWidget(*this));
	views.push_back(new MemoryDockWidget(*this));
	
	// add board view
	views.push_back(new BoardView(*this));
	
	// add cycle view
	views.push_back(new CycleView(*this));

	// set initial state
	run_state = stopped;
	updateActions();

	// StatusBar
	statusBar()->showMessage(tr("Welcome in BoardSim !")) ;
	
	// watcher connect
	connect(&watcher, SIGNAL(directoryChanged(const QString&)), this, SLOT(execDirChanged(const QString&)));	
}

MainWindow::~MainWindow() {
	
	// close event
	onClose();
	
	// program cleanup
	if(prog)
		delete prog;
		
	// cleanup UI
	delete code_viewer;
	if(banner)
		delete banner;
	if(open_exec_icon)
		delete open_exec_icon;
	if(open_board_icon)
		delete open_board_icon;
	if(quit_icon)
		delete quit_icon;
	if(step_icon)
		delete step_icon;
	if(start_icon)
		delete start_icon;
	if(stop_icon)
		delete stop_icon;
	if(run_icon)
		delete run_icon;
	if(break_icon)
		delete break_icon;
	if(window_icon)
		window_icon = 0;
}


/**
 * Update the status bar with the given message.
 * @param s		Message to display.
 */
void MainWindow::updateStatusbar(QString s) {
	statusBar()->showMessage(s, 5000) ;
}


/**
 * Action of opening an ELF file.
 */
void MainWindow::openELF() {
	QString filename = QFileDialog::getOpenFileName(this,tr("Open an ELF file"), ".", tr("ELF Files (*.elf)") ) ;
	loadExec(filename) ;
}


/**
 * Action of opening a board.
 */
void MainWindow::openXML() {
	QString filename = QFileDialog::getOpenFileName(this,tr("Open an XML board definition"), ".", tr("XML Files (*.xml)") );
	if (filename != NULL) {
		loadBoard(filename);
		updateStatusbar("Board definition loaded") ;
	}
}


/**
 * Display an error dialog.
 * @param message	Error message.
 */
void MainWindow::error(QString message) {
	QMessageBox msgBox;
	msgBox.setIcon(QMessageBox::Critical) ;
	msgBox.setText(message);
	msgBox.exec();
}


void MainWindow::updateActions(void) {
	openELFAct->setEnabled(board_ready);
	arm_startAct->setEnabled(board_ready && exec_ready && run_state == stopped);
	arm_stepAct->setEnabled(run_state == paused);
	arm_nextAct->setEnabled(run_state == paused);
	arm_overAct->setEnabled(run_state == paused);
	arm_runAct->setEnabled(run_state == paused);
	arm_breakAct->setEnabled(run_state == running);
	arm_stopAct->setEnabled(run_state == paused);
	openELFAct->setEnabled(board_ready && run_state == stopped);
	openXMLAct->setEnabled(run_state == stopped);
}


/**
 * Start the simulation.
 */
void MainWindow::arm_start() {

	// start simulation
	run_state = paused;
	driver->start();
	_board.start();
	prog->reset();

	// start UI
	updateActions();
	showInSource(current->pc());
	for(QList<IView *>::const_iterator view = views.begin(); view != views.end(); ++view) {
		(*view)->onStart();
		(*view)->update() ;
	}
	
	// handle start at main
	starting = false;
	if(start_at_main) {
		addr_t main = prog->addressOf("main");
		if(main) {
			starting = true;
			if(!_bpts.breaksAt(main)) {
				_main_bp = new AddressBreakPoint(main);
				_bpts.add(_main_bp);
			}
			arm_run();
		}
	}
}

void MainWindow::arm_step() {
	driver->step();
	showInSource(current->pc());
	for(QList<IView *>::const_iterator view = views.begin(); view != views.end(); ++view)
		(*view)->update() ;
}

void MainWindow::arm_next() {
	run_state = running;
	updateActions();
	for(QList<IView *>::const_iterator view = views.begin(); view != views.end(); ++view)
		(*view)->onRun();
	QString file;
	int line;
	prog->lineOf(_board.mainCore()->pc(), file, line);
	next_handler.set(file, line);
	driver->setHandler(next_handler);
	driver->resumeAll();
}

void MainWindow::arm_over() {
	run_state = running;
	updateActions();
	for(QList<IView *>::const_iterator view = views.begin(); view != views.end(); ++view)
		(*view)->onRun();
	over_handler.set(_board.mainCore());
	driver->setHandler(over_handler);
	driver->resumeAll();
}

void MainWindow::arm_run() {
	run_state = running;
	updateActions();
	code_viewer->highlight(0);
	for(QList<IView *>::const_iterator view = views.begin(); view != views.end(); ++view)
		(*view)->onRun();
	driver->setHandler(break_handler);
	driver->resumeAll();
}

void MainWindow::arm_done(void) {
	showInSource(current->pc()) ;
	run_state = paused;
	if(starting) {
		starting = false;
		if(_main_bp) {
			_bpts.remove(_main_bp);
			_main_bp = 0;
		}
	}
	updateActions();
	for(QList<IView *>::const_iterator view = views.begin(); view != views.end(); ++view) {
		(*view)->onBreak();
		(*view)->update();
	}	
}

void MainWindow::arm_break() {
	driver->pauseAll();
}

void MainWindow::arm_stop() {
	run_state = stopped;
	updateActions();
	driver->stop();
	_board.stop();
	code_viewer->highlight(0) ;
	updateStatusbar("Simulation stopped.") ;
	for(QList<IView *>::const_iterator view = views.begin(); view != views.end(); ++view)
		(*view)->onStop() ;
}

void MainWindow::about() {
	QMessageBox msgBox;
	QPixmap *img = new QPixmap(pix_path + "banner.png");
	msgBox.setIconPixmap(*img);
	msgBox.setText("<html><big><b>" + PROG_NAME + "\nversion " + PROG_VERSION + "</b></big></html>");
	msgBox.setInformativeText("<html><body>"
		"<hr/>Authors:"
		"<ul>"
			"<li><a href=\"mailto:casse@irit.fr\">H. Casse</a></li>"
			"<li>C. Baouche</li>"
			"<li>M. Dubet</li>"
		"</ul>"
		"<hr/><p>Copyright (c) 2012 IRIT - UPS<br/>"
		"<a href=\"http://www.gnu.org/licenses/gpl.html\">GNU GENERAL PUBLIC LICENSE V3</a><br/>"
		"<a href=\"http://www.irit.fr\">http://www.irit.fr</a></p>"
		"</body></html>");
	msgBox.exec();
}

QMenu* MainWindow::getViewMenu() {
  return viewMenu ;
}


/**
 * Called when the window close.
 */
bool MainWindow::onClose(void) {
	saveSettings();
	return true;
}


/**
 * Save application settings.
 */
void MainWindow::saveSettings() {
	qDebug() << "save settings";
	if(!settings_path.isNull()) {
		QSettings settings(settings_path, QSettings::NativeFormat);
		settings.clear();

		// save view settings
		for(QList<IView *>::const_iterator view = views.begin(); view != views.end(); ++view)
			(*view)->saveSettings(settings);

		// save breakpoints
		_bpts.saveSettings(settings);

		// save windows settings
		settings.beginGroup("MainWindow");
		settings.setValue("geometry", saveGeometry());
		settings.setValue("windowState", saveState());
		settings.endGroup();
	}
}


/**
 * Load application settings.
 */
void MainWindow::loadSettings() {
	if(!settings_path.isNull()) {
		QSettings settings(settings_path, QSettings::NativeFormat);

		// load view settings
		for(QList<IView *>::const_iterator view = views.begin(); view != views.end(); ++ view)
			(*view)->loadSettings(settings);

		// load breakpoints
		_bpts.loadSettings(settings);

		// load window settings
		settings.beginGroup("MainWindow");
		restoreGeometry(settings.value("geometry").toByteArray());
		restoreState(settings.value("windowState").toByteArray());
		settings.endGroup();

	}
}


/**
 * Run handler for the main window.
 */
MainWindow::MyBreakHandler::MyBreakHandler(MainWindow& window): win(window) {
}


/**
 */
void MainWindow::MyBreakHandler::onDone(Core *core) {
	win.arm_done();
}


/**
 */
bool MainWindow::MyBreakHandler::stops(Core *core) {
	return win._bpts.breaksAt(core->pc());
}


/**
 * @class MainWindow::NextHandler
 * Sheduler handler to implement "next" command.
 */


/**
 */
MainWindow::NextHandler::NextHandler(MainWindow& window): win(window), _line(0) {
}


/**
 */
void MainWindow::NextHandler::onDone(Core *core) {
	win.arm_done();
}


/**
 */
bool MainWindow::NextHandler::stops(Core *core) {
	QString file;
	int line;
	win.prog->lineOf(core->pc(), file, line);
	return line != _line || file != _file;
}


/**
 * @fn class OverHandler;
 * Handler that jumps over a function call or the current line.
 */

/**
 */
MainWindow::OverHandler::OverHandler(MainWindow& window): NextHandler(window), not_found(false) {
}

/**
 */
void MainWindow::OverHandler::set(Core *core) {
	not_found = false;
	QString nfile;
	int cline;
	addr_t a = core->pc();
	win.prog->lineOf(a, _file, cline);
	do {
		a += win.prog->size(a);
		win.prog->lineOf(a, nfile, _line);
		if(_line == -1 || nfile != _file || win.program()->isFunction(a)) {
			not_found = true;
			return;
		}
	} while(cline == _line);
}

/**
 */
void MainWindow::OverHandler::onDone(Core *core) {
	win.arm_done();
}

/**
 */
bool MainWindow::OverHandler::stops(Core *core) {
	if(not_found)
		return NextHandler::stops(core);
	else {
		QString file;
		int line;
		win.prog->lineOf(core->pc(), file, line);
		return line == _line && file == _file;
	}
}


/**
 * Show the given address in the source.
 * @param address	Address to show.
 */
void MainWindow::showInSource(uint32_t address) {
	
	// if mode is disasm, show in disasm
	if(dis_selected) {
		code_viewer->highlightAddress(address);
		source_box->setCurrentIndex(0);
		return;
	}
	
	// get file and line
	int line;
	QString file;
	prog->lineOf(address, file, line);
	
	// if any, look for the source
	ISource *source = 0;
	if(line >= 0)
		source = prog->sourceFor(file);
	
	// else look in the disassembly
	if(!source) {
		source = dis;
		source_box->setCurrentIndex(0);
	}
	
	// display the source and line
	if(cur_source != source) {
		cur_source = source;
		for(int i = 0; i < source_box->count(); i++)
			if(source_box->itemText(i) == source->name()) {
				source_box->setCurrentIndex(i);
				break;
			}
	}
	code_viewer->highlightAddress(address);
}


/**
 * Slot called to change the currently displayed source.
 * @param index		Index in the source box.
 */
void MainWindow::changeSource(int index) {
	if(index < 0)
		code_viewer->clearSource();
	else {
		if(index == 0) {
			cur_source = dis;
			dis_selected = true;
		}
		else {
			QString name = source_box->itemText(index);
			cur_source = prog->sourceFor(name);
			dis_selected = false;
		}
		code_viewer->setSource(cur_source);
		if(run_state != stopped)
			code_viewer->highlightAddress(_board.mainCore()->pc());
		else {
			addr_t a = prog->addressOf("main");
			if(a != 0)
				code_viewer->highlightAddress(a);
		}
	}
}


/**
 * Called when the directory is changed.
 */
void MainWindow::execDirChanged(const QString& path) {
	QFileInfo file(exec_path);
	if(file.exists() && file.lastModified() > exec_date) {
		exec_date = file.lastModified().addSecs(1);
		if(QMessageBox::Yes == QMessageBox::question(this, "Executable changed", "Executable file has changed. Reload it?")) {
			if(run_state != stopped)
				arm_stop();
			updateExec();
		}
	}
}

}	// bsim

