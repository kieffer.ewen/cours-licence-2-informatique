/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <dlfcn.h>
#include <QDomDocument>
#include <QDomElement>
#include <QFile>
#include <QWidget>
#include <QApplication>
#include <QDebug>
#include <gel/gel_elf.h>
#include <Board.hpp>
#include <Core.hpp>

#define WITHOUT_THREAD

namespace bsim {

/**
 * @class MemoryException
 * @brief Base class for all the ARM memory access related exceptions
 */


/** 
 * @class InvalidCallbackException
 * @brief Throwed when there is an access to a memory occupied by a gliss callback, but without any real component connected to
 *
 * Due to GLISS, we have to set callback to entire page, but we just connect a component to some addresses in those pages
 * So for others addresses, this exception is throwed.
 */


/**
 * @class UnfoundComponent
 * @brief Throwed when the asked component name if not found
 *
 */


/**
 * @class Board
 * @brief Main component of the software : class with contains an interface to gliss, the list of modules ..etc.
 * 
 * @par Configuration
 * The board takes its configuration form the top element of a board description files.
 * 
 * The following attributes are supported:
 * @li mck="FREQ" - master clock of the board (FREQ in Hz, default 1000).
 * @li w="INT" - width of the displayed board (in pixels).
 * @li h="INT" - height of the displayed board (in pixels).
 * 
 * The following elements are supported:
 * @li <component ...> - description of a component (see IComponent).
 */


/**
 * Build a simulation board.
 */
Board::Board(void)
:	_main(0),
	_size(0, 0),
	_mck(default_mck),
	prog(0),
	sched(0),
	_io_installed(false)
{
	_prefix = QApplication::applicationDirPath();
	_prefix = _prefix + "/../lib/bsim/";
}


/**
 * Set the current program.
 * @param program	Current program.
 */
void Board::setProgram(Program *program) {
	prog = program;
	_io_installed = false;
}


/**
 * Load the given board description.
 * @param name	Name of file to load.
 * @param mon	Current monitor.
 */
void Board::load(QString name, IMonitor& mon) {
	clear();
	
	// open the XML file
	QDomDocument doc ;
	QFile file(name) ;
	if (!file.open(QIODevice::ReadOnly)) {
		mon.error(QString("cannot open %1: %2").arg(name).arg(file.errorString()));
		return;
	}
	QString msg;
	int line, col;
	if(!doc.setContent(&file, &msg, &line, &col)) {
		mon.error(QString("XML format error in %1:%2:%3 : %2").arg(name).arg(line).arg(col).arg(msg));
		return;
	}
	  
	// look the content
	QDomElement root = doc.documentElement() ;
	_name = root.attribute("name");
	if(root.hasAttribute("w"))
		_size.setWidth(root.attribute("w").toInt());
	if(root.hasAttribute("h"))
		_size.setHeight(root.attribute("h").toInt());
	if(root.hasAttribute("mck"))
		_mck = root.attribute("mck").toInt();
	
	// parse the components
	QDomElement component = root.firstChildElement() ;
	while(!component.isNull()) {
		if(component.tagName() != "component")
			continue;
		QString name = component.attribute("name");
		if(name.isNull()) {
			mon.warn("component without name");
			continue;
		}
		QString klass = component.attribute("class");
		if(klass.isNull()) {
			mon.warn("component without class");
			continue;
		}

		// get the plugin
		void *lib ;
		if(loaded.contains(klass))
			lib = loaded[klass] ;
		else {
			QString path = _prefix, file = klass;
			int p = klass.lastIndexOf("/");
			if(p >= 0) {
				path = _prefix + "/" + klass.mid(0, p);
				file = klass.mid(p + 1);
			}
			#ifdef Q_OS_MAC
				QString l = (path + "/lib" + file + ".dylib") ;
			#else
				QString l = (path + "/lib" + file + ".so") ;
			#endif

			lib = dlopen( l.toStdString().c_str(), RTLD_LAZY) ;
			if (lib == NULL) {
				mon.error(QString("cannot load the plugin for %1:%2: %3").arg(name).arg(klass).arg(dlerror()));
				return;
			}
			loaded.insert(klass, lib);
		}

		// Instanciate component
		try {
			void* (*fun)(Board&, QDomElement, QString) ;
			*reinterpret_cast<void**>(&fun) = (IComponent*)dlsym(lib,"init");
			if(!fun) {
				mon.error(QString("malformed plugin for %1: %2").arg(name).arg(klass));
				return;
			}
			IComponent *icomp = (IComponent*)fun(*this, component, component.attribute("name"));
			icomp->setType(klass);
			components.insert(component.attribute("name"),  icomp);
			Configuration conf(icomp, component, mon);
			icomp->configure(conf, mon);
			Core *core = icomp->toCore();
			if(core) {
				_cores.push_back(core);
				if(!_main)
					_main = core;
			}
		}
		catch(ComponentException& e)	{ throw LoadException(e.message()); }

		// next component
		component = component.nextSiblingElement() ;
	}
}


/**
 */
Board::~Board() {
	clear();
}


/**
 * Find a component by its name.
 * @param name				Name of the component.
 * @return					Found component.
 * @throw LoadException		If the component is not found.
 */
IComponent* Board::getComponent(QString name) {
	if(!components[name])
		throw LoadException(QString("cannot found the component \"%1\"").arg(name)) ;
	return components[name] ;
}


/** 
 * Get the components of the board.
 * @return	Board components.
 */
QMap<QString,IComponent*>& Board::getComponents() {
	return components;
}


/**
 * @fn void Board::addMemoryCallback(arm_address_t addr, C& obj, M read, M write);
 * @brief To connect a callback when there are memory access in GLISS
 * @param addr The address to set the callback
 * @param obj The object to call
 * @param read The method to call when it s a read access
 * @param write when it s a write
 */


/**
 * Clear the content of the board.
 */
void Board::clear(void) {
	
	// free all components
	for(QMap<QString, IComponent*>::const_iterator comp = components.begin();
	comp != components.end(); ++comp)
		delete *comp;
	components.clear();
	
	// other cleanup
	_name = "";
	_cores.clear();
	_main = 0;
	_size = QSize(-1, -1);
	_mck = default_mck;
}


/**
 * @fn int Board::mck(void) const;
 * Get the master clock.
 * @return	Mast clock (in Hz).
 */


/**
 * Called when the simulation starts to let initializing the components.
 */
void Board::start(void) {
	
	// start the components
	for(QMap<QString, IComponent*>::const_iterator comp = components.begin(); comp != components.end(); ++comp)
		(*comp)->start();
		
	// if required, start the IO
	if(!_io_installed) {
		_io_installed = true;
		for(QMap<QString, IComponent*>::const_iterator comp = components.begin(); comp != components.end(); ++comp) {
			const QVector< IRegister *>& regs = (*comp)->getRegisters();
			for(int i = 0; i < regs.size(); i++) {
				IHardRegister *reg = regs[i]->toHard();
				if(reg)
					prog->record(reg);
			}
		}
		
	}
}


/**
 * Called when the simulation stop to let finishing the components.
 */
void Board::stop(void) {
	for(QMap<QString, IComponent*>::const_iterator comp = components.begin(); comp != components.end(); ++comp)
		(*comp)->stop();
}

}	// bsim
