#include <QDebug>

#include <Board.hpp>
#include <MainWindow.hpp>
#include <MemoryDockWidget.hpp>
#include <Program.hpp>

namespace bsim {

/**
 * @class MemoryDockWidget
 * View of the memory of the program.0
 */


/**
 */
MemoryDockWidget::MemoryDockWidget(MainWindow& window)
: 	valid(false),
	_window(window),
	cconf(-1)
{
	setObjectName("memory-view");
	setAllowedAreas(Qt::AllDockWidgetAreas);
	setWindowTitle("Memory");
	
	// prepare memory view
	tw = new QTreeWidget();
	tw->setColumnCount(2);
	QList<QString>* hlist = new QList<QString>();
	hlist->append("Address");
	hlist->append("Value");
	tw->setHeaderLabels(*hlist);

	// prepare address widget
	addr_widget = new QComboBox();
	addr_widget->setEditable(true);
	connect(addr_widget->lineEdit(), SIGNAL(textChanged(const QString &)), this, SLOT(addrChanged(const QString &)) ) ;
	connect(addr_widget->lineEdit(), SIGNAL(returnPressed()), this, SLOT(addrFinished()));
	connect(addr_widget, SIGNAL(currentIndexChanged(int)), this, SLOT(addrSelect(int)));
	addr_widget->setToolTip("Type the address in hexadecimal or the name of a global variable.");
	
	// prepare size widget
	size_widget = new QSpinBox();
	size_widget->setMinimum(1);	
	size_widget->setMaximum(9999);
	size_widget->setValue(conf.size);
	connect(size_widget, SIGNAL(valueChanged(int)), this, SLOT(sizeChanged()) );

	// prepare data size selector
	data_size_select = new QComboBox();
	data_size_select->addItem("byte");
	data_size_select->addItem("half");
	data_size_select->addItem("word");
	data_size_select->setCurrentIndex(conf.data_size);
	connect(data_size_select, SIGNAL(currentIndexChanged(int)), this, SLOT(changeDataSize(int)));
	
	// prepare display selector
	data_display_select = new QComboBox();
	data_display_select->addItem("dec");
	data_display_select->addItem("uns.");
	data_display_select->addItem("hex");
	data_display_select->addItem("bin");
	data_display_select->addItem("char");
	data_display_select->setCurrentIndex(conf.display);
	connect(data_display_select, SIGNAL(currentIndexChanged(int)), this, SLOT(changeDataDisplay(int)));
	
	// structure all together
	vbox = new QVBoxLayout();
	hbox = new QHBoxLayout();
	QWidget* wi = new QWidget();
	QWidget* wi2 = new QWidget();
	hbox->addWidget(addr_widget);
	hbox->addWidget(size_widget);
	hbox->addWidget(data_size_select);
	hbox->addWidget(data_display_select);
	wi2->setLayout(hbox);
	vbox->addWidget(wi2);
	vbox->addWidget(tw);
	wi->setLayout(vbox);
	this->setWidget(wi);

	// prepare the timer
	timer.setInterval(250);
	QObject::connect(&timer, SIGNAL(timeout()), this, SLOT(timeout()));

	// perform the display
	disable();
	setDecimal();
	updateRange();
	window.addDockWidget(Qt::LeftDockWidgetArea, this) ;
	window.getViewMenu()->addAction(toggleViewAction());
}


/**
 * Disable all inputs.
 */
void MemoryDockWidget::enable(void) {
	tw->setEnabled(true);
	addr_widget->setEnabled(true);
	size_widget->setEnabled(true);
	data_size_select->setEnabled(true);
	data_display_select->setEnabled(true);
}


/**
 * Disable all outputs.
 */
void MemoryDockWidget::disable(void) {
	tw->setEnabled(false);
	addr_widget->setEnabled(false);
	size_widget->setEnabled(false);
	data_size_select->setEnabled(false);
	data_display_select->setEnabled(false);
}


/**
 * Update the displayed memory because of simulation execution.
 */
void MemoryDockWidget::updateContent(void) {
	if(!valid || !_window.board().program())
		return;
	for(int i = 0; i < conf.size; i++) {
		QTreeWidgetItem *ad = tw->topLevelItem(i);
		uint32_t value;
		if(conf.data_size == BYTE) {
			uint8_t x;
			_window.board().program()->get(addr + i * (1 << conf.data_size), x);
			value = x;
		}
		else if(conf.data_size == HALF) {
			uint16_t x;
			_window.board().program()->get(addr + i * (1 << conf.data_size), x);
			value = x;
		}
		else
			_window.board().program()->get(addr + i * (1 << conf.data_size), value);
		QString text = toString(value);
		if(ad->text(1) != text) {
			ad->setText(1, text);
			ad->setTextColor(1, QColor("red"));
		}
		else
			ad->setTextColor(1, QColor("black"));			
	}
}


/**
 * Change in UI addresses requires updating the range.
 */
void MemoryDockWidget::updateRange(void) {
	if(!_window.isRunning())
		return;
	tw->clear();
	if(valid) {
		for (int i = 0; i < conf.size; i++) {
			QTreeWidgetItem *newItem = new QTreeWidgetItem(tw) ;
			newItem->setText(0, tr("%1").arg(addr + i * (1 << conf.data_size), 8, 16, QChar('0')));
		}
		updateContent();
	}
}


/**
 * Called when the memory is changed.
 * @param text	New text.
 */
void MemoryDockWidget::addrChanged(const QString & text) {
	if(!_window.isRunning())
		return;
	cconf = -1;
	addr = evaluate(text, valid);
	if(!valid) {
		tw->setEnabled(false);
		_window.updateStatusbar(tr("\"%1\" is neither a valid address, nor an existing symbol").arg(text));
	}
	else {
		tw->setEnabled(true);
		_window.updateStatusbar("");
		updateRange();
	}
}


/**
 * Called when the size has changed.
 */
void MemoryDockWidget::sizeChanged(void) {
	conf.size = size_widget->value();
	if(cconf >= 0)
		confs[cconf].size = conf.size;
	if(valid)
		updateRange();
}


/**
 */
void MemoryDockWidget::update(void) {
	updateContent();
}


/**
 */
void MemoryDockWidget::timeout(void) {
	updateContent();
}

/**
 * Start the timer to refresh periodically the register display.
 */
void MemoryDockWidget::onRun(void) {
	timer.start();
}


/**
 * Start the timer to refresh periodically the register display.
 */
void MemoryDockWidget::onBreak(void) {
	timer.stop();
}


/**
 * Simulation start: enable all and update content
 */
void MemoryDockWidget::onStart(void) {
	enable();
	if(cconf >= 0)
		addrChanged(addr_widget->currentText());
}


/**
 * Simulation end: disable all.
 */
void MemoryDockWidget::onStop(void) {
	disable();
}


/**
 * Slot called on each bit size selector change.
 * Record the change and update the display.
 * @param i	Index of the new current combo box item.
 */
void MemoryDockWidget::changeDataSize(int i) {
	conf.data_size = data_size_t(i);
	if(cconf >= 0)
		confs[cconf].data_size = conf.data_size;
	updateRange();
}


/**
 * Slot called on each data display selector change.
 * Record the change and update the display.
 * @param i	Index of the new current combo box item.
 */
void MemoryDockWidget::changeDataDisplay(int i) {
	conf.display = display_t(i);
	if(cconf >= 0)
		confs[cconf].display = conf.display;
	updateRange();
}


/**
 * Build the string corresponding to the given value
 * according to the current configuration.
 * @param x		Value to display.
 * @return		Corresponding string.
 */
QString MemoryDockWidget::toString(uint32_t x) {
	QVariant v(x);
	switch(conf.display) {
	case DEC:
		return QString("%1").arg(int32_t(x));
	case UDEC:
		return QString("%1").arg(x);
	case HEX:
		return QString("0x%1").arg(x, 2 * (1 << conf.data_size), 16, QChar('0'));
	case BIN:
		return QString("0b%1").arg(x, 8 * (1 << conf.data_size), 2, QChar('0'));
	case CHAR:
		x &= 0xff;
		if(x >= 32 && x < 128)
			return QString("'%1'").arg(char(x));
		else
			return QString("\\x%1").arg(x, 2, 16, QChar('0'));
	default:
		return "???";
	}
}


/**
 * Find the value of a register from its name.
 * @param name	Register name.
 * @param ok	True if the register is found, false else.
 */
MemoryDockWidget::address_t MemoryDockWidget::regValue(QString name, bool& ok) {
	ok = false;
	Core *core = _window.board().mainCore();
	const QVector<IRegister *>& regs = core->getRegisters();
	for(QVector<IRegister *>::const_iterator r = regs.cbegin(); r != regs.cend(); ++r)
		if((*r)->isReadable())
			for(int i = 0; i < (*r)->count(); i++)
				if((*r)->displayName(i).compare(name, Qt::CaseInsensitive) == 0) {
					ok = true;
					address_t a = (*r)->get(i).toULongLong();
					return a;
				}
	return 0;
}


/**
 * Called when the address edition is ended.
 */
void MemoryDockWidget::addrFinished(void) {
	if(!valid)
		addr_widget->removeItem(confs.size());
	else {
		conf.name = addr_widget->currentText();
		for(int i = 0; i < confs.size(); i++) {
			if(confs[i].name == conf.name) {
				cconf = i;
				conf = confs[i];
				updateConf();
				return;
			}
		}
		if(cconf < 0) {
			cconf = confs.size();
			confs.push_back(conf);
		}
	}
}


/**
 * Called when an old configuration is selected.
 */
void MemoryDockWidget::addrSelect(int index) {
	if(index >= 0 && index < confs.size()) {
		cconf = index;
		conf = confs[index];
		updateConf();
	}
}


/**
 * Update the displayed configuration.
 */
void MemoryDockWidget::updateConf(void) {
	size_widget->setValue(conf.size);
	data_size_select->setCurrentIndex(conf.data_size);
	data_display_select->setCurrentIndex(conf.display);
}


/**
 * Evaluate the given string as an address expression.
 * @param text	String expression.
 * @param ok	Set to true if expr is valid, false else.
 */
MemoryDockWidget::address_t MemoryDockWidget::evaluate(QString text, bool& ok) {
	ok = false;

	// empty: ok but invalid
	if(text == "")
		return 0;
	
	// starting with '$', maybe a register name
	if(text[0] == '$')
		return regValue(text.mid(1), ok);
	
	// try to convert to integer
	address_t addr = text.toUInt(&ok, 16);
	if(ok)
		return addr;
		
	// look for a label
	if(_window.program()) {
		addr = _window.program()->addressOf(text);
		if(addr) {
			ok = true;
			return addr;
		}
	}
	
	// bad expression
	return 0;
}


/**
 */
void MemoryDockWidget::loadSettings(QSettings& settings) {
	settings.beginGroup("memory");
	int count = settings.value("count", 0).toInt();
	for(int i = 0; i < count; i++) {
		Config c;
		settings.beginGroup(QString::number(i));
		c.name = settings.value("name", "").toString();
		if(c.name == "")
			continue;
		c.size = settings.value("size", c.size).toInt();
		c.data_size = data_size_t(settings.value("data_size", c.data_size).toInt());
		c.display = display_t(settings.value("display", c.display).toInt());
		confs.push_back(c);
		addr_widget->addItem(c.name);
		settings.endGroup();
	}
	if(confs.size() == 0)
		cconf = -1;
	else {
		cconf = settings.value("current", 0).toInt();
		if(cconf < 0 || cconf >= confs.size())
			cconf = 0;
		addr_widget->setCurrentIndex(cconf);
	}
	settings.endGroup();
}


/**
 */
void MemoryDockWidget::saveSettings(QSettings& settings) {
	settings.beginGroup("memory");
	settings.setValue("current", cconf);
	settings.setValue("count", confs.size());
	for(int i = 0; i < confs.size(); i++) {
		settings.beginGroup(QString::number(i));
		settings.setValue("name", confs[i].name);
		settings.setValue("size", confs[i].size);
		settings.setValue("data_size", confs[i].data_size);
		settings.setValue("display", confs[i].display);
		settings.endGroup();
	}
	settings.endGroup();
}

}	// bsim
