# Find number of CPUs
cpu=1
uname -s | grep -qs Linux && cpu=`cat /proc/cpuinfo | grep '^processor' | wc -l`
uname -s | grep -qs 'Darwin\|BSD\|DragonFly' && cpu=`sysctl -n hw.ncpu`
echo "You seem to have $cpu cpu cores"

# Get GLISS source code
if [ ! -d contrib ]
then
	git clone git://github.com/maattd/gliss.git contrib || exit $?
else
  echo "You already have a contrib/ , I assume that it contains a git repository of gliss"
  (cd contrib && git pull) || exit $?
fi

# Warning: make with several jobs fails
make -C contrib || exit $?
[ -d build ] || mkdir build
cd build
cmake .. || exit $?
make -j $cpu || exit $?
cp simulator ..
