	.include "board.i"

	.global _start

_reset:
	b		_reset
_undef:
	b		_undef
_swi:
	b		_swi
_pabt:
	b		_pabt
_dabt:
	b		_dabt
_reserved:
	.word	0
_irq:
	ldr 	pc, [pc, # -0xF20]
_fiq:
	ldr		pc, [pc, # -0xF20]

@@@@@@@@@@@@
PIO_HANDLER:
	
	stmfd	r13!, {r0-r2}
	ldr		r10, =PIO_BASE
    mov 	r1, #PIO_LED2
    
	ldr		r0, [r10, #PIO_PDSR]
	tst		r0, #PIO_PUSH
	beq		led_on

led_off:
	str		r1, [r10, #PIO_CODR]
	b		led_end	
led_on:
	str		r1, [r10, #PIO_SODR]
led_end:
	
	ldr     r12, =AIC_BASE
	
	mov     r0,#0
    str     r0, [r12, #AIC_EOICR]    
	
	ldmfd	r13!, {r0-r2}
	subs	pc, r14, #4 


@@@@@@@@@@@
TC_HANDLER:
	
	stmfd	r13!, {r0-r2}
	ldr		r10, =PIO_BASE
    mov 	r1, #PIO_LED1
          
    cmp 	r3,#0        
    bne 	led_off2
    
    str 	r1, [r10, #PIO_SODR]
    mov 	r3,#1
    b 		end
    
led_off2:  
    str 	r1, [r10, #PIO_CODR]
    mov 	r3,#0
    
end:       
	
	ldr     r12, =AIC_BASE
	mov     r0,#0
    str     r0, [r12, #AIC_EOICR]    
	
	ldmfd	r13!, {r0-r2}
	subs	pc, r14, #4 


@@@@@@@@@@
PWM_HANDLER:
	stmfd	r13!, {r0-r2}
	ldr		r10, =PIO_BASE
    mov 	r1, #PIO_LED3
    
    cmp 	r5,#0        
    bne 	led_off3
    
    str 	r1, [r10, #PIO_SODR]
    mov 	r5,#1
    b 		end2
    
led_off3:  
    str 	r1, [r10, #PIO_CODR]
    mov 	r5,#0
    
end2:       
	
	ldr     r12, =AIC_BASE
	mov     r0,#0
    str     r0, [r12, #AIC_EOICR]    
	
	ldmfd	r13!, {r0-r2}
	subs	pc, r14, #4 


@ main program
_start:
    
    @@@ init PIO
     
	ldr		r10,=PIO_BASE	
	ldr 	r0,=(PIO_LED1|PIO_LED2|PIO_LED3|PIO_PUSH)
	str 	r0,[r10, #PIO_PER] 		
	ldr 	r0,=(PIO_LED1|PIO_LED2|PIO_LED3)
	str 	r0,[r10, #PIO_OER]
	str 	r0,[r10, #PIO_CODR]	
	ldr 	r0,=(PIO_PUSH)
	str 	r0,[r10, #PIO_ODR]
	str 	r0,[r10, #PIO_IER]
	
	@@@ init TC
	
	ldr r9,=TC_BASE
	mov r0,#0b10            
	str r0,[r9, #TC_CCR0]   
	mvn r0,#0x0             
	str r0,[r9, #TC_IDR0]  
	ldr r0,=0x04000        
	str r0,[r9, #TC_CMR0] 
    ldr r0,=500
	str r0,[r9, #TC_RC0]   
	mov r0,#TC_CCR_CLKEN | TC_CCR_SWTRG      
	str r0,[r9, #TC_CCR0] 
	mov r0,#0x10
	str r0,[r9, #TC_IER0]   
	
	@@@ init PWC
	
	ldr r8,=PWM_BASE		
	ldr r0,=0b1111             
	str r0,[r8, #PWM_DIS] 	
	mov r0,#0x0            
	str r0,[r8, #PWM_MR] 		
	ldr r0,=0x0601         
	str r0,[r8, #PWM_CMR0]			
	ldr r0,=1000           
	str r0,[r8, #PWM_CPRD0]	
	ldr r0,=250            
	str r0,[r8, #PWM_CDTY0]	
	ldr r0,=0x01
	str r0,[r8, #PWM_IER]   
	ldr r0,=0x01             
	str r0,[r8, #PWM_ENA] 
	

	@@@ init AIC
    
    ldr 	r11, =AIC_BASE  
      
    mov     r0,#0x4
    str     r0,[r11, #AIC_IDCR]     
    adr     r0, PIO_HANDLER
    str     r0, [r11, #AIC_SVR2] 
    mov     r0,#0b11                              @@@ Prior 3
    str     r0,[r11,#AIC_SMR2] 
    mov     r0,#0x4
    str     r0,[r11,#AIC_ICCR]
    mov     r0,#0x4
    str     r0,[r11,#AIC_IECR] 
    
    mov     r0,#0x8
    str     r0,[r11, #AIC_IDCR]
    adr     r0, TC_HANDLER
    str     r0, [r11, #AIC_SVR3]
    mov     r0,#0b100                             @@@ Prior 4
    str     r0,[r11,#AIC_SMR3]  
    mov     r0,#0x8
    str     r0,[r11,#AIC_ICCR] 
    mov     r0,#0x8
    str     r0,[r11,#AIC_IECR] 
       
    ldr r0, [r10, #PIO_ISR]  
    mov r3,#0 
    
    mov     r0,#0x400
    str     r0,[r11, #AIC_IDCR]
    adr     r0, PWM_HANDLER
    str     r0, [r11, #AIC_SVR10]
    mov     r0,#0b10                              @@@ Prior 2
    str     r0,[r11,#AIC_SMR10]  
    mov     r0,#0x400
    str     r0,[r11,#AIC_ICCR] 
    mov     r0,#0x400
    str     r0,[r11,#AIC_IECR] 
    
    mov r5,#0 


loop: 
	b 		loop	

