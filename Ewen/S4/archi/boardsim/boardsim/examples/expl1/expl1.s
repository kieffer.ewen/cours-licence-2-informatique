	.include "board.i"
        
	.global _start

_reset:
	b		_reset
_undef:
	b		_undef
_swi:
	b		_swi
_pabt:
	b		_pabt
_dabt:
	b		_dabt
_reserved:
	.word	0


@ main program 
_start:

	ldr	r10,=PIO_BASE
	ldr 	r1,=(PIO_LED)
	str 	r1,[r10, #PIO_PER]
	ldr 	r1,=(PIO_LED)
	str 	r1,[r10, #PIO_OER]
	str 	r1,[r10, #PIO_CODR]
	mov 	r2, #PIO_LED

       
    ldr r9,=TC_BASE
	mov r0,#0b10            
	str r0,[r9, #TC_CCR0]   
	mvn r0,#0x0             
	str r0,[r9, #TC_IDR0]  
	ldr r0,=0x04000        
	str r0,[r9, #TC_CMR0] 
    ldr r0,=500
	str r0,[r9, #TC_RC0]   
	mov r0,#TC_CCR_CLKEN | TC_CCR_SWTRG      
	str r0,[r9, #TC_CCR0] 
        
    mov r3,#0 
loop: 

      	ldr r0,[r9, #TC_SR0]
        tst r0, #TC_SR_CPCS         
        beq loop
        
        cmp r3,#0
        bne led_off
        str r2, [r10, #PIO_SODR]
        mov r3,#1
        b end
led_off:  
        str r2, [r10, #PIO_CODR]
        mov r3,#0
end:   
       b loop
 
