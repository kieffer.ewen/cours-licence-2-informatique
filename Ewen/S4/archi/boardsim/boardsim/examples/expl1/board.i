# Test board board definition
	.equ MCK, 1000

# PIO offsets
	.equ PIO_PER, 0x0
	.equ PIO_PDR, 0x4
	.equ PIO_PSR, 0x8
	.equ PIO_OER, 0x10
	.equ PIO_ODR, 0x14
	.equ PIO_OSR, 0x18
	.equ PIO_SODR, 0x30
	.equ PIO_CODR, 0x34
	.equ PIO_ODSR, 0x38
	.equ PIO_PDSR, 0x3c
	.equ PIO_IER, 0x40
	.equ PIO_IDR, 0x44
	.equ PIO_IMR, 0x48
	.equ PIO_ISR, 0x4c

# TC offsets
	.equ TC_CCR0, 0x0
	.equ TC_CMR0, 0x4
	.equ TC_CV0, 0x10
	.equ TC_RA0, 0x14
	.equ TC_RB0, 0x18
	.equ TC_RC0, 0x1c
	.equ TC_SR0, 0x20
	.equ TC_IER0, 0x24
	.equ TC_IDR0, 0x28
	.equ TC_IMR0, 0x2c
	.equ TC_CCR1, 0x40
	.equ TC_CMR1, 0x44
	.equ TC_CV1, 0x50
	.equ TC_RA1, 0x54
	.equ TC_RB1, 0x58
	.equ TC_RC1, 0x5c
	.equ TC_SR1, 0x60
	.equ TC_IER1, 0x64
	.equ TC_IDR1, 0x68
	.equ TC_IMR1, 0x6c
	.equ TC_CCR2, 0x80
	.equ TC_CMR2, 0x84
	.equ TC_CV2, 0x90
	.equ TC_RA2, 0x94
	.equ TC_RB2, 0x98
	.equ TC_RC2, 0x9c
	.equ TC_SR2, 0xa0
	.equ TC_IER2, 0xa4
	.equ TC_IDR2, 0xa8
	.equ TC_IMR2, 0xac
	.equ TC_BCR, 0xc0
	.equ TC_BMR, 0xc4

# PIO component
	.equ PIO_BASE, 0xfffff400
	.equ PIO_LED, (1 << 0)

# TC component
	.equ TC_BASE, 0xfffa0000
	.equ MCK, 1000
	.equ TC_CCR_CLKEN, 	(1 << 0)
	.equ TC_CCR_CLKDIS, (1 << 1)
	.equ TC_CCR_SWTRG, 	(1 << 2)
	.equ TC_SR_CPCS,	(1 << 4)
	.equ TC_SR_COVFS,	(1 << 0)

