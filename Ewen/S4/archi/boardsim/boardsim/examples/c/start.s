	.global		_start
	.extern		main

	.text
	.equ	MAGIC,	0xBAD0BAD0
	.equ	MAGIC_ADDR, 0x20
	
reset_vec:	b	_start
undef_vec:	b	undef_vec
swi_vec:	b	swi_vec
pabt_vec:	b	pabt_vec
dabt_vec:	b	dabt_vec
rsv_vec:	b	rsv_vec
irq_vec:	ldr pc, [pc,#-0xF20]
fiq_vec:	b	fiq_vec

magic:		.int	MAGIC

_start:

	.equ	I_BIT, (1 << 7)
	.equ	F_BIT, (1 << 6)
	.equ	MODE_FIQ, 0b10001
	.equ	MODE_IRQ, 0b10010
	.equ	MODE_SVC, 0b10011
	.equ	MODE_System, 0b11111

	# initialize stacks
	ldr		r0, =trap_stack_top
	msr		cpsr, #MODE_FIQ | I_BIT | F_BIT
	mov		sp, r0
	msr		cpsr, #MODE_IRQ | I_BIT | F_BIT
	mov		sp, r0
	ldr		r0, =stack_top
	msr		cpsr, #MODE_SVC | I_BIT | F_BIT
	
	ldr		r0, =stack_top
	msr		cpsr, #MODE_System | I_BIT | F_BIT
	mov		sp, r0
	
	# clear AIC
	.equ	AIC_BASE, 0xFFFFF000
	.equ	AIC_IDCR, 0x124
	.equ	AIC_ICCR, 0x128
	.equ	AIC_EOICR, 0x130
	.equ	AIC_SPU, 0x134

	ldr		r0, =AIC_BASE
	mov		r1, #0xffffffff
	str		r1, [r0, #AIC_IDCR] 
	str		r1, [r0, #AIC_ICCR]
	str		r1, [r0, #AIC_EOICR]
	str		r1, [r0, #AIC_EOICR]
	str		r1, [r0, #AIC_EOICR]
	str		r1, [r0, #AIC_EOICR]
	str		r1, [r0, #AIC_EOICR]
	str		r1, [r0, #AIC_EOICR]
	str		r1, [r0, #AIC_EOICR]
	str		r1, [r0, #AIC_EOICR]
	adr		r0, spu_it
	str		r1, [r0, #AIC_SPU]
		
	# finally, start main
	bl	main

_exit:
	b	_exit

spu_it:
	b	spu_it

# trap stack
trap_stack_base:
	.fill	1024,1,0
trap_stack_top:

# user mode stack
stack_base:
	.fill	1024,1,0
stack_top:
