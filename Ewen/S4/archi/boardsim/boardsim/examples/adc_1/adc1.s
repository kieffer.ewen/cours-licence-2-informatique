
	.include "board.i"

	.global _start

_reset:
	b		_reset
_undef:
	b		_undef
_swi:
	b		_swi
_pabt:
	b		_pabt
_dabt:
	b		_dabt
_reserved:
   .word	 0



@ main program
_start:	
	  
	@@@ init USART
    ldr		r10,=US_BASE
    ldr     r0,=100
    str     r0,[r10, #USART_BRGR]		
    
    ldr     r0,=0x018C0                  @@  NORMAL, 8 BITS, ASYNC, NOPARITY, 1.5 STOP BIT, CHANNEL MODE NORMAL
    str 	r0,[r10, #USART_MR]
    ldr     r0,=(US_RSTTX|US_RSTSTA)
    str     r0,[r10, #USART_CR]
    ldr     r0,=(US_TXEN)
    str     r0,[r10, #USART_CR]    
  
    
    @@@ init ADC
	ldr    r12,=ADC_BASE
	mov    r0, #ADC_CHER_CH0
	str    r0,[r12, #ADC_CHER]            @ enable Channel 0 for conversion

wait_trsm:      
	ldr     r0,[r10, #USART_CSR]	
	tst     r0,#US_TXRDY
	beq     wait_trsm
   
    mov    r0,#ADC_START                  @ start conversion
    str    r0,[r12, #ADC_CR]

wait_convert:
    ldr    r0,[r12, #ADC_SR]
    tst    r0, #ADC_EOC0
    bne    wait_convert 
    
    ldr    r1, [r12,#ADC_CDR0]
 		
    mov     r2,r1  
	str     r2,[r10, #USART_THR]
	
	b 		wait_trsm	    
