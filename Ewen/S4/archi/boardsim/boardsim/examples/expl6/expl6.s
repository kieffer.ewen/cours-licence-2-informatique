
	.include "board.i"

	.global _start

_reset:
	b		_reset
_undef:
	b		_undef
_swi:
	b		_swi
_pabt:
	b		_pabt
_dabt:
	b		_dabt
_reserved:
	.word	0

 

@ main program
_start:
    
    @@@ init PIO
     
	ldr		r10,=PIO_BASE	
	ldr 	r0,=(PIO_LED1|PIO_PWM_PWM0)
	str 	r0,[r10, #PIO_PER] 		
	
	ldr 	r0,=(PIO_LED1)
	str 	r0,[r10, #PIO_OER]
	str 	r0,[r10, #PIO_CODR]	
	ldr 	r1,=(PIO_PWM_PWM0)
	str 	r1,[r10, #PIO_ODR]
	str 	r1,[r10, #PIO_IER]
		
	@@@ init PWC
	
	ldr r8,=PWM_BASE		
	ldr r0,=0b1111             
	str r0,[r8, #PWM_DIS] 	
	mov r0,#0x0            
	str r0,[r8, #PWM_MR] 		
	ldr r0,=0x0601         
	str r0,[r8, #PWM_CMR0]			
	ldr r0,=2000           
	str r0,[r8, #PWM_CPRD0]	
	ldr r0,=500            
	str r0,[r8, #PWM_CDTY0]	 
	ldr r0,=0x01             
	str r0,[r8, #PWM_ENA] 
	
       
    mov 	r1, #PIO_LED1    

loop:
	ldr		r0, [r10, #PIO_PDSR]
	tst		r0, #PIO_PWM_PWM0
	bne		led_on

	str		r1, [r10, #PIO_CODR]
	b		led_end
led_on:
	str		r1, [r10, #PIO_SODR]
led_end:
	b		loop



