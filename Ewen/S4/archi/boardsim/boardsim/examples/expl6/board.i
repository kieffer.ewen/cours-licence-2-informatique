# IT board board definition
	.equ MCK, 1000

# PIO offsets
	.equ PIO_PER, 0x0
	.equ PIO_PDR, 0x4
	.equ PIO_PSR, 0x8
	.equ PIO_OER, 0x10
	.equ PIO_ODR, 0x14
	.equ PIO_OSR, 0x18
	.equ PIO_SODR, 0x30
	.equ PIO_CODR, 0x34
	.equ PIO_ODSR, 0x38
	.equ PIO_PDSR, 0x3c
	.equ PIO_IER, 0x40
	.equ PIO_IDR, 0x44
	.equ PIO_IMR, 0x48
	.equ PIO_ISR, 0x4c

# PWM offsets
	.equ PWM_MR, 0x0
	.equ PWM_ENA, 0x4
	.equ PWM_DIS, 0x8
	.equ PWM_SR, 0xc
	.equ PWM_IER, 0x10
	.equ PWM_IDR, 0x14
	.equ PWM_IMR, 0x18
	.equ PWM_ISR, 0x1c
	.equ PWM_CMR0, 0x200
	.equ PWM_CDTY0, 0x204
	.equ PWM_CPRD0, 0x208
	.equ PWM_CCNT0, 0x20c
	.equ PWM_CUPD0, 0x210
	.equ PWM_CMR1, 0x220
	.equ PWM_CDTY1, 0x224
	.equ PWM_CPRD1, 0x228
	.equ PWM_CCNT1, 0x22c
	.equ PWM_CUPD1, 0x230
	.equ PWM_CMR2, 0x240
	.equ PWM_CDTY2, 0x244
	.equ PWM_CPRD2, 0x248
	.equ PWM_CCNT2, 0x24c
	.equ PWM_CUPD2, 0x250
	.equ PWM_CMR3, 0x260
	.equ PWM_CDTY3, 0x264
	.equ PWM_CPRD3, 0x268
	.equ PWM_CCNT3, 0x26c
	.equ PWM_CUPD3, 0x270

# PIO component
	.equ PIO_BASE, 0xfffff400
	.equ PIO_LED1, (1 << 0)
	.equ PIO_PWM_PWM0, (1 << 9)

# PWM component
	.equ PWM_BASE, 0xfffcc000
	.equ PWM_CMR_CALG, 	(1 << 8)
	.equ PWM_CMR_CPOL, 	(1 << 9)
	.equ PWM_CMR_CPDON, 	(1 << 10)
	.equ PWM_CMR_CPDOFF, 	(0 << 10)

