# IT board board definition
	.equ MCK, 1000

# PIO offsets
	.equ PIO_PER, 0x0
	.equ PIO_PDR, 0x4
	.equ PIO_PSR, 0x8
	.equ PIO_OER, 0x10
	.equ PIO_ODR, 0x14
	.equ PIO_OSR, 0x18
	.equ PIO_SODR, 0x30
	.equ PIO_CODR, 0x34
	.equ PIO_ODSR, 0x38
	.equ PIO_PDSR, 0x3c
	.equ PIO_IER, 0x40
	.equ PIO_IDR, 0x44
	.equ PIO_IMR, 0x48
	.equ PIO_ISR, 0x4c

# PIO component
	.equ PIO_BASE, 0xfffff400
	.equ PIO_LED1, (1 << 0)
	.equ PIO_LED2, (1 << 1)
	.equ PIO_PUSH, (1 << 10)

