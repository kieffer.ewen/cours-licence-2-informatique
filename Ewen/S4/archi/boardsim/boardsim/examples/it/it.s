	.include "board.i"

	.global _start

_reset:
	b		_reset
_undef:
	b		_undef
_swi:
	b		_swi
_pabt:
	b		_pabt
_dabt:
	b		_dabt
_reserved:
	.word	0
_irq:
	b		handle_irq
_fiq:
	b		_fiq

@ handle the IRQ
handle_irq:
	stmfd	r13!, {r0-r2}
	ldr		r0, =PIO_BASE
	ldr		r1, state_irq
	mov		r2, #PIO_LED2
	cmp		r1, #0
	beq		off_irq
	str		r2, [r0, #PIO_SODR]
	b		end_irq
off_irq:
	str		r2, [r0, #PIO_CODR]
end_irq:
	mvn		r1, r1
	str		r1, state_irq
	ldmfd	r13!, {r0-r2}
	subs	pc, r14, #4 
state_irq:
	.word	0

@ main program
_start:

	ldr		r10,=PIO_BASE
	ldr 	r0,=(PIO_LED1|PIO_LED2|PIO_PUSH)
	str 	r0,[r10, #PIO_PER]
	ldr 	r0,=(PIO_LED1|PIO_LED2)
	str 	r0,[r10, #PIO_OER]
	str 	r0,[r10, #PIO_CODR]
	ldr 	r0,=PIO_PUSH
	str 	r0,[r10, #PIO_ODR]
	mov 	r1, #PIO_LED1

loop:
	ldr		r0, [r10, #PIO_PDSR]
	tst		r0, #PIO_PUSH
	bne		led_off
led_on:
	str		r1, [r10, #PIO_SODR]
	b		led_end
led_off:
	str		r1, [r10, #PIO_CODR]
led_end:
	b		loop
