
	.include "board.i"

	.global _start

_reset:
	b		_reset
_undef:
	b		_undef
_swi:
	b		_swi
_pabt:
	b		_pabt
_dabt:
	b		_dabt
_reserved:
	.word	0
_irq:
	ldr 	pc, [pc, # -0xF20]
_fiq:
	ldr		pc, [pc, # -0xF20]

@@@@@@
USART_HANDLER:
	
	stmfd	r13!, {r0-r2}
	ldr     r10, =US_BASE

    ldr     r0,[r10, #USART_CSR]
    tst     r0, #US_RXRDY
    beq     end
    
    ldr     r1, [r10, #USART_RHR]  
    
	ldr     r0,[r10, #USART_CSR]	
	tst     r0,#US_TXRDY
	beq     end

    mov     r2, r1  
	str     r2,[r10, #USART_THR]
end:
	ldr     r12, =AIC_BASE
	
	mov     r0,#0
    str     r0, [r12, #AIC_EOICR]    
	
	ldmfd	r13!, {r0-r2}
	subs	pc, r14, #4 




@ main program
_start:	
	

	@@@ init USART
    ldr		r10,=US_BASE
    ldr     r0,=100
    str     r0,[r10, #USART_BRGR]		
    
    ldr     r0,=0x018C0
    str 	r0,[r10, #USART_MR]    
    ldr     r0, =(US_RSTRX|US_RSTTX|US_RSTSTA)
    str     r0,[r10, #USART_CR]
    ldr     r0,=(US_RXEN|US_TXEN)
    str     r0,[r10, #USART_CR]     
    ldr     r0,=0x022
    str     r0,[r10, #USART_IER]
  
    @@@ init AIC
    
    ldr 	r11, =AIC_BASE  
      
    mov     r0,#0x4
    mov     r0,#0x4
    str     r0,[r11, #AIC_IDCR]     
    adr     r0, USART_HANDLER
    str     r0, [r11, #AIC_SVR2] 
    mov     r0,#0b11
    str     r0,[r11,#AIC_SMR2] 
    mov     r0,#0x4
    str     r0,[r11,#AIC_ICCR]
    mov     r0,#0x4
    str     r0,[r11,#AIC_IECR] 
	
loop:   
    b       loop
  

	
 
