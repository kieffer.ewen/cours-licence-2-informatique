# EXPL board board definition
	.equ MCK, 1000

# AIC offsets
	.equ AIC_SMR, 0x0
	.equ AIC_SVR, 0x80
	.equ AIC_IVR, 0x100
	.equ AIC_FVR, 0x104
	.equ AIC_ISR, 0x108
	.equ AIC_IPR, 0x10c
	.equ AIC_IMR, 0x110
	.equ AIC_CISR, 0x114
	.equ AIC_IECR, 0x120
	.equ AIC_IDCR, 0x124
	.equ AIC_ICCR, 0x128
	.equ AIC_ISCR, 0x12c
	.equ AIC_EOICR, 0x130
	.equ AIC_SPU, 0x134
	.equ AIC_DCR, 0x138
	.equ AIC_FFER, 0x140
	.equ AIC_FFDR, 0x144
	.equ AIC_FFSR, 0x148

# USART offsets
	.equ USART_CR, 0x0
	.equ USART_MR, 0x4
	.equ USART_IER, 0x8
	.equ USART_IDR, 0xc
	.equ USART_IMR, 0x10
	.equ USART_CSR, 0x14
	.equ USART_RHR, 0x18
	.equ USART_THR, 0x1c
	.equ USART_BRGR, 0x20
	.equ USART_RTOR, 0x24
	.equ USART_TTGR, 0x28
	.equ USART_FIDI, 0x40
	.equ USART_NER, 0x44
	.equ USART_IF, 0x4c

# US component
	.equ US_BASE, 0xfffc0000
	.equ US_RXRDY, 	(1 << 0)
	.equ US_TXRDY, 	(1 << 1)
	.equ US_TXEMPTY, (1 << 9)
	.equ US_RSTRX, 	(1 << 2)
	.equ US_RSTTX, 	(1 << 3)
	.equ US_RXEN, 	(1 << 4)
	.equ US_RXDIS, 	(1 << 5)
	.equ US_TXEN, 	(1 << 6)
	.equ US_TXDIS, 	(1 << 7)
	.equ US_RSTSTA, (1 << 8)
	.equ US_SYNC_ON, (1 << 8)
	.equ US_SYNC_OFF, (0 << 8)

# AIC component
	.equ AIC_BASE, 0xfffff000
	.equ AIC_SMR0,  0
	.equ AIC_SVR0,  128
	.equ AIC_SMR1,  4
	.equ AIC_SVR1,  132
	.equ AIC_SMR2,  8
	.equ AIC_SVR2,  136
	.equ AIC_SMR3,  12
	.equ AIC_SVR3,  140
	.equ AIC_SMR4,  16
	.equ AIC_SVR4,  144
	.equ AIC_SMR5,  20
	.equ AIC_SVR5,  148
	.equ AIC_SMR6,  24
	.equ AIC_SVR6,  152
	.equ AIC_SMR7,  28
	.equ AIC_SVR7,  156
	.equ AIC_SMR8,  32
	.equ AIC_SVR8,  160
	.equ AIC_SMR9,  36
	.equ AIC_SVR9,  164
	.equ AIC_SMR10,  40
	.equ AIC_SVR10,  168
	.equ AIC_SMR11,  44
	.equ AIC_SVR11,  172
	.equ AIC_SMR12,  48
	.equ AIC_SVR12,  176
	.equ AIC_SMR13,  52
	.equ AIC_SVR13,  180
	.equ AIC_SMR14,  56
	.equ AIC_SVR14,  184
	.equ AIC_SMR15,  60
	.equ AIC_SVR15,  188
	.equ AIC_SMR16,  64
	.equ AIC_SVR16,  192
	.equ AIC_SMR17,  68
	.equ AIC_SVR17,  196
	.equ AIC_SMR18,  72
	.equ AIC_SVR18,  200
	.equ AIC_SMR19,  76
	.equ AIC_SVR19,  204
	.equ AIC_SMR20,  80
	.equ AIC_SVR20,  208
	.equ AIC_SMR21,  84
	.equ AIC_SVR21,  212
	.equ AIC_SMR22,  88
	.equ AIC_SVR22,  216
	.equ AIC_SMR23,  92
	.equ AIC_SVR23,  220
	.equ AIC_SMR24,  96
	.equ AIC_SVR24,  224
	.equ AIC_SMR25,  100
	.equ AIC_SVR25,  228
	.equ AIC_SMR26,  104
	.equ AIC_SVR26,  232
	.equ AIC_SMR27,  108
	.equ AIC_SVR27,  236
	.equ AIC_SMR28,  112
	.equ AIC_SVR28,  240
	.equ AIC_SMR29,  116
	.equ AIC_SVR29,  244
	.equ AIC_SMR30,  120
	.equ AIC_SVR30,  248
	.equ AIC_SMR31,  124
	.equ AIC_SVR31,  252
	.equ HIGH_LEVEL, 	(5 << 2)
	.equ LOW_LEVEL,   (5 << 0)
	.equ POSITIVE_EDGE, 	(5 << 3)
	.equ NEGATIVE_EDGE,	(5 << 1)

