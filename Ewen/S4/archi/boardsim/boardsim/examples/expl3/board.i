# IT board board definition
	.equ MCK, 1000

# AIC offsets
	.equ AIC_SMR, 0x0
	.equ AIC_SVR, 0x80
	.equ AIC_IVR, 0x100
	.equ AIC_FVR, 0x104
	.equ AIC_ISR, 0x108
	.equ AIC_IPR, 0x10c
	.equ AIC_IMR, 0x110
	.equ AIC_CISR, 0x114
	.equ AIC_IECR, 0x120
	.equ AIC_IDCR, 0x124
	.equ AIC_ICCR, 0x128
	.equ AIC_ISCR, 0x12c
	.equ AIC_EOICR, 0x130
	.equ AIC_SPU, 0x134
	.equ AIC_DCR, 0x138
	.equ AIC_FFER, 0x140
	.equ AIC_FFDR, 0x144
	.equ AIC_FFSR, 0x148

# PIO offsets
	.equ PIO_PER, 0x0
	.equ PIO_PDR, 0x4
	.equ PIO_PSR, 0x8
	.equ PIO_OER, 0x10
	.equ PIO_ODR, 0x14
	.equ PIO_OSR, 0x18
	.equ PIO_SODR, 0x30
	.equ PIO_CODR, 0x34
	.equ PIO_ODSR, 0x38
	.equ PIO_PDSR, 0x3c
	.equ PIO_IER, 0x40
	.equ PIO_IDR, 0x44
	.equ PIO_IMR, 0x48
	.equ PIO_ISR, 0x4c

# TC offsets
	.equ TC_CCR0, 0x0
	.equ TC_CMR0, 0x4
	.equ TC_CV0, 0x10
	.equ TC_RA0, 0x14
	.equ TC_RB0, 0x18
	.equ TC_RC0, 0x1c
	.equ TC_SR0, 0x20
	.equ TC_IER0, 0x24
	.equ TC_IDR0, 0x28
	.equ TC_IMR0, 0x2c
	.equ TC_CCR1, 0x40
	.equ TC_CMR1, 0x44
	.equ TC_CV1, 0x50
	.equ TC_RA1, 0x54
	.equ TC_RB1, 0x58
	.equ TC_RC1, 0x5c
	.equ TC_SR1, 0x60
	.equ TC_IER1, 0x64
	.equ TC_IDR1, 0x68
	.equ TC_IMR1, 0x6c
	.equ TC_CCR2, 0x80
	.equ TC_CMR2, 0x84
	.equ TC_CV2, 0x90
	.equ TC_RA2, 0x94
	.equ TC_RB2, 0x98
	.equ TC_RC2, 0x9c
	.equ TC_SR2, 0xa0
	.equ TC_IER2, 0xa4
	.equ TC_IDR2, 0xa8
	.equ TC_IMR2, 0xac
	.equ TC_BCR, 0xc0
	.equ TC_BMR, 0xc4

# TC component
	.equ TC_BASE, 0xfffa0000
	.equ MCK, 1000
	.equ TC_CCR_CLKEN, 	(1 << 0)
	.equ TC_CCR_CLKDIS, (1 << 1)
	.equ TC_CCR_SWTRG, 	(1 << 2)
	.equ TC_SR_CPCS,	(1 << 4)
	.equ TC_SR_COVFS,	(1 << 0)

# AIC component
	.equ AIC_BASE, 0xfffff000
	.equ AIC_SMR0,  0
	.equ AIC_SVR0,  128
	.equ AIC_SMR1,  4
	.equ AIC_SVR1,  132
	.equ AIC_SMR2,  8
	.equ AIC_SVR2,  136
	.equ AIC_SMR3,  12
	.equ AIC_SVR3,  140
	.equ AIC_SMR4,  16
	.equ AIC_SVR4,  144
	.equ AIC_SMR5,  20
	.equ AIC_SVR5,  148
	.equ AIC_SMR6,  24
	.equ AIC_SVR6,  152
	.equ AIC_SMR7,  28
	.equ AIC_SVR7,  156
	.equ AIC_SMR8,  32
	.equ AIC_SVR8,  160
	.equ AIC_SMR9,  36
	.equ AIC_SVR9,  164
	.equ AIC_SMR10,  40
	.equ AIC_SVR10,  168
	.equ AIC_SMR11,  44
	.equ AIC_SVR11,  172
	.equ AIC_SMR12,  48
	.equ AIC_SVR12,  176
	.equ AIC_SMR13,  52
	.equ AIC_SVR13,  180
	.equ AIC_SMR14,  56
	.equ AIC_SVR14,  184
	.equ AIC_SMR15,  60
	.equ AIC_SVR15,  188
	.equ AIC_SMR16,  64
	.equ AIC_SVR16,  192
	.equ AIC_SMR17,  68
	.equ AIC_SVR17,  196
	.equ AIC_SMR18,  72
	.equ AIC_SVR18,  200
	.equ AIC_SMR19,  76
	.equ AIC_SVR19,  204
	.equ AIC_SMR20,  80
	.equ AIC_SVR20,  208
	.equ AIC_SMR21,  84
	.equ AIC_SVR21,  212
	.equ AIC_SMR22,  88
	.equ AIC_SVR22,  216
	.equ AIC_SMR23,  92
	.equ AIC_SVR23,  220
	.equ AIC_SMR24,  96
	.equ AIC_SVR24,  224
	.equ AIC_SMR25,  100
	.equ AIC_SVR25,  228
	.equ AIC_SMR26,  104
	.equ AIC_SVR26,  232
	.equ AIC_SMR27,  108
	.equ AIC_SVR27,  236
	.equ AIC_SMR28,  112
	.equ AIC_SVR28,  240
	.equ AIC_SMR29,  116
	.equ AIC_SVR29,  244
	.equ AIC_SMR30,  120
	.equ AIC_SVR30,  248
	.equ AIC_SMR31,  124
	.equ AIC_SVR31,  252
	.equ HIGH_LEVEL, 	(5 << 2)
	.equ LOW_LEVEL,   (5 << 0)
	.equ POSITIVE_EDGE, 	(5 << 3)
	.equ NEGATIVE_EDGE,	(5 << 1)

# PIO component
	.equ PIO_BASE, 0xfffff400
	.equ PIO_LED1, (1 << 0)
	.equ PIO_LED2, (1 << 3)
	.equ PIO_PUSH, (1 << 10)

