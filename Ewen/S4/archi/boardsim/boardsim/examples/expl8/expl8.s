
	.include "board.i"

	.global _start

_reset:
	b		_reset
_undef:
	b		_undef
_swi:
	b		_swi
_pabt:
	b		_pabt
_dabt:
	b		_dabt
_reserved:
	.word	0

 

@ main program
_start:
    		
	@@@ init PWC
	
	ldr r8,=PWM_BASE		
	ldr r0,=0b1111             
	str r0,[r8, #PWM_DIS] 	
	mov r0,#0x0            
	str r0,[r8, #PWM_MR] 		
	ldr r0,=0x0700         
	str r0,[r8, #PWM_CMR0]			
	ldr r0,=50           
	str r0,[r8, #PWM_CPRD0]	
	ldr r0,=25            
	str r0,[r8, #PWM_CDTY0]	 
	ldr r0,=0x01             
	str r0,[r8, #PWM_ENA] 
	

loop:
	b		loop



