	.include "board.i"

	.global _start

_reset:
	b		_reset
_undef:
	b		_undef
_swi:
	b		_swi
_pabt:
	b		_pabt
_dabt:
	b		_dabt
_reserved:
	.word	0
_irq:
	ldr 	pc, [pc, # -0xF20]
_fiq:
	ldr		pc, [pc, # -0xF20]


PIO_HANDLER:
	
	stmfd	r13!, {r0-r2}
	ldr		r10, =PIO_BASE
    mov 	r1, #PIO_LED1
    mov 	r2, #PIO_LED2
    
	ldr		r0, [r10, #PIO_PDSR]
	tst		r0, #PIO_PUSH1
	beq		led_on

	str		r1, [r10, #PIO_CODR]
	b		led_end	
led_on:
	str		r1, [r10, #PIO_SODR]
led_end:


	ldr		r0, [r10, #PIO_PDSR]
	tst		r0, #PIO_PUSH2
	beq		led_on2

	str		r2, [r10, #PIO_CODR]
	b		led_end2	
led_on2:
	str		r2, [r10, #PIO_SODR]
led_end2:

	ldr     r12, =AIC_BASE
	
	mov     r0,#0
    str     r0, [r12, #AIC_EOICR]    
	
	ldmfd	r13!, {r0-r2}
	subs	pc, r14, #4 




@ main program
_start:
    @@@ init PIO
     
	ldr		r10,=PIO_BASE	
	ldr 	r0,=(PIO_LED1|PIO_LED2|PIO_PUSH1|PIO_PUSH2)
	str 	r0,[r10, #PIO_PER] 		
	ldr 	r0,=(PIO_LED1|PIO_LED2)
	str 	r0,[r10, #PIO_OER]
	str 	r0,[r10, #PIO_CODR]	
	ldr 	r1,=(PIO_PUSH1|PIO_PUSH2)
	str 	r1,[r10, #PIO_ODR]
	str 	r1,[r10, #PIO_IER]

	
	
	@@@ init AIC
    
    ldr 	r11, =AIC_BASE  
      
    mov     r0,#0x4
    str     r0,[r11, #AIC_IDCR]     
    adr     r0, PIO_HANDLER
    str     r0, [r11, #AIC_SVR2] 
    mov     r0,#0b11
    str     r0,[r11,#AIC_SMR2] 
    mov     r0,#0x4
    str     r0,[r11,#AIC_ICCR]
    mov     r0,#0x4
    str     r0,[r11,#AIC_IECR] 
    
      
    ldr r0, [r10, #PIO_ISR]  
  
       
loop: 
	b 		loop	
