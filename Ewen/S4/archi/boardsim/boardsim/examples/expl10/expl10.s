
	.include "board.i"

	.global _start

_reset:
	b		_reset
_undef:
	b		_undef
_swi:
	b		_swi
_pabt:
	b		_pabt
_dabt:
	b		_dabt
_reserved:
	.word	0
_irq:
	ldr 	pc, [pc, # -0xF20]
_fiq:
	ldr		pc, [pc, # -0xF20]
 
@@@@@@
 PIO_HANDLER:
	
	stmfd	r13!, {r0-r2}
	ldr		r10, =PIO_BASE
	ldr     r11, =PWM_BASE
    
    ldr     r1, [r11, #PWM_CDTY0]
    
	ldr		r0, [r10, #PIO_PDSR]
	tst		r0, #PIO_UP
	beq		add_duty
    b		next1
    
add_duty:
    add     r1,r1,#1
	str		r1, [r11, #PWM_CUPD0]
	b       switch_end

next1:
	ldr		r0, [r10, #PIO_PDSR]
	tst		r0, #PIO_DOWN
	beq		sub_duty
    b		switch_end

sub_duty:
	sub     r1,r1,#1
	str		r1, [r11, #PWM_CUPD0]

switch_end:
	ldr     r12, =AIC_BASE
	
	mov     r0,#0
    str     r0, [r12, #AIC_EOICR]    
	
	ldmfd	r13!, {r0-r2}
	subs	pc, r14, #4 



@ main program
_start:

    @@@ init PIO
     
	ldr		r10,=PIO_BASE	
	mov 	r0,#PIO_UP|PIO_DOWN
	str 	r0,[r10, #PIO_PER] 		
	mov 	r1,#PIO_UP|PIO_DOWN
	str 	r1,[r10, #PIO_ODR]
	str 	r1,[r10, #PIO_IER]

    		
	@@@ init PWC
	
	ldr 	r8,=PWM_BASE		
	ldr 	r0,=0b1111             
	str 	r0,[r8, #PWM_DIS] 	
	mov 	r0,#0x0            
	str 	r0,[r8, #PWM_MR] 		
	ldr 	r0,=0x0200         
	str 	r0,[r8, #PWM_CMR0]			
	ldr 	r0,=100           
	str 	r0,[r8, #PWM_CPRD0]	
	ldr 	r0,=50            
	str		r0,[r8, #PWM_CDTY0]	 
	ldr		r0,=0x01             
	str 	r0,[r8, #PWM_ENA] 
	
	
@@@ init AIC
    
    ldr 	r11, =AIC_BASE  
      
    mov     r0,#0x4
    str     r0,[r11, #AIC_IDCR]     
    adr     r0, PIO_HANDLER
    str     r0, [r11, #AIC_SVR2] 
    mov     r0,#0b11
    str     r0,[r11,#AIC_SMR2] 
    mov     r0,#0x4
    str     r0,[r11,#AIC_ICCR]
    mov     r0,#0x4
    str     r0,[r11,#AIC_IECR] 
    
    ldr r0, [r10, #PIO_ISR]  
  
       
loop: 
	b 		loop	
