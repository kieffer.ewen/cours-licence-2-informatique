@ Exo chenillard
  .include "carte.inc"
  .equ LED4, 1 << 30
  .equ LED3, 1 << 29
  .equ LED2, 1 << 28
  .equ LED1, 1 << 27
  .equ SW1, 1 << 24
  .equ SW2, 1 << 25
  .equ MOTIF, 3
  .equ SENS, 0
  .global _start

_start:

	  stmfd sp!,{r0,r1,r10}			@ sauvegarde
	  ldr r1,[sp,#12]			@ attention au décalage
	  ldr r10,=PIOB_BASE			@ adresse de base du PIO
	  ldr r0,=(LED1|LED2|LED3|LED4|SW1|SW2)	@ les PIO à utiliser
	  str r0,[r10, #PIO_PER]
	  ldr r0,=(LED1|LED2|LED3|LED4)		@ les PIO en sortie
	  str r0,[r10, #PIO_OER]
	  str r0,[r10, #PIO_CODR]		@ LED eteintes
	  ldr r0,=(SW1|SW2)
	  str r0,[r10, #PIO_ODR]		@ Bouton en entrée

	  mov r8,#MOTIF
	  mov r7,#SENS
tq_chen:  
	  cmp r7,#1
	  beq autre_sens
	  mov r8,r8,lsl #1			@ Contient des données de bit 1 à 5
	  mov r9,#0
	  and r9,r8, #0x10			@ r9 contient le bit 5
	  mov r9,r9, lsr #4			@ bit 5 déplacé dans bit 0
	  orr r8,r9 @ r9 = r9 OU r8		@ met la bonne valeur dans bit 0
	  bic r8,r8, #0x10
	  b suite

autre_sens:
	  movs r8,r8,asr #1
	  mov r9,#0
	  adc r9,r9,#0
	  mov r9,r9,lsl #4

suite:	  stmfd sp!, {r8}			@ on passe le param par la pile
	  bl affich
	  add sp,sp,#4
	  ldr r0,[r10, #PIO_PDSR]
	  and r1,r0,#SW1
	  cmp r1,#0
	  beq push1
	  and r1,r0,#SW2
	  cmp r1,#0
	  beq push2
	  b tq_chen	  

affich:		@ valeur passée par la pile
	  stmfd sp!,{r1,r2,lr}
	  ldr r1, [sp,#12]
	  mov r2,r1,LSL #27
	  str r2,[r10, #PIO_SODR]
	  mvn r2,r1
	  and r2,r2,#0xF
	  mov r2,r2,LSL #27
	  str r2,[r10, #PIO_CODR]
	  ldmfd sp!,{r1,r2,pc}

push1:
	  mvn r8,r8		@ on inverse r8
	  and r8,r8,#0xF	@ on s'interesse qu'au 4 premiers bits
	  stmfd sp!,{r8}
	  bl affich
	  add sp,sp,#4
	  b tq_chen
push2:
	  mov r7,#1
	  b tq_chen

exit:
_exit:

