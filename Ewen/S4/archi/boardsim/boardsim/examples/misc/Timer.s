 .include "carte.inc"


@ ===============================================================
@ Main application
@ ===============================================================
 .global _start @ Pour éviter le warning de arm-elf-ld

_start:
main: @ Pour le point d'arrêt automatique
 mov r13,#0x20000 @ pile en 0x20000

 @ initialisation des leds
 @ -----------------------
 ldr r1,=PIOB_BASE
 mov r0,#(LED2|LED1)
 str r0,[r1, #PIO_PER] @ Autoriser les ports
 str r0,[r1, #PIO_OER] @ P6 et P16 en sortie
 str r0,[r1, #PIO_CODR] @ eteindre les leds 8 et 1

 @ initialisation du timer0
 @ -----------------------
 ldr r1,=TC0_BASE
 mov r0,#0b10
 str r0,[r1, #TC_CCR0] @ disable clock counter
 mvn r0,#0x0
 str r0,[r1, #TC_IDR0] @ disable interrupt generation
 ldr r0,[r1, #TC_SR0] @ read TC0 status register to acknowledge status
 ldr r0,=0x4004
 str r0,[r1, #TC_CMR0] @ TC0 will use MCK/1024 and auto restart
 mov r0,#1
 str r0,[r1, #TC_CCR0] @ enable clock counter
 mov r0,#0x10
 str r0,[r1, #TC_IER0] @ validate the RC compare interrupt

 @ Initialisation AIC
 @ -----------------------
 ldr r2,=AIC_BASE
 mov r0,#PID_TC0
 str r0,[r2, #AIC_IDCR] @ Interdire l'interruption du Timer0 interrupt dans l'AIC
 adr r0,timer0_irq_handler @ Mettre l'adresse de la routine d'interruption
 str r0,[r2, #AIC_SVR12] @ dans AIC_SVR13 (vecteur relatif à TC0_IRQ)
 ldr r0,=0x4004 @ Définition srcctype et priorité interruption TC0
 str r0,[r2, #AIC_SMR12] @ niveau bas et priorité 4
 mov r0,#PID_TC0
 str r0,[r2, #AIC_ICCR] @ RAZ de l'interruption TC0
 str r0,[r2, #AIC_IECR] @ Autorise l'interruption TC0

 @ paramètres de timing du timer0
 @ -----------------------
 ldr r0,=64453 @ 64453 * MCK/1024 -> une seconde
 str r0,[r1, #TC_RC0] @ Placer le délai dans le registre C

 mov r0,#0b100 @ Mise à 1 bu bit 2 de CCR
 str r0,[r1, #TC_CCR0] @ -> RAZ compteur et démarrage

 @ Tâche de fond
 @ -------------
loop: b loop

 @ Fin de la tâche de fond
 @ ------------------------

exit: b exit
@ ===============================================================
@ Fin du programme principal
@ ===============================================================





@ ===============================================================
@ Gestionnaire de l'interruption du timer 0
@ ===============================================================
timer0_irq_handler:

 @ ------------------------
 @- Ajuster et sauvegarder le registre de retour du mode IRQ dans la pile IRQ
 sub r14, r14, #4 @ r14 pointe une instruction trop loin
 stmfd sp!, {r14}

 @ Sauvegarder SPSR et R0 dans la pile IRQ
 mrs r14, spsr
 stmfd sp!, {r0, r14}

 @ On a sauvé tout ce qu'il fallait, on peut
 @ autoriser les interruptions et passer en mode système
 mrs r14, cpsr
 bic r14, r14, #0x80 @ CPSR[I] <- 0
 orr r14, r14, #0b11111 @ (mode système)
 msr cpsr, r14

 @ Sauvegarde de registres dans la pile utilisateur/système
 stmfd sp!, { r1, r2, r14}

 @ inverser la led
 ldr r1,=PIOB_BASE
 mov r2,#LED2
 ldr r0,[r1, #PIO_PDSR] @ Lecture des lignes d'état du PIO
 tst r0,#LED2
 beq allume
eteint:
 str r2,[r1, #PIO_CODR] @ alors allumer la led 8
 b fin_irq
allume:
 str r2,[r1, #PIO_SODR] @ sinon eteindre la led 8

 @ Fin interruption
 @ ----------------

fin_irq:

 @ -----------------------
 ldr r1,=TC0_BASE
 ldr r0,[r1, #TC_SR0] @ Lecture SR pour le remettre à 0
 @ (pour signifier qu'on a traité l'IT)

 @ Restaurer les registres depuis la pile utilisateur
 ldmfd sp!, { r1, r2, r14}

 @- Interdire les interruptions et repasser en mode IRQ
 mrs r0, cpsr
 bic r0, r0, #0x1F @ CPSR[MO..M4] <- 0b00000
 orr r0, r0, #0b10010010 @ CPSR[I] <- 1
 msr cpsr, r0 @ CPSR[MO..M4] <- 0b10010

 @ Indiquer la prise en compte de l'interruption à l'AIC
 @ = ecriture de n'importe quoi dans AIC_EOICR
 ldr r0, =AIC_BASE
 str r0, [r0, #AIC_EOICR]

 @ Restaurer SPSR_irq et r0 à partir de la pile IRQ
 ldmfd sp!, {r0, r14}
 msr spsr, r14

 @ Restaurer LR_irq depuis la pile IRQ et le mettre directement dans le PC
 ldmfd sp!, {pc}^

@ Fin du sous_programme d'interruption

_exit:	

.end
