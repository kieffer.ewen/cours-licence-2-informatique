.equ	PIOB_BASE, 0x4	@ Adresse de base PIOB
.equ	PIO_PER,		0x00	@ PIO Enable Register 
.equ	PIO_PDR,		0x04	@ PIO Disable Register 
.equ	PIO_PSR,		0x08	@ PIO Status Register 
.equ	PIO_RES0,		0x0C	@ Reserved 0 
.equ	PIO_OER,		0x10	@ Output Enable Register 
.equ	PIO_ODR,		0x14	@ Output Disable Register 
.equ	PIO_OSR,		0x18	@ Output Status Register 
.equ	PIO_RES1,		0x1C	@ Reserved 1 
.equ	PIO_IFER,		0x20	@ Input Filter Enable Register 
.equ	PIO_IFDR,		0x24	@ Input Filter Disable Register 
.equ	PIO_IFSR,		0x28	@ Input Filter Status Register 
.equ	PIO_RES2,		0x2C	@ Reserved 2 
.equ	PIO_SODR,		0x30	@ Set Output Data Register 
.equ	PIO_CODR,		0x34	@ Clear Output Data Register 
.equ	PIO_ODSR,		0x38	@ Output Data Status Register 
.equ	PIO_PDSR,		0x3C	@ Pin Data Status Register 
.equ	PIO_IER,		0x40	@ Interrupt Enable Register 

  .equ LED4, 1 << 3
  .equ LED3, 1 << 2
  .equ LED2, 1 << 1
  .equ LED1, 1 << 0
  .equ SW1, 1 << 4
  .equ SW2, 1 << 5
  .equ SW3, 1 << 6
  .equ MOTIF,2
  .equ SENS, 0
  .global _start

_start:
	  ldr r10,=PIOB_BASE			@ adresse de base du PIO
	  ldr r0,=(LED1|LED2|LED3|LED4|SW1|SW2|SW3)	@ les PIO à utiliser
	  str r0,[r10, #PIO_PER]
	  ldr r0,=(LED1|LED2|LED3|LED4)		@ les PIO en sortie
	  str r0,[r10, #PIO_OER]
	  str r0,[r10, #PIO_CODR]		@ LED eteintes
	  ldr r0,=(SW1|SW2|SW3)
	  str r0,[r10, #PIO_ODR]		@ Bouton en entrée
	  mov r8,#MOTIF
	  mov r7,#SENS

tq_chen:  
	  cmp r7,#1
	  beq autre_sens
	  mov r8,r8,lsl #1			@ Contient des données de bit 1 à 5
	  mov r9,#0
	  and r9,r8, #0x10			@ r9 contient le bit 5
	  mov r9,r9, lsr #4			@ bit 5 déplacé dans bit 0
	  orr r8,r9 @ r9 = r9 OU r8		@ met la bonne valeur dans bit 0
	  bic r8,r8, #0x10
	  b suite

autre_sens:
	  movs r8,r8,asr #1
	  mov r9,#0
	  adc r9,r9,#0
	  mov r9,r9,lsl #4

suite:	  
	  bl affich

test_sw:
	  ldr r0,[r10, #PIO_PDSR]
	  and r1,r0,#SW3
	  cmp r1,#SW3
	  beq exit
	  and r1,r0,#SW1
	  cmp r1,#SW1
	  beq push1
	  and r1,r0,#SW2
	  cmp r1,#SW2
	  beq push2
	  b tq_chen	  

affich:
	  mov r2,r8
	  str r2,[r10, #PIO_SODR]
	  mvn r2,r8
	  and r2,r2,#0xF
	  str r2,[r10, #PIO_CODR]
    b  test_sw

push1:
	  mvn r8,r8		@ on inverse r8
	  and r8,r8,#0xF	@ on s'interesse qu'au 4 premiers bits
	  b tq_chen
push2:
    mov r1,#0
    cmp r7,r1
    beq r7a1
	  mov r7,#0
	  b tq_chen

r7a1:
  mov r7,#1
  b tq_chen

exit:
_exit:

