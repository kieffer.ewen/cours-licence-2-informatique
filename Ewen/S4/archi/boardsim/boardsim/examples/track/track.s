	.include "board.i"

	.equ MOTIF,2
	.equ SENS, 0
	.equ WAIT, 5
	.global _start

# r6 = wait counter
# r7 = directrion
# r8 = patter,
# r10 = PIO_BASE

_start:
	  ldr r10,=PIO_BASE			@ adresse de base du PIO
	  mov r0,#PIO_LED1|PIO_LED2|PIO_LED3|PIO_LED4|PIO_INVERT|PIO_BACK|PIO_END	@ les PIO à utiliser
	  str r0,[r10, #PIO_PER]
	  mov r0,#PIO_LED1|PIO_LED2|PIO_LED3|PIO_LED4		@ les PIO en sortie
	  str r0,[r10, #PIO_OER]
	  str r0,[r10, #PIO_CODR]		@ LED eteintes
	  mov r0,#PIO_INVERT|PIO_BACK|PIO_END
	  str r0,[r10, #PIO_ODR]		@ Bouton en entrée
	  mov r8,#MOTIF
	  mov r7,#SENS
	mov		r6, #1

tq_chen:

# any display ?
	sub		r6, r6, #1
	cmp		r6, #0
	bne		shift_end
	mov		r6, #WAIT

# perform the shift
shift:
	cmp 	r7,#0
	beq 	back
	mov		r9, r8, lsl #1
	mov		r8, r8, lsr #3
	orr		r8, r8, r9
	and		r8, r8, #0x0f
	b 		shift_end
back:
	mov 	r9, r8, lsl #3
	and		r9, r9, #0x08
	mov 	r8, r8, lsr #1
	orr		r8, r8, r9
shift_end:

# display the pattern
display:
	bl 		affich
no_display:

# get PDSR	 
	ldr 	r0,[r10, #PIO_PDSR]

# invert button
test_invert:
		adr		r2, invert_pushed
		ldrb	r3, [r2]
		cmp		r3, #0
		bne		pushed_invert
released_invert:
		tst		r0, #PIO_INVERT
		beq		end_invert
		mov		r3, #1
		strb	r3, [r2]
		b		end_invert
pushed_invert:
		tst		r0, #PIO_INVERT
		bne		end_invert
		mov		r3, #0
		strb	r3, [r2]
		mvn 	r8,r8		@ on inverse r8
		and 	r8,r8,#0xF	@ on s'interesse qu'au 4 premiers bits
end_invert:

# back button
test_back:
		adr		r2, back_pushed
		ldrb	r3, [r2]
		cmp		r3, #0
		bne		pushed_back
released_back:
		tst		r0, #PIO_BACK
		beq		end_back
		mov		r3, #1
		strb	r3, [r2]
		b		end_back
pushed_back:
		tst		r0, #PIO_BACK
		bne		end_back
		mov		r3, #0
		strb	r3, [r2]
		mvn		r7, r7
end_back:

test_end:
	  and r1,r0,#PIO_END
	  cmp r1,#PIO_END
	  beq exit

	  b tq_chen	  

affich:
	mov	r2,r8
	str r2,[r10, #PIO_SODR]
	mvn r2,r8
	and r2,r2,#0xF
	str r2,[r10, #PIO_CODR]
	mov	pc, r14

#push2:
#    mov r1,#0
#    cmp r7,r1
#    beq r7a1
#	  mov r7,#0
#	  b tq_chen

#r7a1:
#  mov r7,#1
#  b tq_chen

exit:
_exit:

# data
invert_pushed:	.byte	0
back_pushed:	.byte	0
end_pushed:		.byte	0

