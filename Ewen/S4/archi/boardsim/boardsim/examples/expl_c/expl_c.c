
#include "board.h"

int main(void) {
	int s = 0;

	// PIO initialization
	PIO_PER = PIO_LED;
	PIO_OER = PIO_LED;
	PIO_CODR = PIO_LED;

	// TC0 initialization
	TC_CCR0 = TC_CLKDIS;      
	TC_IDR0 = ~0;
	TC_CCR0 = 0b10;  
	TC_CMR0 = 0x04000;
	TC_RC0 = 500;
	
	// loop
	while(1) {
        
        if(TC_SR0 & TC_CPCS) {
			if(s)
				PIO_SODR = PIO_LED;
			else
				PIO_CODR = PIO_LED;
			s = !s;
		}
	}

}

void _start(void) {
	main();
	while(1);
}
 

