
	.include "board.i"

	.global _start

_reset:
	b		_reset
_undef:
	b		_undef
_swi:
	b		_swi
_pabt:
	b		_pabt
_dabt:
	b		_dabt
_reserved:
	.word	0
_irq:
	ldr 	pc, [pc, # -0xF20]
_fiq:
	ldr		pc, [pc, # -0xF20]


@@@@@@@@@@@@
PIO_HANDLER:
	
	stmfd	r13!, {r0-r2}
	ldr		r10, =PIO_BASE
    mov 	r1, #PIO_LED
    
	ldr		r0, [r10, #PIO_PDSR]
	tst		r0, #PIO_PWM_PWM0
	beq		led_on

	str		r1, [r10, #PIO_CODR]
	b		led_end	

led_on:
	str		r1, [r10, #PIO_SODR]
led_end:
	
	ldr     r12, =AIC_BASE
	
	mov     r0,#0
    str     r0, [r12, #AIC_EOICR]    
	
	ldmfd	r13!, {r0-r2}
	subs	pc, r14, #4 
 

@ main program
_start:
    
    @@@ init PIO
     
	ldr		r10,=PIO_BASE	
	ldr 	r0,=(PIO_LED|PIO_PWM_PWM0)
	str 	r0,[r10, #PIO_PER] 		
	
	ldr 	r0,=(PIO_LED)
	str 	r0,[r10, #PIO_OER]
	str 	r0,[r10, #PIO_CODR]	
	ldr 	r1,=(PIO_PWM_PWM0)
	str 	r1,[r10, #PIO_ODR]
	str 	r1,[r10, #PIO_IER]
		
	@@@ init PWC
	
	ldr r8,=PWM_BASE		
	ldr r0,=0b1111             
	str r0,[r8, #PWM_DIS] 	
	mov r0,#0x0            
	str r0,[r8, #PWM_MR] 		
	ldr r0,=0x0600         
	str r0,[r8, #PWM_CMR0]			
	ldr r0,=200           
	str r0,[r8, #PWM_CPRD0]	
	ldr r0,=50            
	str r0,[r8, #PWM_CDTY0]	 
	ldr r0,=0x01 	            
	str r0,[r8, #PWM_ENA] 
  
  	@@@ init AIC
    
    ldr 	r9, =AIC_BASE  
      
    mov     r0,#0x4
    str     r0,[r9, #AIC_IDCR]     
    adr     r0, PIO_HANDLER
    str     r0, [r9, #AIC_SVR2] 
    mov     r0,#0b11                             
    str     r0,[r9,#AIC_SMR2] 
    mov     r0,#0x4
    str     r0,[r9,#AIC_ICCR]
    mov     r0,#0x4
    str     r0,[r9,#AIC_IECR] 
    
   
loop:
	b		loop



