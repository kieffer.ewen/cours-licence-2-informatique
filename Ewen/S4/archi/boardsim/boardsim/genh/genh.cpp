/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#include <iostream>

#include <QApplication>
#include <QDataStream>
#include <QDate>
#include <QDebug>
#include <QFile>
#include <QStringList>
#include <QTime>

#include "Board.hpp"
#include "Generator.hpp"

using namespace std;
using namespace bsim;

QString APP = "genh";
QString VERSION = "1.1";

void help(void) {
	cerr << "SYNTAX: genh BOARD.xml (-h|--help)\n\n"
			"-c         generate C header\n"
			"-h|--help	display this help message\n"
			"-o FILE	output to the given file\n";	
}

void option_error(QString msg) {
	help();
	cerr << "\nERROR: " << qPrintable(msg) << endl;
}

void error(QString msg) {
	cerr << "ERROR: " << qPrintable(msg) << endl;
}


extern Generator& c_generator;
extern Generator& asm_generator;


/**
 * genh command.
 */
int main(int argc, char **argv) {
	Generator *gen = &asm_generator;
	QString board_name = "";
	QString output = "";

	// application definition
	QApplication app(argc, argv);
	app.setApplicationName("genh");
	app.setApplicationVersion ("1.0");
	app.setOrganizationName("TRACES - IRIT - Université de Toulouse");

	// argument parsing
	QStringList args = app.arguments();
	QStringList::const_iterator arg = args.begin();
	for(++arg; arg != args.end(); ++arg) {
		if((*arg == "-h") || (*arg == "--help")) {
			help();
			return 1;
		}
		else if(*arg == "-c")
			gen = &c_generator;
		else if(*arg == "-o") {
			++arg;
			if(arg == args.end()) {
				option_error("'-o' option requires an argument !");
				app.exit(1);
			}
			output = *arg;
		}
		else if(board_name == "")
			board_name = *arg;
		else {
			option_error(QString("extraneous argument: %1").arg(*arg));
			app.exit(1);
		}
	}
	
	// result check
	if(board_name == "") {
		option_error("no board name given");
		return 1;
	}
	
	// open the board
	Board board;
	try {
		board.load(board_name, IMonitor::def());
	}
	catch(LoadException& e) {
		error(e.message());
		return 2;
	}
	
	// open the output file
	QString h_name;
	if(output != "")
		h_name = output;
	else if(board_name.endsWith(".xml"))
		h_name = board_name.left(board_name.length() - 4) + gen->extension();
	else
		h_name = board_name + gen->extension();
	QFile file(h_name);
	if(!file.open(QIODevice::WriteOnly)) {
		error(QString("cannot open output file \"%1\"").arg(h_name));
		return 3;
	}
	QTextStream out(&file);
	
	// generate the output
	gen->generate(out, board);
	
	// end
	cout << "SUCCESS: output to \"" << qPrintable(h_name) << "\"\n"; 
}
