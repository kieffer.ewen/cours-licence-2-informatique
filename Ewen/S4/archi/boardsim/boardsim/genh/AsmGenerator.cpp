/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDate>
#include <QTime>

#include <Board.hpp>
#include <IComponent.hpp>
#include <IRegister.hpp>

#include "Generator.hpp"

using namespace bsim;

extern QString APP;
extern QString VERSION;

/**
 * Generator for assembly code.
 */
class AsmGenerator: public Generator {
public:
	virtual QString extension(void) {
		return ".i";
	}

	void genConst(QTextStream& out, const Constant& cst, QString pref = "") {
		out << "\t.equ ";
		if(pref != "")
			out << pref << '_';
		out << cst.name() << ", "; 
		switch(cst.type()) {
		case Constant::NONE:
			break;
		case Constant::UINT:
			out << QString("\t.equ %1%2%3, %4\n")
				.arg(pref)
				.arg(pref == "" ? "" : "_")
				.arg(cst.name())
				.arg(cst.value(), 0, 16);
			break;
		case Constant::MASK:
			out << QString("\t.equ %1%2%3, (1 << %4)\n")
				.arg(pref)
				.arg(pref == "" ? "" : "_")
				.arg(cst.name())
				.arg(cst.value());
			break;
		}
		out << endl;
	}

	virtual void generate(QTextStream& out, Board& board) {
		// generate the header
		out << "# " << board.name() << " board definition\n";
		out << "	.equ MCK, " << board.mck() << "\n\n";
		
		// generate static values
		QSet<QString> types;
		QSet<IComponent *> rcomps;
		QMap<QString, IComponent*> comps = board.getComponents();
		for(QMap<QString, IComponent*>::const_iterator comp = comps.begin(); comp != comps.end(); ++comp)
			if(!types.contains((*comp)->type())) {
				types.insert((*comp)->type());
				
				// output the registers
				const QVector<IRegister *>& regs = (*comp)->getRegisters();
				bool one = false;
				for(int i = 0; i < regs.size(); i++) {
					IHardRegister *reg = regs[i]->toHard();
					if(reg) {
						
						// support case of first register
						if(!one) {
							
							// generate header
							out << QString("# %1 offsets\n").arg((*comp)->type());
							one = true;
							rcomps.insert(*comp);
							
							// generate constants
							QVector<Constant> csts;
							(*comp)->getStaticConstants(csts);
							for(int i = 0; i < csts.size(); i++)
								genConst(out, csts[i], csts[i].isPrefixed() ? (*comp)->type() : "");
							if(csts.size())
								out << endl;
						}
						
						// display register
						genConst(out, Constant(reg->name(), reg->address() - (*comp)->address()), (*comp)->type());
					}
				}
				if(one)
					out << endl;
			}
		
		// generates the basis
		for(QSet<IComponent *>::const_iterator comp = rcomps.begin(); comp != rcomps.end(); comp++) {
			
			// generate the header
			out << "# " << (*comp)->name() << " component\n";
			genConst(out, Constant("BASE", (*comp)->address()), (*comp)->name());
			(*comp)->genHeader(IComponent::assembly, out);
			
			// output the static constants
			QVector<Constant> csts;
			(*comp)->getDynamicConstants(csts);
			for(int i = 0; i < csts.size(); i++)
				genConst(out, Constant(csts[i].name(), csts[i].value()), (*comp)->name());

			// one line space
			out << endl;
		}
	}	
};

/**
 * Generator singleton.
 */
static AsmGenerator _asm_generator;
Generator& asm_generator = _asm_generator;
