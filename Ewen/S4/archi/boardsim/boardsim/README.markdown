# ARM chip simulator
This is a TER Project from Université Toulouse III Paul Sabatier

[Contributors](https://github.com/maattd/arm-simulator/contributors)

# Building
##Dependency
* C++ compiler (GCC 4.x or Clang svn works)
* Qt (QtGui)
* Cmake
* Git
* (optional) Doxygen and GraphViz for documentation
* GLISS

##Ok, I'm sold, I want to compile this !
The easiest way is to use the bootstrap.sh script (read it first, don't
trust me ;) )

It will populate a contrib directory with the current version of GLISS,
an arm interpretor.

Then il will do all the compilation for you, and launch the exec
`build/simulator`

# License
As GLISS is GPLv2, it's GPLv2 ..
