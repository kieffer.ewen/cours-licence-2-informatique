/****************************************************************************
** Meta object code from reading C++ file 'CodeViewer.hpp'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "CodeViewer.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CodeViewer.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_bsim__MyHighlighter[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_bsim__MyHighlighter[] = {
    "bsim::MyHighlighter\0"
};

void bsim::MyHighlighter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData bsim::MyHighlighter::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject bsim::MyHighlighter::staticMetaObject = {
    { &QSyntaxHighlighter::staticMetaObject, qt_meta_stringdata_bsim__MyHighlighter,
      qt_meta_data_bsim__MyHighlighter, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &bsim::MyHighlighter::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *bsim::MyHighlighter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *bsim::MyHighlighter::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_bsim__MyHighlighter))
        return static_cast<void*>(const_cast< MyHighlighter*>(this));
    return QSyntaxHighlighter::qt_metacast(_clname);
}

int bsim::MyHighlighter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QSyntaxHighlighter::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_bsim__CodeViewer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   18,   17,   17, 0x0a,
      41,   36,   17,   17, 0x0a,
      68,   17,   17,   17, 0x0a,
      89,   75,   17,   17, 0x08,
     110,   17,   17,   17, 0x08,
     135,  133,   17,   17, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_bsim__CodeViewer[] = {
    "bsim::CodeViewer\0\0id\0highlight(int)\0"
    "addr\0highlightAddress(uint32_t)\0wrap()\0"
    "newBlockCount\0updateAreaWidth(int)\0"
    "highlightCurrentLine()\0,\0updateArea(QRect,int)\0"
};

void bsim::CodeViewer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CodeViewer *_t = static_cast<CodeViewer *>(_o);
        switch (_id) {
        case 0: _t->highlight((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->highlightAddress((*reinterpret_cast< uint32_t(*)>(_a[1]))); break;
        case 2: _t->wrap(); break;
        case 3: _t->updateAreaWidth((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->highlightCurrentLine(); break;
        case 5: _t->updateArea((*reinterpret_cast< const QRect(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData bsim::CodeViewer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject bsim::CodeViewer::staticMetaObject = {
    { &QPlainTextEdit::staticMetaObject, qt_meta_stringdata_bsim__CodeViewer,
      qt_meta_data_bsim__CodeViewer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &bsim::CodeViewer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *bsim::CodeViewer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *bsim::CodeViewer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_bsim__CodeViewer))
        return static_cast<void*>(const_cast< CodeViewer*>(this));
    if (!strcmp(_clname, "BreakPointListener"))
        return static_cast< BreakPointListener*>(const_cast< CodeViewer*>(this));
    return QPlainTextEdit::qt_metacast(_clname);
}

int bsim::CodeViewer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QPlainTextEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
