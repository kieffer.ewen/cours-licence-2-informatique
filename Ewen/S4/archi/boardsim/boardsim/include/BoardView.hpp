/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_BOARDVIEW_HPP
#define BSIM_BOARDVIEW_HPP

#include <QDockWidget>
#include <QGraphicsView>
#include "IView.hpp"

namespace bsim {

class MainWindow;
class VisualGraph;

// BoardView class
class BoardView: public QDockWidget, public IView {
public:
	BoardView(MainWindow& window);
	virtual ~BoardView();

	// IView overload
	virtual void onClear(void);
	virtual void loadSettings(QSettings& settings);
	virtual void saveSettings(QSettings& settings);
	virtual void onBoardLoad(void);
	virtual void update(void);
	virtual void onRun(void);
	virtual void onBreak(void);

private:
	QGraphicsScene scene;
	QGraphicsView view;
	MainWindow& _window;
	VisualGraph *vgraph;
};

}	// bsim

#endif	// BSIM_BOARDVIEW_HPP
