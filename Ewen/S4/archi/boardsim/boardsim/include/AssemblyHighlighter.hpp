/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_ASSEMBLY_HIGHLIGHTER_HPP
#define BSIM_ASSEMBLY_HIGHLIGHTER_HPP

#include <Highlighter.hpp>

namespace bsim {
	
class AssemblyHighlighter;
	
class AssemblySyntax: public QSyntaxHighlighter {
	Q_OBJECT
public:
	AssemblySyntax(const AssemblyHighlighter& _high, QTextDocument *document);
	virtual void highlightBlock(const QString &text);
private:
	const AssemblyHighlighter& high;
};

class AssemblyHighlighter: public Highlighter {
public:
	typedef enum {
		LABEL = 0,
		DIRECTIVE,
		INST,
		CONSTANT,
		COMMENT,
		LINE_COMMENT,
		NAMED
	} kind_t;

	AssemblyHighlighter(void);
	QSyntaxHighlighter *make(QTextDocument *document) const;
	
private:
	TokenStyle label;
	TokenStyle directive;
	TokenStyle inst;
	TokenStyle constant;
	TokenStyle comment;
	TokenStyle line_comment;
	TokenStyle named;
};

}	// bsim

#endif // BSIM_ASSEMBLY_HIGHLIGHTER_HPP
