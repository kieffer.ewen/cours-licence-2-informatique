/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_SOURCE_H
#define BSIM_SOURCE_H

#include <stdint.h>
#include <QString>

namespace bsim {

class Highlighter;

// ISource interface
class ISource {
public:
	virtual ~ISource();
	virtual QString name(void) = 0;
	virtual QString getText(void) = 0;
	virtual int getLine(uint32_t address) = 0;
	virtual uint32_t getAddress(int line) = 0;
	virtual bool isAddressable(int line) = 0;
	virtual Highlighter *highlighter(void);
};

} // bsim

#endif	// BSIM_SOURCE_H
