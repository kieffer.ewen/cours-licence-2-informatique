/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef MEMORY_DW_H
#define MEMORY_DW_H

#include <QComboBox>
#include <QDockWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMap>
#include <QPushButton>
#include <QSpinBox>
#include <QTimer>
#include <QTreeWidget>
#include <QVBoxLayout>
#include <QWidget>

#include "IView.hpp"
#include "PrintValue.hpp"

extern "C" {
#include "arm/mem.h"
}

namespace bsim {

class MainWindow;

class MemoryDockWidget : public QDockWidget, public PrintValue, public IView {
	Q_OBJECT
	typedef uint64_t address_t;

	typedef enum display_t {
		DEC = 0,
		UDEC = 1,
		HEX = 2,
		BIN = 3,
		CHAR = 4
	} display_t;
	
	typedef enum {
		BYTE = 0,
		HALF = 1,
		WORD = 2
	} data_size_t;

	class Config {
	public:
		Config(void)
		:	name(""),
			size(16),
			data_size(WORD),
			display(DEC)
		{ }

		Config(const Config& conf)
		:	name(conf.name),
			size(conf.size),
			data_size(conf.data_size),
			display(conf.display)
		{ }
	
		QString name;
		int size;
		data_size_t data_size;
		display_t display;
	};
	
public:

	MemoryDockWidget(MainWindow& window);

	// IView overload
	virtual void update(void);
	virtual void onRun(void);
	virtual void onBreak(void);
	virtual void onStart(void);
	virtual void onStop(void);
	virtual void loadSettings(QSettings& settings);
	virtual void saveSettings(QSettings& settings);

private slots:
	void addrChanged(const QString & text);
	void addrFinished(void);
	void addrSelect(int index);
	void sizeChanged(void);
	void timeout(void);
	void changeDataSize(int i);	
	void changeDataDisplay(int i);

private:
	void updateRange(void);
	void updateContent(void);
	QString toString(uint32_t x);
	address_t regValue(QString name, bool& ok);
	address_t evaluate(QString expr, bool& ok);
	void enable(void);
	void disable(void);
	void updateConf(void);

	MainWindow& _window;

	QString name;
	QVBoxLayout* vbox;
	QHBoxLayout* hbox;
	QTreeWidget* tw;
	QComboBox *addr_widget;
	QSpinBox *size_widget;
	QComboBox *data_size_select, *data_display_select;
	QTimer timer;
	bool valid;
	address_t addr;
	Config conf;
	int cconf;
	QVector<Config> confs;
};

}	// bsim

#endif
