/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_PATH_HPP
#define BSIM_PATH_HPP

#include <QDateTime>
#include <QString>

namespace bsim  {

// Path class
class Path {
public:
	inline Path(void) { }
	inline Path(const char *path): str(path) { }
	inline Path(QString path): str(path) { }
	inline Path(const Path& path): str(path.str) { }
	
	const char *toCString(void) const ;
	inline QString toQString(void) const { return str; }
	
	bool isAbsolute(void) const;
	bool isRelative(void) const;
	bool exists(void) const;
	bool isReadable(void) const;
	
	Path parent(void) const;
	Path join(const Path& path) const;
	void set(const Path& path);
	QString extension(void) const;

	QDateTime lastModif();

	inline Path& operator=(const char *path) { set(path); return *this; }
	inline Path& operator=(QString path) { set(path); return *this; }
	inline Path& operator=(const Path& path) { set(path); return *this; }
	inline Path operator/(const char *path) const { return join(path); }
	inline Path operator/(QString path) const { return join(path); }
	inline Path operator/(const Path& path) const { return join(path); }

private:
	QString str;
	mutable QByteArray bytes;
};

}	// bsim

#endif	// BSIM_PATH_HPP
