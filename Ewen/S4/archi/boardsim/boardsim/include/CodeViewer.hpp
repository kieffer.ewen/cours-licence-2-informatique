/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef SIM_CODEVIEWER
#define SIM_CODEVIEWER

#include <stdint.h>
#include <QPlainTextEdit>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextCursor>
#include <QMainWindow>
#include <QStatusBar>
#include <QSyntaxHighlighter>
#include <QSet>

#include "ISource.hpp"
#include "MainWindow.hpp"
#include "Sidebar.hpp"

namespace bsim {

class MainWindow;

class MyHighlighter: public QSyntaxHighlighter {
	Q_OBJECT
public:

	MyHighlighter(QTextDocument *doc):
		QSyntaxHighlighter(doc),
		comment_color("green")
	{ }

	virtual void highlightBlock(const QString &text) {
		for(int i = 0; i < text.length(); i++)
			if(text[i] == '@') {
				setFormat(i, text.length() - i, comment_color);
				break;
			}
	}	
	
private:
	QColor comment_color;
};

// CodeViewer class
class CodeViewer : public QPlainTextEdit, public BreakPointListener {
  Q_OBJECT
public:
	CodeViewer(MainWindow& main_window, QWidget *parent = 0);
	~CodeViewer();
	void setSource(ISource *source);
	void clearSource(void);
	void toogleAddresses();
	
	void lineNumberAreaPaintEvent(QPaintEvent *event);
	int lineNumberAreaWidth(void);
	void breakAreaPaintEvent(QPaintEvent *event);
	int breakAreaWidth(void);

	void setBreaks(bool set);
	void setLines(bool set);
	
	virtual void onAdd(BreakPoint *bp);
	virtual void onRemove(BreakPoint *bp);
	virtual void onEnable(BreakPoint *bp);
	virtual void onDisable(BreakPoint *bp);

public slots:
    void highlight(int id);
    void highlightAddress(uint32_t addr);
    void wrap(void);

protected:
	void resizeEvent(QResizeEvent *event);
	
private slots:
     void updateAreaWidth(int newBlockCount);
     void highlightCurrentLine();
     void updateArea(const QRect &, int);

private:

	// LineNumberArea class
	class LineNumberArea: public QWidget {
	public:
		LineNumberArea(CodeViewer *viewer);
		QSize sizeHint(void) const;
	protected:
		void paintEvent(QPaintEvent *event);
	private:
		CodeViewer *_viewer;
	};
	
	// BreakArea class
	class BreakArea: public QWidget {
	public:
		BreakArea(CodeViewer *viewer);
		QSize sizeHint(void) const;
	protected:
		void paintEvent(QPaintEvent *event);
		virtual void mouseReleaseEvent(QMouseEvent * event);
	private:
		CodeViewer *_viewer;
	};
	
	// private methods
	void setBreak(int x, int y);
	
	// attributes
	Sidebar sd ;
	QColor lineColor;
	QWidget *lineNumberArea;
	BreakArea *break_area;
	bool use_break, use_lines;
	QSet<int> break_lines;
	ISource *_source;
	MainWindow& main;
};

}	// bsim

#endif
