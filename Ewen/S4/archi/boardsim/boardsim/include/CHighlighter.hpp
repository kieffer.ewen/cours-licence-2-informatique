/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_CHIGHLIGHTER_HPP
#define BSIM_CHIGHLIGHTER_HPP

#include <Highlighter.hpp>

namespace bsim {

class CHighlighter;
	
class CSyntax: public QSyntaxHighlighter {
	Q_OBJECT
public:
	CSyntax(const CHighlighter& _high, QTextDocument *document);
	virtual void highlightBlock(const QString &text);
private:
	static const int out_comment = 0, in_comment = 1;
	const CHighlighter& high;
	QMap<QString, const TokenStyle *> kws;
};

class CHighlighter: public Highlighter {
	friend class CSyntax;
public:
	typedef enum {
		COMMENT = 0,
		KEYWORD,
		TYPE,
		NUMBER,
		STRING,
		OPERATOR,
		PREPROC,
		BRACES
	} kind_t;

	CHighlighter(void);
	QSyntaxHighlighter *make(QTextDocument *document) const;
	
private:
	TokenStyle	comment,
				keyword,
				type,
				number,
				string,
				op,
				preproc,
				braces;
};

}	// bsim

#endif	// BSIM_CHIGHLIGHTER_HPP
