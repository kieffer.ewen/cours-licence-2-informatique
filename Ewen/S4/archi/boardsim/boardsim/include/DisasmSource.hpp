/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_DISASM_SOURCE_HPP
#define BSIM_DISASM_SOURCE_HPP

#include <QMap>
#include <QString>
#include <ISource.hpp>

namespace bsim {

class Program;

// DisasmSource class
class DisasmSource: public ISource {
public:
	DisasmSource(Program *prog);
	void update(Program *_prog);

	QString name(void) override;
	QString getText(void) override;
	int getLine(uint32_t address) override;
	uint32_t getAddress(int line) override;
	bool isAddressable(int line) override;

private:
	void init(void);
	Program *_prog;
	QString text;
	QMap<int, uint32_t> addr_map;
	QMap<uint32_t, int> line_map;
};

}	// bsim

#endif // BSIM_DISASM_SOURCE_HPP
