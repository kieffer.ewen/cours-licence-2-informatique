/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_SCHEDULER_H
#define BSIM_SCHEDULER_H

#include <stdint.h>

#include <QList>

#include "common.hpp"

namespace bsim {

class Board;
class Core;

// RunDriver interface
class Scheduler {
public:

	class Handler {
	public:
		virtual void onDone(Core *core) = 0;
		virtual bool stops(Core *core) = 0;
	};
	
	class ForeverHandler: public Handler {
	public:
		virtual void onDone(Core *core) { }
		virtual bool stops(Core *core) { return false; }	
	};
	static ForeverHandler FOREVER;
	
	class CompHandler: public Handler {
	public:
		inline CompHandler(Handler& next): _next(next) { }
		virtual void onDone(Core *core) { }
		virtual bool stops(Core *core) { return _next.stops(core); }
	private:
		Handler& _next;
	};
	
	class BreakPointsHandler: public CompHandler {
	public:
		BreakPointsHandler(QList<uint32_t> breaks, Handler& next = FOREVER);
		virtual bool stops(Core *core);
	private:
		QList<uint32_t> _breaks;
	};
	
	class NextHandler: public CompHandler {
	public:
		NextHandler(QString file, int line, Handler& next = FOREVER);
		virtual bool stops(Core *core);
	private:
		QString _file;
		int _line;
	};
	
	class Event {
		friend class Scheduler;
	public:
		Event(Board& board, time_t time = 0, QString name = QString::null);
		virtual ~Event();
		Scheduler *scheduler(void) const;
		inline time_t time(void) const { return _time; }
		inline QString name(void) const { return _name; }
		virtual void run(void) = 0;
		void cancel(void);
		void reschedule(time_t time);
	private:
		QString _name;
		time_t _time;
		Board& _board;
	};

	Scheduler(Board& board);
	virtual ~Scheduler(void);
	inline void setHandler(Handler& handler) { _handler = &handler; }
	inline Board& board(void) const { return _board; }
	inline Handler& handler(void) const { return *_handler; }
	inline time_t time(void) const {  return _time;  }
	void add(Event *event);
	
	// possibly overload
	virtual void start(void);
	virtual void stop(void) = 0;
	virtual void pause(Core *core) = 0;
	virtual void resume(Core *core) = 0;
	virtual void pauseAll(void) = 0;
	virtual void resumeAll(void) = 0;
	virtual void step(void) = 0;

protected:
	inline void increaseTime(time_t delay = 1) { _time += delay; }
	void processEvents(void);
	inline time_t nextEventTime(void) const { if(events.isEmpty()) return (-1); else return events.first()->time(); }
	
private:
	time_t _time;
	Board& _board;
	Handler *_handler;
	QList<Event *> events;
};

} // bsim

#endif	// BSIM_SCHEDULER_H
