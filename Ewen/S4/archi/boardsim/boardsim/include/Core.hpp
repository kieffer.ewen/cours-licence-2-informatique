/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_CORE_HPP
#define BSIM_CORE_HPP

#include <QList>
#include <QSet>
#include <QString>

#include "IComponent.hpp"
#include "Program.hpp"

namespace bsim {

// Core class
class Core: public IComponent {
public:
	static const int default_freq = 100;
	Core(Board& board, QDomElement configuration);
	inline int frequency(void) const { return freq; }
	inline void setFrequency(int frequency) { freq = frequency; } 

	// ISA methods
	virtual Program *load(QString path, IMonitor& mon = IMonitor::def()) = 0;
	virtual void start(void) = 0;
	virtual void stop(void) = 0;
	virtual void step(void) = 0;
	virtual uint32_t pc(void) = 0;
	virtual uint32_t size(uint32_t a) = 0;

	// component overload
	virtual Core *toCore(void);

private:
	int freq;
};

}	// bsim

#endif	// BSIM_CORE_HPP
