/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_TIMERSCHEDULER_HPP
#define BSIM_TIMERSCHEDULER_HPP

#include <QList>
#include <QTimer>
#include <Scheduler.hpp>

namespace bsim {

// TimerDriver class
class TimerScheduler: public QObject, public Scheduler {
	Q_OBJECT
public:
	static const int default_slices = 50;
	TimerScheduler(Board& board);
	virtual ~TimerScheduler(void) { }
	inline int slices(void) const { return _slices; }
	void setSlices(int slices);
	
	// RunDriver overload
	virtual void start(void);
	virtual void stop(void);
	virtual void pause(Core *core);
	virtual void resume(Core *core);
	virtual void pauseAll(void);
	virtual void resumeAll(void);
	virtual void step(void);

private slots:
	void timeout(void);
private:
	int _slices;
	QTimer timer;
	QList<Core *> running;
	QList<Core *> paused;
	QList<Core *> todo;
};

}	// bsim

#endif 	// BSIM_TIMERSCHEDULER_HPP
