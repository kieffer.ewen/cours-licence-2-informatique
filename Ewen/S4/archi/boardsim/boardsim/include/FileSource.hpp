/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_FILE_SOURCE_HPP
#define BSIM_FILE_SOURCE_HPP

#include <QDateTime>

#include <Exception.hpp>
#include <ISource.hpp>
#include <Path.hpp>

namespace bsim {

class Program;

class FileSourceException: public Exception {
public:
	inline FileSourceException(QString message): Exception(message) { }
};


// FileSource class
class FileSource: public ISource {
public:
	FileSource(QString name, Path path, Program& program);
	
	QString name(void) override;
	QString getText(void) override;
	int getLine(uint32_t address) override;
	uint32_t getAddress(int line) override;
	bool isAddressable(int line) override;
	Highlighter *highlighter(void) override;

private:
	QString _name;
	Path _path;
	Program& prog;
	QString text;
	Highlighter *high;
};

}	// bsim

#endif	// BSIM_FILE_SOURCE_HPP
