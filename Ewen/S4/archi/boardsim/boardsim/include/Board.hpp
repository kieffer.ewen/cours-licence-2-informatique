/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef SIM__BOARD
#define SIM__BOARD

#include <QMap>
#include <QSet>
#include <QString>
#include <gel/gel.h>
#include <Core.hpp>
#include <IComponent.hpp>
#include <Exception.hpp>
#include <MethodPtr.hpp>

/*extern "C" { 
#	include "arm/mem.h"
}*/

namespace bsim {

class Core;
class IComponent;
class MainWindow ;
class Program;
class Scheduler;

// useful
inline QString mkAddr(uint32_t addr) { return QString("0x%1").arg(addr, 8, 16, QChar('0')); }

// exceptions
class LoadException: public Exception {
public:
	inline LoadException(QString message = ""): Exception(message) { }
};


// Board class
class Board {
public:
	static const int default_mck = 1000;

	Board(void);
	~Board(void);
	inline QString name(void) const { return _name; }
	inline QString prefix(void) const { return _prefix; }

	// components
	void load(QString name, IMonitor& monitor);
	IComponent* getComponent(QString name);
	QMap<QString, IComponent*>& getComponents();
	inline const QList<Core *>& cores(void) const { return _cores; }
	inline Core *mainCore(void) const { return _main; }

	// program management
	inline Program *program(void) const { return prog; }
	void setProgram(Program *program);
	
	// simulation handling
	inline Scheduler *scheduler(void) const { return sched; }
	inline void setScheduler(Scheduler *scheduler) { sched = scheduler; }
	inline int mck(void) const { return _mck; }
	void start(void);
	void stop(void);

	// display
	inline const QSize& size(void) const { return _size; }

private:
	void clear(void);

	// board simulation
	QString _name;
	QSize _size;
	QMap<QString, void*> loaded ;
	QMap<QString, IComponent*> components ;
	QList<Core *> _cores;
	Core *_main;
	int _mck;
	bool _io_installed;
	
	// processor simulation
	QString _prefix;
	Program *prog;
	Scheduler *sched;
};

}	// bsim

#endif
