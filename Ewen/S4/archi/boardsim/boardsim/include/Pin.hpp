#ifndef BSIM_PIN_H
#define BSIM_PIN_H

#include <stdint.h>

#include <QDomElement>
#include <QMap>
#include <QString>
#include <QVariant>

#include "common.hpp"

namespace bsim {

class IComponent;

// Pin interface
class Pin {
public:

	Pin(IComponent *component, QString name = "");
	inline QString name(void) const { return _name; }
	inline IComponent *component(void) const { return _comp; }

	void connect(Pin *pin);
	inline Pin *connection(void) const { return _pin; }

	// signal interface
	void change(uint64_t time, bool value);
	virtual void onChange(uint64_t time, bool value);
	virtual QString asIRQ(void);

	// serial interface
	static QString MODE, WORDL, SYNC, PARITY, NBSTOP, MSB, RATE, CHMODE;
	typedef enum {
		NORMAL = 0,
		RS285 = 1,
		HARDWARE_HANDSHAKING = 2,
		MODEM = 3,
		ISO7816_NT = 4,
		IDO7816_T = 5,
		IRDA = 6
	} mode_t;
	typedef enum {
		NOPAR = 0,
		ODD = 1,
		EVEN = 2,
		FORCED_1 = 3,
		FORCED_0 = 4,
		MULTIDROP = 5
	} parity_t;
	typedef enum {
		MSB_FIRST = 0,
		MSB_LAST = 1
	} msb_t;
    typedef enum {
		NORMAL_CH = 0,
		ECHO = 1,
		LOCAL_LOOPBACK=2,
		REMOTE_LOOPBACK=3
	} chmode_t;
	
	typedef QMap<QString, QVariant> config_t;

	void send(uint64_t time, uint32_t word);
	virtual void onReceive(uint64_t time, uint32_t word);
	void configure(uint64_t time, const config_t& configuration);
	virtual void onConfigure(uint64_t time, const config_t& configuration);

	// analog interface
	virtual void sendAnalog(uint64_t time, double volt);
	virtual void receiveAnalog(uint64_t time, double volt);
	
	// electric sender interface
	virtual void setElectric(time_t t, double U, double I);

	// physics interface
	typedef enum {
		NO_UNIT = 0,
		HEAT = 1,
		ANGLE = 2
	} unit_t;
	virtual void setPhysics(time_t time, double value, unit_t unit);

protected:

	// electric receiver interface
	virtual void getElectric(time_t t, double U, double I);

	// physics interface
	virtual void getPhysics(time_t time, double value, unit_t unit);

private:
	QString _name;
	Pin *_pin;
	IComponent *_comp;
};


// SerialPin class
class SerialPin: public Pin {
public:
	inline SerialPin(IComponent *component, QString name = ""): Pin(component, name) { }
	virtual void onReceive(uint64_t time, uint32_t word);
	virtual void onConfigure(uint64_t time, const config_t& configuration);
	inline QVariant get(QString id) const { return my_conf[id]; } 
	void set(QString id, QVariant value);
protected:
	virtual void onWord(uint64_t time, uint32_t word) = 0;
	virtual void onError(uint64_t time, QString id) = 0;
private:
	config_t my_conf;
	QString error_id;
};

} // bsim

#endif	// BSIM_PIN_H
