/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef REGISTER_DW_H
#define REGISTER_DW_H

#include <QDockWidget>
#include <QTableWidget>
#include <QTreeWidget>
#include <QWidget>
#include <QTimer>
#include <QMenu>
#include "IView.hpp"

namespace bsim {

class Board;
class AbstractRegisterNode;
class MainWindow;

// RegisterDockWidget class
class RegisterDockWidget: public QDockWidget, public IView {
  Q_OBJECT
public:
	RegisterDockWidget(MainWindow& window) ;
	
	// IView overload
	virtual void onClear(void);
	virtual void loadSettings(QSettings& settings);
	virtual void saveSettings(QSettings& settings);
	virtual void onBoardLoad(void);
	virtual void update(void);
	virtual void onRun(void);
	virtual void onBreak(void);

public slots:
	void timeout(void);
	void startContextualMenu(const QPoint& pt);
	void setDef(void);
	void setDec(void);
	void setHex(void);
	void setBin(void);
	void copy(void);

private:
	QTreeWidget* tw;
	QTimer timer;
	Board *_board;
	MainWindow& _window;

	// contextual menu management
	QMenu menu;
	QAction *copy_action;
	AbstractRegisterNode *current;
};

}	// bsim

#endif
