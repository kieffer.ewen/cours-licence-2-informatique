/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_HIGHLIGHTER_HPP
#define BSIM_HIGHLIGHTER_HPP

#include <QMap>
#include <QSyntaxHighlighter>
#include <QTextCharFormat>
#include <QVector>

namespace bsim {

// TokenType class
class TokenStyle {
public:
	inline TokenStyle(QString name, QString description = ""): _name(name), desc(description) { }
	inline QString name(void) const { return _name; }
	inline QString description(void) const { return desc; }
	QTextCharFormat format;
private:
	QString _name;
	QString desc;
};

// Highlighter class
class Highlighter {
public:

	static void registerForExt(QString ext, Highlighter *highlighter);
	static void unregisterForExt(QString ext);
	static Highlighter *getForExt(QString ext);

	inline Highlighter(QString name): _name(name) { }
	inline QString name(void) const { return _name; }
	inline int count(void) const { return _styles.size(); }
	inline TokenStyle *style(int i) const { return _styles[i]; }
	inline TokenStyle *operator[](int i) const { return style(i); }
	virtual QSyntaxHighlighter *make(QTextDocument *document) const = 0;

protected:
	void add(TokenStyle *style);
private:
	QString _name;
	QVector<TokenStyle *> _styles;
	static QMap<QString, Highlighter *> exts;
};

}	// bsim

#endif	// BSIM_HIGHLIGHTER_HPP
