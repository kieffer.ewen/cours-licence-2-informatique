/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_CYCLEVIEW_HPP
#define BSIM_CYCLEVIEW_HPP

#include <QLabel>
#include <QStyle>
#include <QTimer>
#include <QLineEdit>
#include <IView.hpp>

namespace bsim {

class MainWindow;

// CycleView class	
class CycleView: public /*QLabel*/ QLineEdit, public IView {
	Q_OBJECT
public:
	static const QString none;

	CycleView(MainWindow& window);
	virtual ~CycleView(void);

	// IView overload
	virtual void onRun(void);
	virtual void onBreak(void);
	virtual void update(void);
	virtual void onStart(void);
	virtual void onStop(void);
	
	// Widget overload
	virtual QSize sizeHint(void) const;

private slots:
	void timeout(void);

private:
	MainWindow& win;
	QTimer timer;
};

	
}	// bsim

#endif	// BSIM_CYCLEVIEW_HPP
