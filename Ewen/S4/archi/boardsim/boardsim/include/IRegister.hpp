/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef BSIM_IREGISTER_H
#define BSIM_IREGISTER_H

#include <QString>
#include <QVariant>
#include <QVector>

#include "common.hpp"
#include <MethodPtr.hpp>

namespace bsim {

class Board;
class Core;
class IComponent;
class IHardRegister;
class IMonitor;
class ICoreRegister;
class GlissWrapper;

// IRegister class
class IRegister {
public:

	static const int
		NONE = 0,
		INT = 1,
		BITS = 2,
		ADDR = 3,
		FLOAT = 4;

	static const int
		READ = 0x01,
		WRITE = 0x02,
		BOTH = READ | WRITE;

	static const int
		DEF = 0,
		DEC = 1,
		HEX = 2,
		BIN = 3;
		
	typedef enum {
		VAL = 0,
		ENUM = 1,
		ARRAY = 2
	} field_t;

	class EnumValue {
	public:
		inline EnumValue(void): _val(0) { }
		inline EnumValue(QString name, uint32_t val): _name(name), _val(val) { }
		inline QString name(void) const { return _name; }
		inline uint32_t value(void) const { return _val; }
	private:
		QString _name;
		uint32_t _val;
	};

	class EnumSet {
	public:
		inline EnumSet(void): _vals(0) { }
		inline EnumSet& add(EnumValue v)
			{	if(!_vals)  _vals = new QVector<EnumValue>();
				_vals->append(v); return *this; }
		inline operator QVector<EnumValue> *(void)
			{ QVector<EnumValue> * r = _vals; _vals = 0; return r; }
		
		inline EnumSet& operator&(EnumValue v) { add(v); return *this; }
		inline EnumSet& operator+(EnumValue v) { add(v); return *this; }
	private:
		QVector<EnumValue> *_vals;
	};

	static EnumSet _;
	static inline EnumValue val(QString name, uint32_t value) { return EnumValue(name, value); }

	class Field {
	public:
	
		Field(IRegister& reg, QString name, int bit);
		Field(IRegister& reg, QString name, int up, int lo, QVector<EnumValue> *vals = 0, field_t type = VAL);
		~Field(void);
		
		inline QString name(void) const { return _name; }
		inline int size(void) const { return _up - _lo + 1; }
		inline int bit(void) const { return _lo; }
		inline int up(void) const { return _up; }
		inline int low(void) const { return _lo; }
		inline bool hasValues(void) const { return _vals; }
		inline const QVector<EnumValue>& values(void) const { return *_vals; }
		inline field_t type(void) const { return _type; }
		
	private:
		QString _name;
		int _up, _lo;
		QVector<EnumValue> *_vals;
		field_t _type;
	};
	
	class AbstractAccessor {
	public:
		virtual ~AbstractAccessor(void);
		virtual void access(addr_t a, int s, void *d);
	};
	static AbstractAccessor null_accessor;
	
	virtual QString name(int i = -1) = 0;
	virtual QString qualifiedName(IComponent *component, int i = -1);
	virtual QString displayName(int i = -1);
	virtual int count(void) = 0;
	virtual int size(void) = 0;
	virtual int type(void) = 0;
	virtual QVariant get(int i = 0) = 0;
	virtual void set(QVariant value, int i = 0) = 0;
	virtual int access(void) = 0;
	inline bool isReadable(void) { return access() & READ; }
	inline bool isWritable(void) { return access() & WRITE; }
	inline bool isReadWrite(void) { return access() == BOTH; }
	inline bool isOnlyReadable(void) { return access() == READ; }
	inline bool isOnlyWritable(void) { return access() == WRITE; }
	virtual IHardRegister *toHard(void) = 0;
	virtual ICoreRegister *toCore(void) = 0;
	virtual QString format(int i = 0, int fmt = DEF);
	
	const QVector<Field *> fields(void) const { return _fields; }

protected:
	virtual ~IRegister(void) { }
	
private:
	QVector<Field *> _fields;
};


// IHardRegister class
class IHardRegister: public IRegister {
	friend class IComponent;
public:

	template <class M, class R>
	class Maker {
	public:
		inline Maker(R *reg): r(reg) { }
		inline M& name(QString name) { r->_name = name; return *static_cast<M *>(this); }
		inline M& format(QString format) { r->_format = format; return *static_cast<M *>(this); }
		inline M& type(int type) { r->_type = type; return *static_cast<M *>(this); }
		inline M& address(int address) { r->_address = address; return *static_cast<M *>(this); }
		inline operator R *(void) { return r; }
	protected:
		R *r;
	};

	class Make {
		friend class IHardRegister;
	public:
		inline Make(void): _count(1), _size(32), _stride(0), _type(BITS), _access(READ | WRITE), _offset(0) { }
		inline Make& name(QString name) { _name = name; return *this; }
		inline Make& format(QString format) { _format = format; return *this; }
		inline Make& count(int count) { _count = count; return *this; }
		inline Make& size(int size) { _size = size; return *this; }
		inline Make& stride(int stride) { _stride = stride; return *this; }
		inline Make& type(int type) { _type = type; return *this; }
		inline Make& access(int access) { _access = access; return *this; }
		inline Make& offset(uint32_t offset) { _offset = offset; return *this; }
		inline Make& read(void) { _access = READ; return *this; }
		inline Make& write(void) { _access = WRITE; return *this; }
	private:
		QString _name;
		QString _format;
		int _count;
		int _size;
		int _type;
		int _access;
		int _stride;
		addr_t _offset;
	};
	inline Make make(void) { return Make(); }

	IHardRegister(IComponent *comp, const Make& make);
	
	uint32_t address(void) const;
	inline int stride(void) const { return _stride; }
	virtual void read(addr_t addr, void *data, int size) = 0;
	virtual void write(addr_t addr, void *data, int size) = 0;
	inline IComponent *component() const { return _comp; }
	
	// IRegister overload
	virtual QString name(int i = -1);
	virtual int count(void);
	virtual int size(void);
	virtual int type(void);
	virtual int access(void);
	virtual IHardRegister *toHard(void);
	virtual ICoreRegister *toCore(void);

protected:
	IHardRegister(Board& board, int count = 1);
	IComponent *_comp;
	IMonitor& monitor() const;
	QString _name, _format;
	int _count, _size, _type, _access, _stride;
	uint32_t _address;
};


// Getter class
template <class T>
class Getter: public MethodPtr {
public:
	virtual T get(void) = 0;
};
template <class T, class C, typename M>
class GetterImpl: public Getter<T> {
public:
	GetterImpl(C& o, M m): obj (o), meth(m) { }
	virtual void exec(addr_t addr, int size, void* data) { *(T *)data = (obj.*meth)(); }
	virtual T get(void) { return (obj.*meth)(); }
private:
	C& obj ;
	M meth ;
};


// Setter class
template <class T>
class Setter: public MethodPtr {
public:
	virtual void set(const T& v) = 0;
};
template <class T, class C, typename M>
class SetterImpl: public Setter<T> {
public:
	SetterImpl(C& o, M m): obj (o), meth(m) { }
	virtual void exec(addr_t addr, int size, void* data) {  (obj.*meth)(*(T *)data); }
	virtual void set(const T& v) { (obj.*meth)(v); }
private:
	C& obj ;
	M meth ;
};


// HardRegister32 class
class HardRegister32: public IHardRegister {
public:
	class Maker: public IHardRegister::Maker<Maker, HardRegister32> {
	public:
		inline Maker(HardRegister32 *reg): IHardRegister::Maker<Maker, HardRegister32>(reg) { }
		template <class C, typename M> inline Maker& getter(C& o, M m) { r->setGetter(new GetterImpl<int32_t, C, M>(o, m)); return *this; }
		template <class C, typename M> inline Maker& setter(C& o, M m) { r->setSetter(new SetterImpl<int32_t, C, M>(o, m)); return *this; }
	};

	static inline Maker make(Board& board) { return Maker(new HardRegister32(board)); }
	static inline Maker make(HardRegister32 *r) { return Maker(r); }

	virtual QVariant get(int i = 0);
	virtual void set(QVariant value, int i = 0);

	virtual void read(addr_t addr, void *data, int size);
	virtual void write(addr_t addr, void *data, int size);
	virtual void observe(addr_t addr, void *data, int size);

	void setGetter(Getter<int32_t> *getter);
	void setSetter(Setter<int32_t> *setter);
protected:
	Getter<int32_t> *_getter;
	Setter<int32_t> *_setter;
	HardRegister32(Board& board);
};


// 32-bits hardware register with with software event
template <class T, class C, typename M, typename S>
class GetterImplWithSoftIO: public Getter<T> {
public:
	GetterImplWithSoftIO(C& o, M m, S s): obj (o), meth(m), smeth(s) { }
	virtual void exec(addr_t addr, int size, void* data) { *(T *)data = (obj.*smeth)(); }
	virtual T get(void) { return (obj.*meth)(); }
private:
	C& obj;
	M meth;
	S smeth;
};

template <class T, class C, typename M, typename S>
class SetterImplWithSoftIO: public Setter<T> {
public:
	SetterImplWithSoftIO(C& o, M m, S s): obj (o), meth(m), smeth(s) { }
	virtual void exec(addr_t addr, int size, void* data) {  (obj.*smeth)(*(T *)data); }
	virtual void set(const T& v) { (obj.*meth)(v); }
private:
	C& obj;
	M meth;
	S smeth;
};

class HardRegister32WithSoftIO: public HardRegister32 {
public:
	class Maker: public IHardRegister::Maker<Maker, HardRegister32WithSoftIO> {
	public:
		inline Maker(HardRegister32WithSoftIO *reg): IHardRegister::Maker<Maker, HardRegister32WithSoftIO>(reg) { }
		template <class C, typename M, typename S>
		inline Maker& getter(C& o, M m, S s) { r->setGetter(new GetterImplWithSoftIO<int32_t, C, M, S>(o, m, s)); return *this; }
		template <class C, typename M, typename S>
		inline Maker& setter(C& o, M m, S s) { r->setSetter(new SetterImplWithSoftIO<int32_t, C, M, S>(o, m, s)); return *this; }
	};

	static inline Maker make(Board& board) { return Maker(new HardRegister32WithSoftIO(board)); }
	static inline Maker make(HardRegister32WithSoftIO *r) { return Maker(r); }

protected:
	HardRegister32WithSoftIO(Board& board);
};


// array of 32-bits hardware registers
template <class T, class C, typename M>
class ArraySetterImpl: public Setter<T> {
public:
	ArraySetterImpl(C& o, M m, int i): obj (o), meth(m), _i(i) { }
	virtual void exec(addr_t addr, int size, void* data) {  (obj.*meth)(_i, *(T *)data); }
	virtual void set(const T& v) { (obj.*meth)(_i, v); }
private:
	C& obj ;
	M meth ;
	int _i;
};

template <class T, class C, typename M>
class ArrayGetterImpl: public Getter<T> {
public:
	ArrayGetterImpl(C& o, M m, int i): obj (o), meth(m), _i(i) { }
	virtual void exec(addr_t addr, int size, void* data) { *(T *)data = (obj.*meth)(_i); }
	virtual T get(void) { return (obj.*meth)(_i); }
private:
	C& obj ;
	M meth ;
	int _i;
};

class ArrayHardRegister32: public IHardRegister {
public:
	class Maker: public IHardRegister::Maker<Maker, ArrayHardRegister32> {
	public:
		inline Maker(ArrayHardRegister32 *reg): IHardRegister::Maker<Maker, ArrayHardRegister32>(reg) { }
		template <class C, typename M> inline Maker& getter(C& o, M m) {
			for(int i = 0; i < r->count(); i++)
				r->setGetter(i, new ArrayGetterImpl<int32_t, C, M>(o, m, i));
			return *this;
		}
		template <class C, typename M> inline Maker& setter(C& o, M m) {
			for(int i = 0; i < r->count(); i++)
				r->setSetter(i, new ArraySetterImpl<int32_t, C, M>(o, m, i));
			return *this;
		}
	};

	static inline Maker make(Board& board, int size) { return Maker(new ArrayHardRegister32(board, size)); }

	virtual QVariant get(int i = 0);
	virtual void set(QVariant value, int i = 0);

	virtual void read(addr_t addr, void *data, int size);
	virtual void write(addr_t addr, void *data, int size);
	virtual void observe(addr_t addr, void *data, int size);

	void setGetter(int i, Getter<int32_t> *getter);
	void setSetter(int i, Setter<int32_t> *setter);

protected:
	int _size;
	Getter<int32_t> **_getter;
	Setter<int32_t> **_setter;
	ArrayHardRegister32(Board& board, int count);
};

}	// bsim



#endif	// BSIM_IREGISTER_H
