/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_IVIEW_H
#define BSIM_IVIEW_H

class QSettings;

namespace bsim {

// IView class
class IView {
public:

	virtual ~IView();

	// file activity
	virtual void onClear(void);
	virtual void onBoardLoad(void);
	virtual void onExecLoad(void);
	virtual void loadSettings(QSettings& settings);
	virtual void saveSettings(QSettings& settings);
	
	// simulation activity
	virtual void onStart(void);
	virtual void onRun(void);
	virtual void onBreak(void);
	virtual void update(void);
	virtual void onStop(void);
};

}	// bsim

#endif	// BSIM_IVIEW_H
