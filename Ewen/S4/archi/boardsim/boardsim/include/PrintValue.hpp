#pragma once

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <QString>

class PrintValue {
  private:
  protected:
    QString changeBase(int,int);
    QString makeQStringFromInt(int v) ;
    bool binary ;
    bool hexa ;
  public:
    void setHexa() ;
    void setBinary() ;
    void setDecimal() ;
};
