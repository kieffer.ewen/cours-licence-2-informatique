/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_PROGRAM_HPP
#define BSIM_PROGRAM_HPP

/*#include <gel/gel.h>
#include <gel/debug_line.h>*/

#include "common.hpp"
#include "Core.hpp"
#include "IComponent.hpp"
#include "Path.hpp"

namespace bsim {

class Board;
class FileSource;

// Program class
class Program {
public:
	Program(Path path, IMonitor& mon = IMonitor::def());
	virtual ~Program();

	virtual void reset();

	// debugging
	void getSources(QList<FileSource *>& sources);
	FileSource *sourceFor(QString file);
	virtual addr_t addressOf(QString file, int line) = 0;
	virtual void lineOf(addr_t address, QString& file, int& line) = 0;

	// memory access
	typedef struct section_t {
		inline section_t(uint32_t b, uint32_t s): base(b), size(s) { }
		uint32_t base;
		uint32_t size;
	} section_t;
	virtual void getSections(QList<section_t>& sections) = 0;
	virtual void get(addr_t addr, uint8_t& val) = 0;
	virtual void get(addr_t addr, uint16_t& val) = 0;
	virtual void get(addr_t addr, uint32_t& val) = 0;

	// specialize functions
	virtual uint32_t size(uint32_t address) = 0;
	virtual QString disasm(uint32_t address) = 0;
	virtual uint32_t start(void) const = 0;
	virtual uint32_t exit(void) const = 0;
	virtual addr_t addressOf(QString name) const = 0;
	virtual bool isFunction(addr_t a) = 0;

	// IO registers
	void record(IHardRegister *reg);
	void release(IHardRegister *reg);

protected:
	virtual bool fillSources(QMap<QString, FileSource *>& srcs) = 0;
	virtual void listen(addr_t addr) = 0;
	void onRead(addr_t a, void *data, int size);
	void onWrite(addr_t a, void *data, int size);

private:
	Path _path;
	
	// source management
	Path findSource(QString name);
	QList<Path> src_paths;
	QMap<QString, FileSource *> _sources;
	bool _source_done;

	// callback management
	QMap<addr_t, IHardRegister *> callbacks;
};

}	// bsim

#endif	// BSIM_PROGRAM_HPP
