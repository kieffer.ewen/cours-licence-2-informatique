/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_DISPLAY_HPP
#define BSIM_DISPLAY_HPP

#include <QDomElement>
#include <QGraphicsItemGroup>
#include <QMap>
#include <Pin.hpp>

namespace bsim {

// Visual class
class Visual {
public:

	typedef enum {
		NO_AXIS = 0,
		HORIZONTAL = 1,
		VERTICAL = 2
	} axis_t;

	typedef enum {
		NO_POS = 0,
		BEFORE = 1,
		LEFT = 2,
		CENTER = 3,
		RIGHT = 4,
		AFTER = 5,
		ABOVE = 1,
		TOP = 2,
		BOTTOM = 4,
		BELOW = 5
	} pos_t;

	typedef enum {
		NO_DIR = 0,
		NORTH = 1,
		EAST = 2,
		SOUTH = 3,
		WEST = 4
	} dir_t;

	static inline bool isHorizontal(dir_t ori) { return ori == EAST || ori == WEST; }
	static inline bool isVertical(dir_t ori) { return ori == NORTH || ori == SOUTH; }
	static dir_t parseDir(QString text, dir_t def);
	static dir_t reverseDir(dir_t dir);
	static QGraphicsItem *makeEdge(int x1, int y1, dir_t or1, int x2, int y2, dir_t or2);  
};


// predeclaration
class Board;
class IComponent;
class Pin;
class VisualComponent;


// VisualPin class
class VisualPin: public Visual {
public:
	VisualPin(VisualComponent& component, Pin *pin, dir_t dir, int offset, bool with_label = true);
	inline VisualComponent& component(void) const { return _comp; }
	inline Pin *pin(void) const { return _pin; }
	inline dir_t dir(void) const { return _dir; }
	inline int offset(void) const { return _off; }
	inline bool withLabel(void) const  { return _wlab; }
	inline QString label(void) const { return _pin->name(); }

	void parse(QDomElement element);
	int x(void);
	int y(void);
	
private:
	VisualComponent& _comp;
	Pin *_pin;
	dir_t _dir;
	int _off;
	bool _wlab;
};


// VisualComponent class
class VisualComponent: public QGraphicsItemGroup, public Visual {
public:
	VisualComponent(IComponent *component);
	inline IComponent *component(void) const { return _comp; }
	inline const QMap<Pin *, VisualPin *>& pins(void) const { return _pins; }

	void parse(QDomElement element);
	void add(VisualPin *pin);
	void remove(VisualPin *pin);

	void make(QString name);
	void makeLabel(QString name);
	void makeBox(void);
	void makePins(void);
	void makePin(VisualPin *pin);

	bool displayed;
	int x, y, w, h;
	int lab_dir, lab_xpos, lab_ypos, lab_xpad, lab_ypad;
	QColor text_color, fill_color, border_color;

private:
	IComponent *_comp;
	QMap<Pin *, VisualPin *> _pins;
};


// VisualGraph class
class VisualGraph: public Visual {
public:
	VisualGraph(Board& board, QGraphicsScene& scene);
	inline Board& board(void) const { return _board; }
	inline const QList<VisualComponent *>& components(void) const { return _comps; }
	void add(VisualComponent *component);
	void add(IComponent *component);
	void clear(void);
	
private:
	Board& _board;
	QList<VisualComponent *> _comps;
	QGraphicsScene& _scene;
	QMap<Pin *, VisualPin *> pins;
};

} // bsim

#endif	// BSIM_DISPLAY_HPP
