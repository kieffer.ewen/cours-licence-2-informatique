/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_BITS_HPP
#define BSIM_BITS_HPP

namespace bsim {

template <class T, int _i>
class Bit {
public:
	inline Bit(T& w): _w(w) { }
	inline bool bit(void) const { return _w & (1 << _i); }
	inline void set(void) { _w |= 1 << _i; }
	inline void clear(void) { _w &= ~(1 << _i); }
	inline operator bool(void) const { return bit(); }
	inline bool operator==(int v) const { return bit() == v; }
	inline bool operator==(bool v) const { return bit() == v; }
	inline bool operator!=(int v) const { return bit() != v; }
	inline bool operator!=(bool v) const { return bit() != v; }
	inline Bit& operator=(int v) { if(v) set(); else clear(); return *this; }
	inline Bit& operator=(bool v) { if(v) set(); else clear(); return *this; }
private:
	T& _w;
};

template <class T, int _n>
class BitInArray {
public:
	inline BitInArray(T *w): _w(w) { }
	inline bool bit(int idx) const { return _w[idx] & (1 << _n); }
	inline void set(int idx) { _w[idx] |= 1 << _n; }
	inline void clear(int idx) { _w[idx] &= ~(1 << _n); }
	
	class Delegate {
	public:
		inline Delegate(BitInArray& b, int i): _b(b), _i(i) { }
		inline operator bool(void) const { return _b.bit(_i); }
		inline BitInArray& operator=(bool v)
			{ if(v) _b.set(_i); else _b.clear(_i); return _b; }
		inline BitInArray& operator=(int v)
			{ if(v) _b.set(_i); else _b.clear(_i); return _b; }				
	private:
		BitInArray& _b;
		int _i;
	};

	inline bool operator[](int idx) const { return bit(idx); }
	inline Delegate operator[](int idx) { return Delegate(*this, idx); }

private:
	T *_w;
};

template <class T, int _u, int _l>
class BitField {
public:
	inline BitField(T& w): _w(w) { }
	inline T get(void) const { return (_w >> _l) & mask(); }
	inline void set(T v) const { _w |= v << _l; _w &= ~(v << _l); }
	inline operator T(void) const { return get(); }
	inline BitField& operator=(T v) { set(v); return *this; }
private:
	inline T mask(void) const { return (1 << (_u + 1 - _l)) - 1; }
	T& _w;
};

template <class T, int _u, int _l>
class BitFieldInArray {
public:
	inline BitFieldInArray(T *w): _w(w) { }
	inline T get(int idx) const { return (_w[idx] >> _l) & mask(); }
	inline void set(int idx, T v) const { _w[idx] |= v << _l; _w[idx] &= ~(v << _l); }

	class Delegate {
	public:
		inline Delegate(BitFieldInArray& b, int i): _b(b), _i(i) { }
		inline operator T(void) const { return _b.get(_i); }
		inline BitFieldInArray& operator=(T v)
			{ _b.set(_i, v); return _b; }
	private:
		BitFieldInArray& _b;
		int _i;
	};

	inline T operator[](int idx) const { return get(idx); }
	inline Delegate operator[](int idx) { return Delegate(*this, idx); }

private:
	inline uint32_t mask(void) const { return (1 << (_u + 1 - _l)) - 1; }
	T *_w;
};

template <class T>
class BitVector {
public:
	inline BitVector(void): _w(0) { }
	inline BitVector(T w): _w(w) { }
	inline bool bit(int i) const { return (_w >> i) & 0b1; }
	inline void set(int i) { _w |= 1 << i; }
	inline void clear(int i) { _w &= ~(1 << i); }
	
	inline operator T(void) const { return _w; }
	inline BitVector& operator=(T w) { _w = w; return *this; }
	inline BitVector& operator&=(T w) { _w &= w; return *this; }
	inline BitVector& operator|=(T w) { _w |= w; return *this; }
	
	class Delegate {
	public:
		inline Delegate(BitVector& b, int i): _b(b), _i(i) { }
		inline operator bool(void) const { return _b.bit(_i); }
		inline BitVector& operator=(bool v) { if(v) _b.set(_i); else _b.clear(_i); return _b; }
	private:
		BitVector& _b;
		int _i;
	};
	
	inline bool operator[](int i) const { return bit(i); }
	inline Delegate operator[](int i) { return Delegate(*this, i); }
	
private:
	T _w;
};

} // bsim

#endif	// BSIM_BITS_HPP
