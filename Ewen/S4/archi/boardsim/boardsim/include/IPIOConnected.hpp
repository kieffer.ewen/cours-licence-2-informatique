#ifndef SIM__IPIOCONNECT
#define SIM__IPIOCONNECT

#include "IComponent.hpp"

namespace bsim {

/**
 * \brief Interface for plugin which want to connect to the PIO
 *
 * All plugins which need access to a PIO pin should subclass this interface
 *
 *
 */
class IPIOConnected : public IComponent {
  public:
    virtual bool ODSR() = 0 ;
    virtual void SODR() = 0 ;
    virtual void CODR() = 0 ;
    virtual bool PDSR() = 0 ;
  private:

};

}	// bsim
#endif
