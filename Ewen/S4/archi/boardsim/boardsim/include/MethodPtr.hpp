/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM__METHODPTR_HPP
#define BSIM__METHODPTR_HPP

#include "common.hpp"

namespace bsim {

// Method Ptr class
class MethodPtr {
public:
	virtual ~MethodPtr(void) {}
	virtual void exec(addr_t addr, int size, void* data) = 0;
};


// MethodPtrImpl class
template<typename C, typename M>
class MethodPtrImpl : public MethodPtr {
public:
	MethodPtrImpl (C& o, M m): obj(o), meth(m) { }
	void exec (addr_t addr, int size, void* data) { (obj.*meth)(addr, size, data); }
private:
	C& obj;
	M meth;
};

}	// bsim

#endif	// BSIM__METHODPTR_HPP
