/****************************************************************************
** Meta object code from reading C++ file 'RegisterDockWidget.hpp'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "RegisterDockWidget.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'RegisterDockWidget.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_bsim__RegisterDockWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      26,   25,   25,   25, 0x0a,
      39,   36,   25,   25, 0x0a,
      67,   25,   25,   25, 0x0a,
      76,   25,   25,   25, 0x0a,
      85,   25,   25,   25, 0x0a,
      94,   25,   25,   25, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_bsim__RegisterDockWidget[] = {
    "bsim::RegisterDockWidget\0\0timeout()\0"
    "pt\0startContextualMenu(QPoint)\0setDef()\0"
    "setDec()\0setHex()\0setBin()\0"
};

void bsim::RegisterDockWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        RegisterDockWidget *_t = static_cast<RegisterDockWidget *>(_o);
        switch (_id) {
        case 0: _t->timeout(); break;
        case 1: _t->startContextualMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 2: _t->setDef(); break;
        case 3: _t->setDec(); break;
        case 4: _t->setHex(); break;
        case 5: _t->setBin(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData bsim::RegisterDockWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject bsim::RegisterDockWidget::staticMetaObject = {
    { &QDockWidget::staticMetaObject, qt_meta_stringdata_bsim__RegisterDockWidget,
      qt_meta_data_bsim__RegisterDockWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &bsim::RegisterDockWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *bsim::RegisterDockWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *bsim::RegisterDockWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_bsim__RegisterDockWidget))
        return static_cast<void*>(const_cast< RegisterDockWidget*>(this));
    if (!strcmp(_clname, "IView"))
        return static_cast< IView*>(const_cast< RegisterDockWidget*>(this));
    return QDockWidget::qt_metacast(_clname);
}

int bsim::RegisterDockWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDockWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
