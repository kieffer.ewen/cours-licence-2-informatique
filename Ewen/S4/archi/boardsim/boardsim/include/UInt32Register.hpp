/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_UINT32REGISTER_HPP
#define BSIM_UINT32REGISTER_HPP

#include "IRegister.hpp"

namespace bsim {

class Board;

namespace uint32 {
	inline static bool bit(uint32_t v, int i)
		{ return v & (1 << i); }
	inline static void set(uint32_t& v, int i)
		{ v |= (1 << i); }
	inline static void clear(uint32_t& v, int i)
		{ v &= ~(1 << i); }
	inline static uint32_t mask(int n)
		{ return (1 << n) - 1; }
	inline static uint32_t get(uint32_t v, int up, int lo)
		{ return (v >> lo) & mask(up - lo + 1); }
	inline static void set(uint32_t& v, int up, int lo, uint32_t w)
		{ v |= w << lo; v &= ~(mask(up - lo + 1) << lo) | (w << lo); } 

	class BitRef {
	public:
		inline BitRef(uint32_t& v, int i): _v(v), _i(i) { }
		inline operator bool(void) const { return uint32::bit(_v, _i); }
		inline BitRef& operator=(bool b) { if(b) uint32::set(_v, _i); else uint32::clear(_v, _i); return *this; } 
	private:
		uint32_t& _v;
		int _i;
	};

	class FieldRef {
	public:
		inline FieldRef(uint32_t& v, int up, int lo): _v(v), _up(up), _lo(lo) { }
		inline operator uint32_t(void) const { return uint32::get(_v, _up, _lo); }
		inline FieldRef& operator=(uint32_t w) { uint32::set(_v, _up, _lo, w); return *this; } 
	private:
		uint32_t& _v;
		int _up, _lo;
	};
	
	class ArrayRef {
	public:
		inline ArrayRef(uint32_t& v, int i): _v(v), _i(i) { }
		inline ArrayRef& operator=(bool w)
				{ if(w) uint32::set(_v, _i); else uint32::clear(_v, _i); return *this; }
		inline operator bool(void) const { return uint32::bit(_v, _i); }
	private:
		uint32_t& _v;
		int _i;
	};

}

class UInt32: public IHardRegister {
public:

	template <class T>
	class Getter: public AbstractAccessor {
	public:
		inline Getter(T& o, uint32_t (T::*f)(void)): _o(o), _f(f) { }
		virtual void access(addr_t a, int s, void *d)
			{ if(s == 4) *static_cast<uint32_t *>(d) = (_o.*_f)(); }
	private:
		T& _o;
		uint32_t (T::*_f)(void);
	};

	template <class T>
	class Setter: public AbstractAccessor {
	public:
		inline Setter(T& o, void (T::*f)(uint32_t v)): _o(o), _f(f) { }
		virtual void access(addr_t a, int s, void *d)
			{ if(s == 4)  (_o.*_f)(*static_cast<uint32_t *>(d)); }
	private:
		T& _o;
		void (T::*_f)(uint32_t v);
	};

	class Make: public IHardRegister::Make {
		friend class UInt32;
		friend class UInt32Register;
	public:
		inline Make(void): _get(&IRegister::null_accessor), _set(&IRegister::null_accessor) { size(32); count(1); }
		inline Make& name(QString name) { IHardRegister::Make::name(name); return *this; }
		inline Make& format(QString format) { IHardRegister::Make::format(format); return *this; }
		inline Make& type(int type) { IHardRegister::Make::type(type); return *this; }
		inline Make& access(int access) { IHardRegister::Make::access(access); return *this; }
		inline Make& offset(uint32_t offset) { IHardRegister::Make::offset(offset); return *this; }
		inline Make& read(void) { IHardRegister::Make::read(); return *this; }
		inline Make& write(void) { IHardRegister::Make::write(); return *this; }
		template <class T> inline Make& get(T *o, uint32_t (T::*f)(void)) { _get = new Getter<T>(*o, f); return *this; }
		template <class T> inline Make& set(T *o, void (T::*f)(uint32_t v)) { _set = new Setter<T>(*o, f); return *this; }
	private:
		AbstractAccessor *_get, *_set;
	};
	static inline Make make(void) { return Make(); }
	
	UInt32(IComponent *comp, const Make& make);
	virtual ~UInt32(void);
	virtual void read(addr_t addr, void *data, int size);
	virtual void write(addr_t addr, void *data, int size);

protected:
	AbstractAccessor *_get, *_set;

};

class UInt32ControlRegister: public UInt32 {
public:
	UInt32ControlRegister(IComponent *comp, const Make& make);
	virtual QVariant get(int i = 0);
	virtual void set(QVariant value, int i = 0);
	
	class Bit: public Field {
	public:
		Bit(UInt32ControlRegister& reg, QString name, int i);
		inline bool bit(uint32_t v) { return uint32::bit(v, low()); }
		inline bool operator()(uint32_t v) { return bit(v); }
	};
};

class UInt32Register: public UInt32 {
public:

	UInt32Register(IComponent *comp, const Make& make);
	virtual QVariant get(int i = 0);
	virtual void set(QVariant value, int i = 0);

	inline operator uint32_t(void) const { return _v; }
	inline operator uint32_t&(void) { return _v; }
	inline uint32_t operator*(void) const { return _v; }
	inline uint32_t& operator*(void) { return _v; }
	
	inline UInt32Register& operator=(int v) { _v = v; return *this; }
	inline UInt32Register& operator=(uint32_t v) { _v = v; return *this; }
	
	class Bit: public Field {
	public:
		inline Bit(UInt32Register& r, QString name, int bit):
			Field(r, name, bit), _r(r) { }
		inline bool operator*(void) const { return uint32::bit(_r._v, bit()); }
		inline operator bool(void) const { return uint32::bit(_r._v, bit()); }
		inline Bit& operator=(bool b)
			{ if(b) uint32::set(_r._v, bit()); else uint32::clear(_r._v, bit()); return *this; } 
	private:
		UInt32Register& _r;
	};

	class BitField: public Field {
	public:
		inline BitField(UInt32Register& r, QString name, int up, int lo, QVector<EnumValue> *vals = 0):
			Field(r, name, up, lo, vals, vals ? IRegister::ENUM : IRegister::VAL), _r(r) { }
		inline uint32_t operator*(void) const { return uint32::get(_r._v, up(), low()); }
		inline operator uint32_t(void) const { return uint32::get(_r._v, up(), low()); }
		inline BitField& operator=(uint32_t v) { uint32::set(_r._v, up(), low(), v); return *this; }
	private:
		UInt32Register& _r;
	};

	class BitEnum: public Field {
	public:
		inline  BitEnum(UInt32Register& r, QString name, int up, int lo, QVector<EnumValue> *vals):
			Field(r, name, up, lo, 0, IRegister::ENUM), _r(r) { }
		inline uint32_t operator*(void) const { return uint32::get(_r._v, up(), low()); }
		inline operator uint32_t(void) const { return uint32::get(_r._v, up(), low()); }
		inline BitEnum& operator=(uint32_t v) { uint32::set(_r._v, up(), low(), v); return *this; }
	private:
		UInt32Register& _r;
	};

	class BitArray: public Field {
	public:
		inline BitArray(UInt32Register& r, QString name, int up, int lo):
			Field(r, name, up, lo, 0, IRegister::ARRAY), _r(r) { }
		inline bool operator[](int i) const { return uint32::bit(_r._v, low() + i); }
		inline uint32::ArrayRef operator[](int i) { return uint32::ArrayRef(_r._v, low() + i); }
	private:
		UInt32Register& _r;
	};

private:
	uint32_t get_reg(void);
	void set_reg(uint32_t v);
	uint32_t _v;
};

class UInt32Array: public IHardRegister {
public:

	template <class T>
	class Getter: public AbstractAccessor {
	public:
		inline Getter(UInt32Array& a, T& o, uint32_t (T::*f)(int)): _a(a), _o(o), _f(f) { }
		virtual void access(addr_t a, int s, void *d)
			{ if(s == 4) *static_cast<uint32_t *>(d) = (_o.*_f)((a - _a.address()) / _a.stride()); }
	private:
		UInt32Array& _a;
		T& _o;
		uint32_t (T::*_f)(int i);
	};

	template <class T>
	class Setter: public AbstractAccessor {
	public:
		inline Setter(UInt32Array& a, T& o, void (T::*f)(int, uint32_t v)): _a(a), _o(o), _f(f) { }
		virtual void access(addr_t a, int s, void *d)
			{ if(s == 4)  (_o.*_f)((a - _a.address()) / _a.stride(), *static_cast<uint32_t *>(d)); }
	private:
		UInt32Array& _a;
		T& _o;
		void (T::*_f)(int i, uint32_t v);
	};

	class Make: public IHardRegister::Make {
		friend class UInt32Array;
	public:
		inline Make(int cnt): _get(&IRegister::null_accessor), _set(&IRegister::null_accessor) { size(32); count(cnt); }
		inline Make& name(QString name) { IHardRegister::Make::name(name); return *this; }
		inline Make& format(QString format) { IHardRegister::Make::format(format); return *this; }
		inline Make& type(int type) { IHardRegister::Make::type(type); return *this; }
		inline Make& access(int access) { IHardRegister::Make::access(access); return *this; }
		inline Make& offset(uint32_t offset) { IHardRegister::Make::offset(offset); return *this; }
		inline Make& read(void) { IHardRegister::Make::read(); return *this; }
		inline Make& write(void) { IHardRegister::Make::write(); return *this; }
		inline Make& stride(uint32_t stride) { IHardRegister::Make::stride(stride); return *this; }
		template <class T> inline Make& get(UInt32Array& a, T *o, uint32_t (T::*f)(int)) { _get = new Getter<T>(a, *o, f); return *this; }
		template <class T> inline Make& set(UInt32Array& a, T *o, void (T::*f)(int, uint32_t)) { _set = new Setter<T>(a, *o, f); return *this; }
	private:
		AbstractAccessor *_get, *_set;
	};
	static inline Make make(int count) { return Make(count); }

	UInt32Array(IComponent *comp, const Make& make);
	~UInt32Array(void);
	virtual QVariant get(int i = 0);
	virtual void set(QVariant value, int i = 0);
	virtual void read(addr_t addr, void *data, int size);
	virtual void write(addr_t addr, void *data, int size);
	
	inline uint32_t at(int i) const { return _v[i]; }
	inline uint32_t& at(int i) { return _v[i]; }
	inline uint32_t operator[](int i) const { return at(i); }
	inline uint32_t& operator[](int i) { return at(i); }
	inline int size(void) const { return _size; }

	class Bit: public Field {
	public:
		inline Bit(UInt32Array& r, QString name, int bit): Field(r, name, bit), _r(r) { }
		inline bool operator[](int i) const { return uint32::bit(_r[i], low()); }
		inline uint32::BitRef operator[](int i) { return uint32::BitRef(_r[i], low()); }
	private:
		UInt32Array& _r;
	};

	class BitField: public Field {
	public:
		inline BitField(UInt32Array& r, QString name, int up, int lo):
			Field(r, name, up, lo, 0, IRegister::VAL), _r(r) { }
		inline uint32_t operator[](int i) const { return uint32::get(_r._v[i], up(), low()); }
		inline uint32::FieldRef operator[](int i) { return uint32::FieldRef(_r[i], up(), low()); }
	private:
		UInt32Array& _r;
	};

	class BitEnum: public Field {
	public:
		inline BitEnum(UInt32Array& r, QString name, int up, int lo, QVector<EnumValue> *vals):
			Field(r, name, up, lo, vals, IRegister::ENUM), _r(r) { }
		inline uint32_t operator[](int i) const { return uint32::get(_r._v[i], up(), low()); }
		inline uint32::FieldRef operator[](int i) { return uint32::FieldRef(_r._v[i], up(), low()); }
	private:
		UInt32Array& _r;
	};

	class BitArray: public Field {
	public:
		inline BitArray(UInt32Array& r, QString name, int up, int lo):
			Field(r, name, up, lo, 0, IRegister::ARRAY), _r(r) { }
		inline bool operator[](int i) const { return uint32::bit(_r._v[i], low() + i); }
		inline uint32::ArrayRef operator[](int i) { return uint32::ArrayRef(_r._v[i], low() + i); }
	private:
		UInt32Array& _r;
	};

private:
	uint32_t get_reg(int i);
	void set_reg(int i, uint32_t v);
	uint32_t *_v;
	AbstractAccessor *_get, *_set;	
};

}	// bsim

#endif	// BSIM_UINT32REGISTER_HPP
