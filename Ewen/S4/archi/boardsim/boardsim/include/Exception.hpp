#ifndef BOARSIM_EXCEPTION_H
#define BOARSIM_EXCEPTION_H

#include <QString>

namespace bsim {
	
// Exception class
class Exception {
public:
	inline Exception(void) { }
	inline Exception(QString message): msg(message) { }
	inline QString message(void) const { return msg; }
private:
	QString msg;
};	

};	// bsim

#endif	// BOARSIM_EXCEPTION_H
