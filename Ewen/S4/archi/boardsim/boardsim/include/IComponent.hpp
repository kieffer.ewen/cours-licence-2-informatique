/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef SIM__ICOMPONENT
#define SIM__ICOMPONENT

#include <QWidget>
#include <QVector>
#include <exception>
#include <QtXml>
#include <QGraphicsScene>

#include "common.hpp"
#include "Exception.hpp"
#include "IRegister.hpp"
#include "Pin.hpp"

namespace bsim {

class Board;
class Core;
class IComponent;
class MainWindow;
class VisualGraph;


// ComponentException class
class ComponentException: public Exception {
public:
	ComponentException(QString name, QString type, QString message = "");
};


// Monitor interface
class IMonitor {
public:
	virtual ~IMonitor() { }
	virtual void warn(QString message) = 0;
	virtual void error(QString message) = 0;
	virtual void info(QString message) = 0;
	static inline IMonitor& def() { return *mon; }
	static inline void setDef(IMonitor& m) { mon = &m; }
private:
	static IMonitor *mon;
};


// Constant class
class Constant {
public:

	typedef enum {
		NONE	= 0,
		UINT	= 1,
		MASK	= 2,
		FIELD 	= 3,
		ADDR	= 4
	} type_t;

	inline Constant(void): _pref(false), _type(NONE), _val(0), _shift(0) { }
	inline Constant(QString name, uint32_t value, bool pref = true)
		: _name(name), _val(value), _pref(pref), _type(UINT), _shift(0) { }
	inline Constant(type_t type, QString name, uint32_t value, bool pref = true)
		: _name(name), _val(value), _pref(pref), _type(type), _shift(0) { }
	inline Constant(type_t type, QString name, uint32_t value, uint16_t shift, bool pref = true)
		: _name(name), _val(value), _shift(shift), _pref(pref), _type(type) { }

	static Constant val(QString name, uint32_t value, bool pref = true)
		{ return Constant(UINT, name, value, pref); }
	static Constant mask(QString name, int shift, bool pref = true)
		{ return Constant(MASK, name, 1, shift, pref); }
	static Constant field(QString name, uint16_t value, int shift, bool pref = true)
		{ return Constant(FIELD, name, value, shift, pref); }

	inline QString name(void) const { return _name; }
	inline uint32_t value(void) const { return _val; }
	inline bool isPrefixed(void) const { return _pref; }
	inline type_t type(void) const { return _type; }
	inline uint8_t shift(void) const { return _shift; }

private:

	QString _name;
	uint32_t _val;
	uint8_t _shift;
	bool _pref;
	type_t _type;
};

class Configuration {
public:
	Configuration(IComponent *comp, QDomElement& elt, IMonitor& monitor = IMonitor::def());
	
	bool isDefined(QString name);
	bool get(QString name, bool def);
	QString get(QString name, QString def);
	int get(QString name, int def);
	double get(QString name, double def);
	QColor get(QString name, QColor def);
	
private:
	IComponent *_comp;
	QDomElement& _elt;
	IMonitor& _mon;
};


// IComponent interface
class IComponent {
	friend class Board;
	friend class IHardRegister;
public:

	typedef enum {
		no_lang = 0,
		assembly = 1,
		C = 2
	} lang_t;

	IComponent(Board& board, QDomElement configuration, IMonitor& monitor = IMonitor::def());
	
	inline const QVector<Pin *>& pins(void) const { return _pins; }
	Pin *getPin(QString name);
	inline QString name(void) const { return _name; }
	inline QString type(void) const { return _type; }
	inline Board& board(void) const { return _board; }
	inline const QVector<IRegister *>& getRegisters(void) const { return regs; }
	inline uint32_t address(void) const { return _address; }
	virtual QString namePrefix(void);
	virtual QString typePrefix(void);
	
	// simulation
	virtual void start(void);
	virtual void stop(void);
	
	// display
	inline QDomElement configuration(void) const { return conf; }
	inline bool displayed(void) const { return disp; }

	// to override
	virtual ~IComponent(void);
	virtual void getStaticConstants(QVector<Constant>& csts) const;
	virtual void getDynamicConstants(QVector<Constant>& csts) const;
	virtual Core *toCore(void);
	virtual void makeVisual(VisualGraph& graph);
	virtual void genHeader(lang_t lang, QTextStream& out);
	virtual void configure(Configuration& conf, IMonitor& monitor = IMonitor::def());

	// deprecated
	virtual void install(QGraphicsScene& scene);
	virtual void uninstall(QGraphicsScene& scene);
	virtual void installUI(MainWindow& window);
	virtual void uninstallUI(MainWindow& window);
	virtual void genHeader(QTextStream& out);
	
protected:
	inline void setName(QString name) { _name = name; }
	inline void setType(QString type) { _type = type; }
	inline void setAddress(uint32_t address) { _address = address; }
	void configure(QDomElement dom);
	virtual void configureLink(QDomElement element);
	virtual void configureOther(QDomElement element);
	void add(Pin *pin);
	void add(IRegister *reg);
	void add(IHardRegister *r);
	time_t time(void);

	// services
	IMonitor& mon;
	void warn(QString message);
	void error(QString message);
	void info(QString message);
	QTextStream& log(void);
	
	// bit manipulation
	static inline bool bit(uint32_t w, int n) { return w & (1 << n); }
	static inline void set(uint32_t& w, int n) { w |= (1 << n); }
	static inline void clear(uint32_t& w, int n) { w &= ~(1 << n); }
	static inline void setb(uint32_t& w, int n, bool v) { if(v) set(w, n); else clear(w, n); }
	
	// bit field manipulation
	static inline uint32_t mask(int s) { return (1 << s) - 1; }
	static inline uint32_t mask(int n, int s) { return mask(s) << n; }
	static inline uint32_t bitf(uint32_t w, int n, int s) { return (w >> n) & mask(s); }               //////
	static inline void setf(uint32_t& w, int n, int s, uint32_t v) { w = (w & ~mask(n, s)) | ((v & mask(s)) << n); }      
	
private:
	QDomElement conf;
	bool disp;
	QVector<Pin *> _pins;
	QVector<IRegister *> regs;
	QString _name, _type;
	Board& _board;
	uint32_t _address;
};

}	// bsim

#endif
