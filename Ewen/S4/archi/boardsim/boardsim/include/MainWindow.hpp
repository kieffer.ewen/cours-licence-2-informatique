/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM__MAINWINDOW_HPP
#define BSIM__MAINWINDOW_HPP

#include <QAction>
#include <QActionGroup>
#include <QMainWindow>
#include <QMenu>
#include <QToolBar>
#include <QMenuBar>
#include <QApplication>
#include <QString>
#include <QComboBox>
#include <QFileSystemWatcher>
#include <QLinkedList>

#include "Board.hpp"
#include "BreakPoint.hpp"
#include "common.hpp"
#include "RegisterDockWidget.hpp"
#include "Scheduler.hpp"

namespace bsim {

class CodeViewer;
class DisasmSource;
class ISource;
class Program;

class LogMonitor: public QObject, public IMonitor {
	Q_OBJECT
public:
	LogMonitor(int max = 1024);
	void reset();
	inline int max() const { return _max; }
	inline const QLinkedList<QString>& log() const { return _log; }
	void warn(QString message) override;
	void error(QString message) override;
	void info(QString message) override;
signals:
	void cleanFirst();
	void newMessage(QString message);
private:
	void append(QString m);
	QLinkedList<QString> _log;
	int _size, _max;
};


// MainWindow class
class MainWindow : public QMainWindow {
	Q_OBJECT
   
public :
	static const QString PROG_NAME ;
	static const QString PROG_VERSION ;

	MainWindow(QString , QString) ; // ELF + XML
	MainWindow(void);
	~MainWindow(void);
	QMenu* getViewMenu(void) ;
	inline CodeViewer& getCodeViewer(void) { return *code_viewer; }
	inline Board& board(void) { return _board; }
	
	BreakPointManager& getBreakPointManager(void) { return _bpts; }
	inline Program *program(void) const { return prog; }
	inline bool isRunning(void) const;
	void loadBoard(QString path);
	void loadExec(QString path);
	void updateExec();
	inline bool isBoardReady() const { return board_ready; }

	LogMonitor log;

public slots:
	void updateStatusbar(QString message) ;

private slots:
	void arm_step(void);
	void arm_next(void);
	void arm_over(void);
	void arm_stop(void);
	void arm_start(void);
	void arm_run(void);
	void arm_break(void);
	void arm_done(void);
	void openELF(void);
	void openXML(void);
	void about(void);
	bool onClose(void);
	void changeSource(int index);
	void execDirChanged(const QString& path);
   
private :
	void showInSource(uint32_t address);
	void init(void);
	void error(QString message);
	void updateActions(void);
	void saveSettings();
	void loadSettings();
	
	// MyBreakHandler class
	class MyBreakHandler: public Scheduler::Handler {
	public:
		MyBreakHandler(MainWindow& window);
		virtual void onDone(Core *core);
		virtual bool stops(Core *core);
	private:
		MainWindow& win;
	} break_handler;
	
	// NextHandler class
	class NextHandler: public Scheduler::Handler {
	public:
		NextHandler(MainWindow& window);
		inline void set(QString file, int line) { _file = file; _line = line; }
		virtual void onDone(Core *core);
		virtual bool stops(Core *core);		
	protected:
		MainWindow& win;
		QString _file;
		int _line;
	} next_handler;

	class OverHandler: public NextHandler {
	public:
		OverHandler(MainWindow& window);
		void set(Core *core);
		virtual void onDone(Core *core);
		virtual bool stops(Core *core);
	private:
		bool not_found;
	} over_handler;

	// state
	bool board_ready;
	bool exec_ready;

	// simulation	
	Board _board;
	Core *current;
	Scheduler *driver;
	Program *prog;
	enum {
		stopped = 0,
		running,
		paused
	} run_state;
	bool starting;
	
	// break points
	BreakPointManager _bpts;
	BreakPoint *_main_bp;
	
	// configuration
	bool start_at_main;
	
	// UI
	QString settings_path;
	QString pix_path;
	DisasmSource *dis;
	ISource *cur_source;
	bool dis_selected;

	// UI Actions
	QAction *openELFAct ;
	QAction *openXMLAct ;
	QAction *quit_action;
	
	// Simu Action
	QAction* arm_startAct ;
	QAction* arm_stepAct ;
	QAction* arm_nextAct ;
	QAction* arm_overAct ;
	QAction* arm_runAct ;
	QAction* arm_breakAct ;
	QAction* arm_stopAct ;
	QAction *arm_overact ;

	// help actions
	QAction* aboutQtAct ;
	QAction* aboutAct ;

	// UI
	QToolBar *tb, *file_tb;
	QMenu *fileMenu ;
	QMenu *simulationMenu ;
	QMenu *viewMenu ;
	QMenu *help_menu;
	CodeViewer *code_viewer;
	QComboBox *source_box;

	// icons
	QIcon	*open_exec_icon,
			*open_board_icon,
			*quit_icon;
	QIcon	*step_icon,
			*start_icon,
			*stop_icon,
			*run_icon,
			*break_icon,
			*next_icon,
			*over_icon;
	QIcon	*banner;
	QIcon	*window_icon;

	// docks
	QList<IView *> views;
	
	// watcher
	QString exec_path;
	QFileSystemWatcher watcher;
	QDateTime exec_date;
};

inline bool MainWindow::isRunning(void) const { return run_state != stopped; }

}	// bsim

#endif	// BSIM__MAINWINDOW_HPP

