/****************************************************************************
** Meta object code from reading C++ file 'MainWindow.hpp'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "MainWindow.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainWindow.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_bsim__MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      26,   18,   17,   17, 0x0a,
      51,   17,   17,   17, 0x08,
      62,   17,   17,   17, 0x08,
      73,   17,   17,   17, 0x08,
      84,   17,   17,   17, 0x08,
      96,   17,   17,   17, 0x08,
     106,   17,   17,   17, 0x08,
     118,   17,   17,   17, 0x08,
     129,   17,   17,   17, 0x08,
     139,   17,   17,   17, 0x08,
     149,   17,   17,   17, 0x08,
     162,   17,  157,   17, 0x08,
     178,  172,   17,   17, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_bsim__MainWindow[] = {
    "bsim::MainWindow\0\0message\0"
    "updateStatusbar(QString)\0arm_step()\0"
    "arm_next()\0arm_stop()\0arm_start()\0"
    "arm_run()\0arm_break()\0arm_done()\0"
    "openELF()\0openXML()\0about()\0bool\0"
    "onClose()\0index\0changeSource(int)\0"
};

void bsim::MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->updateStatusbar((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->arm_step(); break;
        case 2: _t->arm_next(); break;
        case 3: _t->arm_stop(); break;
        case 4: _t->arm_start(); break;
        case 5: _t->arm_run(); break;
        case 6: _t->arm_break(); break;
        case 7: _t->arm_done(); break;
        case 8: _t->openELF(); break;
        case 9: _t->openXML(); break;
        case 10: _t->about(); break;
        case 11: { bool _r = _t->onClose();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 12: _t->changeSource((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData bsim::MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject bsim::MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_bsim__MainWindow,
      qt_meta_data_bsim__MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &bsim::MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *bsim::MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *bsim::MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_bsim__MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int bsim::MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
