/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_BREAKPOINT_HPP
#define BSIM_BREAKPOINT_HPP

#include <QSet>
#include <QSettings>
#include <QString>

#include "common.hpp"

namespace bsim {
	
class BreakPointManager;
class Program;
	
class BreakPoint {
	friend class BreakPointManager;
public:
	BreakPoint(void);
	BreakPoint(QSettings& settings);
	static BreakPoint *load(QSettings& settings);
	inline bool isEnabled(void) const { return _enabled; }
	inline bool isDisabled(void) const { return !_enabled; }
	void enable(void);
	void disable(void);
	
	virtual QString name(void) = 0;
	virtual addr_t address(void) = 0;

	virtual void saveSettings(QSettings &settings);

protected:
	virtual ~BreakPoint(void);
	BreakPointManager *_man;
	virtual QString type(void) = 0;

private:
	bool _enabled;
};

class AddressBreakPoint: public BreakPoint {
public:
	AddressBreakPoint(addr_t addr);
	AddressBreakPoint(QSettings& settings);
	virtual void saveSettings(QSettings &settings);
	virtual QString name(void);
	virtual addr_t address(void);	
protected:
	virtual QString type(void);
private:
	addr_t _addr;
};

class SourceBreakPoint: public BreakPoint {
public:
	SourceBreakPoint(QString file, int line);
	SourceBreakPoint(QSettings& settings);
	virtual void saveSettings(QSettings &settings);
	virtual QString name(void);
	virtual addr_t address(void);	
protected:
	virtual QString type(void);
private:
	QString _file;
	int _line;
};

class BreakPointListener {
public:
	virtual void onAdd(BreakPoint *bp) = 0;
	virtual void onRemove(BreakPoint *bp) = 0;
	virtual void onEnable(BreakPoint *bp) = 0;
	virtual void onDisable(BreakPoint *bp) = 0;
};

class BreakPointManager {
public:
	BreakPointManager(void);
	void setProgram(Program *prog);
	
	inline const QList<BreakPoint *> breakPoints(void) const { return _bpts; }
	inline Program *program(void) const { return _prog; }

	void loadSettings(QSettings& settings);
	void saveSettings(QSettings& settings);

	void add(BreakPoint *bp);
	void remove(BreakPoint *bp);
	void enable(BreakPoint *bp);
	void disable(BreakPoint *bp);

	bool breaksAt(addr_t addr);
	BreakPoint *breakPointAt(addr_t addr) const;
	BreakPoint *breakPointAt(QString file, int line) const;
	void add(BreakPointListener *list);
	void remove(BreakPointListener *list);

private:
	
	Program *_prog;
	bool _ready;
	QSet<addr_t> _enabled;
	QList<BreakPoint *> _bpts;
	QList<BreakPointListener *> _lists;
};
	
}	// bsim

#endif	// BSIM_BREAKPOINT_HPP
