/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <QPainter>
#include <QGraphicsRectItem>
#include <IComponent.hpp>
#include <Display.hpp>
#include <Board.hpp>
#include <Scheduler.hpp>

//#define TRACE_PROPLED
#ifdef TRACE_PROPLED
#	define TRACE(c)		c
#else
#	define TRACE(c)
#endif


namespace bsim {


/**
 * Represents a LED with a brightness proportional to the energy put
 * on its input, that is, the time of level high on its input.
 * 
 * @par Pins
 * @li @c in - the input of the PropLED (a 1 gives energy to the LED, 0 no energy)
 * 
 * @par Configuration
 * @li attribute @c negative = "yes" - make the LED to work in negative logic (0 for switch on)
 * @li attribute @c levels - number of levels in the gradient of the LED brightness
 * @li attribute @c freq - frequency (in Hz) of measures (default 10).
 * @li attribute @c color - LED color(in RGB).
 * 
 * @ingroup components
 */
class PropLED: public IComponent {
public :
	const static int w = 20, h = 30;

	/**
	 * Build a PropLED.
	 * @param board	Container board.
	 * @param xml		Configuration XML node.
	 * @param n		Name of the component.
	 */
    PropLED(Board& board, QDomElement xml, QString name)
    :	IComponent(board, xml),
		color(QColor::fromRgb(173, 255, 47)),
		border(QColor::fromRgb(196, 196, 196)),
		in(*this),
		event(board, *this),
		val(false),
		energy(0),
		period(1),
		last(0)
	{
		add(&in);
		configure(xml);
		
		// get configuration
		neg = xml.attribute("negative") == "yes";
		level_nb = 16;
		if(xml.hasAttribute("levels"))
			level_nb = xml.attribute("levels").toInt();
		if(level_nb < 0)
			level_nb = 16;
		freq = 10;
		if(xml.hasAttribute("freq"))
			freq = xml.attribute("freq").toInt();
		if(freq < 0)
			freq = 10;
		if(xml.hasAttribute("color")) {
			QColor c(xml.attribute("color"));
			if(c.isValid())
				color = c;
		}
		
		// build the gradient
		levels = new QColor[level_nb + 1];
		for(int i = 0; i < level_nb; i++) {
			levels[i] = color;
			levels[i].setAlpha(i * 256 / level_nb);
		}
		levels[level_nb] = color;
		
		// all is fine
		info("succesfull initialization");
	}

	// IComponent overload
	virtual void makeVisual(VisualGraph& graph) {
		VisualComponent *vcomp = new VisualComponent(this);
		
		// configure it
		vcomp->fill_color = color;
		vcomp->w = w;
		vcomp->h = h;
		vcomp->lab_ypos = Visual::ABOVE;
		vcomp->lab_dir = Visual::VERTICAL;
		vcomp->add(new VisualPin(*vcomp, &in, Visual::SOUTH, w / 2, false));
		vcomp->parse(configuration());
		//on = vcomp->fill_color;
		
		// set the display
		rect = new QGraphicsRectItem(vcomp->x, vcomp->y, vcomp->w, vcomp->h);
		rect->setBrush(QBrush(levels[0]));
		rect->setPen(QPen(border));
		vcomp->addToGroup(rect);
		vcomp->makeLabel(name());
		vcomp->makePins();

		// update the display
		graph.add(vcomp);
		doDisplay();
	}


	virtual void start(void) {
		val = false;
		energy = 0;
		last = 0;
		period = board().mck() / freq;
		TRACE(qDebug() << "PropLED: schedule at " << board().scheduler()->time() + period;)
		event.reschedule(board().scheduler()->time() + period);
	}

private:

	void recompute(uint64_t time, bool value) {
		if(val ^ neg)
			energy += time - last;
		if(value ^ neg)
			last = time;
		val = value;
	}

	void doChange(uint64_t time, bool value) {
		recompute(time, value);
		TRACE(qDebug() << "PropLED: changed to " << value << " at " << time << " with energy = " << energy;)
	}
	
	void doDisplay(void) {
		TRACE(qDebug() << "PropLED: ENERGY = " << energy << " at " << board().scheduler()->time();)
		rect->setBrush(levels[level_nb * energy / period]);
	}
	
	void doUpdate(void) {
		time_t now = board().scheduler()->time();
		recompute(now, val);
		doDisplay();
		energy = 0;
		//last = now;
		event.reschedule(now + period);		
	}

	class InPin: public Pin {
	public:
		InPin(PropLED& led): Pin(&led, "in"), _led(led) { }
		virtual void onChange(uint64_t time, bool value) { _led.doChange(time, value); }
	private:
		PropLED& _led;
	};
	
	class PeriodEvent: public Scheduler::Event {
	public:
		PeriodEvent(Board& _board, PropLED& led): Scheduler::Event(_board,0, "PropLED::PeriodEvent"), _led(led) { }
		virtual void run(void) { _led.doUpdate(); }
	private:
		PropLED& _led;
	};
	
	bool neg, val;
	QGraphicsRectItem *rect;
	QColor color, border;
	int level_nb;
	QColor *levels;
	time_t energy;
	time_t period;
	time_t last;
	int freq;
	InPin in;
	PeriodEvent event;
};

extern "C" IComponent* init (Board& board, QDomElement xml, QString name) {
	return new PropLED(board,xml,name) ;
}

}	// bsim

