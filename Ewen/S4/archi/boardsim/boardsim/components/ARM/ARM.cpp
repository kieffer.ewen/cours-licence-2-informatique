/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>

#include <Board.hpp>
#include <Core.hpp>
#include <Display.hpp>
#include <ISource.hpp>
#include <Scheduler.hpp>
#include <Program.hpp>

extern "C" {
#	include "arm/api.h"
#	include "arm/debug.h"
#	include "gel/gel.h"
#	include "gel/debug_line.h"
#	include "gel/gel_elf.h"
}

//#define TRACE_ARM
#ifdef TRACE_ARM
#	define TRACE(d, c)		log() << c << endl
#else
#	define TRACE(d, c)
#endif


namespace bsim {

static const int MEMORY_PAGE_SIZE = 4096;

/**
 * Specialization of Program for ARM ISA.
 */
class ARMProgram: public Program {
public:

	/**
	 */
	ARMProgram(Path path, IMonitor& mon = IMonitor::def())
	:	Program(path, mon),
		_memory(0),
		_platform(0),
		_state(0),
		_decoder(0),
		_file(0),
		_lines(0),
		_ready(false),
		_funs_init(false),
		image(nullptr)
	{
		
		// initialize the data structure
		_platform = arm_new_platform();
		arm_lock_platform(_platform);
		_memory = arm_get_memory(_platform, 0);
		_state = arm_new_state(_platform);
		_decoder = arm_new_decoder(_platform);
		arm_set_cond_state(_decoder, _state);
		
		// load the file
		_file = gel_open(path.toCString(), 0, 0 /*GEL_OPEN_NOPLUGINS*/);
		if(!_file) {
			mon.error(QString("cannot open %s: %s").arg(path.toQString()).arg(gel_strerror()));
			return;
		}
		
		// build the image
		image = gel_image_load(_file, gel_default_env(), 0);
		if(!image) {
			mon.error(QString("cannot build image for %s: %s").arg(path.toQString()).arg(gel_strerror()));
			return;
		}
	
		// install the image
		reset();
	
		// cleanup
		_ready = true;
	}
	
	/**
	 */
	~ARMProgram(void) {
		if(image != nullptr)
			gel_image_close(image);
		if(_lines)
			gel_delete_line_map(_lines);
		if(_file)
			gel_close(_file);
		if(_decoder)
			arm_delete_decoder(_decoder);
		if(_state)
			arm_delete_state(_state);
		if(_platform)
			arm_unlock_platform(_platform);
	}
	
	///
	void reset() override {
		if(image != nullptr) {
			gel_image_info_t info;
			gel_image_infos(image, &info);
			for(int i = 0; i < info.membersnum; i++) {
				gel_cursor_t cursor;
				gel_block2cursor(info.members[i], &cursor);
				arm_mem_write(_memory, gel_cursor_vaddr(cursor), gel_cursor_addr(&cursor), gel_cursor_avail(cursor));
			}
		}
	}

	/**
	 * Get the current platform.
	 * @return	Current platform.
	 */
	inline arm_platform_t *platform(void) const { return _platform; }
	
	/**
	 * Test if the program is ready to use.
	 * @return	True if ready, false else.
	 */
	inline bool isReady(void) const { return _ready; }

	/**
	 */
	virtual uint32_t size(uint32_t address) {
		//arm_inst_t *inst = arm_decode_ARM(_decoder, address);
		//return arm_get_inst_size(inst) / 8;
		// TODO until we get support for thumb markers
		return 4;
	}

	/**
	 */
	virtual QString disasm(uint32_t address) {
		char buff[100];
		arm_inst_t *inst = arm_decode_ARM(_decoder, address);
		arm_disasm(buff, inst);
		return buff;
	}
	
	/**
	 */
	virtual uint32_t start(void) const {
		assert(_file);
		return _file->entry;
	}
	
	/**
	 */
	virtual uint32_t exit(void) const {
		return addressOf("_exit");
	}
	
	/**
	 */
	virtual void getSections(QList<section_t>& sections) {
		for(int i = 1; i < _file->sectnum; i++) {
			gel_sect_t *sect = gel_getsectbyidx(_file, i);
			gel_sect_info_t info;
			gel_sect_infos(sect, &info);
			if(info.flags & SHF_EXECINSTR)
				sections.push_back(section_t(info.vaddr, info.size));
		}
	}
	
	/**
	 */
	virtual addr_t addressOf(QString name) const {
		assert(_file);
		gel_sym_iter_t iter;
		gel_sym_t *sym;
		for(sym = gel_sym_first(&iter, _file); sym; sym = gel_sym_next(&iter)) {
			gel_sym_info_t info;
			gel_sym_infos(sym, &info);
			if(name == info.name)
				return info.vaddr;
		}
		return 0;
	}

	/**
	 * Find source file and line for the given address.
	 * Set line to -1 if it fails.
	 * @param address	Address to look (file, line) information for.
	 * @param file		Output the source file name.
	 * @param line		Output the source line name.
	 */
	virtual void lineOf(uint32_t address, QString& file, int& line) {
		if(!initLines()) {
			line = -1;
			return;
		}
		else {
			const char *cfile;
			if(gel_line_from_address(_lines, address, &cfile, &line) < 0)
				line = -1;
			else
				file = cfile;
		}
	}

	/**
	 */
	virtual void get(addr_t addr, uint8_t& val) {
		val = arm_mem_read8(_memory, addr);
	}
	
	/**
	 */
	virtual void get(addr_t addr, uint16_t& val) {
		val = arm_mem_read16(_memory, addr);
	}

	/**
	 */
	virtual void get(addr_t addr, uint32_t& val) {
		val = arm_mem_read32(_memory, addr);
	}

	/**
	 */
	virtual addr_t addressOf(QString file, int line) {
		if(!initLines())
			return 0;
		else
			return gel_address_from_line(_lines, file.toLocal8Bit().data(), line);
	}

	/**
	 */
	bool isFunction(addr_t a) override {
		if(!_funs_init) {
			_funs_init = true;
			assert(_file);
			gel_sym_iter_t iter;
			gel_sym_t *sym;
			for(sym = gel_sym_first(&iter, _file); sym; sym = gel_sym_next(&iter)) {
				gel_sym_info_t info;
				gel_sym_infos(sym, &info);
				if(ELF32_ST_TYPE(info.info) == STT_FUNC)
					_funs.insert(info.vaddr);
			}
		}
		return _funs.contains(a);
	}

protected:

	virtual bool initLines(void) {
		if(!_lines)
			_lines = gel_new_line_map(_file);
		return _lines;
	}

	virtual bool fillSources(QMap<QString, FileSource *>& srcs) {
		if(!initLines())
			return false;
		gel_line_iter_t iter;
		gel_location_t loc;
		loc = gel_first_line(&iter, _lines);
		while(loc.file) {
			QMap<QString, FileSource *>::const_iterator i = srcs.find(loc.file);
			if(i == srcs.end()) {
				FileSource *source = sourceFor(loc.file);
				if(source)
					srcs.insert(loc.file, source);
			}
			loc = gel_next_line(&iter);
		}
		return true;
	}

	virtual void listen(addr_t addr) {
		uint32_t paddr = addr - (addr % MEMORY_PAGE_SIZE);
		if(!io_pages.contains(paddr)) {
			io_pages.insert(paddr);
			arm_set_range_callback(_memory, paddr, paddr, callback, static_cast<void  *>(this));
		}
	}

private:
	static void callback(addr_t addr, int size, void *data, int type, void *internal) {
		ARMProgram *prog = static_cast<ARMProgram *>(internal);
		if(type == ARM_MEM_READ)
			prog->onRead(addr, data, size);
		else
			prog->onWrite(addr, data, size);
	}
	
	arm_memory_t *_memory;
	arm_platform_t *_platform;
	arm_state_t *_state;
	arm_decoder_t *_decoder;
	gel_file_t *_file;
	gel_line_map_t *_lines;
	bool _ready;
	QSet<addr_t> io_pages;
	QSet<addr_t> _funs;
	bool _funs_init;
	gel_image_t *image;
};


/**
 * Core for ARM ISA.
 */
class ARM: public Core {

	class ITPin: public Pin {
	public:
			ITPin(ARM *arm, QString name, uint32_t mask)
				: Pin(arm, name), _arm(arm), _mask(mask) { }
			
			void onChange(uint64_t time, bool value) {
				if(!value)
					_arm->its |= _mask;
			}

	private:
		ARM *_arm;
		uint32_t _mask;
	};

	class CPSRRegister: public IRegister {
	public:
		CPSRRegister(ARM& arm): _arm(arm) { }
		virtual QString name(int i = -1) { return "CPSR"; }
		virtual int count(void) { return 1; }
		virtual int size(void) { return 32; }
		virtual int type(void) { return IRegister::BITS; }
		virtual int access(void) { return READ | WRITE; }
		virtual QVariant get(int i) { if(_arm._state) return _arm._state->Ucpsr; else return QVariant(); }
		virtual void set(QVariant value, int i) { if(_arm._state) _arm._state->Ucpsr = value.toInt(); }
		virtual IHardRegister *toHard(void) { return 0; }
		virtual ICoreRegister *toCore(void) { return 0; }
		virtual QString format(int i, int fmt) {
			
			static QString modes[32];
			static bool first = true;
			if(first) {
				first = false;
				for(int i = 0; i < 32; i++)
					modes[i] = "_INV_";
				modes[0b10000] = "_USR_";
				modes[0b10001] = "_FIQ_";
				modes[0b10010] = "_IRQ_";
				modes[0b10011] = "_SVR_";
				modes[0b10111] = "_ABT_";
				modes[0b11011] = "_UND_";
				modes[0b11111] = "_SYS_";
			}
			
			if(fmt != DEF)
				return IRegister::format(i, fmt);
			QString res = "";
			res = res + ((_arm._state->Ucpsr & (1 << 31)) ? "N" : "- "); 
			res = res + ((_arm._state->Ucpsr & (1 << 30)) ? "Z" : "- "); 
			res = res + ((_arm._state->Ucpsr & (1 << 29)) ? "C" : "- "); 
			res = res + ((_arm._state->Ucpsr & (1 << 28)) ? "V" : "- ");
			res = res + " ... ";
			res = res + ((_arm._state->Ucpsr & (1 << 7)) ? "I" : "- "); 
			res = res + ((_arm._state->Ucpsr & (1 << 6)) ? "F" : "- "); 
			res = res + ((_arm._state->Ucpsr & (1 << 5)) ? "T" : "- ");
			res = res + modes[_arm._state->Ucpsr & 0x1f];
			return res;
		}

	private:
		ARM& _arm;
	};
	
	class Register: public IRegister {
	public:
		Register(ARM& arm, register_bank_t *bank): _arm(arm), _bank(bank) { }
		
		virtual QString name(int i) { if(i < 0) return "R"; else return QString("R%1").arg(i); }
		virtual int count(void) { return 16; }
		virtual int size(void) { return 32; }
		virtual int type(void) { return INT;  }
		virtual int access(void) { return READ | WRITE; }
		virtual IHardRegister *toHard(void) { return 0; }
		virtual ICoreRegister *toCore(void) { return 0; }
		
		virtual QVariant get(int i) {
			if(_arm._state) {
				register_value_t v = arm_get_register(_arm._state, _bank->id, i);
				return v.iv;
			}
			else
				return QVariant();
		}
		
		virtual void set(QVariant value, int i) {
			if(_arm._state) {
				register_value_t v;
				v.iv = value.toInt();
				arm_set_register(_arm._state, _bank->id, i, v);
			}
		}
		
	private:
		ARM& _arm;
		register_bank_t *_bank;
	};

public:
	static const uint32_t
		irq_mask = 1 << 7,
		fiq_mask = 1 << 6,
		both_mask = irq_mask | fiq_mask;				

	ARM(Board& board, QDomElement xml, QString name)
	:	Core(board, xml),
		_state(0),
		sim(0),
		irq(-1),
		fiq(-1),
		_prog(0),
		its(0)
	{
		// add IT pins
		irq_pin = new ITPin(this, "IRQ", irq_mask);
		add(irq_pin);
		fiq_pin = new ITPin(this, "FIQ", fiq_mask);
		add(fiq_pin);

		// add registers
		register_bank_t *banks = debug_get_registers();
		for(int i = 0; banks[i].id >= 0; i++)
			if(QString("CPSR") != banks[i].name)
				IComponent::add(new Register(*this, &banks[i]));
		IComponent::add(new CPSRRegister(*this));
	
		// configure the component
		configure(xml);

		// look for irq and fiq
		exns = arm_exceptions();
		for(int i = 0; exns[i].name; i++) {
			if(QString("IRQ") == exns[i].name)
				irq = i;
			else if(QString("FIQ") == exns[i].name)
				fiq = i;
		}
	}
	
	/**
	 */
	Program *load(QString path, IMonitor& mon) {
		ARMProgram *prog = new ARMProgram(path, mon);
		if(!prog->isReady()) {
			delete prog;
			prog = 0;
		}
		_prog = prog;
		return prog;
	}
	
	void genHeader(lang_t lang, QTextStream& out) {
		switch(lang) {
		case assembly:
			break;
		case C:
			out << "/* ARM */\n"
				<< "#define	MASK_IRQ	{ asm(\"STMFD sp!, { r0 }\"); asm(\"MRS r0, CPSR\"); asm(\"ORR r0, r0, #0x80\"); asm(\"MSR CPSR, r0\"); asm(\"LDMFD sp!, { r0 }\"); }\n"
				<< "#define	UNMASK_IRQ	{ asm(\"STMFD sp!, { r0 }\"); asm(\"MRS r0, CPSR\"); asm(\"BIC r0, r0, #0x80\"); asm(\"MSR CPSR, r0\"); asm(\"LDMFD sp!, { r0 }\"); }\n"
				<< endl;
			break;
		}
	}

	virtual void makeVisual(VisualGraph& graph) {

		// configure the component
		VisualComponent *vcomp = new VisualComponent(this);
		vcomp->w = 100;
		vcomp->h = 100;
		vcomp->add(new VisualPin(*vcomp, irq_pin, Visual::EAST, 30));
		vcomp->add(new VisualPin(*vcomp, fiq_pin, Visual::EAST, 70));
		vcomp->parse(configuration());

		// prepare UI
		vcomp->make(name());
		vcomp->makePins();
		graph.add(vcomp);
	}

	virtual void start(void) {
	
		// cleanup
		stop();
		
		// create simulation state
		getSim();
		its = 0;
	}
	
	virtual void stop(void) {
		if(sim) {
			arm_delete_sim(sim);
			sim = 0;
			_state = 0;
		}
		else if(_state) {
			arm_delete_state(_state);
			_state = 0;
		}
	}

	virtual void step(void) {
		if(arm_is_sim_ended(sim))
			return;
		uint32_t dits = its & ~_state->Ucpsr;
		if(dits) {
			if(dits & fiq_mask) {
				TRACE(board().scheduler()->time(),  "raising FIQ from " << mkAddr(pc()));
				exns[fiq].fun(_state);
				its &= ~fiq_mask;
			}
			else if(dits & irq_mask) {
				TRACE(board().scheduler()->time(), "raising IRQ from " << mkAddr(pc()));
				exns[irq].fun(_state);
				its &= ~irq_mask;
			}
		}
		arm_inst_t *i = arm_next_inst(sim);
		TRACE(board().scheduler()->time(), "stepping on " << mkAddr(pc()) << " its=" << its << ", " << i->ident);
		if(i->ident == ARM_UNKNOWN) {
			error("unkown instruction executed at ");
			arm_free_inst(i);
			arm_set_sim_ended(sim);
			return;
		}
		arm_free_inst(i);
		arm_step(sim);
		TRACE(board().scheduler()->time(), "next inst at " << mkAddr(pc()));
	}

	virtual uint32_t pc(void) {
		return arm_next_addr(getSim());
	}

	virtual uint32_t size(uint32_t address) {
		arm_inst_t *inst = arm_decode(getSim()->decoder, address);
		return arm_get_inst_size(inst) / 8;
	}
	
	virtual QString disasm(uint32_t address) {
		char buff[100];
		arm_inst_t *inst = arm_decode(getSim()->decoder, address);
		arm_disasm(buff, inst);
		//uint32_t ucode = arm_mem_read32(arm_get_memory(_platform, 0), adr_start);
		return buff;
	}

private:

	arm_sim_t *getSim(void) {
		if(!sim && _prog) {
			_state = arm_new_state(_prog->platform()) ;
			sim = arm_new_sim(_state, board().program()->start(), board().program()->exit());
		}
		return sim;
	}

	uint32_t its;
	arm_state_t *_state;
	arm_sim_t *sim;
	int irq, fiq;
	Pin *irq_pin, *fiq_pin;
	arm_exception_t *exns;
	ARMProgram *_prog;
};

/**
 * Called to build a new ARM.
 */
extern "C" IComponent* init(Board& board, QDomElement xml, QString name) {
	return new ARM(board, xml, name);
}

}	// bsim
