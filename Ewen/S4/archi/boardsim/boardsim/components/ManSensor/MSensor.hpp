/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_COMPONENT_MANSENSOR
#define BSIM_COMPONENT_MANSENSOR

#include "Board.hpp"

#include <QDomElement>
#include <QObject>
#include <QGraphicsRectItem>

namespace bsim {

class ManSensor: public QObject, public IComponent {
	Q_OBJECT
public :

    ManSensor(Board&, QDomElement, QString);
    virtual ~ManSensor(void);
	void send(double value);

	// IComponent overload
	virtual void makeVisual(VisualGraph& graph);

private slots:
	void valueChanged(int value);

private:
	const static int grad_pad = 4;
	
	QString unit;
	double min_unit, max_unit;
	double min_volt, max_volt;
	int precision;

	class OutPin: public Pin {
	public:
		OutPin(ManSensor& sensor);
	private:
		ManSensor& _sensor;
	};
	
	OutPin out;
};

}	// bsim

#endif	//  COMPONENT_MANSENSOR

