/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <QPainter>
#include <QGraphicsRectItem>
#include <QSlider>
#include <QGraphicsProxyWidget>
#include <MainWindow.hpp>
#include <Display.hpp>
#include "MSensor.hpp"

//#define DEBUG_MANSENSOR
#ifdef DEBUG_MANSENSOR
#	define DEBUG(x)	log() << x << endl
#else
#	define DEBUG(x)
#endif

namespace bsim {


ManSensor::OutPin::OutPin(ManSensor& sensor): Pin(&sensor, "out"), _sensor(sensor) {
}


/**
 * @class ManSensor
 * This component represents a manual sensor, that is, a sensor whose value may be changed
 * by hand from the user interface. It configured by setting the unit to convert,
 * maximum and minimum values, and the output matching voltage taken linearly from
 * a minimum and maximum voltage.
 * 
 * @par Pins
 * @li @c out - the output of the sensor
 * 
 * @par Configuration
 * @li attribute @c unit - the name of the measure unit,
 * @li attribute @c min_unit - minimum value for the measure unit (default to 0),
 * @li attribute @c max_unit - maximum value for the measure unit (default to 100),
 * @li attribute @c min_volt - minimum value for the voltage (default to 0),
 * @li attribute @c max_volt - maximum value for the voltage (default to 5),
 * @li attribute @c precision - precision in bits of ADC connected to the sensor (default to 10).
 * 
 * @ingroup components
 */

/**
 * Build a manual sensor.
 * @param board		Container board.
 * @param xml			Configuration XML node.
 * @param n			Name of the component.
 */
ManSensor::ManSensor(Board& board, QDomElement xml, QString n)
:	IComponent(board, xml),
	min_unit(0),
	max_unit(100),
	min_volt(0),
	max_volt(5),
	precision(10),
	out(*this)
{
	add(&out);
	configure(xml);
	if(xml.hasAttribute("unit"))
		unit = xml.attribute("unit");
	if(xml.hasAttribute("min_unit"))
		min_unit = xml.attribute("min_unit").toDouble();
	if(xml.hasAttribute("max_unit"))
		max_unit = xml.attribute("max_unit").toDouble();
	if(xml.hasAttribute("min_volt"))
		min_volt = xml.attribute("min_volt").toDouble();
	if(xml.hasAttribute("max_volt"))
		max_volt = xml.attribute("max_volt").toDouble();
	if(xml.hasAttribute("precision"))
		precision = xml.attribute("precision").toInt();
	info("ManSensor: successfull initialization");
}


/**
 */
ManSensor::~ManSensor(void) {
}


/**
 * Emit the given measure value as a voltage.
 * @param value	Measure value to convert to voltage.
 */
void ManSensor::send(double value) {
	double volt = (value - min_unit) * (max_volt - min_volt) / (max_unit - min_unit) + min_volt;
	DEBUG("emitting(" << volt << "V)");
	out.sendAnalog(board().scheduler()->time(), volt);
}


/**
 * Triggered when the slider move.
 */
void ManSensor::valueChanged(int value) {
	double volt = value * (max_volt - min_volt) / (1 << precision) + min_volt;
	DEBUG("emitting(" << volt << "V)");
	out.sendAnalog(board().scheduler()->time(), volt);
}


/**
 */
void ManSensor::makeVisual(VisualGraph& graph) {
	IComponent::makeVisual(graph);
	VisualComponent *vcomp = new VisualComponent(this);
	
	// configure it
	vcomp->lab_ypos = Visual::ABOVE;
	vcomp->lab_dir = Visual::HORIZONTAL;
	vcomp->add(new VisualPin(*vcomp, &out, Visual::SOUTH, 10, false));
	vcomp->setHandlesChildEvents(false);
	
	// make the slider
	QSlider *slider = new QSlider();
	slider->setRange(0, 1 << precision);
	slider->setSingleStep(1);
	connect(slider, SIGNAL(valueChanged(int)), this, SLOT(valueChanged(int)));
	QGraphicsProxyWidget *proxy = new QGraphicsProxyWidget();
	proxy->setWidget(slider);
	proxy->setPos(vcomp->x, vcomp->y);
	QRectF rect = proxy->rect();
	
	// add max and min label
	QGraphicsTextItem *max = new QGraphicsTextItem(QString("%1 %2").arg(max_unit).arg(unit));
	QFontMetrics fm(max->font());
	max->setPos(vcomp->x + slider->width() + grad_pad, vcomp->y - fm.ascent());
	vcomp->addToGroup(max);
	QGraphicsTextItem *min = new QGraphicsTextItem(QString("%1 %2").arg(min_unit).arg(unit));
	min->setPos(vcomp->x + slider->width() + grad_pad, vcomp->y + slider->height() - fm.ascent());
	vcomp->addToGroup(min);
	
	// configure the component
	vcomp->w = rect.width();
	vcomp->h = rect.height();
	vcomp->addToGroup(proxy);
	vcomp->makeLabel(name());
	vcomp->makePins();

	// update the display
	graph.add(vcomp);
}


extern "C" IComponent* init (Board& board, QDomElement xml, QString name) {
	return new ManSensor(board,xml,name) ;
}

}	// bsim

