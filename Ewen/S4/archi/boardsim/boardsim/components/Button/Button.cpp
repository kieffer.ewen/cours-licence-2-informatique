/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#define BSIM_BUTTON_SVG

#include <QDebug>
#include <QDomElement>
#include <QGraphicsItemGroup>
#include <QObject>
#include <QPainter>
#include <QPushButton>
#include <QWidget>
#ifdef BSIM_BUTTON_SVG
#	include <QGraphicsSvgItem>
#endif

#include "Board.hpp"
#include "Display.hpp"
#include "Scheduler.hpp"

namespace bsim {

class ButtonItem;

/**
 * @class Button
 * Implements a simple push button.
 *
 * @par Pins
 * @li @c out -- signal emitted by the button (1 when pushed, 0 when released).
 *
 * @par Configuration
 * @li attribute @c negative = "yes" - causes the button to work in negative logic (1 if released)
 *
 * @ingroup components
 */
class Button: public IComponent {
public :

	Button(Board& board, QDomElement conf, QString name)
	:	IComponent(board, conf),
		out(*this),
		button(nullptr),
		val(false),
		negative_logic(false)
#		ifdef BSIM_BUTTON_SVG
			,
			pushed_pix(nullptr),
			released_pix(nullptr)
#		endif
	{
		add(&out);
		IComponent::configure(conf);
		info("succesfull initialization");
	}

	void configure(Configuration &conf, IMonitor &monitor) override {
		negative_logic = conf.get("negative", negative_logic);
	}

	void pressed(void) {
		val =  !negative_logic;
		out.change(board().scheduler()->time(), val);
	}

	void released(void) {
		val = negative_logic;
		out.change(board().scheduler()->time(), val);
	}

	void start(void) override {
		qDebug() << "starting " << name();
		released();
	}

	void makeVisual(VisualGraph& graph) override {

		// get the images
#		ifdef BSIM_BUTTON_SVG
			pushed_pix = new QGraphicsSvgItem(board().prefix() + "/button-pushed.svg");
			released_pix = new QGraphicsSvgItem(board().prefix() + "/button-released.svg");
#		endif

		// configure the display
		VisualComponent *comp = new VisualComponent(this);
#		ifdef BSIM_BUTTON_SVG
			int h = 5;
			comp->w = released_pix->boundingRect().width();
			comp->h = released_pix->boundingRect().height();
#		else
			int h = ButtonItem::size / 2;
			comp->w = ButtonItem::size;
			comp->h = ButtonItem::size;
#		endif
		comp->fill_color = QColor("red");
		comp->lab_ypos = Visual::AFTER;
		comp->lab_dir = Visual::VERTICAL;
		comp->add(new VisualPin(*comp, &out, Visual::NORTH, h, false));
		comp->parse(configuration());

		// build the display
		comp->setHandlesChildEvents(false);
		button = new ButtonItem(comp, *this);
		comp->addToGroup(button);
		comp->makeLabel(name());
		comp->makePins();
		graph.add(comp);
	}

private:

	class OutPin: public Pin {
	public:

		OutPin(Button& button): Pin(&button, "out"), _button(button) {
		}

		virtual void onChange(uint64_t time, bool value) {
			_button.error(QString("received signal on out-only pin \"out\" in \"%1\"").arg(_button.name()));
		}

	private:
		Button& _button;
	};

#	ifdef BSIM_BUTTON_SVG
		class ButtonItem: public QGraphicsItem {
		public:
			static const int size = 20, pad = 2;

			ButtonItem(VisualComponent *vcomp, Button& b): button(b), pushed(false) {
				setAcceptedMouseButtons(Qt::LeftButton);
				setFlag(QGraphicsItem::ItemIsSelectable,true);
				setPos(vcomp->x, vcomp->y);
				//button.pushed_pix->setPos(vcomp->x, vcomp->y);
				//button.released_pix->setPos(vcomp->x, vcomp->y);
			}

			virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget * widget) {
				if(pushed)
					button.pushed_pix->paint(painter, option, widget);
				else
					button.released_pix->paint(painter, option, widget);
			}

			virtual QRectF boundingRect(void) const {
				return button.released_pix->boundingRect();
			}

		protected:
			virtual void mousePressEvent(QGraphicsSceneMouseEvent * event) {
				if(!pushed) {
					pushed = true;
					update();
					button.pressed();
				}
			}

			virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent * event) {
				if(pushed) {
					pushed = false;
					update();
					button.released();
				}
			}

		private:
			Button& button;
			bool pushed;
		};

#	else
		class ButtonItem: public QGraphicsItem {
		public:
			static const int size = 20, pad = 2;

			ButtonItem(VisualComponent *vcomp, Button& button): _button(button), pushed(false), back("white"), line("grey") {
				color = vcomp->fill_color;
				setPos(vcomp->x, vcomp->y);
				w = vcomp->w;
				h = vcomp->h;
				released_path.addEllipse(pad, pad, w - 2 * pad, h - 2 *pad);
				pushed_path.addEllipse(pad * 2, pad * 2, w - 4 * pad, h - 4 *pad);
				setAcceptedMouseButtons(Qt::LeftButton);
				setFlag(QGraphicsItem::ItemIsSelectable,true);
			}

			virtual void paint(QPainter * painter, const QStyleOptionGraphicsItem *option, QWidget * widget) {
				painter->fillRect(0, 0, w, h, QBrush(back));
				painter->setPen(QPen(line));
				painter->drawRect(0, 0, w, h);
				if(pushed) {
					painter->fillPath(released_path, QBrush(line));
					painter->fillPath(pushed_path, QBrush(color));
				}
				else
					painter->fillPath(released_path, QBrush(color));
			}

			virtual QRectF boundingRect(void) const {
				return QRectF(0, 0, w, h);
			}

		protected:
			virtual void mousePressEvent(QGraphicsSceneMouseEvent * event) {
				if(!pushed) {
					pushed = true;
					update();
					_button.pressed();
				}
			}

			virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent * event) {
				if(pushed) {
					pushed = false;
					update();
					_button.released();
				}
			}

		private:
			Button& _button;
			bool pushed;
			int w, h;
			QColor color, back, line;
			QPainterPath pushed_path, released_path;
		};
#	endif

	bool val;
	bool negative_logic;
	OutPin out;
	ButtonItem *button;
#	ifdef BSIM_BUTTON_SVG
		QGraphicsSvgItem *pushed_pix, *released_pix;
#	endif
};

extern "C" IComponent* init(Board& board,QDomElement xml, QString name) {
  return new Button(board, xml, name);
}

}	// bsim

