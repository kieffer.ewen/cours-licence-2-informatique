/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>

#include <Board.hpp>
#include <Display.hpp>
#include <IComponent.hpp>

namespace bsim { namespace electric {

/**
 * MOSFET component represent a Metal-Oxide Semicondutor Field-Effect
 * Transistor. Mainly, for the simulation, a switch controlled by a 5V
 * line (coming from a PIO or PWM) on high-power line.
 * 
 * In the board configuration, its class must be "electric/MOSFET".
 * 
 * It provides three pins:
 * @li @c GATE -- to control the switch (0 open, 1 closed).
 * @li @c SOURCE -- to connect the power source,
 * @li @c DRAIN -- to connect the device to deliver power.
 * 
 * @seealso PowerSource
 */
class MOSFET: public IComponent {
public:
	static const int pad = 10, bar = 20, pin = 20, ray = 5, space = 20; 

	MOSFET(Board& board, QDomElement xml, QString name)
	:	IComponent(board, xml),
		SOURCE(*this),
		DRAIN(this, "DRAIN"),
		GATE(*this),
		_U(0),
		_I(0),
		gate(false),
		from(0)
	{
		add(&SOURCE);
		add(&DRAIN);
		add(&GATE);
		IComponent::configure(xml);
	}

	void makeVisual(VisualGraph& graph) {

		// configure the component
		VisualComponent *vcomp = new VisualComponent(this);
		vcomp->w = pad + bar + ray + pad;
		vcomp->h = pin + space + pin;
		
		// put the pins
		vcomp->add(new VisualPin(*vcomp, &GATE, Visual::WEST, pin + space / 2));
		vcomp->add(new VisualPin(*vcomp, &SOURCE, Visual::SOUTH, pad + bar));
		vcomp->add(new VisualPin(*vcomp, &DRAIN, Visual::NORTH, pad + bar));
		vcomp->parse(configuration());
		
		// build the MOSFET
		QGraphicsLineItem *line = new QGraphicsLineItem(
			vcomp->x + pad + bar, vcomp->y,
			vcomp->x + pad + bar, vcomp->y + pin);
		vcomp->addToGroup(line);
		line = new QGraphicsLineItem(
			vcomp->x + pad + bar, vcomp->y + pin + space,
			vcomp->x + pad + bar, vcomp->y + 2 * pin + space);
		vcomp->addToGroup(line);
		line = new QGraphicsLineItem(
			vcomp->x, vcomp->y + pin + space / 2,
			vcomp->x + pin, vcomp->y + pin + space / 2);
		vcomp->addToGroup(line);
		line = new QGraphicsLineItem(
			vcomp->x + pad, vcomp->y + pin,
			vcomp->x + pad + bar, vcomp->y + pin + space);
		vcomp->addToGroup(line);
		QGraphicsEllipseItem *circle = new QGraphicsEllipseItem(
			vcomp->x + pad + bar - ray, vcomp->y + pin - ray,
			ray * 2, ray * 2);
		circle->setBrush(QColor("white"));
		vcomp->addToGroup(circle);
		circle = new QGraphicsEllipseItem(
			vcomp->x + pad + bar - ray, vcomp->y + pin + space - ray,
			ray * 2, ray * 2);
		circle->setBrush(QColor("white"));
		vcomp->addToGroup(circle);
		
		graph.add(vcomp);
	}	
private:
	int from;
	
	void setGate(time_t time, bool value) {
		//qDebug() << "DEBUG: setGate " << value;		
		if(value == gate)
			return;
		gate = value;
		if(gate)
			DRAIN.setElectric(time, _U, _I);
		else
			DRAIN.setElectric(time, 0, 0);
	}
	
	void setSource(time_t t, double U, double I) {
		qDebug() << "getting " << U << "V, " << I << "A";
		_U = U;
		_I = I;
		if(gate)
			DRAIN.setElectric(t, U, I);
	}
	
	class GatePin: public Pin {
	public:
		GatePin(MOSFET& mos): Pin(&mos, "GATE"), _mos(mos) { }
		virtual void onChange (uint64_t time, bool value) { _mos.setGate(time, value); }
	private:
		MOSFET& _mos;
	} GATE;
	
	class SourcePin: public Pin {
	public:
		SourcePin(MOSFET& mos): Pin(&mos, "SOURCE"), _mos(mos) { }
	protected:
		virtual void getElectric(uint64_t time, double U, double I) { _mos.setSource(time, U, I); }
	private:
		MOSFET& _mos;
	} SOURCE;
	
	Pin DRAIN;
	bool gate;
	double _U, _I;
};

/**
 * Called to build a new PWM.
 */
extern "C" IComponent* init(Board& board,QDomElement xml, QString name) {
	return new MOSFET(board, xml, name);	
}
		
} }	// bsim::electric 
