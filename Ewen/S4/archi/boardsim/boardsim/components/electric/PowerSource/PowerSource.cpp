/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <QPixmap>

#include <Board.hpp>
#include <Display.hpp>
#include <IComponent.hpp>
#include <Scheduler.hpp>

namespace bsim { namespace electric {

/**
 * This component represents a perfect power source as couple of
 * electric elements (U = voltage, I = intensity).
 * 
 * To be used in a board configuration, the class must be "electric/PowerSource".
 * 
 * It only provides one pin:
 * @li @c OUT -- where to the current is delivered.
 * 
 * In the board configuration, it supports the following attributes:
 * @li @c U -- voltage of the source (in volt),
 * @li @c I -- intensity of the source (in Ampere).
 */
class PowerSource: public IComponent {
public:
	PowerSource(Board& board, QDomElement xml, QString name)
	:	IComponent(board, xml),
		START(*this),
		OUT(this, "OUT"),
		_U(0),
		_I(0)
	{
		add(&OUT);
		IComponent::configure(xml);
	}

	virtual void configure(Configuration& conf, IMonitor& monitor) {
		_U = conf.get("U", 220.);
		_I = conf.get("I", 1.);
	}

	virtual void start(void) {
		IComponent::start();
		START.reschedule(START.scheduler()->time());
	}

	void makeVisual(VisualGraph& graph) {
	
		// configure the component
		VisualComponent *vcomp = new VisualComponent(this);
		vcomp->w = 80;
		vcomp->h = 100;
		
		// put the pins
		vcomp->add(new VisualPin(*vcomp, &OUT, Visual::NORTH, vcomp->w / 2));
		vcomp->parse(configuration());

		// prepare UI
		icon = QPixmap(board().prefix() + "/electric/battery.png");
		icon = icon.scaled(vcomp->w,vcomp->h, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
		QGraphicsPixmapItem *item = new QGraphicsPixmapItem(icon);
		vcomp->addToGroup(item);
		vcomp->setPos(vcomp->x, vcomp->y);
		vcomp->makeLabel(QString("%1\n%2 V\n%3 A").arg(name()).arg(_U).arg(_I));
		vcomp->makePins();
		graph.add(vcomp);
	}	

private:

	class StartEvent: public Scheduler::Event {
	public:
		StartEvent(PowerSource& src): Scheduler::Event(src.board()), _src(src) { }
		virtual void run(void)
			{ _src.OUT.setElectric(scheduler()->time(), _src._U, _src._I); }
	private:
		PowerSource& _src;
	} START;

	Pin OUT;
	double _U, _I;
	QPixmap icon;
};

/**
 * Called to build a new PWM.
 */
extern "C" IComponent* init(Board& board,QDomElement xml, QString name) {
	return new PowerSource(board, xml, name);	
}
		
} }	// bsim::electric 
