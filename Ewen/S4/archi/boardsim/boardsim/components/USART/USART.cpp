/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
 
#include <QLabel>
#include <QDebug>
#include <Display.hpp>
#include <IRegister.hpp>
#include <Scheduler.hpp>
#include <Board.hpp>
#include "USART.hpp"

//#define TRACE_USART
#ifdef TRACE_USART
#	define TRACE(c)		c
#else
#	define TRACE(c)
#endif


namespace bsim{
/**
 * @class USART
 * 
 * @li Programmmable Baud Rate Generator
 * @li 5- to 9-bit full duplex synchronous or asynchronous serial communication   
 *    - 1, 1.5 or 2 stop bits in Asynchronous Mode
 *    - 1 or 2 stop bits in Synchronous Mode
 *    - Parity generation and error detection 
 *    - Framing error detection, overrun error detection 
 *    - MSB or LSB first
 *    - Optional break generation and detection
 *    - By 8 or by 16 over-sampling receiver frequency 
 *    - Hardware handshaking RTS - CTS
 *    - Modem Signals Management DTR-DSR-DCD-RI on USART1
 *    - Receiver time-out and transmitter timeguard
 *    - Multi-drop Mode with address generation and detection
 * @li RS485 with driver control signal
 * @li ISO7816, T = 0 or T = 1 Protocols for interfacing with smart cards
 *    - NACK handeling, error counter with repetition and iteration limit
 * @li IrDA modulation and demodulation
 *    - Communication at up to 115.2 Kbps
 * @li Test Modes
 *    - Remote Loopback, Local Loopback, Automatic Echo
 * 
 * @par Pins
 * @li @c SCK -- I/O pin,
 * @li @c TXD -- I/O pin,
 * @li @c RXD -- input pin,
 * @li @c RI  -- input pin,
 * @li @c DSR -- input pin,
 * @li @c DCD -- input pin,
 * @li @c DTR -- output pin,
 * @li @c CTS -- input pin,
 * @li @c RTS -- output pin,
 * @li @c INT -- output pin.
 * 
 * @par Configuration
 * No configuration.
 * 
 * @par Registers
 * The following registers are 32-bits wide.
 * @li @c CR --  controls the transmission/reception (read/write),
 * @li @c MR --  configurates the USART mode (read/write),
 * @li @c IER -- enables interrupts by writing 1 on the corresponding bit in IMR (write),
 * @li @c IDR -- disables interrupts by writing 0 on the corresponding bit in IMR (write),
 * @li @c IMR -- the state of the interrupts, enabled/ disabled (read),
 * @li @c CSR -- the state of the channels (read),
 * @li @c RHR -- defines the character to be received and its nature (read),
 * @li @c THR -- defines the next character to be transmitted and its nature (write),
 * @li @c BRGR -- defines the Baudrate resolution (read/write),
 * @li @c RTOR -- whether the Receiver Time-out is enabled or not and the Time-out delay's value (read/write),
 * @li @c TTGRR -- whether the Transmitter Timeguard is enabled or not and the Timeguard delay's value (read/write),
 * @li @c FIDI -- contains FI Over DI Ratio value (read/write),
 * @li @c NER -- gives the total number of errors that occured during an ISO7816 transfer (read/write),
 * @li @c IF --  sets the filter of the IrDA demodulator (read/write),
 * 
 * @ingroup components
 */

/**
 * Constructor.
 * @param board		Current board.
 * @param xml		Configuration.
 * @param n			Component logic name.
 */	                            	

class outPin: public SerialPin{
public:
	outPin (USART& _usart, QString name):SerialPin(&_usart, name), usart(_usart) {
     }	
      
    void onChange(uint64_t time, bool value) { }
    
    void onReceive(uint64_t time, uint32_t word)  {
	}
	
	void onConfigure(uint64_t time, const config_t& configuration) {
		}
	
	void onWord(uint64_t time, uint32_t word) { 
		}
	
	void onError(uint64_t time, QString id) { 
		}
		
private:
	USART& usart;
};

class inPin: public SerialPin {
public:
	inPin (USART& _usart, QString _name): SerialPin(&_usart, _name), usart(_usart), name(_name) {
     }	
    
	void onChange(uint64_t time, bool value) { 
		if(value) {	
			if(name == "RI1") {
				usart.CSR.RI = 1;
				usart.CSR.RIIC = 1;
			}
			else if(name == "DSR1") {
				usart.CSR.DSR = 1;
				usart.CSR.DSRIC = 1;
			}
			else if(name == "DCD1") {
				usart.CSR.DCD = 1;
				usart.CSR.DCDIC = 1;
			}
			else if(name == "CTS1" || name == "CTS0") {
				usart.CSR.CTS = 1;
				usart.CSR.CTSIC = 1;
			}
		}
		else {	
		    if(name == "RI1") {
				usart.CSR.RI = 0;
				usart.CSR.RIIC = 1;
			}
			else if(name == "DSR1") {
				usart.CSR.DSR = 0;
				usart.CSR.DSRIC = 1;
			}
			else if(name == "DCD1") {
				usart.CSR.DCD = 0;
				usart.CSR.DCDIC = 1;
			}
			else if(name == "CTS1" || name == "CTS0")  {
				usart.CSR.CTS = 1;
				usart.CSR.CTSIC = 0;
			}	
		}
		usart.changeOUT(usart.it());  
	}
    
	void onConfigure(uint64_t time, const config_t& configuration)
	{	
	}
	
	void onWord(uint64_t time, uint32_t word) 
	{ 		
		if(name=="RXD0" || name=="RXD1" )  
		{		
			usart.r_word = word;
			uint32_t r_time = usart.getConfiguration(false);	
		    if(usart.CR.RXEN && !usart.CR.RXDIS)
				usart.start_rcpt(r_time);   // if receiver is enabled => start reception	
		} 
	}
	
	void onError(uint64_t time, QString id) 
	{
		qDebug() << "RECEPTION ERROR ON : "<< id << "  at : " << time << endl;
	}	
	// Active level page 305
	
private:
	USART& usart;
	QString name;
};


 /**
  * Transmission event.
  */
class TransmitEVT: public Scheduler::Event {
public:
	TransmitEVT(Board& _board,time_t tm, USART& _us)
		: Event(_board, tm), us(_us) { } 
	inline void run(void) { us.endOfTransmission(); }    
    inline void resched(time_t t) { reschedule(t); }  
private:
	USART& us;
};

// reception event
class ReceptEVT: public Scheduler::Event {
	public:
	ReceptEVT(Board& _board, time_t tm, USART& _us)
		: Event(_board, tm), us(_us), time(tm) { } 
	 
	void run(void) {
		if(!us.CSR.RXRDY)
			us.CSR.RXRDY = 1;
		else
			us.CSR.OVRE = 1;
		us.setf(us.RHR,0,7, us.r_word); 
		TRACE(qDebug() << "Received word : " << char(us.RHR) <<endl;) 		
		us.changeOUT(us.it());   		
		us.CR.set(8); // set RSTSTA => clear PARE, FRAME, OVRE, RXBRK in CSR
		us.CSR.RXBRK = 0;
		us.CSR.OVRE = 0;
		us.CSR.FRAME = 0;
		us.CSR.PARE = 0;
	}
	
	 void resched(time_t t)     
    {
	    reschedule(t);    
	}  
    
    
private:
    time_t time;
	USART& us;
};

USART::USART(Board& board, QDomElement xml, QString n)
:	IComponent(board, xml),
	CR		(this, 	UInt32::make().name("CR")	.offset(0x000).set(this, &USART::setCR)),
	MR		(this,	UInt32::make().name("MR")	.offset(0x004).set(this, &USART::setMR)),
	IER		(this,	UInt32::make().name("IER")	.offset(0x008).set(this, &USART::setIER)),
	IDR		(this,	UInt32::make().name("IDR")	.offset(0x00c).set(this, &USART::setIDR)),
	IMR		(this,	UInt32::make().name("IMR")	.offset(0x010).get(this, &USART::getIMR)),
	CSR		(this,	UInt32::make().name("CSR")	.offset(0x014).get(this, &USART::getCSR)),
	RHR		(this,	UInt32::make().name("RHR")	.offset(0x018).get(this, &USART::getRHR)),
	THR		(this, 	UInt32::make().name("THR")	.offset(0x01C).set(this, &USART::setTHR)),
	BRGR	(this,	UInt32::make().name("BRGR")	.offset(0x020).set(this, &USART::setBRGR)),
	RTOR	(this,	UInt32::make().name("RTOR")	.offset(0x024).set(this, &USART::setRTOR)),
	TTGR	(this,	UInt32::make().name("TTGR")	.offset(0x028).set(this, &USART::setTTGR)),
	FIDI	(this,	UInt32::make().name("FIDI")	.offset(0x040).set(this, &USART::setFIDI)),
	NER		(this,	UInt32::make().name("NER")	.offset(0x044).get(this, &USART::getNER).read()),
	IF		(this, 	UInt32::make().name("IF")	.offset(0x04C).set(this, &USART::setIF))
{

    // initialize registers
	setAddress(0xFFFC0000);
    
    *CSR = 0;
	RHR = 0;
	THR = 0;
	NER = 0;
    *MR = 0;
    *CR = 0;
	*IMR = 0; BRGR=0X0; RTOR=0X0; TTGR=0X0; FIDI=0X174; IF=0X0;
    timeguard=0; 
	time_out=0;  
    
     add(INT= new outPin(*this, "INT"));	
	// configure from XML
	configure(xml);
	
	// get configuration
	channel_nb = 0;
	sck_FREQ = 200;
	
	if(xml.hasAttribute("channel"))
		channel_nb = xml.attribute("channel").toInt();     // if channel_nb = 1 => USART1 is chosen  otherwise USART0 
	if(channel_nb < 0 || channel_nb > 1)
		channel_nb = 0;
   
	if(xml.hasAttribute("sckfreq"))
		sck_FREQ = xml.attribute("sckfreq").toInt();		// the frequency on the SCK pin given in the xml configuration
						
	// initialize pins

	if(channel_nb == 0)	  
    {
		add(SCK= new inPin(*this, "SCK0"));
		add(TXD= new outPin(*this, "TXD0")); 
	    add(RTS= new outPin(*this, "RTS0"));
		add(RXD= new inPin(*this, "RXD0"));
		add(CTS= new inPin(*this, "CTS0")); 
	}
    else	  
    {    
		add(SCK= new inPin(*this, "SCK1"));
		add(TXD= new outPin(*this, "TXD1"));
		add(DTR= new outPin(*this, "DTR1"));
		add(RTS= new outPin(*this, "RTS1")); 
		add(RXD= new inPin(*this, "RXD1"));
		add(RI=  new inPin(*this, "RI1"));
		add(DSR= new inPin(*this, "DSR1"));
		add(DCD= new inPin(*this, "DCD1"));
		add(CTS= new inPin(*this, "CTS1"));   	
	}		
		
	// initialize events
	t_event =new  TransmitEVT(board, 0, *this);
	r_event =new  ReceptEVT(board, 0, *this);
	 	
	// debug 
	info(QString("initialized (base=%1)").arg(address(), 8, 16, QChar('0')));
	
}

/**
 */
USART::~USART(void) {
}


/**
 */
void USART ::makeVisual(VisualGraph& graph) {
		// configure the component
		VisualComponent *vcomp = new VisualComponent(this);
		vcomp->w = 80;
		vcomp->h = 120;
		        
		vcomp->add(new VisualPin(*vcomp, INT, Visual::WEST, (15)));	
		if(channel_nb==1)	  
       {
			vcomp->add(new VisualPin(*vcomp, SCK, Visual::EAST, (65)));
			vcomp->add(new VisualPin(*vcomp, TXD, Visual::WEST, (90)));
			vcomp->add(new VisualPin(*vcomp, DTR, Visual::WEST, (100)));
			vcomp->add(new VisualPin(*vcomp, RTS, Visual::WEST, (110)));			
			vcomp->add(new VisualPin(*vcomp, RXD, Visual::EAST, (15)));	
			vcomp->add(new VisualPin(*vcomp, RI, Visual::EAST, (25)));	
			vcomp->add(new VisualPin(*vcomp, DSR, Visual::EAST, (35)));	
			vcomp->add(new VisualPin(*vcomp, DCD, Visual::EAST, (45)));	
			vcomp->add(new VisualPin(*vcomp, CTS, Visual::EAST, (55)));	
		}
		else	  
        {
			vcomp->add(new VisualPin(*vcomp, SCK, Visual::EAST, (65)));
			vcomp->add(new VisualPin(*vcomp, TXD, Visual::WEST, (90)));
		    vcomp->add(new VisualPin(*vcomp, RTS, Visual::WEST, (100)));
		    vcomp->add(new VisualPin(*vcomp, RXD, Visual::EAST, (25)));	
		    vcomp->add(new VisualPin(*vcomp, CTS, Visual::EAST, (55)));	
    	}
	
		vcomp->parse(configuration());

		// prepare UI
		vcomp->make(name());
		vcomp->makePins();
		graph.add(vcomp);
}	

/** 
 * generates the baud rate according to the chosen clock which can be external provided by the signal on the SCK pin.
 * @param sckFRQ	The frequency of the external clock on the SCK pin.
 * */
uint32_t USART::baudRateGen(uint32_t sckFRQ) {
	uint32_t BR, selectedClk, sck, internMCK;
	BR = 0;
	sck = 0;
	internMCK = 0;

	switch(MR.USCLKS) {
	case 0b00: selectedClk = board().mck();  internMCK = 1; break;
	case 0b01: selectedClk = board().mck() / 8; break;
	case 0b11: sck = 1; break;
	}
	        
	if(bitf(BRGR, 0, 15) != 0) {	    // BRGR.CD != 0
		
		// not in ISO7816 mode
		if((MR.MODE != 4) && (MR.MODE != 6)) {	// MODE
			
			// Asynchronous Mode
			if(bit(MR, 8) == 0) {	// SYNC = 0
				
				// BRGR.FP = 0
				if(bitf(BRGR, 16, 18) == 0) {
					
					if(MR.OVER == 0)
						// BR = selectedClk / 16 / CD
						BR = selectedClk / 16 / bitf(BRGR, 0, 15);
					else
						// BR = selectClk / 8 / CD
						BR = selectedClk / 8 / bitf(BRGR, 0, 15);
				}
				
				// BRGR.FP != 0
				else {	
					if(MR.OVER == 0)
						// BR = selectClk / 16 * CD + FP / 8
						BR = selectedClk / 16 / (bitf(BRGR, 0, 15) + bitf(BRGR, 16, 18) / 8);        
					else
						// BR = selectClk / 8 * BRGR + FP / 8
						BR = selectedClk / 8 / (bitf(BRGR, 0, 15) + bitf(BRGR, 16, 18) / 8);
				}
			} 
			
			// Synchronous Mode
			else {   
				if(sck == 1) {
					if(board().mck() >= 4.5 * sckFRQ)
						BR = sckFRQ; 					        
				}	 						
				else {
					//if(internMCK==1)  // sck pin  duty cycle, page 310	// TODO useful?
					BR = selectedClk / bitf(BRGR,0,15);                           
				}	
			}
		}
		
		// ISO7816 mode  
		else {   
			MR.CLKO = 1;     // CLKO (MR)
			BR = selectedClk /bitf(FIDI, 0,10)*bitf(BRGR,0,15);      
			// provide the resulting clock to sck pin
		}
	}
	TRACE(qDebug() << "BR = " << BR);
	return BR; 
}


/**
 * Allows to get the configuration chosen by the user.
 * @return	Transmission time.
 * */
uint32_t USART::getConfiguration(bool transmission) {
	uint32_t baud_rate, bit_nb, par,par_bit, mode, sync, msb, nbstop, chmode;

	
	chmode = 0;
	baud_rate = baudRateGen(sck_FREQ); 
	
	if(MR.MODE9 == 1)
		bit_nb= 9;
	else {
		switch(MR.CHRL) {
		case 0b00: bit_nb = 5; break;
		case 0b01: bit_nb = 6; break;
		case 0b10: bit_nb = 7; break;
		case 0b11: bit_nb = 8; break;
		}	
	}
	
	switch(MR.PAR) {
	case 0b000:	par = 2; par_bit = 1;break;
	case 0b001: par = 1; par_bit = 1;break;
	case 0b010:	par = 4; par_bit = 1;break;
	case 0b011:	par = 3; par_bit = 1;break;
	case 0b100:	par = 0; par_bit = 0; break;
	case 0b101:	par = 0; par_bit = 0; break;
	case 0b110:	par = 5; par_bit = 1;break;
	case 0B111:	par = 5; par_bit = 1;break;	
	}

	switch(MR.MODE) {
	case 0: mode = 0; break;
	case 1: mode = 1; break;
	case 2: mode = 2; break;
	case 3: mode = 3; break;
	case 4: mode = 4; break;
	case 6: mode = 5; break;
	case 8: mode = 6; break;
	}
	
	if(MR.SYNC == 1)
		sync = 1;
	else
		sync = 0;
	
	if(MR.MSBF == 1)
		msb=1;
	else
		msb = 0;
	
	switch(MR.NBSTOP)  {
	case 0b00:  nbstop = 1; break;
	case 0b01:  nbstop = 1.5; break;
	case 0b10:  nbstop = 2; break;
	case 0b11:	assert(false); break;	// reserved
	}
	
	switch(MR.CHMODE) {
	case 0: chmode = 0; break;
	case 1: chmode = 1; break;
	case 2: chmode = 2; break;
	case 3: chmode = 3; break;
	} 	
    
    // prepare for transmitting the configuration
    if(transmission) {
		setConfiguration(*TXD, "mode", mode);
		setConfiguration(*TXD, "wordl", bit_nb);
		setConfiguration(*TXD, "sync", sync);
		setConfiguration(*TXD, "parity", par);
		setConfiguration(*TXD, "nbstop", nbstop);
		setConfiguration(*TXD, "msb", msb);
		setConfiguration(*TXD, "rate", baud_rate);
		setConfiguration(*TXD, "chmode", chmode);	
	}

	// compute the delay
	// delay = (2 - MR.OVER) * BRGR * (1 + bit_nb + par_bit + nbstop);
	uint32_t delay = (2 - MR.OVER * bitf(BRGR, 0, 16)) * (1 + bit_nb + par_bit + nbstop);
	return delay;         
}


/**
 * Configurates a pin
 * @param pn the pin (TXD,..) we want to set up.
 * */
void USART::setConfiguration(SerialPin& pn, QString id, QVariant value) {
	pn.set(id, value); // configure the pn pin
}


/** 
 * resets the internal state machine 
 * */
void USART::us_resetR(void) {	
    *CSR = 0;
    NER = 0; 
    CR.RXEN = 0;
    CR.RXDIS = 1;
    CSR.TXRDY = 1;
	CSR.TXEMPTY = 1;
}


void USART::us_resetT(void) {	
    *CSR = 0;
    NER = 0; 
    CR.TXEN = 0;
    CR.TXDIS = 1;
	CSR.TXRDY = 1;
	CSR.TXEMPTY = 1;
}


/**
 * starts the reception
 **/ 
 void USART ::start_rcpt(uint32_t r_time) {
	 r_event->resched(board().scheduler()->time() + r_time);
}

/**
 * Called when a character transmission is ended. Either a new character
 * has been written to THR and new transmission starts, or there is no
 * more character to send and USART re-enter idle mode.
 */
void USART::endOfTransmission(void) {
		
	// send the actual character
	TRACE(qDebug()<< "End of transmission " << endl;) 	
	TRACE(qDebug()<< "THR = "<< tx_barrel <<endl;)	
	sendOUT(tx_barrel);	// send to TXD pin
	
	// no incoming character
	if(CSR.TXRDY) {
		THR = 0;
		CSR.TXEMPTY = 1;
		changeOUT(it());   
	}
	
	// send the new character
	else
		startTransmission();
}


/**
 * Implement the start of a character transmission, mainly rescheduling
 * the transmit event and set bits TXRDY and TXEMPTY.
 */
void USART::startTransmission(void) {
	tx_barrel = THR;
	CSR.TXRDY = 1;
	CSR.TXEMPTY = 0;
	changeOUT(it());
	TRACE(qDebug() << "transmission duration = " << getConfiguration(true));
	TRACE(qDebug() << "start of transmission of "<< QChar(tx_barrel)
				   << " at " << board().scheduler()->time() << endl;)	    
	TRACE(qDebug() << "TXEMPTY = "<< bit(CSR,9) << endl;) 
	uint32_t t_time = board().scheduler()->time() + getConfiguration(true); 
	t_event->resched(t_time);
}



/**
 * transmits the word on the TXD pin
 * */
void USART::sendOUT(uint32_t thr) {
     TXD->send(board().scheduler()->time(), thr);              
}


/**
 * verifies if there is an interrupt to be asserted.  
 */
uint32_t USART::it() {
	return getCSR()& IMR;
}


/**
 * */
void USART ::changeOUT(uint32_t changes) {
	for(int i = 0; i < 20; i++) {
		uint32_t mask = 1 << i;
		if(changes & mask)
			INT->change(board().scheduler()->time(), true);			
	}
}


/**
 * Get the CSR register.
 */
uint32_t USART ::getCSR(void) {
	return CSR; 
}


/**
 * 
 * */
void USART ::setMR (uint32_t v)  {
	*MR = v;
}


/**
 * Set the CR register.
 */
void USART ::setCR (uint32_t v)  {
	*CR = v;
	
	if(CR.RSTRX) {
		us_resetR();
		r_event->cancel();  
	}
  
	if(CR.RSTTX) { 	
		us_resetT();
		t_event->cancel();  
	}
  
	if(CR.RXEN)
		CR.RXDIS = 0;
  
	if(CR.RXDIS) {
		CR.RXEN = 0;
		r_event->cancel(); 
	}
	
	if(CR.TXEN) {
		CR.TXDIS = 0;
		CSR.TXRDY = 1;
		CSR.TXEMPTY = 1;
	}

	if(CR.TXDIS) {
		CR.TXEN = 0;
		t_event->cancel();             
	}
		
	if(CR.RSTSTA) {   
		CSR.RXBRK = 0;
		CSR.OVRE = 0;
		CSR.FRAME = 0;
		CSR.PARE = 0;
	}
  
	if(CR.STTTO)
		CSR.TIMEOUT = 0;
		
	if(CR.RSTIT) {
		if(MR.MODE == 4 || MR.MODE == 6)
			CSR.ITERATION = 0;
	}
	
	if(CR.RSTNACK)
		CSR.NACK = 0;
		
	if(CR.RETTO)
		RTOR = 0;
		
	if(CR.DTREN)
		DTR->change(board().scheduler()->time(), false);
		
	if(CR.DTRDIS)
		DTR->change(board().scheduler()->time(), true);
	
	if(CR.RTSEN)
		RTS->change(board().scheduler()->time(), false);
	
	if(CR.RTSDIS)
		RTS->change(board().scheduler()->time(), true);
}
                  
                  
/**
 * Set IER.
 */    
void USART ::setIER (uint32_t v)  {
	IMR |= v;
}


/**
 * Set IDR.
 */
void USART ::setIDR (uint32_t v)  {
	IMR &= ~v;
}


/**
 * Set THR.
 */
void USART::setTHR (uint32_t v)  {
	THR = v;
	if(CR.TXEN && !CR.TXDIS) {
		
		// record a new character
		CSR.TXRDY = 0;
		changeOUT(it());
    
		// no transmission: start transmission
		if(CSR.TXEMPTY)
			// TODO we should wait 1 cycle...
			startTransmission();
	}
}


/**
 * Set BRGR.
 */
void USART ::setBRGR (uint32_t v)  {
	setf(BRGR , 0, 31, v);
}


/**
 * Set RTOR.
 */
void USART::setRTOR (uint32_t v)  {
	setf(RTOR, 0, 15, v);
	if(bitf(RTOR, 0, 15) == 0)
		time_out=0;
	else
		time_out= bitf(RTOR,0,15)* (1/ baudRateGen(sck_FREQ)); 
}

void USART ::setTTGR (uint32_t v)  {	
	setf(TTGR , 0, 15, v);	
	if(bitf(TTGR ,0,15) == 0)
		timeguard = 0;
	else
		timeguard= bitf(TTGR , 0, 15) * (1 / baudRateGen(sck_FREQ));
}
    
void USART ::setFIDI (uint32_t v)  {
	setf(FIDI , 0, 31, v);
}

void USART ::setIF (uint32_t v)  {
	setf(IF , 0, 31, v);
}

/**
 */	
void USART::genHeader(QTextStream& out) {
#	if 0 
	out << "	.equ US_RXRDY, 	(1 << 0)\n";
	out << "	.equ US_TXRDY, 	(1 << 1)\n";
	out << "	.equ US_TXEMPTY, (1 << 9)\n";
	
	out << "	.equ US_RSTRX, 	(1 << 2)\n";
	out << "	.equ US_RSTTX, 	(1 << 3)\n";
	out << "	.equ US_RXEN, 	(1 << 4)\n";
	out << "	.equ US_RXDIS, 	(1 << 5)\n";
	out << "	.equ US_TXEN, 	(1 << 6)\n";
	out << "	.equ US_TXDIS, 	(1 << 7)\n";
	out << "	.equ US_RSTSTA, (1 << 8)\n";
	
	out << "	.equ US_SYNC_ON, (1 << 8)\n";
	out << "	.equ US_SYNC_OFF, (0 << 8)\n";
#	endif
}

void USART::getStaticConstants(QVector<Constant>& csts) const {
	
	// CSR
	/*csts.append(Constant::mask("US_RXRDY",		0, false));
	csts.append(Constant::mask("US_TXRDY",		1, false));
	csts.append(Constant::mask("US_RXEMPTY",	9, false));*/

	// CR
	/*csts.append(Constant::mask("US_RSTRX",		2, false));
	csts.append(Constant::mask("US_RSTTX",		3, false));
	csts.append(Constant::mask("US_RXEN",		4, false));
	csts.append(Constant::mask("US_RXDIS",		5, false));
	csts.append(Constant::mask("US_TXEN",		6, false));
	csts.append(Constant::mask("US_TXDIS",		7, false));
	csts.append(Constant::mask("US_RSTSTA",		8, false));*/
	
	// MR
	/*csts.append(Constant::field("US_USCLKS_MCK",	0b00,	 4, false));
	csts.append(Constant::field("US_CHRL_8",		0b11,	 6, false));
	csts.append(Constant::field("US_PAR_NONE",		0b100,	 9, false));
	csts.append(Constant::field("US_NSTOP_1",		0b00,	12, false));
	csts.append(Constant::field("US_CHMODE_NORMAL",	0b00,	14, false));*/
}


uint32_t USART::getRHR(void) { 
	CSR.RXRDY = 0;
	return RHR; 
}

	
/**
 * Called to build a new USART.
*/
extern "C" IComponent* init(Board& board,QDomElement xml, QString name) {
	return new USART (board, xml, name);	
}
	
}  // bsim
