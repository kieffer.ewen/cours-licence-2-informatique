#ifndef COMPONENT_USART
#define COMPONENT_USART

#include <stdint.h>
#include <QDomElement>

#include "Board.hpp"
#include "IComponent.hpp"
#include "Scheduler.hpp"
#include "UInt32Register.hpp"

namespace bsim {
	
// pre-declarations
class outPin;          
class inPin;
class ReceptEVT;
class TransmitEVT;

// USART class	  
class USART: public IComponent {
	  friend class outPin;
	  friend class inPin;
	  friend class TransmitEVT;
	  friend class ReceptEVT;
	   
public:
	USART(Board& b, QDomElement dom, QString name);
	~USART(void);
               
	virtual void makeVisual(VisualGraph& graph);    
	void genHeader(QTextStream& out); 
	virtual void getStaticConstants(QVector<Constant>& csts) const;
	virtual QString typePrefix(void) { return "US"; }
	
	// register accessors
	void setMR (uint32_t v) ;
	void setCR (uint32_t v) ;                     
	void setIER (uint32_t v) ;  
	void setIDR (uint32_t v) ;	
	inline uint32_t getIMR(void) { return IMR; }   
	uint32_t getRHR(void);
	void setTHR (uint32_t v) ;
	void setBRGR (uint32_t v) ;
	void setRTOR (uint32_t v) ;
	void setTTGR (uint32_t v) ;    
	void setFIDI (uint32_t v) ;
	void setIF (uint32_t v) ;
	uint32_t getCSR(); 
	inline uint32_t getNER(void) 
      { uint32_t val = NER; NER = 0; return val; }
	
	// tool functions
	uint32_t baudRateGen(uint32_t sckFRQ);
	void us_resetT();
	void us_resetR();
	void start_rcpt(uint32_t r_time);
	void changeOUT(uint32_t changes);
	uint32_t it();
	uint32_t getConfiguration(bool transmission);
	void setConfiguration(SerialPin& pn, QString id, QVariant value);
	void sendOUT(uint32_t thr);
	void endOfTransmission(void);
	void startTransmission(void);
	
	// internal state
	uint32_t tx_barrel;

private:

	// CR register
	class CRClass: public UInt32Register {
	public:
		CRClass(USART *us, const Make& make): UInt32Register(us, make),
			RSTRX	(*this, "RSTRX",	 2),
			RSTTX	(*this, "RSTTX",	 3),
			RXEN	(*this, "RXEN",		 4),
			RXDIS	(*this, "RXDIS",	 5),
			TXEN	(*this, "TXEN",		 6),
			TXDIS	(*this, "TXDIS", 	 7),
			RSTSTA	(*this, "RSTSTA", 	 8),
			STTBRK	(*this, "STTBRK", 	 9),
			STPBRK	(*this, "STPBRK", 	10),
			STTTO	(*this, "STTTO", 	11),
			SENDA	(*this, "SENDA", 	12),
			RSTIT	(*this, "RSTIT", 	13),
			RSTNACK	(*this, "RSTNACK",	14),
			RETTO	(*this, "RETTO",	15),
			DTREN	(*this, "DTREN", 	16),
			DTRDIS	(*this, "DTRDIS", 	17),
			RTSEN	(*this, "RTSEN", 	18),
			RTSDIS	(*this, "RTSDIS",	19)
			{ }
	
		Bit	RSTRX,
			RSTTX,
			RXEN,
			RXDIS,
			TXEN,
			TXDIS,
			RSTSTA,
			STTBRK,
			STPBRK,
			STTTO,
			SENDA,
			RSTIT,
			RSTNACK,
			RETTO,
			DTREN,
			DTRDIS,
			RTSEN,
			RTSDIS;
	} CR;
	
	// MR register
	class MRClass: public UInt32Register {
	public:
		MRClass(USART *us, const Make& make): UInt32Register(us, make),
			MODE			(*this, "MODE",				 3,  0),
			USCLKS			(*this, "USCLKS",			 5,  4, _
				& val("MCK", 		0b00)
				& val("MCK_DIV",	0b01)
				& val("SCK", 		0b11)),
			CHRL			(*this, "CHRL",				 7,  6, _
				& val("5", 0b00)
				& val("6", 0b01)
				& val("7", 0b10)
				& val("8", 0b11)),
			SYNC			(*this, "SYNC",					 8),
			PAR				(*this, "PAR",				11,  9, _
				& val("EVEN", 		0b000)
				& val("ODD",		0b001)
				& val("FORCED_0",	0b010)
				& val("FORCED_1",	0b011)
				& val("NONE",		0b100)
				& val("MULTI",		0b110)),
			NBSTOP			(*this, "NBSTOP",			13, 12, _
				& val("1",		0b00)
				& val("1_5",	0b01)
				& val("2",		0b10)),
			CHMODE			(*this, "CHMODE",			15, 14, _
				& val("NORMAL",	0b00)
				& val("ECHO",	0b01)
				& val("LOCAL",	0b10)
				& val("REMOTE",	0b11)),
			MSBF			(*this, "MSBF",					16),
			MODE9			(*this, "MODE9",				17),
			CLKO			(*this, "CLKO",					18),
			OVER			(*this, "OVER",					19),
			INACK			(*this, "INACK",				20),
			DSNACK			(*this, "DSNACK",				21),
			MAX_ITERATION	(*this, "MAX_ITERATION",	26, 24),
			FILTER			(*this, "FILTER",				28)
			{ }
		BitField MODE,
				 USCLKS,
				 CHRL;
		Bit		 SYNC;
		BitField PAR,
				 NBSTOP,
				 CHMODE;
		Bit		 MSBF,
				 MODE9,
				 CLKO,
				 OVER,
				 INACK,
				 DSNACK;
		BitField MAX_ITERATION;
		Bit	 	 FILTER;
	} MR;

	// CSR and IMR registers
	class CSRClass: public UInt32Register {
	public:
		CSRClass(USART *us, const Make& make): UInt32Register(us, make),
			RXRDY	  (*this, "RXRDY", 		 0),
			TXRDY	  (*this, "TXRDY",		 1),
			RXBRK	  (*this, "RXBRK",		 2),
			ENDRX	  (*this, "ENDRX",		 3),
			ENDTX	  (*this, "ENDTX",		 4),
			OVRE	  (*this, "OVRE",		 5),
			FRAME	  (*this, "FRAME",		 6),
			PARE	  (*this, "PARE",		 7),
			TIMEOUT	  (*this, "TIMEOUT",	 8),
			TXEMPTY	  (*this, "TXEMPTY",	 9),
			ITERATION (*this, "ITERATION",	10),
			TXBUFE	  (*this, "TXBUFE", 	11),
			RXBUFF	  (*this, "RXBUFE", 	12),
			NACK	  (*this, "NACK", 		13),
			RIIC	  (*this, "RIIC", 		16),
			DSRIC	  (*this, "DSRIC", 		17),
			DCDIC	  (*this, "DCDIC", 		18),
			CTSIC	  (*this, "CTSIC", 		19),
			RI	  	  (*this, "RI", 		20),
			DSR	  	  (*this, "DSR", 		21),
			DCD		  (*this, "DCD", 		22),
			CTS		  (*this, "CTS", 		23)
			{ }
		Bit	RXRDY,
			TXRDY,
			RXBRK,
			ENDRX,
			ENDTX,
			OVRE,
			FRAME,
			PARE,
			TIMEOUT,
			TXEMPTY,
			ITERATION,
			TXBUFE,
			RXBUFF,
			NACK,
			RIIC,
			DSRIC,
			DCDIC,
			CTSIC,
			RI,
			DSR,
			DCD,
			CTS;
	} IMR, CSR;

	// registers
	UInt32Register
		RHR,
		THR,
		BRGR,
		RTOR,
		TTGR,
		FIDI,
		NER,
		IF;
	
	UInt32ControlRegister
		IER,
		IDR;
	
	// pins
	Pin *INT;  
	SerialPin *SCK;
	SerialPin *TXD; 
	SerialPin *RXD; 
	SerialPin *RI;
	SerialPin *DSR;
	SerialPin *DCD;
	SerialPin *DTR;                                                              
	SerialPin *CTS;
	SerialPin *RTS; 
      
	// internals
	uint32_t timeguard, time_out;  
	int channel_nb;  
	int sck_FREQ;    
	uint32_t r_word;

	// events
	TransmitEVT *t_event;
	ReceptEVT *r_event;  
};
	
}	// bsim

#endif
