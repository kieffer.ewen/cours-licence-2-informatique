/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_TERM_WIDGET_H
#define BSIM_TERM_WIDGET_H

#include <QtGui>
#include "unix/TerminalModel.h"
#include "unix/TerminalView.h"
#include "QTerminalInterface.h"
#include "MyTermModel.h"

namespace bsim {

class TermWidget: public QTerminalInterface {
    Q_OBJECT
public:
    TermWidget(QWidget *parent = 0);
    virtual ~TermWidget();
    
    void setTerminalFont(const QFont &font); 
    void setSize(int h, int v);
    void sendText(const QString& text);
    void setCursorType(CursorType type, bool blinking);
	inline void receiveBlock(const char *buffer, int len) { m_terminalModel->receiveBlock(buffer, len); }
	inline TermModel *model(void) const { return m_terminalModel; }

public slots:
    void copyClipboard();
    void pasteClipboard();
        
protected:
    void focusInEvent(QFocusEvent *focusEvent);
    void showEvent(QShowEvent *);
    virtual void resizeEvent(QResizeEvent *);   
    
private:
    void initialize();
    //void connectToPty();

    TerminalView *m_terminalView;
    TermModel *m_terminalModel;
};


} // bsim

#endif	// BSIM_TERM_WIDGET_H
