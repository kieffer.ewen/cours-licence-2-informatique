/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <termios.h>
#include "MyTermWidget.h"

namespace bsim {

TermWidget::TermWidget(QWidget *parent)
: QTerminalInterface(parent) {
    setMinimumSize(100, 100);
    initialize();
}

void TermWidget::initialize() {
	
	// initialize the view
    m_terminalView = new TerminalView(this);
    m_terminalView->setKeyboardCursorShape(TerminalView::UnderlineCursor);
    m_terminalView->setBlinkingCursor(true);
    m_terminalView->setBellMode(TerminalView::NotifyBell);
    m_terminalView->setTerminalSizeHint(true);
    m_terminalView->setContextMenuPolicy(Qt::CustomContextMenu);
    m_terminalView->setTripleClickMode(TerminalView::SelectWholeLine);
    m_terminalView->setTerminalSizeStartup(true);
    m_terminalView->setSize(80, 40);
    m_terminalView->setScrollBarPosition(TerminalView::ScrollBarRight);

    connect(m_terminalView, SIGNAL(customContextMenuRequested(QPoint)),
            this, SLOT(handleCustomContextMenuRequested(QPoint)));

	// set the terminal font
#ifdef Q_OS_MAC
    QFont font = QFont("Monaco");
    font.setStyleHint(QFont::TypeWriter);
    font.setPointSize(11);
#else
    QFont font = QFont("Monospace");
    font.setStyleHint(QFont::TypeWriter);
    font.setPointSize(10);
#endif
    setTerminalFont(font);
    setFocusProxy(m_terminalView);
    setFocus(Qt::OtherFocusReason);

    //m_kpty = new KPty();
    //m_kpty->open();

	// set the terminal model
    m_terminalModel = new TermModel();
	m_terminalModel->setAutoClose(true);
    m_terminalModel->setCodec(QTextCodec::codecForName("UTF-8"));
    m_terminalModel->setHistoryType(HistoryTypeBuffer(1000));
    m_terminalModel->setDarkBackground(true);
    m_terminalModel->setKeyBindings("");
	m_terminalModel->run();
    m_terminalModel->addView(m_terminalView);
}

TermWidget::~TermWidget() {
    emit destroyed();
}

void TermWidget::setTerminalFont(const QFont &font) {
    if(!m_terminalView)
        return;
    m_terminalView->setVTFont(font);
}

void TermWidget::setSize(int h, int v) {
    if(!m_terminalView)
        return;
    m_terminalView->setSize(h, v);
}

void TermWidget::sendText(const QString& text) {
    m_terminalModel->sendText(text);
}

void TermWidget::setCursorType(CursorType type, bool blinking) {
    switch(type) {
        case UnderlineCursor: m_terminalView->setKeyboardCursorShape(TerminalView::UnderlineCursor);
        case BlockCursor: m_terminalView->setKeyboardCursorShape(TerminalView::BlockCursor);
        case IBeamCursor: m_terminalView->setKeyboardCursorShape(TerminalView::IBeamCursor);
    }
    m_terminalView->setBlinkingCursor(blinking);
}

void TermWidget::focusInEvent(QFocusEvent *focusEvent) {
    Q_UNUSED(focusEvent);
    m_terminalView->updateImage();
    m_terminalView->repaint();
    m_terminalView->update();
}

void TermWidget::showEvent(QShowEvent *) {
    m_terminalView->updateImage();
    m_terminalView->repaint();
    m_terminalView->update();
}

void TermWidget::resizeEvent(QResizeEvent*) {
    m_terminalView->resize(this->size());
    m_terminalView->updateImage();
    m_terminalView->repaint();
    m_terminalView->update();
}

void TermWidget::copyClipboard() {
    m_terminalView->copyClipboard();
}

void TermWidget::pasteClipboard() {
    m_terminalView->pasteClipboard();
}

}	// bsim
