/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef BSIM_TERM_MODEL_H
#define BSIM_TERM_MODEL_H

#include <QtCore/QStringList>
#include <QtCore>
#include <QWidget>
#include "unix/SelfListener.h"
#include "unix/History.h"

class Emulation;
class PseudoTerminal;
class TerminalView;

namespace bsim {

class TermModel: public QObject {
	Q_OBJECT
public:
	Q_PROPERTY(QString keyBindings READ keyBindings WRITE setKeyBindings)
	Q_PROPERTY(QSize size READ size WRITE setSize)

	TermModel(void);
	~TermModel(void);

	void setProfileKey(const QString& profileKey);
	QString profileKey() const;

	void addView(TerminalView* widget);
	void removeView(TerminalView* widget);
	QList<TerminalView*> views(void) const;

	void sendText(const QString& text) const;
	inline void receiveBlock(const char* buffer, int len ) { onReceiveBlock(buffer, len); }
	Emulation *emulation(void) const;

	void setHistoryType(const HistoryType& type);
	const HistoryType& historyType(void) const;
	void clearHistory(void);
	void setMonitorActivity(bool);
	bool isMonitorActivity(void) const;
	void setMonitorSilence(bool);
	bool isMonitorSilence(void)  const;
	void setMonitorSilenceSeconds(int seconds);
	void setKeyBindings(const QString& id);
	QString keyBindings(void) const;
	void setAutoClose(bool b) { _autoClose = b; }
	QSize size(void);
	void setSize(const QSize& size);
	void setCodec(QTextCodec* codec);
	void setDarkBackground(bool darkBackground);
	bool hasDarkBackground(void) const;
	void refresh(void);

public slots:
	void run();
	void close();

signals:
	void started();
	void finished();
	void receivedData(const QString& text);
	void titleChanged();
	void profileChanged(const QString& profile);
	void stateChanged(int state);
	void bellRequest(const QString& message);
	void changeTabTextColorRequest(int);
	void changeBackgroundColorRequest(const QColor&);
	void openUrlRequest(const QString& url);
	void resizeRequest(const QSize& size);
	void profileChangeCommandReceived(const QString& text);

private slots:
	void done(int);
	void onReceiveBlock(const char* buffer, int len );
	void monitorTimerDone();
	void onViewSizeChange(int height, int width);
	void onEmulationSizeChange(int lines , int columns);
	void activityStateSet(int);
	void viewDestroyed(QObject* view);
	void sendData(const char* buf, int len);

private:
	void updateTerminalSize(void);
	WId windowId(void) const;

	int            _uniqueIdentifier;
	PseudoTerminal* _shellProcess;
	Emulation*    _emulation;
	QList<TerminalView*> _views;
	QTimer*        _monitorTimer;

	bool           _monitorActivity;
	bool           _monitorSilence;
	bool           _notifiedActivity;
	bool           _masterMode;
	bool           _autoClose;
	bool           _wantedClose;

	int            _silenceSeconds;

	bool           _addToUtmp;
	bool           _fullScripting;

	SelfListener  *_selfListener;

	QColor         _modifiedBackground; // as set by: echo -en '\033]11;Color\007
	QString        _profileKey;
	bool _hasDarkBackground;
};

}

#endif // BSIM_TERM_MODEL_H
