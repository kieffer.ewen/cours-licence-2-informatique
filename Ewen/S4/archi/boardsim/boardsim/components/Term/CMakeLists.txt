
# sources
set(SOURCES
	"Term.cpp"
	"TermWidget.cpp"
	"TermModel.cpp" 
	"libqterminal/unix/BlockArray.cpp"
	"libqterminal/unix/Emulation.cpp"
	"libqterminal/unix/Filter.cpp"
	"libqterminal/unix/History.cpp"
	"libqterminal/unix/KeyboardTranslator.cpp"
	"libqterminal/unix/konsole_wcwidth.cpp"
	"libqterminal/unix/Screen.cpp"
	"libqterminal/unix/ScreenWindow.cpp"
	"libqterminal/unix/TerminalCharacterDecoder.cpp"
	"libqterminal/unix/Vt102Emulation.cpp"
	"libqterminal/unix/SelfListener.cpp"
	"libqterminal/unix/TerminalView.cpp"
)
#set_target_properties(qterminal PROPERTIES COMPILE_FLAGS -fPIC)
#target_link_libraries(qterminal ${QT_LIBRARIES})

# Qt support
set(MOC_HEADERS
	"MyTerm.h"
	"MyTermWidget.h"
	"MyTermModel.h"
	"libqterminal/QTerminalInterface.h"
	"libqterminal/unix/Emulation.h"
	"libqterminal/unix/Filter.h"
	"libqterminal/unix/ScreenWindow.h"
	"libqterminal/unix/SelfListener.h"
	"libqterminal/unix/TerminalView.h"
	"libqterminal/unix/Vt102Emulation.h"	
)
message(STATUS "SOURCES=${SOURCES}")
QT5_WRAP_CPP(SOURCES ${MOC_HEADERS})


# plugin
include_directories("libqterminal")
add_library(Term SHARED ${SOURCES})
target_link_libraries(Term bsim ${QT_LIBRARIES})
install(TARGETS Term LIBRARY DESTINATION "lib/bsim")

