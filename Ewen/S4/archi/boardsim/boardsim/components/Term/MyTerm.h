/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_TERM_H
#define BSIM_TERM_H

#include <QObject>
#include <QDockWidget>
#include "unix/TerminalView.h"
#include "unix/Vt102Emulation.h"
#include "MyTermWidget.h"
#include "Board.hpp"
#include "IView.hpp"
#include "Scheduler.hpp"


namespace bsim {

class Term: public QObject,public IComponent {
	Q_OBJECT
public :
    Term(Board& board, QDomElement element, QString name);
	~Term(void);

	// IComponent override
	virtual void installUI(MainWindow& window);
	virtual void uninstallUI(MainWindow& window);
	virtual void start(void);
	virtual void stop(void);

public slots:
	void sendData(const char* data,int len);

private:

	void doUpdate(uint8_t c);
	void nextChar(void);
	void defaultConf(void);
	int initDelay(void);

	class InPin: public SerialPin {
	public:
		InPin(Term& term);
	protected:
		virtual void onWord(uint64_t time, uint32_t word);
		virtual void onError(uint64_t time, QString id);
	private:
		Term& _term;
	};
	
	class OutPin: public SerialPin {
	public:
		OutPin(IComponent *component): SerialPin(component, "out") { }
		virtual void onWord(uint64_t time, uint32_t word) { }
		virtual void onError(uint64_t time, QString id) { }
	};

	class View: public QDockWidget, public IView {
	public:
		View(Term& _term, MainWindow& window);
		inline Term& terminal(void) const { return term; }
	private:
		Term& term;
	};
	
	class SentEvent: public Scheduler::Event {
	public:
		SentEvent(Board& board, Term& term);
		virtual void run(void);
	private:
		Term& t;
	};
	
	// link configuration
	Pin::mode_t mode;
	int wordl;
	bool sync;
	Pin::parity_t parity;
	int nbstop;
	Pin::msb_t msb;
	int rate;
	
	// component data
	InPin in;
	OutPin out;
	QQueue<char> buf;
	bool sending, started;
	SentEvent event;
	int delay;

	// view data
	View *view;
	TermWidget *widget;
};

}	// bsim

#endif	// BSIM_TERM_H
