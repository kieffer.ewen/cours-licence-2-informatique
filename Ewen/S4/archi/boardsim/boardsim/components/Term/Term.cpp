/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include "MainWindow.hpp"
#include "MyTerm.h"

//#define TRACE_TERM
#ifdef TRACE_TERM
#	define TRACE(c)		c
#else
#	define TRACE(c)
#endif

namespace bsim {

/**
 * This is a pseudo-component providing display of the received character
 * and allowing to send character over a serial connection.
 * 
 * @p Pins
 * @li in -- to connect emiting wire,
 * @li out -- to connect receiving wire.
 */
Term::Term(Board& board, QDomElement element, QString name)
:	IComponent(board, element),
	in(*this),
	out(this),
	widget(0),
	sending(false),
	started(false),
	event(board, *this),
	rate(0),
	msb(Pin::MSB_FIRST),
	nbstop(0),
	parity(Pin::NOPAR),
	sync(false),
	wordl(0),
	mode(Pin::NORMAL),
	view(nullptr),
	delay(0)
{
	add(&in);
	add(&out);
	configure(element);
	info("succesfull initialization");
}

Term::~Term(void) {
}


/**
 */
void Term::start(void) {
	started = true;
	buf.clear();
	sending = false;
}


/**
 */
void Term::stop(void) {
	started = false;
}


/**
 * Receive a character and send it to the display.
 * @param c	Received character.
 */
void Term::doUpdate(uint8_t c) {
	if(widget)
		widget->receiveBlock((char *)&c, 1);
}


/**
 */
void Term::installUI(MainWindow& window) {
	view = new View(*this, window);
	view->setObjectName("serial-view");
	connect(widget->model()->emulation(), SIGNAL(sendData(const char*,int)), this, SLOT(sendData(const char*,int))); 
}


/**
 */
void Term::uninstallUI(MainWindow& window) {
}


/**
 */
Term::SentEvent::SentEvent(Board& board, Term& term): Scheduler::Event(board, 0, "Term::SentEvent"), t(term) {
}


/**
 */
void Term::SentEvent::run(void) {
	t.nextChar();
}


/**
 * Called to send data back to the serial communication.
 * @param data		Buffer pointer.
 * @param len		Buffer length.
 */
void Term::sendData(const char* data, int len) {
	if(!started)
		return;
	TRACE(qDebug() << "DEBUG: receiving " << len << "bytes: ";)
	for(int i = 0; i < len; i++) {
		TRACE(qDebug() << i << " -> " << data[i]<< endl;)   ////
		buf.enqueue(data[i]);
	}
	if(!sending) {
		sending = true;
		event.reschedule(board().scheduler()->time() + delay);
	}
}


/**
 * Called to send the next character.
 */
void Term::nextChar(void) {
	char c = buf.dequeue();
	TRACE(qDebug() << "DEBUG: sending " << c;)
	out.send(board().scheduler()->time(), c);
	if(!buf.empty())
		event.reschedule(board().scheduler()->time() + delay);
	else
		sending = false;
}


/**
 * Put the default serial link configuration.
 */
void Term::defaultConf(void) {
	
	// record the default configuration
	mode = Pin::NORMAL;
	wordl = 8;
	sync = false;
	parity = Pin::NOPAR;
	nbstop = 2;
	msb = Pin::MSB_LAST;
	rate = 57600;
	initDelay();
	
	// set the out pin
	out.set(Pin::MODE, mode);
	out.set(Pin::WORDL, wordl);
	out.set(Pin::SYNC, sync);
	out.set(Pin::PARITY, parity);
	out.set(Pin::NBSTOP, nbstop);
	out.set(Pin::MSB, msb);
	out.set(Pin::RATE, rate);
}


/**
 * Initialize the delay variable.
 */
int Term::initDelay(void) {
	int size = wordl + 1;
	if(parity != Pin::NOPAR)
		size++;
	size += nbstop / 2 + 1;
	delay = board().mck() * size / rate;
	if(delay < 1)
		delay = 1;
}


/**
 */
Term::InPin::InPin(Term& term): SerialPin(&term, "in"), _term(term) {
}


/**
 */
void Term::InPin::onWord(uint64_t time, uint32_t word) {
	 TRACE(qDebug() << "DEBUG: Term.onReceive(" << time << ", " << char(word) << ")";)
	_term.doUpdate(word);
}


/**
 */
void Term::InPin::onError(uint64_t time, QString id) {
	TRACE(qDebug() << "DEBUG: bad reception because of " << id;)
}


/**
 * Build the view of the console.
 */
Term::View::View(Term& _term, MainWindow& window): term(_term) {
	setAllowedAreas(Qt::AllDockWidgetAreas) ;
	setWindowTitle("Serial Terminal");
	term.widget = new TermWidget();
	setWidget(term.widget);
	term.widget->show();
	window.addDockWidget(Qt::RightDockWidgetArea, this) ;
	window.getViewMenu()->addAction(toggleViewAction());
}

extern "C" IComponent* init(Board& board, QDomElement xml, QString name) {
	return new Term(board, xml, name) ;
}

}	// bsim
