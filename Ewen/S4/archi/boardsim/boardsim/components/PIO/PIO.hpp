/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef COMPONENT_PIO
#define COMPONENT_PIO

#include <stdint.h>
#include "Board.hpp"
#include "IComponent.hpp"

#include <QDomElement>

namespace bsim {

// pre-declaration
class PIOPin;
class PeriphPin;
class EnablePin;
class PIORegisters;

// PIO
class PIO: public IComponent {
	friend class PIOPin;
	friend class PeriphPin;
	friend class EnablePin;

public : 
	PIO(Board& b, QDomElement dom, QString name);
    ~PIO(void);
	virtual void makeVisual(VisualGraph& graph);
    
    void setPER(uint32_t v) ;
    void setPDR(uint32_t v) ;
    inline uint32_t getPSR(void) const { return PSR; }
    void setOER(uint32_t v) ;
    void setODR(uint32_t v) ;
    inline uint32_t getOSR(void) const { return OSR; }
    void setSODR(uint32_t v) ;
    void setCODR(uint32_t v) ;
    inline uint32_t getODSR(void) const { return ODSR; }
    inline uint32_t getPDSR(void) const { return PDSR; }

     //////////////
    void setIER(uint32_t v) ;
    void setIDR(uint32_t v) ;
    inline uint32_t getIMR(void) const { return IMR; }
    uint32_t getISR(void);
    uint32_t getISRIO(void);
    uint32_t it();
    ////////////////

	// IComponent overload
	virtual void getDynamicConstants(QVector<Constant>& csts) const;

private:
	uint32_t PSR, ODSR, ABSR, OSR, A, B, EA, EB;
	inline uint32_t out(void) const { return (PSR & ODSR) | (~PSR & ((~ABSR & A) | (ABSR & B))); }
	// problem with documentation where ~OSR is taken instead
	inline uint32_t enable_out(void) const { return (PSR & OSR) | (~PSR & ((~ABSR & EA) | (ABSR & EB))); }
	uint32_t PDSR, IMR, ISR;
	void changeOut(uint32_t changes);
	void makeRange(VisualGraph& graph, QDomElement element);
	Pin *io[32];
	Pin *a[32], *ea[32], *b[32], *eb[32];	
	
	////////
	 Pin *outIT;
	 QVector<Constant *> constants;
	 bool constants_done;
};

}	// bsim

#endif
