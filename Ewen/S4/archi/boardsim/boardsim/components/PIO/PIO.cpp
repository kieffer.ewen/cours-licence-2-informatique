/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QLabel>
#include <QDebug>
#include <Display.hpp>
#include <IRegister.hpp>
#include <Scheduler.hpp>
#include "PIO.hpp"

//#define TRACE_PIO
#ifdef TRACE_PIO
#	define TRACE(c)		c
#else
#	define TRACE(c)
#endif

namespace bsim {

/**
 * @class PIO
 * Implements a PIO as used in ATMEL AT91SAM7 micro-controler.
 * 
 * PIO is responsible for managing IO on 32 bidirectional lines.
 * In addition, the PIO may also be traverse by two internal peripherals for each IO line.
 * 
 * @par Pins
 * @li @c 0 - @c 31 -- each of this number matches an IO line of the PIO,
 * @li @c A0 - @c A31 -- identify the peripheral A connected on the IO line of the same number,
 * @li @c B0 - @c B31 -- identify the peripheral B connected on the IO line of the same number,
 * @li @c EA0 - @c EA31 -- line to the peripheral A letting know if it enabled or not,
 * @li @c EAB - @c EB31 -- line to the peripheral B letting know if it enabled or not,
 * 
 * @par Configuration
 * No configuration.
 * 
 * @par Compatibility
 * @li @c PER -- ok
 * @li @c PDR -- ok
 * @li @c PSR -- ok
 * @li @c OER -- ok
 * @li @c ODR -- ok
 * @li @c OSR -- ok
 * @li @c IFER -- unsupported
 * @li @c IFDR -- unsupported
 * @li @c IFSR -- unsupported
 * @li @c SODR -- ok
 * @li @c CODR -- ok
 * @li @c ODSR -- ok
 * @li @c PDSR -- ok
 * @li @c IER -- ok
 * @li @c IDR -- ok
 * @li @c IMR -- ok
 * @li @c ISR -- ok
 * @li @c PUDR -- unsupported
 * @li @c PUER -- unsupported
 * @li @c PUSR -- unsupported
 * @li @c ASR -- unsupported
 * @li @c BSR -- unsupported
 * @li @c ABSR -- unsupported
 * @li @c OWER -- unsupported
 * @li @c OWDR -- unsupported
 * @li @c OWSR -- unsupported
 * 
 * @ingroup components
 */

/**
 * Constructor.
 * @param board		Current board.
 * @param xml		Configuration.
 * @param n			Component logic name.
 */
static const int ID_OSR = 0,
				 ID_ODSR = 1,
				 ID_ABSR = 2,
				 ID_PSR = 3,
				 ID_PDSR = 4,
				 ID_ISR = 5,
				 ID_IMR = 6,
				 ID_LASTR = 7;

inline int min(int x, int y) { return x > y ? y : x; }
inline int max(int x, int y) { return x > y ? x : y; }

// PIO pin
class PIOPin: public Pin {
public:
	PIOPin(PIO& _pio, int num): Pin(&_pio, QString::number(num)), n(num), pio(_pio) {
	}
	
	void onChange(uint64_t time, bool value) {
		uint32_t mask = 1 << n;
        TRACE(qDebug() << "PIO input signal Value = " << value <<" at " <<  pio.board().scheduler()->time() <<endl;)
        TRACE(qDebug() << "INT ID =" << n <<endl;)
	
		// set
		if(value) {
		
			if(!(pio.PDSR & mask)) {
				pio.PDSR |= mask;
				TRACE(qDebug() << "PDSR = " << QString::number(pio.PDSR);)
			}
		}
		
		// clear
		else {
			if(pio.PDSR & mask) {
				pio.PDSR &= ~mask;
				TRACE(qDebug() << "PDSR = " << QString::number(pio.PDSR);)
			}
		}
		
			pio.ISR |=mask;  
		    pio.outIT->change(pio.board().scheduler()->time(), pio.it());		
	}
	
private:
	int n;
	PIO& pio;
};


// PeriphPin class
class PeriphPin: public Pin {
public:
	PeriphPin(PIO& _pio, int num, bool b)
	:	Pin(&_pio, QString(b ? "B%1" : "A%1").arg(num)),
		n(num), pio(_pio) {
	}

	void onChange(uint64_t time, bool value) {
	}

private:
	int n;
	PIO& pio;
};


// EnablePin class
class EnablePin: public Pin {
public:
	EnablePin(PIO& _pio, int num, bool b)
	:	Pin(&_pio, QString(b ? "EB%1" : "EA%1").arg(num)),
		n(num), pio(_pio) {
	}

	void onChange(uint64_t time, bool value) {
	}

private:
	int n;
	PIO& pio;
};

class outPin: public Pin {
public:
	outPin(PIO& _pio, QString name): Pin(&_pio, name), pio(_pio) {
	}
	
	void onChange(uint64_t time, bool value) { 	 
	}
	
private:
	PIO& pio;
};

PIO::PIO(Board& board, QDomElement xml, QString n):
IComponent(board, xml),
constants_done(false) {
	
	// initialize registers
	setAddress(0xFFFFF400);
	PSR = 0xffffffff;
	ODSR = 0;
	ABSR = 0;
	OSR = 0;
	PDSR = 0;
	IMR = 0;
	ISR = 0;
	A = 0;
	B = 0;
	EB = 0;
	EA = 0;
	
	// initialize
	for(int i = 0; i < 32; i++) {
		add(io[i] = new PIOPin(*this, i));
		add(a[i] = new PeriphPin(*this, i, false));
		add(ea[i] = new EnablePin(*this, i, false));
		add(b[i] = new PeriphPin(*this, i, true));
		add(eb[i] = new EnablePin(*this, i, true));                
	}

    add(outIT= new outPin(*this, "INT")); 

	// configure from XML
	configure(xml);
		
	// declare the visible IO registers
	add(HardRegister32::make(board).name("PER").address(0x00).type(IRegister::BITS).setter(*this, &PIO::setPER));
	add(HardRegister32::make(board).name("PDR").address(0x04).type(IRegister::BITS).setter(*this, &PIO::setPDR));
	add(HardRegister32::make(board).name("PSR").address(0x08).type(IRegister::BITS).getter(*this, &PIO::getPSR));

	add(HardRegister32::make(board).name("OER").address(0x10).type(IRegister::BITS).setter(*this, &PIO::setOER));
	add(HardRegister32::make(board).name("ODR").address(0x14).type(IRegister::BITS).setter(*this, &PIO::setODR));
	add(HardRegister32::make(board).name("OSR").address(0x18).type(IRegister::BITS).getter(*this, &PIO::getPSR));

	add(HardRegister32::make(board).name("SODR").address(0x30).type(IRegister::BITS).setter(*this, &PIO::setSODR));
	add(HardRegister32::make(board).name("CODR").address(0x34).type(IRegister::BITS).setter(*this, &PIO::setCODR));
	add(HardRegister32::make(board).name("ODSR").address(0x38).type(IRegister::BITS).getter(*this, &PIO::getODSR));
	
	add(HardRegister32::make(board).name("PDSR").address(0x3C).type(IRegister::BITS).getter(*this, &PIO::getPDSR));
	
	
	
	add(HardRegister32::make(board).name("IER").address(0x40).type(IRegister::BITS).setter(*this, &PIO::setIER));
	add(HardRegister32::make(board).name("IDR").address(0x44).type(IRegister::BITS).setter(*this, &PIO::setIDR));
    add(HardRegister32::make(board).name("IMR").address(0x48).type(IRegister::BITS).getter(*this, &PIO::getIMR));
    add(HardRegister32WithSoftIO::make(board).name("ISR").address(0x4C).type(IRegister::BITS).getter(*this, &PIO::getISRIO,&PIO::getISR));

	// debug
	info(QString("initialized (base=%1)").arg(address(), 8, 16, QChar('0')));
}


/**
 */
PIO::~PIO() {
}


/**
 */
void PIO::makeVisual(VisualGraph& graph) {
	QDomNodeList nodes = configuration().elementsByTagName("range");
	for(int i = 0; i < nodes.count(); i++)
		makeRange(graph, nodes.at(i).toElement());
}


/**
 * Build the display for a range of pins.
 * @param element	Element to get configuration from.
 */
void PIO::makeRange(VisualGraph& graph, QDomElement element) {
	
	// get the pins range
	int low = max(0, min(31, element.attribute("low").toInt()));
	int high = max(0, min(31, element.attribute("high").toInt()));
	//qDebug() << "low=" << low << ", high=" << high;
	
	// get the configuration of the range
	VisualComponent *group = new VisualComponent(this);
	group->parse(element);
	Visual::dir_t side = Visual::parseDir(element.attribute("side"), Visual::EAST);
	
	// vertical range
	static const int size = 16;
	if(side == Visual::EAST || side == Visual::WEST) {
		group->lab_dir = Visual::VERTICAL;
		group->lab_xpos = side == Visual::EAST ? Visual::LEFT : Visual::RIGHT;
		group->h = group->lab_ypad * 2 + size * (high - low + 1);
		for(int i = low; i <= high; i++) {
			group->add(new VisualPin(*group, io[i], side, group->lab_ypad + size * (i - low) + size / 2, true));
			group->add(new VisualPin(*group, a[i], Visual::reverseDir(side), group->lab_ypad + size * (i - low) + size / 4, false));
			group->add(new VisualPin(*group, a[i], Visual::reverseDir(side), group->lab_ypad + size * (i - low) + size * 3 / 4, false));
		}
		/////
		group->add(new VisualPin(*group, outIT, Visual::WEST, (40)));
		
	}
	else {
		group->lab_dir = Visual::HORIZONTAL;
		group->lab_ypos = side == Visual::NORTH ? Visual::BOTTOM : Visual::TOP;
		group->w = group->lab_xpad * 2 + size * (high - low + 1);
		for(int i = low; i <= high; i++) {
			group->add(new VisualPin(*group, io[i], side, group->lab_ypad + size * (i - low) + size / 2, true));
			group->add(new VisualPin(*group, a[i], Visual::reverseDir(side), group->lab_ypad + size * (i - low) + size / 4, false));
			group->add(new VisualPin(*group, a[i], Visual::reverseDir(side), group->lab_ypad + size * (i - low) + size * 3 / 4, false));
		}
	}
	
	// build the box
	group->make(name());
	group->makePins();
	graph.add(group);
}


/**
 * Changes the output pins according to the given mask.
 * @param changes	Mask with a 1 for each pin whose state changes.
 */
void PIO::changeOut(uint32_t changes) {
	for(int i = 0; i < 32; i++) {
		uint32_t mask = 1 << i;
		if(changes & mask)
			io[i]->change(board().scheduler()->time(), mask & out());
	}
}

/**
 * verifies if there is an interrupt to be asserted  
 */
uint32_t PIO::it() 
{ 	
	return (getISR() & IMR);  
}

void PIO::setIER (uint32_t val) {
	IMR |= val;
}

void PIO::setIDR (uint32_t val) {
	IMR &= ~val;
}

uint32_t PIO::getISR() { 
	uint32_t val= ISR;
    ISR=0;   
   
    return val;
}	

uint32_t PIO::getISRIO()
{
 return ISR;
}


void PIO::setPER (uint32_t val) {
	uint32_t before = out() & enable_out();
	PSR |= val;
	uint32_t after = out() & enable_out();
	changeOut(before ^ after);
}

void PIO::setPDR (uint32_t val) {
	uint32_t before = out() & enable_out();
	PSR &= ~val;
	uint32_t after = out() & enable_out();
	changeOut(before ^ after);
}

void PIO::setOER (uint32_t val) {
	uint32_t before = out() & enable_out();
	OSR |= val;
	uint32_t after = out() & enable_out();
	changeOut(before ^ after);
}

void PIO::setODR (uint32_t val) {
	uint32_t before = out() & enable_out();
	OSR &= ~val;
	uint32_t after = out() & enable_out();
	changeOut(before ^ after);
}

/**
 * Handle write to SODR.
 * @param addr	Written address.
 * @param size	Written size.
 * @param data	Data pointer (to uint32_t).
 */
void PIO::setSODR (uint32_t val) {
	uint32_t before = out() & enable_out();
	ODSR |= val;
	PDSR |= val;
	uint32_t after = out() & enable_out();
	changeOut(before ^ after);
}


/**
 * Handle write to SODR.
 */
void PIO::setCODR(uint32_t v) {
	uint32_t before = out() & enable_out();
	ODSR &= ~v;
	PDSR &= ~v;
	uint32_t after = out() & enable_out();
	changeOut(before ^ after);
}


/**
 */
void PIO::getDynamicConstants(QVector<Constant>& csts) const {
	for(int i = 0; i < 32; i++)
		if(io[i]->connection()) {
			QString name = io[i]->connection()->component()->name();
			if(io[i]->connection()->component()->pins().size() > 1)
				name += "_" + io[i]->connection()->name();
			csts.append(Constant(name, 1 << i, false));
		}
}


/**
 * Called to build a new PIO.
 */
extern "C" IComponent* init(Board& board,QDomElement xml, QString name) {
	return new PIO(board, xml, name);
}

}	// bsim
