#include "Timer.hpp"

namespace timer
{
	Timer::Timer(Board& board, QDomElement element, QString n) :
		InterfaceComponent(board, element, n)
	{
		board.addMemoryCallback (0xFFFA0000 + 0x00, *this, &Timer::Empty, &Timer::CCR_Writer);

		board.addMemoryCallback (0xFFFA0000 + 0x04, *this, &Timer::CMR_Reader, &Timer::CMR_Writer);

		board.addMemoryCallback (0xFFFA0000 + 0x08, *this, &Timer::Empty, &Timer::Empty);
		board.addMemoryCallback (0xFFFA0000 + 0x0C, *this, &Timer::Empty, &Timer::Empty);

		board.addMemoryCallback (0xFFFA0000 + 0x10, *this, &Timer::CV_Reader, &Timer::CV_Writer);

		board.addMemoryCallback (0xFFFA0000 + 0x14, *this, &Timer::RA_Reader, &Timer::RB_Writer);
		board.addMemoryCallback (0xFFFA0000 + 0x18, *this, &Timer::RB_Reader, &Timer::RB_Writer);
		board.addMemoryCallback (0xFFFA0000 + 0x1C, *this, &Timer::RC_Writer, &Timer::RC_Writer);

		board.addMemoryCallback (0xFFFA0000 + 0x20, *this, &Timer::SR_Reader, &Timer::Empty);
		board.addMemoryCallback (0xFFFA0000 + 0x24, *this, &Timer::Empty, &Timer::IER_Writer);
		board.addMemoryCallback (0xFFFA0000 + 0x28, *this, &Timer::Empty, &Timer::IDR_Writer);
		board.addMemoryCallback (0xFFFA0000 + 0x2C, *this, &Timer::IMR_Reader, &Timer::Empty);

		board.addMemoryCallback (0xFFFE00C0, *this, &Timer::BCR_Reader, &Timer::BCR_Writer);
		board.addMemoryCallback (0xFFFE00C4, *this, &Timer::BMR_Reader, &Timer::BMR_Writer);
	}

	/**
	 * Set the activation of the Timer
	 * 
	 * TC_CCR[0] = 1 : the counter clock is enable
	 * TC_CCR[1] = 1 : the counter clock is disable
	 * TC_CCR[2] = 1 : the software trigger is activate
	 */
	void Timer::CCR_Writer(arm_address_t addr, int size, void* ptr_Data)
	{
		Register* ptr_Reg;

		qDebug() << "Read CCR register";

		ptr_Reg = get_ptr_RegisterByName("TC_CCR0");
		ptr_Reg->setValue(*(uint32_t*)ptr_Data);
		ptr_Widget->updateBinaryValue(ptr_Reg);
	}

	/**
	 * Set the configuration of the Timer
	 */
	void Timer::CMR_Writer(arm_address_t addr, int size, void* ptr_Data)
	{
		Register* ptr_Reg;

		qDebug() << "Write CMR register";

		ptr_Reg = get_ptr_RegisterByName("TC_CMR0");
		ptr_Reg->setValue(*(uint32_t*)ptr_Data);
		ptr_Widget->updateBinaryValue(ptr_Reg);
	}

	/**
	 * Read the configuration of the Timer
	 */
	void Timer::CMR_Reader(arm_address_t addr, int size, void* ptr_Data)
	{
		qDebug() << "Read CMR register";
	}

	/**
	 * Set the curent value of the counter
	 */
	void Timer::CV_Writer(arm_address_t addr, int size, void* ptr_Data)
	{
		Register* ptr_Reg;
		
		qDebug() << "Write CV register";
		
		ptr_Reg = get_ptr_RegisterByName("TC_CV0");
		ptr_Reg->setValue(*(uint32_t*)ptr_Data);
		ptr_Widget->updateBinaryValue(ptr_Reg);
	}

	/**
	 * Read the curent value of the counter
	 */
	void Timer::CV_Reader(arm_address_t addr, int size, void* ptr_Data)
	{
		qDebug() << "Read CV register";
	}

	/**
	 * useless for the moment
	 */
	void Timer::RA_Reader(arm_address_t addr, int size, void* ptr_Data)
	{
		qDebug() << "Read RA register";
	}

	/**
	 * useless for the moment
	 */
	void Timer::RA_Writer(arm_address_t addr, int size, void* ptr_Data)
	{
		Register* ptr_Reg;

		ptr_Reg = get_ptr_RegisterByName("TC_RA0");
		ptr_Reg->setValue(*(uint32_t*)ptr_Data);
		ptr_Widget->updateBinaryValue(ptr_Reg);

		qDebug() << "Write RA register";
	}
	
	/**
	 * useless for the moment
	 */
	void Timer::RB_Reader(arm_address_t addr, int size, void* ptr_Data)
	{
		qDebug() << "Read RB register";
	}

	/**
	 * useless for the moment
	 */
	void Timer::RB_Writer(arm_address_t addr, int size, void* ptr_Data)
	{
		Register* ptr_Reg;

		qDebug() << "Write RB register";

		ptr_Reg = get_ptr_RegisterByName("TC_RB0");
		ptr_Reg->setValue(*(uint32_t*)ptr_Data);
		ptr_Widget->updateBinaryValue(ptr_Reg);
	}

	/**
	 * Get the value that the counter has to obtain to trigger an interruption
	 */
	void Timer::RC_Reader(arm_address_t addr, int size, void* ptr_Data)
	{
		qDebug() << "Read RC register";
	}

	/**
	 * Set the value that the counter has to obtain to trigger an interruption
	 */
	void Timer::RC_Writer(arm_address_t addr, int size, void* ptr_Data)
	{
		Register* ptr_Reg;

		qDebug() << "Write RC register";

		ptr_Reg = get_ptr_RegisterByName("TC_RC0");
		ptr_Reg->setValue(*(uint32_t*)ptr_Data);
		ptr_Widget->updateBinaryValue(ptr_Reg);
	}

	/**
	 * Get the state of the Timer
	 */
	void Timer::SR_Reader(arm_address_t addr, int size, void* ptr_Data)
	{
		Register* ptr_Reg;
		uint32_t dataValue;
		
		qDebug() << "Read SR register";

		dataValue = *(uint32_t*)ptr_Data;
		dataValue = 0;

		ptr_Reg = get_ptr_RegisterByName("TC_SR0");
		ptr_Reg->setValue(dataValue);
		ptr_Widget->updateBinaryValue(ptr_Reg);
	}

	void Timer::IER_Writer(arm_address_t addr, int size, void* ptr_Data)
	{
		Register* ptr_Reg;

		qDebug() << "Write IER register";

		ptr_Reg = get_ptr_RegisterByName("TC_IER0");
		ptr_Reg->setValue(*(uint32_t*)ptr_Data);
		ptr_Widget->updateBinaryValue(ptr_Reg);
	}

	void Timer::IDR_Writer(arm_address_t addr, int size, void* ptr_Data)
	{
		Register* ptr_Reg;

		qDebug() << "Write IDR register";

		ptr_Reg = get_ptr_RegisterByName("TC_IDR0");
		ptr_Reg->setValue(*(uint32_t*)ptr_Data);
		ptr_Widget->updateBinaryValue(ptr_Reg);
	}

	void Timer::IMR_Reader(arm_address_t addr, int size, void* ptr_Data)
	{

		qDebug() << " Read IMR register";
	}

	/**
	 * Common register for all Timers
	 */
	void Timer::BCR_Reader(arm_address_t addr, int size, void* ptr_Data)
	{
		qDebug() << "Read BCR register";
	}

	/**
	 * Common register for all Timers
	 */
	void Timer::BCR_Writer(arm_address_t addr, int size, void* ptr_Data)
	{
		Register* ptr_Reg;
		
		qDebug() << "Write BCR register";

		ptr_Reg = get_ptr_RegisterByName("TC_BCR");
		ptr_Reg->setValue(*(uint32_t*)ptr_Data);
		ptr_Widget->updateBinaryValue(ptr_Reg);
	}

	/**
	 * Common register for all Timers
	 */
	void Timer::BMR_Reader(arm_address_t addr, int size, void* ptr_Data)
	{
		qDebug() << "Read BMR register";
	}

	/**
	 * Common register for all Timers
	 */
	void Timer::BMR_Writer(arm_address_t addr, int size, void* ptr_Data)
	{
		Register* ptr_Reg;

		qDebug() << "Write BMR register";

		ptr_Reg = get_ptr_RegisterByName("TC_BMR");
		ptr_Reg->setValue(*(uint32_t*)ptr_Data);
		ptr_Widget->updateBinaryValue(ptr_Reg);
	}

	void Timer::Empty(arm_address_t addr, int size, void* ptr_Data)
	{
		qDebug() << "Empty function";
	}

	Timer::~Timer()
	{}

	extern "C" IComponent* init(Board& board, QDomElement element, QString name)
	{
		return new Timer(board, element, name);
	}
}
