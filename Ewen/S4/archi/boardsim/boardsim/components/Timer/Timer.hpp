#ifndef TIMER_H
#define TIMER_H

#include "InterfaceComponent.hpp"

using namespace interfaceComponent;

namespace timer
{
	class Timer : public InterfaceComponent
	{

	public :
		Timer(Board&, QDomElement, QString);

		void CCR_Writer(arm_address_t addr, int size, void* ptr_data);

		void CMR_Reader(arm_address_t addr, int size, void* ptr_data);
		void CMR_Writer(arm_address_t addr, int size, void* ptr_data);

		void CV_Writer(arm_address_t addr, int size, void* ptr_data);
		void CV_Reader(arm_address_t addr, int size, void* ptr_data);

		void RA_Reader(arm_address_t addr, int size, void* ptr_data);
		void RA_Writer(arm_address_t addr, int size, void* ptr_data);

		void RB_Reader(arm_address_t addr, int size, void* ptr_data);
		void RB_Writer(arm_address_t addr, int size, void* ptr_data);

		void RC_Reader(arm_address_t addr, int size, void* ptr_data);
		void RC_Writer(arm_address_t addr, int size, void* ptr_data);

		void SR_Reader(arm_address_t addr, int size, void* ptr_data);

		void IER_Writer(arm_address_t addr, int size, void* ptr_data);
		void IDR_Writer(arm_address_t addr, int size, void* ptr_data);

		void IMR_Reader(arm_address_t addr, int size, void* ptr_data);

		void BCR_Reader(arm_address_t addr, int size, void* ptr_data);
		void BCR_Writer(arm_address_t addr, int size, void* ptr_data);

		void BMR_Reader(arm_address_t addr, int size, void* ptr_data);
		void BMR_Writer(arm_address_t addr, int size, void* ptr_data);

		void Empty(arm_address_t addr, int size, void* ptr_data);

		~Timer();
	};
}

#endif
