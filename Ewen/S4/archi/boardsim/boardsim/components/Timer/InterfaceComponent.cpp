#include "InterfaceComponent.hpp"

namespace interfaceComponent
{
	InterfaceComponent::InterfaceComponent(Board& b, QDomElement element, QString n)
	{
		Parser parser(element);
		
		registerSize = 32;
		name = n;
		ptr_Board = &b;
		ptr_Registers = parser.getRegisters();
	
		ptr_Widget = new RegistersView(ptr_Registers);
		ptr_Board->addGUI(ptr_Widget, name);
	}

	Register* InterfaceComponent::get_ptr_RegisterByName(const QString name)
	{
		int i;
		Register* ptr_Ret;
		i = 0;
		
		while(i < ptr_Registers->size())
		{
			if(ptr_Registers->value(i)->getName().compare(name) == 0)
			{
				ptr_Ret = ptr_Registers->value(i);
				break;
			}
			i++;
		}
		return ptr_Ret;
	}

	Register* InterfaceComponent::get_ptr_RegisterByAddr(const QString addr)
	{
		int i;
		Register* ptr_Ret;
		i = 0;
		
		while(i < ptr_Registers->size())
		{
			if(ptr_Registers->value(i)->getAddress().compare(addr) == 0)
			{
				ptr_Ret = ptr_Registers->value(i);
				break;
			}
			i++;
		}
		return ptr_Ret;
	}
}
