#include "RegistersView.hpp"

namespace interfaceComponent
{
	RegistersView::RegistersView()
	{
		qDebug() << "Tromperie !!!!!!! !!";
	}

	RegistersView::RegistersView(const QVector<Register*>* ptr_ListRegisters) :
		QTableWidget(ptr_ListRegisters->size(), 2)
	
	{
		qDebug() << "Creation des widgets !!";
		
		int i;
		setHorizontalHeaderItem(0, new QTableWidgetItem("Register"));
		setHorizontalHeaderItem(1, new QTableWidgetItem("Value"));

	
		i = 0;
		for(; i < ptr_ListRegisters->size(); i++)
		{
			setItem(i, 0, new QTableWidgetItem(ptr_ListRegisters->value(i)->getName()));
			
			/**
			 * By default, registers value is showed in binary
			 */
			setItem(i, 1, new QTableWidgetItem(getBinaryValue(ptr_ListRegisters->value(i))));
		}
	}

	void RegistersView::updateBinaryValue(Register* ptr_Reg)
	{
		int i;
		QTableWidgetItem* ptr_Widget;
		
		ptr_Widget = findWidget(ptr_Reg->getName());
		ptr_Widget->setText(getBinaryValue(ptr_Reg));
		editItem(ptr_Widget);
	}

	void RegistersView::updateDecimalValue(Register* ptr_Reg)
	{
		int i;
		QTableWidgetItem* ptr_Widget;
		
		ptr_Widget->setText(QString::number(ptr_Reg->getDecimalValue()));
		ptr_Widget->setText(getBinaryValue(ptr_Reg));
		editItem(ptr_Widget);
	}

	QTableWidgetItem* RegistersView::findWidget(const QString name)
	{
		QTableWidgetItem* ptr_Widget;
		int i;

		i = 0;
		while(i < rowCount())
		{
			if(item(i, 0)->text().compare(name) == 0)
			{
				ptr_Widget = item(i, 1);
				break;
			}
			i++;
		}

		return ptr_Widget;
	}

	QString RegistersView::getBinaryValue(Register* ptr_Reg)
	{
		QString ret;
		int i;
		bool* ptr_RegisterValue;

		ptr_RegisterValue = ptr_Reg->getBinaryValue();
		
		for(i = ptr_Reg->getSize() - 1; i >= 0; i--)
			if(ptr_RegisterValue[i])
				ret += "1";
			else
				ret += "0";

		return ret;
	}

	RegistersView::~RegistersView()
	{}

}
