add_library(Thermistor SHARED "Thermistor.cpp")
target_link_libraries(Thermistor bsim ${QT_LIBRARIES})
install(TARGETS Thermistor LIBRARY DESTINATION "lib/bsim/physics")
install(FILES thermometer.png DESTINATION "lib/bsim/physics")
