/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>
#include <QPixmap>

#include <Board.hpp>
#include <Display.hpp>
#include <IComponent.hpp>
#include <Scheduler.hpp>

//#define THERMISTOR_DEBUG
#ifdef THERMISTOR_DEBUG
#	define TRACE(c)		c
#else
#	define TRACE(c)
#endif

namespace bsim { namespace physics {

/**
 */
class Thermistor: public IComponent {
public:
	static const int width = 30, height = 90;

	Thermistor(Board& board, QDomElement xml, QString name)
	:	IComponent(board, xml),
		OUT(this, "OUT"),
		IN(*this),
		_min_temp(0),
		_max_temp(0),
		_min_volt(0),
		_max_volt(0)
	{
		add(&OUT);
		add(&IN);
		IComponent::configure(xml);
	}

	virtual void configure(Configuration& conf, IMonitor& monitor) {
		_min_temp = conf.get("min-temp", -10.0);
		_max_temp = conf.get("max-temp", 150.0);
		_min_volt = conf.get("min-volt", 0.0);
		_max_volt = conf.get("max-volt", 5.0);		
	}

	void makeVisual(VisualGraph& graph) {
	
		// configure the component
		VisualComponent *vcomp = new VisualComponent(this);
		vcomp->w = width;
		vcomp->h = height;
		vcomp->parse(configuration());
		
		// put the pins
		vcomp->add(new VisualPin(*vcomp, &IN, Visual::EAST, vcomp->h - 10));
		vcomp->add(new VisualPin(*vcomp, &OUT, Visual::WEST, 10));

		// prepare UI
		icon = QPixmap(board().prefix() + "/physics/thermometer.png");
		icon = icon.scaled(vcomp->w,vcomp->h, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
		QGraphicsPixmapItem *item = new QGraphicsPixmapItem(icon);
		vcomp->addToGroup(item);
		vcomp->setPos(vcomp->x, vcomp->y);
		vcomp->makeLabel(name());
		vcomp->makePins();
		graph.add(vcomp);
	}	

private:

	void update(time_t t, double temp) {
		double r = (temp - _min_temp) * (_max_volt - _min_volt)
				 / (_max_temp - _min_temp) + _min_volt;
		if(r < _min_volt)
			r = _min_volt;
		else if(r > _max_volt)
			r = _max_volt;
		TRACE(qDebug() << "Therm: T=" << temp << ", V=" << r
				 << ", T range=[" << _min_temp << ", " << _max_temp << "]");
		OUT.sendAnalog(t, r);
	}

	class InPin: public Pin {
	public:
		InPin(Thermistor& therm): Pin(&therm, "IN"), _therm(therm) { }
		virtual void getPhysics(time_t time, double value, unit_t unit)
			{ if(unit == HEAT) _therm.update(time, value); }
	private:
		Thermistor& _therm;
	} IN;

	Pin OUT;
	double _min_temp, _max_temp;
	double _min_volt, _max_volt;
	QPixmap icon;
};

/**
 * Called to build a new PWM.
 */
extern "C" IComponent* init(Board& board,QDomElement xml, QString name) {
	return new Thermistor(board, xml, name);	
}
		
} }	// bsim::electric 
