/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#define BSIM_BOILER_SVG

#include <QDebug>

#include <Board.hpp>
#include <Display.hpp>
#include <IComponent.hpp>
#include <Scheduler.hpp>
#ifdef BSIM_BOILER_SVG
#	include <QGraphicsSvgItem>
#endif

//#define BOILER_DEBUG
#ifdef BOILER_DEBUG
#	define TRACE(c)	c
#else
#	define TRACE(c)
#endif

namespace bsim { namespace physics {

/**
 * Component simulating a boiler:
 * @li containing a mass m of liquid which as specific mass heat capacity,
 * @li an alimentation,
 * @li a thermic leakage representing the loss of energy due to the environment.
 * As a default, the boiler is configured to contains 10cl of water
 * with an environment temperature of 20 °C.
 * 
 * The class must be "physics/Boiler" inside the board description.
 * 
 * The following pins are available:
 * @li @c POWER (electric interface) -- to power the boiler,
 * @li @c TEMP (physics interfgace) -- to measure the temperature.
 * 
 * The following attributes can be used to configure the boiler:
 * @li @c heat-capacity -- mass heat capacity (in °C*s/kg)
 * @li @c mass -- mass of liquid (in kg)
 * @li @c leakage -- heat leakage due to the environment (in W)
 * @li @c temperature -- initial temperature (in °C)
 * @li @c ambiant-temp -- ambiant temperature (in °C)
 * @li @c frequency -- simulation update frequency (default to 4Hz).
 * 
 */
class Boiler: public IComponent {
public:
	static const int
		bubble_cnt = 8,
		bubble_bot = 10,
		bubble_top = 20,
		bubble_size = 8;

	Boiler(Board& board, QDomElement xml, QString name)
	:	IComponent(board, xml),
		TICK(*this),
		POWER(*this),
		TEMP(this, "TEMP"),
		_cap(0),
		_mass(0),
		_leak(0),
		_temp(0),
		_pow(0),
		_last(0),
		_init_temp(0),
		_min_temp(0),
		_freq(25),
		text(nullptr),
		_cur_pow(0),
		_max_temp(100),
		bank(nullptr),
		vcomp(nullptr)
	{
		add(&POWER);
		add(&TEMP);
		bubble_color = QColor(0xeb, 0xf0, 0xf5);
		IComponent::configure(xml);
	}

	void start (void) override {
		_temp = _init_temp;
		_cur_pow = 0;
		_pow = 0;
		_last = 0;
		updateDisplay(true);
		TICK.start();
	}

	void makeVisual(VisualGraph& graph) {

		// configure the component
		vcomp = new VisualComponent(this);
		vcomp->w = 80;
		vcomp->h = 100;
		vcomp->parse(configuration());
		
		// put the pins
		vcomp->add(new VisualPin(*vcomp, &POWER, Visual::SOUTH, vcomp->w / 2));
		vcomp->add(new VisualPin(*vcomp, &TEMP, Visual::WEST, 10));

		// build all
		vcomp->parse(configuration());

		// prepare UI
#		ifdef BSIM_BOILER_SVG
		bank = new QGraphicsSvgItem(board().prefix() + "/physics/tank.svg");
		bank->setPos(vcomp->x, vcomp->y);
		vcomp->w = bank->boundingRect().width();
		vcomp->h = bank->boundingRect().height();
		vcomp->addToGroup(bank);
#		else
			vcomp->makeBox();
#		endif
		vcomp->makePins();
		text = new QGraphicsSimpleTextItem(makeVisualName());
		text->setPos(vcomp->x + vcomp->w / 2 - 25, vcomp->y + vcomp->h / 2 - 10);
		text->setZValue(200);
		vcomp->addToGroup(text);
		graph.add(vcomp);

		// initialize bubbles
		for(int i = 0; i < bubble_cnt; i++)
			bubs[i] = new Bubble(*this);
	}
	
	virtual void configure(Configuration &conf, IMonitor &monitor) {
		_cap = conf.get("heat-capacity", 4200.);
		_mass = conf.get("mass", 0.01);
		_leak = conf.get("leakage", 1.);
		_init_temp = conf.get("temperature", 20.0);
		_temp = _init_temp;
		_min_temp = conf.get("ambiant-temp", 20.0);
		_max_temp = conf.get("max-temp", _max_temp);
		_freq = conf.get("frequency", 4);
	}
	
private:

	class Bubble: public QGraphicsEllipseItem {
	public:
		Bubble(Boiler& _b)
		: 	QGraphicsEllipseItem(0, 0, bubble_size, bubble_size),
			b(_b), x(0), y(0), stopped(true)
		{
			setVisible(false);
			setPen(b.bubble_color);
			setBrush(b.bubble_color);
			setZValue(100);
			b.vcomp->addToGroup(this);
		}

		bool isRunning() {
			return !stopped;
		}

		void reset() {
			stopped = true;
			setVisible(false);
		}

		void start() {
			x = qrand() % (b.vcomp->w - bubble_size) + b.vcomp->x;
			y = b.vcomp->y + b.vcomp->h - bubble_bot;
			setPos(x, y);
			setVisible(true);
			stopped = false;
		}

		void stop() {
			stopped = true;
		}

		void step(int s) {
			y -= s;
			if(y < b.vcomp->y + bubble_top) {
				if(stopped)
					setVisible(true);
				else
					start();
			}
			else
				setPos(x, y);
		}

	private:
		Boiler& b;
		int x, y;
		bool stopped;
	};

	QString makeVisualName(void) {
		return QString("%1\n%2 C").arg(name()).arg(_temp, 5, 'f', 2);
	}

	void updateDisplay(bool reset = false) {
		int cnt = bubble_cnt - (_max_temp - _temp) * (bubble_cnt + 1) / (_max_temp - _min_temp);
		if(cnt < 0)
			cnt = 0;
		else if(cnt > bubble_cnt)
			cnt = bubble_cnt;
		for(int i = 0; i < bubble_cnt; i++) {
			if(i >= cnt) {
				if(bubs[i]->isRunning()) {
					if(reset)
						bubs[i]->reset();
					else
						bubs[i]->stop();
				}
			}
			else {
				if(bubs[i]->isRunning())
					bubs[i]->step(cnt * (vcomp->h - bubble_bot - bubble_top) / _freq / 5);
				else
					bubs[i]->start();
			}
		}
		text->setText(makeVisualName());
	}

	void update(time_t t) {

		// compute actual power
		double dt = (t - _last);
		_pow += _cur_pow * dt;

		// compute leakage
		double st = (_temp - _min_temp) * _leak / _freq;

		// compute provided temperature
		double at = _pow / _cap / _mass / board().mck();

		// derive temperature
		_temp = _temp + at - st;
		if(_temp < _min_temp)
			_temp = _min_temp;
		else if(_temp > _max_temp)
			_temp = _max_temp;
		_last = t;
		TRACE(qDebug() << "Boiler: t=" << _temp
			<< ", heat=" << at
			<< ", leak=" << st);
		TEMP.setPhysics(t, _temp, Pin::HEAT);

		// reset count
		_pow = 0;
		_last = t;

		// animate bubbles
		updateDisplay();
	}
	
	void updateTick(time_t t) {
		update(t);
	}

	class Tick: public Scheduler::Event {
	public:
		Tick(Boiler& boiler): Scheduler::Event(boiler.board()), _boiler(boiler) { }
		void start(void)		{ reschedule(time() + _boiler.board().mck() / _boiler._freq); }
		virtual void run(void)	{ _boiler.updateTick(time()); start(); }
	private:
		Boiler& _boiler;
	} TICK;
	
	class POWERPin: public Pin {
	public:
		POWERPin(Boiler& boiler): Pin(&boiler, "POWER"), _boiler(boiler) { }
	protected:
		void getElectric(time_t t, double U, double I) override {
			double W = U * I;
			_boiler._pow += _boiler._cur_pow * (t - _boiler._last);
			_boiler._cur_pow = W;
			_boiler._last = t;
		}
	private:
		Boiler& _boiler;
	} POWER;

	int _freq;
	double _cap, _mass, _leak, _temp, _pow, _last, _init_temp, _min_temp, _max_temp;
	Pin TEMP;
	QGraphicsSimpleTextItem *text;
	double _cur_pow;
	VisualComponent *vcomp;
	Bubble *bubs[bubble_cnt];
#	ifdef BSIM_BOILER_SVG
		QGraphicsSvgItem *bank;
#	endif
	QColor bubble_color;
};

/**
 * Called to build a new Boiler.
 */
extern "C" IComponent* init(Board& board,QDomElement xml, QString name) {
	return new Boiler(board, xml, name);	
}
		
} }	// bsim::physics 
