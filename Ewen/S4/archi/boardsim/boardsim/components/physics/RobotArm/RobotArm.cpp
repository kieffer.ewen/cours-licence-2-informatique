/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>

#include <Board.hpp>
#include <Display.hpp>
#include <IComponent.hpp>
#include <Scheduler.hpp>
#include <QGraphicsSvgItem>
#include <QGraphicsPathItem>
#include <tgmath.h>

namespace bsim { namespace physics {

/**
 * Component simulating a robot arm, that is, two part controlled by PWM
 * It takes two servotor ANGLE and one electro-magnet as input.
 * Output two physical pin representing if black or white element is present
 *
 * The class must be "physics/RobotArm" inside the board description.
 *
 * The following pins are available:
 * @li @c ARM1  -- Servo1 ANGLE input.
 * @li @c ARM2  -- Servo2 ANGLE input.
 * @li @c ACT   -- input activate the electro-magnet
 * @li @c BLK 	-- Black sensor output.
 * @li @c WHT 	-- White sensor output.
 */
class RobotArm: public IComponent {
public:
	typedef enum {
		BLACK	= 0,
		WHITE	= 1
	} color_t;

	static const int
		HANDLE_SIZE = 100,
		HANDLE_SMALL = 14,
		HANDLE_BIG = 24,
		ANGLE1_ORG = 270,
		ANGLE2_ORG = 360;

	int
		DX1 = 30,
		DY1 = 0,
		DX2 = DX1,
		DY2 = DY1-HANDLE_SIZE,
		DXE = DX2+HANDLE_SIZE-5,
		DYE = DY2+25,
		DXCUBE = DX1+50,
		DYCUBE = DY1,
		DXBS = DX1+100,
		DYBS = DY1+30,
		DXWS = DX1+150,
		DYWS = DY1+30,
		VCOMP_X,
		VCOMP_Y;

	color_t CURRENT_COLOR;
	bool MAGNET_ON = false;

	RobotArm(Board& board, QDomElement xml, QString name)
	:	IComponent(board, xml),
		TICK(*this),
		ARM1(*this),
		ARM2(*this),
		ACT(*this),
		BLK(this, "BLK"),
		WHT(this, "WHT"),
		_angle1(0),
		_angle2(0),
		_handle1(nullptr),
		_handle2(nullptr),
		_body(nullptr),
		_bStorage(nullptr),
		_emagnet(nullptr)
	{
		add(&ARM1);
		add(&ARM2);
		add(&ACT);
		add(&BLK);
		add(&WHT);
		IComponent::configure(xml);
	}

	void makeVisual(VisualGraph& graph) {
		VisualComponent *vcomp = new VisualComponent(this);

		// configure the component
		vcomp->parse(configuration());

		// build the display
		vcomp->add(new VisualPin(*vcomp, &ARM1, Visual::SOUTH, vcomp->w/6));
		vcomp->add(new VisualPin(*vcomp, &ARM2, Visual::SOUTH, (vcomp->w/6)*2));
		vcomp->add(new VisualPin(*vcomp, &ACT, Visual::SOUTH, (vcomp->w/6)*3));
		vcomp->add(new VisualPin(*vcomp, &BLK, Visual::SOUTH, (vcomp->w/6)*4));
		vcomp->add(new VisualPin(*vcomp, &WHT, Visual::SOUTH, (vcomp->w/6)*5));

		QPainterPath *p1 = new QPainterPath();
		p1->arcTo(-HANDLE_BIG / 2, 0, HANDLE_BIG, HANDLE_BIG, 90, 180);
		p1->lineTo(HANDLE_SIZE, HANDLE_BIG / 2 + HANDLE_SMALL / 2);
		p1->arcTo(HANDLE_SIZE - HANDLE_SMALL / 2, HANDLE_BIG / 2 - HANDLE_SMALL / 2, HANDLE_SMALL, HANDLE_SMALL, 270, 180);
		p1->lineTo(0, 0);
		_handle1 = new QGraphicsPathItem(*p1);
		_handle1->setTransformOriginPoint(0, HANDLE_BIG / 2);
		_handle1->setPos(vcomp->x + DX1 - HANDLE_BIG / 2, vcomp->y + DY1 - HANDLE_BIG / 2);
		_handle1->setBrush(Qt::blue);
		_handle1->setOpacity(true);
		_handle1->setRotation(ANGLE1_ORG);
		vcomp->addToGroup(_handle1);

		_handle2 = new QGraphicsPathItem(*p1);
		_handle2->setTransformOriginPoint(0, HANDLE_BIG / 2);
		_handle2->setPos(vcomp->x + DX2 - HANDLE_BIG / 2, vcomp->y + DY2 - HANDLE_BIG / 2);
		_handle2->setBrush(Qt::blue);
		_handle2->setOpacity(true);
		_handle2->setRotation(ANGLE2_ORG);
		vcomp->addToGroup(_handle2);

		p1 = new QPainterPath();
		p1->lineTo(5, 0);
		p1->lineTo(5, -5);
		p1->lineTo(5, 0);
		p1->lineTo(10, 0);
		p1->lineTo(10, 10);
		p1->lineTo(0, 10);
		p1->lineTo(0, 0);
		_emagnet = new QGraphicsPathItem(*p1);
		_emagnet->setPos(vcomp->x + DXE - HANDLE_BIG / 2, vcomp->y + DYE - HANDLE_BIG / 2);
		_emagnet->setBrush(Qt::gray);
		_emagnet->setOpacity(true);
		vcomp->addToGroup(_emagnet);

		p1 = new QPainterPath();
		p1->lineTo(20, 0);
		p1->lineTo(20, 20);
		p1->lineTo(0, 20);
		p1->lineTo(0, 0);
		_cube = new QGraphicsPathItem(*p1);
		changeColor();
		_cube->setPos(vcomp->x + DXCUBE - HANDLE_BIG / 2, vcomp->y + DYCUBE - HANDLE_BIG / 2);
		_cube->setOpacity(true);
		vcomp->addToGroup(_cube);

		p1 = new QPainterPath();
		p1->lineTo(2, 0);
		p1->lineTo(2, 30);
		p1->lineTo(32, 30);
		p1->lineTo(32, 0);
		p1->lineTo(34, 0);
		p1->lineTo(34, 34);
		p1->lineTo(0, 34);
		p1->lineTo(0, 0);
		_bStorage = new QGraphicsPathItem(*p1);
		_bStorage->setPos(vcomp->x + DXBS - HANDLE_BIG / 2, vcomp->y + DYBS - HANDLE_BIG / 2);
		_bStorage->setBrush(Qt::black);
		_bStorage->setOpacity(true);
		vcomp->addToGroup(_bStorage);

		_wStorage = new QGraphicsPathItem(*p1);
		_wStorage->setPos(vcomp->x + DXWS - HANDLE_BIG / 2, vcomp->y + DYWS - HANDLE_BIG / 2);
		_wStorage->setBrush(Qt::white);
		_wStorage->setOpacity(true);
		vcomp->addToGroup(_wStorage);

		vcomp->lab_ypos = Visual::BELOW;
		vcomp->lab_ypad = 10;
		vcomp->makeLabel(name());
		graph.add(vcomp);
		VCOMP_X = vcomp->x;
		VCOMP_Y = vcomp->y;
	}

	virtual void start (void) {
		TICK.start();
		_angle1 = 0;
		_angle2 = 0;
	}

private:
	void changeColor(){
		time_t now = board().scheduler()->time();
		int color = rand()%2;
		if (color == 0){
			CURRENT_COLOR = BLACK;
			BLK.change(now, 1);
			WHT.change(now, 0);
			_cube->setBrush(Qt::black);
		} else {
			CURRENT_COLOR = WHITE;
			WHT.change(now, 1);
			BLK.change(now, 0);
			_cube->setBrush(Qt::white);
		}
		DXCUBE = DX1+50;
		DYCUBE = DY1;
		_cube->setPos(VCOMP_X + DXCUBE - HANDLE_BIG / 2, VCOMP_Y + DYCUBE - HANDLE_BIG / 2);
	}

	void updateArm1() {
		_handle1->setRotation(ANGLE1_ORG + _angle1);
		updateArm2();
	}

	void updateArm2() {
		_handle2->setRotation(ANGLE2_ORG + _angle2);
		DX2 = DX1 + sin(_angle1/60) * HANDLE_SIZE;
		DY2 = DY1 - cos(_angle1/60) * HANDLE_SIZE;
		_handle2->setPos(VCOMP_X + DX2 - HANDLE_BIG / 2, VCOMP_Y + DY2 - HANDLE_BIG / 2);
		DXE = DX2-5+cos(_angle2/60)*HANDLE_SIZE;
		DYE = DY2+25+sin(_angle2/60)*HANDLE_SIZE;
		_emagnet->setPos(VCOMP_X + DXE - HANDLE_BIG / 2, VCOMP_Y + DYE - HANDLE_BIG / 2);
		if(MAGNET_ON){
			DXCUBE = DXE;
			DYCUBE = DYE;
		_cube->setPos(VCOMP_X + DXCUBE - HANDLE_BIG / 2, VCOMP_Y + DYCUBE - HANDLE_BIG / 2);
		}

	}

	void changeArmAngle(time_t time, double value, Pin::unit_t unit, int armID) {
		if(unit == Pin::ANGLE){
			switch (armID){
				case 1 : _angle1 = value; updateArm1(); break;
				case 0 : _angle2 = value; updateArm2(); break;
				default : qDebug() << "ERROR: Invalid ARM id\n"; break;
			}
		} else {
			qDebug() << "ERROR: Unit type must be ANGLE \n";
		}
	}

	// Return true if magnet is on cube
	bool magnetOnCube(){
		return ((DXCUBE <= DXE) && (DXE <= DXCUBE+20) && (DYCUBE <= DYE) && (DYE <= DYCUBE+20));
	}

	// Return true if cube is on good storage
	bool cubeOnGoodStorage(){
		if (CURRENT_COLOR == BLACK){
			return ((DXBS+2 <= DXCUBE) && (DXCUBE <= DXBS+32) && (DYBS <= DYCUBE) && (DYCUBE <= DYBS+30));
		} else {
			return ((DXWS+2 <= DXCUBE) && (DXCUBE <= DXWS+32) && (DYWS <= DYCUBE) && (DYCUBE <= DYWS+30));
		}
	}

	void ACTEvent(uint64_t time, bool s) {
		if((!MAGNET_ON) && (magnetOnCube())){
			MAGNET_ON = true;
		}
		else if (MAGNET_ON) {
			MAGNET_ON = false;
		}
	}


	void updateCube(time_t t) {
		if((!MAGNET_ON) && cubeOnGoodStorage()){
			changeColor();
		}
	}

	void gravity(time_t t) {
		bool upToBs = (DXCUBE>=DXBS) && (DXCUBE<=DXBS+34);
		bool upToWs = (DXCUBE>=DXWS) && (DXCUBE<=DXWS+34);
		bool cond1 = (!MAGNET_ON) && (DYCUBE<DY1);
		bool cond2 = (upToBs || upToWs) && (DYCUBE<DY1+30);
		if(cond1 || cond2){
			DYCUBE += 1;
			_cube->setPos(VCOMP_X + DXCUBE - HANDLE_BIG / 2, VCOMP_Y + DYCUBE - HANDLE_BIG / 2);

		}
	}
	
	void update(time_t t) {
		gravity(t);
		updateCube(t);
	}

	class Tick: public Scheduler::Event {
	public:
		Tick(RobotArm& robot): Scheduler::Event(robot.board()), _RobotArm(robot) { }
		void start(void)		{ reschedule(time() + _RobotArm.board().mck() / 100); }
		void run(void) override { _RobotArm.update(time()); start(); }
	private:
		RobotArm& _RobotArm;
	} TICK;

	class ARM1Pin: public Pin {
	public:
		ARM1Pin(RobotArm& robot): Pin(&robot, "ARM1"), _RobotArm(robot) {}
		virtual void getPhysics(time_t time, double value, unit_t unit) {
			_RobotArm.changeArmAngle(time, value, unit, 0); 
		}
	private:
		RobotArm& _RobotArm;
	}ARM1;

	class ARM2Pin: public Pin {
	public:
		ARM2Pin(RobotArm& robot): Pin(&robot, "ARM2"), _RobotArm(robot) {}
		virtual void getPhysics(time_t time, double value, unit_t unit) {
			_RobotArm.changeArmAngle(time, value, unit, 1); 
		}
	private:
		RobotArm& _RobotArm;
	}ARM2;

	class ACTPin: public Pin {
	public:
		ACTPin(RobotArm& robot): Pin(&robot, "ACT"), _RobotArm(robot) {}
		virtual void onChange(uint64_t time, bool s) override {
			_RobotArm.ACTEvent(time, s);
		}
	private:
		RobotArm& _RobotArm;
	}ACT;
	
	Pin BLK, WHT;
	double _angle1, _angle2;

	QAbstractGraphicsShapeItem *_handle1;
	QAbstractGraphicsShapeItem *_handle2;
	QAbstractGraphicsShapeItem *_emagnet;
	QAbstractGraphicsShapeItem *_cube;
	QAbstractGraphicsShapeItem *_bStorage;
	QAbstractGraphicsShapeItem *_wStorage;
	QGraphicsSvgItem *_body;

};

/**
 * Called to build a new servo-motor.
 */
extern "C" IComponent* init(Board& board, QDomElement xml, QString name) {
	return new RobotArm(board, xml, name);
}

} }	// bsim::physics
