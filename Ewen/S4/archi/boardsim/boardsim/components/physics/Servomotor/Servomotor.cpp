/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QDebug>

#include <Board.hpp>
#include <Display.hpp>
#include <IComponent.hpp>
#include <Scheduler.hpp>
#include <QGraphicsSvgItem>
#include <QGraphicsPathItem>

namespace bsim { namespace physics {

/**
 * Component simulating a servo-motor, that is, a motor controlled by PWM
 * with a period of 2ms setting an angle between 0° (PWM 1ms) et 180° (PWM 2ms).
 * As input, it takes a PWM signal in the electricity domain of the micro-controller
 * and outputs a physical pin representing an angle in degree.
 * 
 * The class must be "physics/Servomotor" inside the board description.
 * 
 * The following pins are available:
 * @li @c PWM (micro-controller signal) -- PWM signal,
 * @li @c ANGLE (physics interface -- ) -- output angle in degree (°).
 * 
 * The following attributes can be used to configure the boiler:
 * @li @c min-pwm -- minimum width in PWM (in ns, default 1000),
 * @li @c max-pwm -- maximum width in PWM or period (in ns, default 2000),
 * @li @c min-angle -- minimum angle (in °, default 0°),
 * @li @c max-angle -- maximum angle (in °, default 180°),
 * @li @c velocity -- move velocity (in °/s, default 60°),
 * @li @c freq -- update frequency (in Hz, 10Hz).
 * 
 */
class Servomotor: public IComponent {
public:
	static const int
		HANDLE_DX = 50,
		HANDLE_DY = 6,
		HANDLE_SIZE = 60,
		HANDLE_SMALL = 14,
		HANDLE_BIG = 24;

	Servomotor(Board& board, QDomElement xml, QString name)
	:	IComponent(board, xml),
		TICK(*this),
		PWM(*this),
		ANGLE(this, "ANGLE"),
		_freq(50),
		_min_pwm(1000),
		_max_pwm(2000),
		_min_angle(0),
		_max_angle(180),
		_velocity(60),
		_min_pow(0),
		_max_pow(0),
		_pow(0),
		_angle(0),
		_target(0),
		_up(0),
		_handle(nullptr),
		_body(nullptr)
	{
		add(&PWM);
		add(&ANGLE);
		IComponent::configure(xml);
	}

	void makeVisual(VisualGraph& graph) {
		VisualComponent *vcomp = new VisualComponent(this);

		// get the SVG image
		_body = new QGraphicsSvgItem(board().prefix() + "/physics/servo-body.svg");

		// configure the component
		vcomp->parse(configuration());
		vcomp->w = _body->boundingRect().width();
		vcomp->h = _body->boundingRect().height();

		// build the display
		vcomp->add(new VisualPin(*vcomp, &PWM, Visual::SOUTH, vcomp->w * 9.5 / 10));
		_body->setPos(vcomp->x, vcomp->y);
		vcomp->addToGroup(_body);
		QPainterPath *p = new QPainterPath();
		p->arcTo(-HANDLE_BIG / 2, 0, HANDLE_BIG, HANDLE_BIG, 90, 180);
		p->lineTo(HANDLE_SIZE, HANDLE_BIG / 2 + HANDLE_SMALL / 2);
		p->arcTo(HANDLE_SIZE - HANDLE_SMALL / 2, HANDLE_BIG / 2 - HANDLE_SMALL / 2, HANDLE_SMALL, HANDLE_SMALL, 270, 180);
		p->lineTo(0, 0);
		_handle = new QGraphicsPathItem(*p);
		_handle->setTransformOriginPoint(0, HANDLE_BIG / 2);
		_handle->setPos(vcomp->x + HANDLE_DX - HANDLE_BIG / 2, vcomp->y + HANDLE_DY - HANDLE_BIG / 2);
		_handle->setBrush(Qt::white);
		_handle->setOpacity(true);
		_handle->setRotation(180 + _min_angle);
		vcomp->addToGroup(_handle);
		vcomp->lab_ypos = Visual::BELOW;
		vcomp->lab_ypad = -40;
		vcomp->makeLabel(name());
		graph.add(vcomp);
	}
	
	virtual void configure(Configuration &conf, IMonitor &monitor) {
		_freq = conf.get("freq", _freq);
		_min_pwm = conf.get("min-pwm", _min_pwm);
		_max_pwm = conf.get("max-pwm", _max_pwm);
		_min_angle = conf.get("min-angle", _min_angle);
		_max_angle = conf.get("max-angle", _max_angle);
		_velocity = conf.get("velocity", _velocity);
	}
	
	virtual void start (void) {
		TICK.start();
		_max_pow = board().mck() / _freq;
		_min_pow = _max_pow * _min_pwm / _max_pwm;
		_pow = 0;
		_angle = _min_angle;
		_target = _min_angle;
		_up = 0;
	}
	
private:

	void update(time_t t) {
		int _old_pow = _pow, _old_up = _up;

		// complete remaining time
		if(_up) {
			_pow += t - _up;
			_up = t;
		}

		// compute target angle
		if(_pow <= _min_pow)
			_target = _min_angle;
		else if(_pow >= _max_pow)
			_target = _max_angle;
		else
			_target = (_pow - _min_pow) * (_max_angle - _min_angle) / (_max_pow - _min_pow) + _min_angle;

		// compute the next step
		double s = _target - _angle;
		if(s > _velocity / _freq)
			s = _velocity / _freq;
		else if(s < -_velocity / _freq)
			s = -_velocity / _freq;
		_angle += s;
		ANGLE.setPhysics(t, _angle, Pin::ANGLE);

		// display the servo-motor
		_handle->setRotation(180 + _angle);

		// reset power
		_pow = 0;
	}
	
	class Tick: public Scheduler::Event {
	public:
		Tick(Servomotor& servo): Scheduler::Event(servo.board()), _servo(servo) { }
		void start(void)		{ reschedule(time() + _servo.board().mck() / _servo._freq); }
		void run(void) override { _servo.update(time()); start(); }
	private:
		Servomotor& _servo;
	} TICK;
	
	class PWMPin: public Pin {
	public:
		PWMPin(Servomotor& servo): Pin(&servo, "PWM"), _servo(servo) { }
	protected:
		void onChange(uint64_t time, bool s) override {
			if(s) {
				if(_servo._up == 0)
					_servo._up = time;
			}
			else {
				_servo._pow += time - _servo._up;
				_servo._up = 0;
			}
		}
	private:
		Servomotor& _servo;
	} PWM;

	Pin ANGLE;
	int _freq;
	double _min_pwm, _max_pwm, _min_angle, _max_angle, _velocity;
	int _min_pow, _max_pow, _pow;
	double _angle, _target;
	int _up;

	QAbstractGraphicsShapeItem *_handle;
	QGraphicsSvgItem *_body;
};

/**
 * Called to build a new servo-motor.
 */
extern "C" IComponent* init(Board& board,QDomElement xml, QString name) {
	return new Servomotor(board, xml, name);
}
		
} }	// bsim::physics 
