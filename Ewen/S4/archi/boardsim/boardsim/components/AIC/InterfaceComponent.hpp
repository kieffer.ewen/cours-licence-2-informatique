#ifndef INTERFACE_COMPONENT_H
#define INTERFACE_COMPONENT_H

#include "Board.hpp"
#include "IComponent.hpp"

#include "Parser.hpp"
#include "Register.hpp"
#include "RegistersView.hpp"

#include <QVector>
#include <QDomElement>
#include <QWidget>
#include <QLabel>
#include <QDebug>

namespace interfaceComponent
{
	class InterfaceComponent : public IComponent
	{
	public :
		InterfaceComponent(Board& board, QDomElement element, QString n);

	protected :
		Register* get_ptr_RegisterByName(const QString name);
		Register* get_ptr_RegisterByAddr(const QString addr);

		Board* ptr_Board;
		int address;
		QString name;
		Parser parser;
		QVector<Register*>* ptr_Registers;
		RegistersView* ptr_Widget;
		int registerSize;
	};
}

#endif
