#ifndef REGISTER_H
#define REGISTER_H

#include <QString>
#include <QDebug>
#include <stdint.h>

namespace interfaceComponent
{
	class Register
	{

	public :
		Register(const QString n, const QString type, const QString addr);
		Register(const QString n);
		~Register();
		QString getName();
		QString getType();
		QString getAddress();
		void setValue(const uint32_t v);
		uint32_t getDecimalValue();
		bool* getBinaryValue();
		int getSize();

	private :
		QString name;
		QString type;
		QString address;
		int registerSize;
		uint32_t decimalValue;
		bool binaryValue[32];
	};
}

#endif
