#include "Parser.hpp"

namespace interfaceComponent
{
	Parser::Parser()
	{}

	Parser::Parser(const QDomElement& e)
	{
		element = e;
	}

	Parser::~Parser()
	{}

	QDomElement Parser::getElement(const QString tag)
	{
		QDomNode parent;

		parent = element.parentNode();

		return parent.firstChildElement(tag);
	}
	
	QString Parser::getElementValue(const QString tag, const QString attribut)
	{
		QDomNode parent;
		QDomElement e;

		parent = element.parentNode();
		e = getElement(tag);

		return e.attribute(attribut);
	}

	QString Parser::getConnectedPort()
	{
		return getElementValue("link", "port");
	}

	QString Parser::getAddressValue()
	{
		return getElementValue("addr", "value");
	}

	QString Parser::getConnectedName()
	{
		return getElementValue("link", "to");
	}

	QVector<Register*>* Parser::getRegisters()
	{
		QVector<Register*> * listRegisters;
		int i;
		QDomElement e;
		QDomNodeList list;
	
		e = getElement("registers");
		list = e.elementsByTagName("register");
		i = 0;
		listRegisters = new QVector<Register*>();
		while(i < list.length())
		{
			e = list.at(i).toElement();
			listRegisters->append(new Register(e.attribute("name")));
			i++;
		}

		return listRegisters;
	}

	void Parser::setElement(const QDomElement& e)
	{
		element = e;
	}
}
