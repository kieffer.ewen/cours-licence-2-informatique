/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_AIC_HPP
#define BSIM_AIC_HPP


#include <stdint.h>
#include <QDomElement>

#include <bits.hpp>
#include <Board.hpp>
#include <IComponent.hpp>
#include <Scheduler.hpp>
#include <UInt32Register.hpp>

namespace bsim {

// tool classes
class outPin;          
class inPin; 
class Stack;

// AIC class
class AIC: public IComponent {
	friend class outPin;
	friend class inPin;
	friend class Stack;
	
public :
	static const uint32_t BASE_ADDR = 0xFFFFF000;

	AIC(Board& b, QDomElement dom, QString name);

	virtual void makeVisual(VisualGraph& graph);    
	virtual void getStaticConstants(QVector<Constant>& csts) const;
         
	uint32_t getIVR(void);
	uint32_t getIVRIO(void);
	void setIVR(uint32_t v);

	uint32_t getFVR(void);
	uint32_t getFVRIO(void);
	uint32_t setFVR(uint32_t v);

	uint32_t getEOICR(void);
	uint32_t observeEOICR(void);
	void setEOICR(uint32_t v);

	inline uint32_t getISR(void) const { return ISR; }          
	inline uint32_t getIPR(void) const { return IPR; }        
	inline uint32_t getIMR(void) const { return IMR; }         
	inline uint32_t getCISR(void) const { return CISR; }		
	inline uint32_t getFFSR(void) const { return FFSR; }
		
	void setIECR(uint32_t v);
	void setIDCR(uint32_t v);
	void setICCR(uint32_t v);
	void setISCR(uint32_t v);
	void setSPU(uint32_t v);
	void setDCR(uint32_t v);
	void setFFER(uint32_t v);
	void setFFDR(uint32_t v);
	  
	uint32_t getSMR(int i);
	void setSMR(int i, int32_t v); 

private:

	static const int
		ID_SYS		= 0,
		LOW_LEVEL	= 0b00,
		NEG_EDGE	= 0b01,
		HIGH_LEVEL	= 0b10,
		POS_EDGE	= 0b11;
	Bit<uint32_t, 0> NFIQ;
	Bit<uint32_t, 1> NIRQ;
	Bit<uint32_t, 0> PROT;
	Bit<uint32_t, 1> GMSK;
	BitField<uint32_t, 4, 0> IRQID;
	//BitFieldInArray<uint32_t, 2, 0> PRIOR;
	//BitFieldInArray<uint32_t, 6, 5> SRCTYPE;

	inline int maxPRIOR(void);
	void onSourceEvent(int pin, bool val);
	void triggerIT(int it); 

	// registers
	class SMRClass: public UInt32Array {
	public:
		SMRClass(IComponent *comp, const Make& make);
		BitField SRCTYPE, PRIOR;
	};
	SMRClass SMR;
	UInt32Array	SVR;
	uint32_t	IVR,
				FVR,
				ISR,
				CISR,
				SPU,
				DCR;
	BitVector<uint32_t>	IPR, IMR, FFSR;

	// internal
	uint16_t ST[8];
	uint8_t SP;
	inline void push(void)	{ ST[SP] = IRQID; SP++; }
	inline void pop(void) 	{ SP--; IRQID = ST[SP]; }
      
	// pins
	Pin *pidSource[32];  
	Pin *nIRQ, *nFIQ;
};
	
}	// bsim

#endif	// BSIM_AIC_HPP

