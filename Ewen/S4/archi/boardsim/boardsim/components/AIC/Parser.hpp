#ifndef PARSER_H
#define PARSER_H

#include <QVector>
#include "Register.hpp"
#include <QDomElement>

namespace interfaceComponent
{
	class ParserException : public std::exception {} ;

	class Parser 
	{

	public :
		Parser();
		Parser(const QDomElement&);
		~Parser();

		QVector<Register*>* getRegisters();
		QString getConnectedName();
		QString getConnectedPort();
		QString getAddressValue();
		void setElement(const QDomElement&);
		
	private:
		QDomElement getElement(const QString tag);
		QString getElementValue(const QString tag, const QString attribut);

	public :
		QDomElement element;

	};
}

#endif
