#include "Register.hpp"

namespace interfaceComponent
{

	Register::Register(const QString n, const QString t, const QString addr)
	{
		name = n;
		type = t;
		address = addr;
		registerSize = 32;
		setValue(0);
	}

	Register::Register(const QString n)
	{
		name = n;
		type = "read";
		address = "0";
		registerSize = 32;
		setValue(0);
	}

	Register::~Register()
	{}

	QString Register::getName()
	{
		return name;
	}

	QString Register::getType()
	{
		return type;
	}

	QString Register::getAddress()
	{
		return address;
	}

	void Register::setValue(const uint32_t v)
	{
		int i;

		decimalValue = v;
		for(i = (registerSize - 1); i >= 0; i--)
		{
			binaryValue[i] = (decimalValue & (1 << i));
		}
	}

	uint32_t Register::getDecimalValue()
	{
		return decimalValue;
	}

	bool* Register::getBinaryValue()
	{
		return binaryValue;
	}

	int Register::getSize()
	{
		return registerSize;
	}
}
