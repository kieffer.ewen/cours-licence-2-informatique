#ifndef REGISTERS_VIEW_H
#define REGISTERS_VIEW_H

#include "Register.hpp"

#include <QTableWidget>
#include <QVector>

namespace interfaceComponent
{

	class RegistersView : public QTableWidget
	{
	public :
		RegistersView();
		RegistersView(const QVector<Register*>* listRegisters);
		~RegistersView();

		void updateDecimalValue(Register* reg);
		void updateBinaryValue(Register* reg);
		void updateHexaValue(Register* reg);

	private :
		QString getBinaryValue(Register* ptr_Reg);
		QTableWidgetItem* findWidget(const QString name);
	};

}

#endif
