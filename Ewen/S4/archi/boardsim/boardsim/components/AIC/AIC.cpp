/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
 
#include <QLabel>
#include <QDebug>
#include <Display.hpp>
#include <IRegister.hpp>
#include <Scheduler.hpp>
#include <Board.hpp>
#include "AIC.hpp"

//#define TRACE_AIC
#ifdef TRACE_AIC
#	define TRACE(c)		log() << c << endl
#else
#	define TRACE(c)
#endif


namespace bsim{

/**
 * @class AIC
 * 
 * @li Controls the interrupt lines (nIRQ and FIQ) of an ARM Processor 
 * @li Individually masked and vectored interrupt sources
 *    - Source 0 is reserved for the FIQ
 *    - Source 1 is reserved for system peripherals
 *    - Other sources control the peripheral interrupt or external interrupts
 *    - Programmable edge-triggered or level-sensitive internal sources
 *    - Programmable positive/negative edge-triggered or high/low level-sensitive external sources.
 * @li 8-level Priority Controller
 *    - Drives the normal interrupt nIRQ of the processor 
 *    - Handles priority of the interrupt sources
 *    - Higher priority interrupt can be served during service of a lower priority interrupt 
 * @li Vectoring
 *    - Optimize interrupt service routine branch and execution
 *    - One 32-bit vector register per interrupt source
 *    - Interrupt vector register reads the sorresponding current interrupt vector
 * @li Protect mode
 *    - Easy debugging by preventing automatic operations
 * @li Fast forcing
 *    - Permits redirecting any interrupt source on the fast interrupt.
 * @li General interrupt mask
 *    - Provides processor synchronization on events without triggering an interrupt 
 * 
 * @par Pins
 * @li @c IRQ -- output pin,
 * @li @c FIQ -- output pin,
 * @li @c pidSource0 - @c pidSource31 -- Input pin
 * 
 * @par Configuration
 * No configuration.
 * 
 * @par Compaitiblity
 * @li @c SMR0- @c SMR31 -- ok
 * @li @c SVR0- @c SVR31 -- edge SRC_TYPEs are not implemented and considered as levels.
 * @li @c IVR  -- ok
 * @li @c FVR  -- ok
 * @li @c ISR  -- ok
 * @li @c IPR  -- ok
 * @li @c IMR  -- ok
 * @li @c CISR -- ok
 * @li @c IECR -- ok
 * @li @c IDCR -- ok
 * @li @c ICCR -- ok
 * @li @c ISCR -- ok
 * @li @c EOICR -- ok
 * @li @c SPU --  ok
 * @li @c DCR --  ok
 * @li @c FFER -- ok
 * @li @c FFDR -- ok
 * @li @c FFSR -- ok
 * 
 * @ingroup components
 */


/**
 * Out pin for nIRQ or nFIQ.
 */
class outPin: public Pin {
public:
	outPin(AIC& _aic, QString name): Pin(&_aic, name), aic(_aic)
		{ } 
	void onChange(uint64_t time, bool value)
		{ }

private:
       AIC& aic;
};


/**
 * Pin for source events.
 */
class inPin: public Pin {
public:
	inPin(AIC& _aic, int num): Pin(&_aic, QString::number(num)), n(num), aic(_aic) {
	}
	
	void onChange(uint64_t time, bool value) {
		aic.onSourceEvent(n, !value);
	}

private:
	int n;
	AIC& aic;
};	


/**
 */
AIC::AIC(Board& board, QDomElement xml, QString n)
:	IComponent(board, xml),
	NFIQ(CISR),
	NIRQ(CISR),
	PROT(DCR),
	GMSK(DCR),
	IRQID(ISR),
	SMR(this, UInt32Array::make(32).name("SMR").offset(0x00)),
	SVR(this, UInt32Array::make(32).name("SVR").offset(0x80))
{
    // initialize registers
	setAddress(BASE_ADDR);
	IVR		= 0;
	FVR		= 0;
	ISR		= 0;
	IPR		= 0;
	IMR		= 0;
	CISR	= 0;
	FFSR	= 0;
	SPU		= 0;
	DCR		= 0;
	SP		= 0;
	
	// configure pins
	add(nFIQ = new outPin(*this, "FIQ"));  
	add(nIRQ = new outPin(*this, "IRQ"));
	for(int i = 0; i < 32; i++) {
		add(pidSource[i] = new inPin(*this, i));                                      
		SVR[i] = 0;
		SMR[i] = 0;
	} 
    
	// declare registers 
	add(HardRegister32WithSoftIO::make(board).name("IVR").address(0x100).type(IRegister::BITS).getter(*this, &AIC::getIVRIO, &AIC::getIVR).setter(*this, &AIC::setIVR,  &AIC::setIVR));
	add(HardRegister32WithSoftIO::make(board).name("FVR").address(0x104).type(IRegister::BITS).getter(*this, &AIC::getFVRIO, &AIC::getFVR).setter(*this, &AIC::setFVR,  &AIC::setFVR));
	add(HardRegister32WithSoftIO::make(board).name("EOICR").address(0x130).type(IRegister::BITS).getter(*this, &AIC::observeEOICR, &AIC::getEOICR).setter(*this, &AIC::setEOICR, &AIC::setEOICR));

	add(HardRegister32::make(board).name("ISR").address(0x108).type(IRegister::BITS).getter(*this, &AIC::getISR));
	add(HardRegister32::make(board).name("IPR").address(0x10C).type(IRegister::BITS).getter(*this, &AIC::getIPR));
	add(HardRegister32::make(board).name("IMR").address(0x110).type(IRegister::BITS).getter(*this, &AIC::getIMR));
	add(HardRegister32::make(board).name("CISR").address(0x114).type(IRegister::BITS).getter(*this, &AIC::getCISR));
	add(HardRegister32::make(board).name("IECR").address(0x120).type(IRegister::BITS).setter(*this, &AIC::setIECR));
	add(HardRegister32::make(board).name("IDCR").address(0x124).type(IRegister::BITS).setter(*this, &AIC::setIDCR));
	add(HardRegister32::make(board).name("ICCR").address(0x128).type(IRegister::BITS).setter(*this, &AIC::setICCR));
	add(HardRegister32::make(board).name("ISCR").address(0x12C).type(IRegister::BITS).setter(*this, &AIC::setISCR));
	add(HardRegister32::make(board).name("SPU").address(0x134).type(IRegister::BITS).setter(*this, &AIC::setSPU));
	add(HardRegister32::make(board).name("DCR").address(0x138).type(IRegister::BITS).setter(*this, &AIC::setDCR));
	add(HardRegister32::make(board).name("FFER").address(0x140).type(IRegister::BITS).setter(*this, &AIC::setFFER));
	add(HardRegister32::make(board).name("FFDR").address(0x144).type(IRegister::BITS).setter(*this, &AIC::setFFDR));

	add(HardRegister32::make(board).name("FFSR").address(0x148).type(IRegister::BITS).getter(*this, &AIC::getFFSR));
	
	// log the initialization
	configure(xml);		
	info(QString("initialized (base=%1)").arg(address(), 8, 16, QChar('0')));
}


/**
 * Called each time on input pin receives an event.
 * @param pin	Pin number.
 * @param val	Pin value.
 */
void AIC::onSourceEvent(int pin, bool val) {
	TRACE("source event " << pin << " with " << val);
	switch(SMR.SRCTYPE[pin]) {
	case LOW_LEVEL:
	case NEG_EDGE:
		if(val)
			return;
		break;
	case HIGH_LEVEL:
	case POS_EDGE:
		if(!val)
			return;
		break;
	}
	triggerIT(pin);
}


/**
 * Trigger an interrupt.
 * @param it	Interrupt number.
 */
void AIC::triggerIT(int it) {
	if(GMSK == 0) {
		IPR[it] = 1;
		if(IMR[it] == 1) {
			if(it == ID_SYS || FFSR[it] == 1) {
				TRACE("nFIQ asserted");
				NFIQ = 1;
				nFIQ->change(time(), false);
			}
			else if(SP < 8
			&& (IRQID == 0 || SMR.PRIOR[it] > SMR.PRIOR[IRQID])) {
				TRACE("nIRQ asserted");
				NIRQ = 1;
				nIRQ->change(time(), false);
			}
		}
	}
}


/**
 * Get the IT with maximum priority from the IPR (masked by IMR).
 * @return	IT with max priority if any, -1 else.
 */
int AIC::maxPRIOR(void) {
	BitVector<uint32_t> m = IPR & IMR;
	int max_it = -1, max_pri = -1;
	for(int i = 0; i < 32; i++) {
		if(m[i] == 1 && int(SMR.PRIOR[i]) > max_pri) {
			max_it = i;
			max_pri =  SMR.PRIOR[i];
		}
	}
	return max_it;
}


/**
 */
void AIC ::makeVisual(VisualGraph& graph) {

	// configure the component
	VisualComponent *vcomp = new VisualComponent(this);
	vcomp->w = 80;
	vcomp->h = 330;
	
	// add the pins	        
	vcomp->add(new VisualPin(*vcomp, nFIQ, Visual::WEST, (70)));
	vcomp->add(new VisualPin(*vcomp, nIRQ, Visual::WEST, (30)));		
		
	for(int i = 0; i < 32; i++) 
		vcomp->add(new VisualPin(*vcomp, pidSource[i], Visual::EAST, (10+i*10)));
	
	// parse the configuration
	vcomp->parse(configuration());

	// prepare UI
	vcomp->make(name());
	vcomp->makePins();
	graph.add(vcomp);
}


/**
 * Get the IVER register value.
 * @return	IVR value
 */
uint32_t  AIC::getIVR(void) {
	if(NIRQ == 0)
		IVR = SPU;
	else {
		int i = maxPRIOR();
		IVR = SVR[i];
		if(PROT == 0) {
			push();
			IRQID = i;
			IPR[i] = 0;
			NIRQ = 0;
			nIRQ->change(time(), true);
		}
	}
	TRACE("reading IVR = " << (void *)intptr_t(IVR)); 
	return IVR;
}


/**
 * Get IVR value for observation.
 */
uint32_t  AIC::getIVRIO(void) {
	return IVR;
}


/**
 * Set the IVR register.
 * @param	Register value.
 */
void AIC::setIVR(uint32_t v) {
	if(PROT == 1) {
		int i = maxPRIOR();
		push();
		IRQID = i;
		IPR[i] = 0;
		NIRQ = 0;
		nIRQ->change(time(), true);
	}
}


/**
 * Get the FVR register triggering the start of FIQ.
 */
uint32_t AIC:: getFVR(void) {
	if(NFIQ == 0)
		FVR = SPU;
	else {
		FVR = SVR[0];
		if(PROT == 0) {
			NFIQ = 0;
			nFIQ->change(time(), true);
		}
	}
	TRACE("reading FVR = " << (void *)intptr_t(FVR));
	return FVR;
}


/**
 * Get the FVR register for observation purpose.
 */
uint32_t AIC::getFVRIO() {
	return FVR;
}


/**
 * Set the FVR register, recording the IT start if PROT = 0.
 */
uint32_t AIC::setFVR(uint32_t v) {
	if(PROT == 1) {
		NFIQ = 0;
		nFIQ->change(time(), true);
	}
}


/**
 * Enable interrupts.
 */
void AIC::setIECR(uint32_t v) {
	IMR |= v;   
}


/**
 * Disable interrupts.
 */
void AIC::setIDCR(uint32_t v) {
	IMR &= ~v;
}


/**
 * Cause an interrupt.
 */
void AIC::setISCR(uint32_t v) { 
	BitVector<uint32_t> b(v);
	for(int i=0; i < 31; i++)   
		if(b[i] == 1)
			triggerIT(i);
}


/**
 * Clear a pending interrupt.
 */
void AIC::setICCR(uint32_t v) {
	IPR &= ~v; 
}


/**
 * Enable fast IT forcing.
 */
void AIC::setFFER(uint32_t v) {
	FFSR |=v;
}


/**
 * Disable fast IT forcing.
 */
void AIC::setFFDR(uint32_t v) {
	FFSR &= ~v;
}	


/**
 * Set SPU.
 */
void AIC::setSPU(uint32_t v) {
	SPU = v;
}


/**
 * Set the DCR regiqter.
 */
void AIC::setDCR(uint32_t v) {
	DCR = v;
}


/**
 * Read EOICR causing interrupt ending if PROT = 0.
 */
uint32_t AIC::getEOICR(void) {
	if(PROT != 1)
		setEOICR(0);
}


/**
 * Get EOICR for observation purpose.
 * Always return 0.
 */
uint32_t AIC::observeEOICR(void) {
	return 0;
}


/**
 * Set the EOICR to acknowledge interrupt.
 * @param v	Assigned value.
 */
void AIC::setEOICR(uint32_t v) {
	if(SP > 0)
		pop();
	else
		NIRQ = 0;
	if(GMSK == 0) {
		if((IPR & IMR & FFSR) != 0) {
			NFIQ = 1;
			nFIQ->change(time(), false);
		}
		else if((IPR & IMR & ~FFSR) != 0) {
			int i = maxPRIOR();
			if(IRQID == 0 || SMR.PRIOR[i] > SMR.PRIOR[IRQID]) {
				NIRQ = 1;
				nIRQ->change(time(), false);
			}
		}
	}
}


/**
 */
void AIC::getStaticConstants(QVector<Constant>& csts) const {
	for(int i = 0; i < 32; i++)
		if(pidSource[i]->connection())
			csts.append(Constant("ID_" + pidSource[i]->connection()->asIRQ(), i, false));
	csts.append(Constant("LOW_LEVEL", 0 << 5));
	csts.append(Constant("NEGATIVE_EDGE", 1 << 5));
	csts.append(Constant("HIGH_LEVEL", 2 << 5));
	csts.append(Constant("POSITIVE_EDGE", 3 << 5));
}


AIC::SMRClass::SMRClass(IComponent *comp, const Make& make)
:	UInt32Array(comp, make),
	SRCTYPE	(*this, "SRCTYPE",	6, 5),
	PRIOR	(*this, "PRIOR",	2, 0)
{ }

	
/**
 * Called to build a new AIC.
*/
extern "C" IComponent* init(Board& board,QDomElement xml, QString name) {
	return new AIC(board, xml, name);	
}
	
}  // bsim
