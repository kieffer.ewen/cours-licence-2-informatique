/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_ADC_HPP
#define BSIM_ADC_HPP

#include <stdint.h>
#include <QDomElement>

#include <Board.hpp>
#include <IComponent.hpp>
#include <Scheduler.hpp>
#include <UInt32Register.hpp>

namespace bsim {
     class outPin;          
     class inPin; 
	 class SleepEVT;
	 class ConvertEVT;
	 
	 
class ADC: public IComponent {
	 friend class outPin;
	 friend class inPin;
	 friend class SleepEVT; 	 
	 friend class ConvertEVT;

public: 
	ADC(Board& b, QDomElement dom, QString name);
	~ADC(void);

	virtual void makeVisual(VisualGraph& graph);    
	void genHeader(QTextStream& out); 
    
	uint32_t getSR(void);   
	uint32_t getSRIO(void);
		            
	inline uint32_t getIMR(void) { return IMR; }         
	inline uint32_t getCHSR(void) { return CHSR; }		
	inline uint32_t getCDR(int i) { SR.EOC[i] = 0; return CDR[i]; }
		
	inline uint32_t getLCDR(void)  {   
		SR.EOC[LDATA] = 0;
		SR.DRDY = 0;
		return LCDR; 
	}
	
	void setCR(uint32_t v); 
	void setMR(uint32_t v); 
	void setCHER(uint32_t v); 
	void setCHDR(uint32_t v);
	void setIER(uint32_t v); 
	void setIDR(uint32_t v);

	void changeOUT(uint32_t changes);
	uint32_t it();
	void adc_reset();
	void start_convrt();
	void convert(); 

	class CRClass: public UInt32ControlRegister {
	public:
		CRClass(IComponent *adc, const Make& make);
		Bit START;
		Bit SWRST;
	} CR;
	
	class MRClass: public UInt32Register {
	public:
		MRClass(IComponent *adc, const Make& make);
		BitField
			SHTIM,
			STARTUP,
			PRESCAL;
		Bit
			SLEEP,
			LOWRES;
		BitField
			TRGSEL;
		Bit
			TRGEN;
	} MR;
	
	class SRClass: public UInt32Register {
	public:
		SRClass(IComponent *adc, const Make& make);
		BitArray
			EOC,
			OVRE;
		Bit
			DRDY,
			GOVRE,
			ENDRX,
			RXBUFF;
	} SR;

	UInt32Register CHSR, IMR, LCDR;
	UInt32Array CDR;
	UInt32ControlRegister IER, IDR, CHER, CHDR;
      
	Pin *ADPin[8];
	Pin *INT;	//, *ADTRG, *TCPIN0, *TCPIN1, *TCPIN2; //, *ADVREF
	SleepEVT *st_event;
	ConvertEVT *ct_event[8];

	uint32_t ADC_Clk, Start_Up_T, Sample_Hold_T, ADC_DIV; 
	double ADV_REF, AD[8];
	uint32_t LDATA, channel_nb;   // the number of the last converted channel     
};
	
}	// bsim

#endif	// BSIM_ADC_HPP
