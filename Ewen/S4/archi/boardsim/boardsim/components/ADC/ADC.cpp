/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
 
#include <QLabel>
#include <QDebug>
#include <Display.hpp>
#include <IRegister.hpp>
#include <Scheduler.hpp>
#include <Board.hpp>
#include "ADC.hpp"

//#define BSIM_ADC_DEBUG
#ifdef BSIM_ADC_DEBUG
#	define TRACE(c)		qDebug() << c
#else
#	define TRACE(c)
#endif


namespace bsim{
/**
 * @class ADC
 * 
 * @li 8-channel ADC
 * @li 10-bit 384 K samples/sec. Successive Aprooximation Register ADC
 * @li Integrated 8 to 1 multiplexer, offering eight independent analog inputs
 * @li External voltage reference for better accuracy on low voltage inputs   
 * @li Individual enable and disable of each channel
 * @li Multiple trigger sources
 *	   - Hardware or software trigger
 *     - External trigger pin
 *     - Timer Counter 0 to 2 outputs TIOA0 to TIOA2 trigger
 * 
 * 
 * @par Pins
 * @li @c AD0 - @c AD7 -- input pin,
 * @li @c ADTRG        -- input pin,
 * @li @c TCPIN        -- input pin,   
 * @li @c INT          -- output pin.
 * 
 * @par Configuration
 * No configuration.
 * 
 * @par Registers
 * The following registers are 32-bits wide 
 * @li @c CR -- controls the conversion  (write),
 * @li @c MR -- configurates the ADC    (read/write),
 * @li @c CHER -- enables a channel by writing 1 on its corresponding bit in CHSR (write),
 * @li @c CHDR -- disables a channel by writing 1 on its corresponding bit in CHSR (write),
 * @li @c CHSR -- the enabled/disabled channels (read),
 * @li @c SR   -- the status of the ADC  (read),
 * @li @c LCDR -- contains the last converted data  (read),
 * @li @c IER  -- enables interrupts for the ADC channels by writing 1 on the corresponding bit in IMR (write),
 * @li @c IDR  -- disables interrupts for the ADC channels by writing 0 on the corresponding bit in IMR  (write),
 * @li @c IMR  -- the state of the interrupts, enabled/ disabled (read), 
 * @li @c CDR0 - @c CDR1 -- contains the converted data of the corresponding channel (read).
 * 
 * 
 * 
 * @ingroup components
 */

/**
 * Constructor.
 * @param board		Current board.
 * @param xml		Configuration.
 * @param n			Component logic name.
 */

class outPin: public Pin{
public:
	outPin (ADC& _adc, QString name): Pin(&_adc, name), adc(_adc) {
	}

	void onChange(uint64_t time, bool value) { }

private:
	ADC& adc;
};

class inPin: public Pin {
public:
	inPin (ADC& _adc, int channel, QString name): Pin(&_adc, name), adc(_adc), ch(channel) { }

	/*void onChange(uint64_t time, bool value) {
		 if(value)
		 {	
			  if(adc.bit(adc.MR,0))                     // if a hardware trigger is selected
			  { 
				  if(name == "ADTRG")       if(adc.bitf(adc.MR,1,3) == 6)     adc.start_convrt();      
			      if(name == "TCPIN0")      if(adc.bitf(adc.MR,1,3) == 0)     adc.start_convrt();   
		          if(name == "TCPIN1")      if(adc.bitf(adc.MR,1,3) == 1)     adc.start_convrt();   
		          if(name == "TCPIN2")      if(adc.bitf(adc.MR,1,3) == 2)     adc.start_convrt();   
		      }
		}
    }*/

	void receiveAnalog(uint64_t time, double volt) {
		adc.AD[ch] = volt;
		adc.channel_nb = 0;
		TRACE(qDebug() << "ADC: ch=" << ch << ", AD=" << volt);
	}

private:
	ADC& adc;
	int ch;
}; 

// SLEEP Mode Event
class SleepEVT: public Scheduler::Event {
public:
	SleepEVT(Board& _board,time_t tm, ADC& _adc): Event(_board, tm), adc(_adc) { }
	void run(void) { adc.convert(); }
	void resched(time_t t) { reschedule(t); }
private:
	ADC& adc;
};

// End of Conversion Event
class ConvertEVT: public Scheduler::Event {
public:
	ConvertEVT(Board& _board,time_t tm, ADC& _adc, uint32_t _ch )
: Event(_board, tm), adc(_adc), channel(_ch), cnt(0), full(false)
{ }

	void run(void) {	

		// record the value
		if(!full)
			cnt &= 0xff;
		adc.LCDR = cnt;
		adc.CDR[channel] = cnt;
		TRACE("ADC: ConvertEVT: ch=" << channel << ", cnt=" << cnt);

		// notify the conversion
		adc.LDATA = channel;             
		adc.SR.EOC[channel] = 1;
		adc.SR.DRDY = 1;
		adc.changeOUT(adc.it());
	}

	void resched(time_t t, uint32_t ct, bool fl) {
		reschedule(t);
		cnt = ct;
		full = fl;
	} 

private:
	ADC& adc;
	uint32_t cnt;
	uint32_t channel;
	bool full;
}; 

ADC::ADC(Board& board, QDomElement xml, QString n)
:	IComponent(board, xml),
	CR		(this, UInt32::make()		.name("CR")		.offset(0x00).set(this, &ADC::setCR)),
	MR		(this, UInt32::make()		.name("MR")		.offset(0x04).set(this, &ADC::setMR)),
	CHER	(this, UInt32::make()		.name("CHER")	.offset(0x10).set(this, &ADC::setCHER)),
	CHDR	(this, UInt32::make()		.name("CHDR")	.offset(0x14).set(this, &ADC::setCHDR)),
	CHSR	(this, UInt32::make()		.name("CHSR")	.offset(0x18).get(this, &ADC::getCHSR)),
	SR		(this, UInt32::make()		.name("SR")		.offset(0x1c).get(this, &ADC::getSR)),
	LCDR	(this, UInt32::make()		.name("LCDR")	.offset(0x20).get(this, &ADC::getLCDR)),
	IER		(this, UInt32::make()		.name("IER")	.offset(0x24).set(this, &ADC::setIER)),
	IDR		(this, UInt32::make()		.name("IDR")	.offset(0x28).set(this, &ADC::setIDR)),
	IMR		(this, UInt32::make()		.name("IMR")	.offset(0x2c).get(this, &ADC::getIMR)),
	CDR		(this, UInt32Array::make(8)	.name("CDR")	.offset(0x30).get(CDR, this, &ADC::getCDR))
{

	// initialize registers
	setAddress(0xFFFD8000);

	add(INT    = new outPin(*this, "INT"));
	/*add(ADTRG  = new inPin(*this, "ADTRG"));
    add(TCPIN0 = new inPin(*this, "TC0"));
    add(TCPIN1 = new inPin(*this, "TC1"));    
    add(TCPIN2 = new inPin(*this, "TC2"));*/

	for(int i = 0; i < 8; i++)
		add(ADPin[i] = new inPin(*this, i, QString("AD%1").arg(i)));

	st_event =new  SleepEVT(board, 0, *this);

	for(int i = 0; i < 8; i++)
		ct_event[i] = new  ConvertEVT(board, 0, *this, i);

	// configure from XML
	configure(xml);


	// get configuration
	ADV_REF = 5;

	if(xml.hasAttribute("ADVREF"))
		ADV_REF= xml.attribute("ADVREF").toInt();     
	if(ADV_REF < 0 || ADV_REF > 10)
		ADV_REF = 5;

	// debug 
	info(QString("initialized (base=%1)").arg(address(), 8, 16, QChar('0')));	

	adc_reset();
}

ADC::~ADC() 
{}

/**
 */
void ADC::makeVisual(VisualGraph& graph) {

	// configure the component
	VisualComponent *vcomp = new VisualComponent(this);
	vcomp->w = 90;
	vcomp->h = 130;

	for(int i = 0; i < 8; i++)
		vcomp->add(new VisualPin(*vcomp, ADPin[i], Visual::EAST, 35 + i * 10));
	vcomp->add(new VisualPin(*vcomp, INT, Visual::WEST, 15));

	/*vcomp->add(new VisualPin(*vcomp, TCPIN0, Visual::WEST, 35));
	vcomp->add(new VisualPin(*vcomp, TCPIN1, Visual::WEST, 45));
	vcomp->add(new VisualPin(*vcomp, TCPIN2, Visual::WEST, 55));
	vcomp->add(new VisualPin(*vcomp, ADTRG,  Visual::WEST, 85));*/

	vcomp->parse(configuration());

	// prepare UI
	vcomp->make(name());
	vcomp->makePins();
	graph.add(vcomp);
}	

/** 
 * resets the internal state machine 
 * */
void ADC::adc_reset()
{	
	*MR = 0x0;
	*SR = 0x000C0000;
	IMR = 0;
	CHSR = 0;
	LCDR = 0X0;
	for(int i = 0; i < CDR.count(); i++)
		CDR[i] = 0;

	ADC_Clk = 0; ADC_DIV = 0;
	Start_Up_T = 0; 
	Sample_Hold_T = 0;
	for(int i = 0; i < 8; i++)
		AD[i] = 0;
}

/**
 * Start the conversions.
 */
void ADC::start_convrt() {
	if(MR.SLEEP)
		st_event->resched(board().scheduler()->time() + Start_Up_T * ADC_DIV);                 // SLEEP mode is activated 
	else {
		channel_nb = 0;
		convert();
	}
}	

/**
 * 
 * */
void ADC::convert() {
	uint32_t t; 

	// current channel active?
	TRACE("channel_nb=" << channel_nb);
	while(channel_nb < 8 && !bit(CHSR, channel_nb))
		channel_nb++;
	if(channel_nb >= 8) {
		TRACE("ADC: conversion cycle ended");
		return;
	}

	// 10-bit conversion
	if(!MR.LOWRES) {
		if(SR.DRDY)
			SR.GOVRE = 1;
		if(SR.EOC[channel_nb])
			SR.OVRE[channel_nb] = 1;
		changeOUT(it());
		t =  (1023 * AD[channel_nb]) / ADV_REF;
		time_t etime = board().scheduler()->time() + (Sample_Hold_T + 10) * ADC_DIV;
		ct_event[channel_nb]->resched(etime, t, true);
		TRACE("ADC: 10-bit conversion scheduled for " << etime << " at " << board().scheduler()->time());
	}

	// 8-bit conversion
	else {
		if(SR.DRDY)
			SR.GOVRE = 1;
		if(SR.EOC[0])
			SR.OVRE[0] = 1;
		changeOUT(it());
		t =  (255 * AD[channel_nb])/ ADV_REF;
		time_t etime = board().scheduler()->time()+ (Sample_Hold_T + 10)*ADC_DIV;
		ct_event[channel_nb]->resched(etime, t, false);
		TRACE("ADC: 8-bit conversion scheduled for " << etime << " at " << board().scheduler()->time());
	}
}	


/**
 * verifies if there is an interrupt to be asserted.  
 */
uint32_t ADC ::it()
{
	return getSR()& IMR;
}
/**
 * */
void ADC ::changeOUT(uint32_t changes)
{
	for(int i = 0; i < 31; i++) 
	{
		uint32_t mask = 1 << i;
		if(changes & mask)        INT->change(board().scheduler()->time(), true);			
	}
}

/**
 * */
uint32_t ADC ::getSR() {         
	uint32_t val=SR;
	setf(SR,8,15,0);
	clear(SR,17);

	return val; 
}

uint32_t ADC ::getSRIO() {
	return SR; 
}

/**
 */
void ADC::setCR(uint32_t v) {
	if(CR.SWRST(v))
		adc_reset(); 
	if(CR.START(v) && !MR.TRGEN)
		start_convrt();
}	

void ADC::setMR(uint32_t v) {
	setf(MR , 0, 31, v);

	ADC_Clk =  board().mck() / ((bitf(MR, 8,13) + 1) * 2); 
	ADC_DIV = ((bitf(MR, 8,13) + 1) * 2);

	Start_Up_T = ((bitf(MR, 16,20) + 1) * 8) / ADC_Clk; 
	Sample_Hold_T = (bitf(MR,24,27)) / ADC_Clk;	
}	

void ADC::setCHER(uint32_t v){
	CHSR |= v;
}	

void ADC::setCHDR(uint32_t v){
	CHSR &= ~v;
}	

void ADC::setIER(uint32_t v){
	IMR |= v;
}	

void ADC::setIDR(uint32_t v){
	IMR &= ~v;
}

/**
 */	
void ADC::genHeader(QTextStream& out) {

	out << "	.equ ADC_CHER_CH0, 	(1 << 0)\n";
	out << "	.equ ADC_CHER_CH1, 	(1 << 1)\n";
	out << "	.equ ADC_CHER_CH2,  (1 << 2)\n";
	out << "	.equ ADC_CHER_CH3, 	(1 << 3)\n";
	out << "	.equ ADC_CHER_CH4, 	(1 << 4)\n";
	out << "	.equ ADC_CHER_CH5,  (1 << 5)\n";
	out << "	.equ ADC_CHER_CH6, 	(1 << 6)\n";
	out << "	.equ ADC_CHER_CH7,  (1 << 7)\n";

	out << "	.equ ADC_SWRST, 	(1 << 0)\n";
	out << "	.equ ADC_START, 	(1 << 1)\n";

	out << "	.equ ADC_TRGEN, 	(1 << 0)\n";
	out << "	.equ ADC_LOWRES, (1 << 4)\n";
	out << "	.equ ADC_SLEEP, 	(1 << 5)\n"; 		

	out << "	.equ ADC_EOC0, 	(1 << 0)\n";
	out << "	.equ ADC_EOC1, 	(1 << 1)\n";
	out << "	.equ ADC_EOC2,  (1 << 2)\n";
	out << "	.equ ADC_EOC3, 	(1 << 3)\n";
	out << "	.equ ADC_EOC4, 	(1 << 4)\n";
	out << "	.equ ADC_EOC5,  (1 << 5)\n";
	out << "	.equ ADC_EOC6, 	(1 << 6)\n";
	out << "	.equ ADC_EOC7,  (1 << 7)\n";
}

/**
 */
ADC::CRClass::CRClass(IComponent *adc, const Make& make)
:	UInt32ControlRegister(adc, make),
	SWRST(*this, "SWRST", 0),
	START(*this, "START", 1)
{
}


/**
 */
ADC::MRClass::MRClass(IComponent *adc, const Make& make)
:	UInt32Register(adc, make),
	TRGEN	(*this, "TRGEN",        0),
	TRGSEL	(*this, "TRGSEL",   3,  1),
	LOWRES	(*this, "LOWRES",	    4),
	SLEEP	(*this, "SLEEP",	    5),
	PRESCAL	(*this, "PRESCAL", 15,  8),
	STARTUP	(*this, "STARTUP", 22, 16),
	SHTIM	(*this, "SHTIM",   27, 24)
{
}


ADC::SRClass::SRClass(IComponent *adc, const Make& make)
:	UInt32Register(adc, make),
	EOC		(*this, "EOC", 	 	 7,  0),
	OVRE	(*this, "OVRE", 	15,  8),
	DRDY	(*this, "DRDY", 		16),
	GOVRE	(*this, "GOVRE", 		17),
	ENDRX	(*this, "ENDRX", 		18),
	RXBUFF	(*this, "RXBUFF",		19)
{
}


/**
 * Called to build a new ADC.
 */
extern "C" IComponent* init(Board& board,QDomElement xml, QString name) {
	return new ADC (board, xml, name);	
}

}  // bsim

