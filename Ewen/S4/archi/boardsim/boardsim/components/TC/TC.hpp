#ifndef COMPONENT_TC
#define COMPONENT_TC

#include <stdint.h>
#include "Board.hpp"
#include "IComponent.hpp"
#include <Scheduler.hpp>
#include <QDomElement>

namespace bsim {
class TCPin;
class outPin;
class synPin;
class TCEvent;  
class RCEvent;            

class TC: public IComponent {
	  friend class TCPin;
	  friend class outPin;
	  friend class synPin;
	  friend class TCEvent;
	  friend class RCEvent;
public : 
   	 TC(Board& b, QDomElement dom, QString name);
     ~TC(void);
    
	virtual void makeVisual(VisualGraph& graph);
    
    void setCCR0(uint32_t v) ;
    void setCMR0(uint32_t v) ;
    void setIER0(uint32_t v) ;  
    void setIDR0(uint32_t v) ;
    uint32_t getSR0();
    uint32_t getSR0IO();
	uint32_t getCV0();
	inline uint32_t getIMR0(void) const { return IMR0; }
	inline uint32_t getRA0(void) const { return RA0; }
	inline void setRA0(uint32_t v) { RA0 = v; }
	inline uint32_t getRB0(void) const { return RB0; }
	inline void setRB0(uint32_t v) { RB0 = v; } 
	inline uint32_t getRC0(void) const { return RC0; }
	inline void setRC0(uint32_t v) { RC0 = v; }
    
    void setCCR1(uint32_t v) ;
    void setCMR1(uint32_t v) ;
    void setIER1(uint32_t v) ;  
    void setIDR1(uint32_t v) ;
    uint32_t getSR1();
    uint32_t getSR1IO();
    uint32_t getCV1();
    inline uint32_t getIMR1(void) const { return IMR1; }
	inline uint32_t getRA1(void) const { return RA1; }
    inline void setRA1(uint32_t v) { RA1 = v; }
	inline uint32_t getRB1(void) const { return RB1; }
    inline void setRB1(uint32_t v) { RB1 = v; } 
	inline uint32_t getRC1(void) const { return RC1; }
    inline void setRC1(uint32_t v) { RC1 = v; }
    
    void setCCR2(uint32_t v) ;
    void setCMR2(uint32_t v) ;
    void setIER2(uint32_t v) ;  
    void setIDR2(uint32_t v) ;
    uint32_t getSR2();
    uint32_t getSR2IO();
    uint32_t getCV2();
    inline uint32_t getIMR2(void) const { return IMR2; }
	inline uint32_t getRA2(void) const { return RA2; }
    inline void setRA2(uint32_t v) { RA2 = v; }
	inline uint32_t getRB2(void) const { return RB2; }
    inline void setRB2(uint32_t v) { RB2 = v; } 
	inline uint32_t getRC2(void) const { return RC2; }
    inline void setRC2(uint32_t v) { RC2 = v; }
   
   
    void setBCR(uint32_t v) ;
	void setBMR(uint32_t v) ;
    
    void changeOut(uint32_t changes, int channel);
    
    uint32_t it0();
    uint32_t it1();
    uint32_t it2();
    
   
private:
    
    time_t T0[3];
    int div(int i);
    time_t period(int i);

	void startCLK(int channel, uint32_t cmr, uint32_t sr);
	void resetCLK(int channel, bool all);
	virtual void getStaticConstants(QVector<Constant>& csts) const;

    uint32_t
		BCR,
		BMR,
		
		CCR0,
		CMR0,
		SR0,
		CV0,
		IMR0,
		RA0,
		RB0,
		RC0,
		
		CCR1,
		CMR1,
		SR1,
		CV1,
		IMR1,
		RA1,
		RB1,
		RC1,
		
		CCR2,
		CMR2,
		SR2,
		CV2,
		IMR2,
		RA2,
		RB2,
		RC2;  
   
    Board& board; 

    Pin *tclk[3];
    Pin *tioa[3];
    Pin *tiob[3]; 
    Pin *INT0;
    Pin *INT1;
    Pin *INT2;
    
    Pin *sync;
    
    RCEvent *rc_event0, *rc_event1, *rc_event2;
    TCEvent *tc_event0, *tc_event1, *tc_event2; 
};
	
}	// bsim

#endif
