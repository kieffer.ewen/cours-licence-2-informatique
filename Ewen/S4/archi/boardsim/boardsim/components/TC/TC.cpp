/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#include <QLabel>
#include <QDebug>
#include <Display.hpp>
#include <IRegister.hpp>
#include <Scheduler.hpp>
#include <Board.hpp>
#include "TC.hpp"

//#define TRACE_TC
#ifdef TRACE_TC
#	define TRACE(c)		c
#else
#	define TRACE(c)
#endif

namespace bsim {
/**
 * @class TC
 * 
 * @li Three 16-bit Timer channels.
 *  - Two output compare or one input capture per channel.
 * 
 * @li Wide range of functions including:
 *   - Frequency measurment
 *   - Event counting
 *   - Interval measurment
 *   - Pulse generation
 *   - Delay timing
 *   - Pulse wide modulation
 *   - Up/down capabilities.
 * 
 * @li Each channel can independently operate in two different modes:
 *    - Capture Mode 
 *    - Wave Mode
 *  
 * @li Each channel is user-configurable and contains:
 *   - Three external clock inputs.
 * 
 * @li Five internal clock inputs. 
 * 
 * @par Pins
 * @li @c TCLK0 - @c TCLK2 -- external clock input,
 * @li @c TIOA0 - @c TIOA2 -- I/O line A,
 * @li @c TIOB0 - @c TIOB2 -- I/O line B.
 * @li @c INT0  - @c INT2  -- output pin.
 * 
 * @par Configuration
 * No configuration.
 * 
 * @par Registers
 * There are 12 registers for each channel.
 * The following registers are 32-bits wide and each bit is associated to a line of the same bit number.
 * @li @c CCR -- controls the channel, Enable/Disable Clock, reset the counter (write),
 * @li @c CMR -- controls the mode of the channel, Wave Mode/Capture Mode (read/write),
 * @li @c CV  -- contains the current value of the counter in real time (read),
 * @li @c RA  -- contains the register A value (read/write),
 * @li @c RB  -- contains the register B value (read/write),
 * @li @c RC  -- contains the register C value  (read / write),
 * @li @c SR  -- indicates the interrupts that were occured since its last read (read),
 * @li @c IER -- enables interrupts by writing 1 on the corresponding bit in IMR (write),
 * @li @c IDR -- disables interrupts by writing 0 on the corresponding bit in IMR (write),
 * @li @c IMR -- the state of the interrupts, enabled/ disabled (read),
 * @li @c BCR -- allows the three channels to be started simultaneously with the same instruction (write),
 * @li @c BMR -- defines the external clock inputs for each channel (read/write).
 * @ingroup components
 */

/**
 * Constructor.
 * @param board		Current board.
 * @param xml		Configuration.
 * @param n			Component logic name.
 */
 

/**
 * the TIOA and TIOB signals were not implemented in this program and so do the WAVE mode.
 * */
class TCPin: public Pin {
public:
	TCPin(TC& _tc, int num): Pin(&_tc, QString::number(num)), n(num), tc(_tc)
		{ }
	
	virtual void onChange(uint64_t time, bool value) { }
	
private:
	int n;
	TC& tc;
};


// IndexRegister class
class IndexedRegister: public HardRegister32  {
public:
	IndexedRegister(int index, Board& board): HardRegister32(board), _index(index) { }
	virtual QString qualifiedName(IComponent *component, int i)
		{ return component->name() + QString::number(_index) + "_" + name(); }
	virtual QString displayName(int i)
		{ return "TC" + QString::number(_index) + "_" + name(); }
private:
	int _index;
};


// StatusRegister class
class StatusRegister: public HardRegister32WithSoftIO  {
public:
	StatusRegister(int index, Board& board): HardRegister32WithSoftIO(board), _index(index) { }
	virtual QString qualifiedName(IComponent *component, int i)
		{ return component->name() + QString::number(_index) + "_" + name(); }
	virtual QString displayName(int i)
		{ return "TC" + QString::number(_index) + "_" + name(); }
private:
	int _index;
};

// Output Pin
class outPin: public Pin {
public:
	outPin(TC& _tc, int n): Pin(&_tc, QString("INT%1").arg(n)), tc(_tc), _n(n) { }
	virtual void onChange(uint64_t time, bool value) { }
	virtual QString asIRQ(void) { return QString("TC%1").arg(_n); }

private:
	TC& tc;
	int _n;
};

// Synchronization Pin
class synPin: public Pin {
public:
	synPin (TC& _tc, QString name): Pin(&_tc, name), tc(_tc) {
	}
	
	void onChange(uint64_t time, bool value) 
	{    
		if(value)  if(!tc.bit(tc.BCR,0))  tc.setBCR(0);	
	}
	
private:
	TC& tc;
};

// COVFS event
class TCEvent: public Scheduler::Event {
	 public:
	 TCEvent(Board& _board, time_t tm, TC& _tc, int chnl ):Scheduler::Event(_board, tm),tc(_tc), time(tm), channel(chnl)
	 { } 
	 
	void run(void)
	{		 
		switch(channel)
       {		
		   
	       case 0:{   
		
	     	 tc.set(tc.SR0,0);   

			if(tc.bit(tc.IMR0, 0))   tc.changeOut(tc.it0(),0); 
			 		
		     tc.resetCLK(0, false); 

		    break;    	            			
           }
	      case 1:{   
		
	     	tc.set(tc.SR1,0);			     

			if(tc.bit(tc.IMR1, 0))   tc.changeOut(tc.it1(),1); 
 		
             tc.resetCLK(1, false); 
 
		    break;    	            			
          }
	    case 2:{   
		
	     	 tc.set(tc.SR2,0);		     
	 	     

			if(tc.bit(tc.IMR2, 0))   tc.changeOut(tc.it2(),2); 


             tc.resetCLK(2, false); 
 
		    break;    	            			
          }
	  }		
    }
    
   void resched(time_t t, int chl)     
   {
	  reschedule(t);
      channel=chl;    
	}  
    

	private:
	int channel;
	time_t time;
	TC& tc;

};
 // CV = RC event
class RCEvent: public Scheduler::Event {
	 public:
	 RCEvent(Board& _board, time_t tm, TC& _tc, int chnl ): Scheduler::Event(_board, tm),tc(_tc), time(tm), channel(chnl)
	 {
	 } 
	 
	void run() 
	{  
		TRACE(qDebug() << "RC Event run  at " << tc.board.scheduler()->time() <<endl;)
		 switch(channel)
		{		 	
			case 0:
			{   

                tc.set(tc.SR0,4);	    
				

				if(tc.bit(tc.IMR0, 4))      tc.changeOut(tc.it0(),0); 

				   	 		
				if(tc.bit(tc.CMR0,14))	        
				{				
					tc.resetCLK(0, false); 
					tc.startCLK(0,tc.CMR0, tc.SR0);
				} 
				break;    	            			
			}
			case 1:
			{   		
				tc.set(tc.SR1,4);		     

				if(tc.bit(tc.IMR1, 4))   tc.changeOut(tc.it1(),1); 
							
				if(tc.bit(tc.CMR1,14))	       
				{
					tc.resetCLK(1, false); 
					tc.startCLK(1, tc.CMR1, tc.SR1);
				} 
				break;    	            			
		   }
		  case 2:
		  {   
		
				tc.set(tc.SR2,4);			     	             
				if(tc.bit(tc.IMR2, 4))   tc.changeOut(tc.it2(),2); 
				if(tc.bit(tc.CMR2,14))	       
				{
					tc.resetCLK(2, false); 
					tc.startCLK(2,tc.CMR2, tc.SR2);
				} 
				break;    	            			
          }
	  }	
   }
     
   void resched(time_t t, int chl)     
   {
	reschedule(t);
	channel=chl;  
	   
	}  
     
private:
	int channel;
	time_t time;
	TC& tc;

};


/**
 */
TC::TC (Board& _board, QDomElement xml, QString n): IComponent(_board, xml), board(_board) {
	
	// initialize registers
	setAddress(0xFFFA0000);
	
    IMR0 = 0;
    CMR0 = 0;      
    CV0 = 0;
    RC0=0;
    SR0 = 0;
    CCR0 = 0; RA0=0; RB0=0;
      
    IMR1 = 0;
    CMR1 = 0;       
    CV1 = 0;
    RC1=0;
    SR1 = 0;
    CCR1 = 0; RA1=0; RB1=0;
    
    IMR2 = 0;
    CMR2 = 0;      
    CV2 = 0;
    RC2=0;
    SR2 = 0;
    CCR2 = 0; RA2=0; RB2=0;
    
    BCR = 0;
    BMR = 0x0;
    
    add(INT0 = new outPin(*this, 0)); 
    add(INT1 = new outPin(*this, 1));
    add(INT2 = new outPin(*this, 2));
    
    add(sync= new synPin(*this, "SYNC")); 
     
    for(int i = 0; i < 3; i++)
		T0[i] = 0;		
     
    for(int i = 0; i < 3; i++) {
		add(tclk[i] = new TCPin(*this, i));
		add(tioa[i] = new TCPin(*this, i));   
		add(tiob[i] = new TCPin(*this, i));
	}  
         
	rc_event0 = new  RCEvent(board, 0, *this, 0);
	rc_event1 = new  RCEvent(board, 0, *this, 1);
	rc_event2 = new  RCEvent(board, 0, *this, 2);
       
	tc_event0 = new  TCEvent(board, 0, *this, 0);
	tc_event1 = new  TCEvent(board, 0, *this, 1);
	tc_event2 = new  TCEvent(board, 0, *this, 2);
       
       
	// configure from XML
	configure(xml);
		
	// Channel 0
	add(HardRegister32::make(new IndexedRegister(0, board)).name("CCR").address(0x00).type(IRegister::BITS).setter(*this, &TC::setCCR0));
	add(HardRegister32::make(new IndexedRegister(0, board)).name("CMR").address(0x04).type(IRegister::BITS).setter(*this, &TC::setCMR0));
	add(HardRegister32::make(new IndexedRegister(0, board)).name("CV").address(0x10).type(IRegister::BITS).getter(*this, &TC::getCV0));
	add(HardRegister32::make(new IndexedRegister(0, board)).name("RA").address(0x14).type(IRegister::BITS).setter(*this, &TC::setRA0).getter(*this, &TC::getRA0));
	add(HardRegister32::make(new IndexedRegister(0, board)).name("RB").address(0x18).type(IRegister::BITS).setter(*this, &TC::setRB0).getter(*this, &TC::getRB0));
	add(HardRegister32::make(new IndexedRegister(0, board)).name("RC").address(0x1C).type(IRegister::BITS).setter(*this, &TC::setRC0).getter(*this, &TC::getRC0));
	add(HardRegister32WithSoftIO::make(new StatusRegister(0, board)).name("SR").address(0x20).type(IRegister::BITS).getter(*this, &TC::getSR0IO, &TC::getSR0));
	add(HardRegister32::make(new IndexedRegister(0, board)).name("IER").address(0x24).type(IRegister::BITS).setter(*this, &TC::setIER0));
	add(HardRegister32::make(new IndexedRegister(0, board)).name("IDR").address(0x28).type(IRegister::BITS).setter(*this, &TC::setIDR0));
	add(HardRegister32::make(new IndexedRegister(0, board)).name("IMR").address(0x2C).type(IRegister::BITS).getter(*this, &TC::getIMR0));
  
	// Channel 1
	add(HardRegister32::make(new IndexedRegister(1, board)).name("CCR").address(0x40+0x00).type(IRegister::BITS).setter(*this, &TC::setCCR1));
	add(HardRegister32::make(new IndexedRegister(1, board)).name("CMR").address(0x40+0x04).type(IRegister::BITS).setter(*this, &TC::setCMR1));
	add(HardRegister32::make(new IndexedRegister(1, board)).name("CV").address(0x40+0x10).type(IRegister::BITS).getter(*this, &TC::getCV1));
	add(HardRegister32::make(new IndexedRegister(1, board)).name("RA").address(0x40+0x14).type(IRegister::BITS).setter(*this, &TC::setRA1).getter(*this, &TC::getRA1));
	add(HardRegister32::make(new IndexedRegister(1, board)).name("RB").address(0x40+0x18).type(IRegister::BITS).setter(*this, &TC::setRB1).getter(*this, &TC::getRB1));
	add(HardRegister32::make(new IndexedRegister(1, board)).name("RC").address(0x40+0x1C).type(IRegister::BITS).setter(*this, &TC::setRC1).getter(*this, &TC::getRC1));
	add(HardRegister32WithSoftIO::make(new StatusRegister(1, board)).name("SR").address(0x40+0x20).type(IRegister::BITS).getter(*this, &TC::getSR1IO, &TC::getSR1));
	add(HardRegister32::make(new IndexedRegister(1, board)).name("IER").address(0x40+0x24).type(IRegister::BITS).setter(*this, &TC::setIER1));
	add(HardRegister32::make(new IndexedRegister(1, board)).name("IDR").address(0x40+0x28).type(IRegister::BITS).setter(*this, &TC::setIDR1));
	add(HardRegister32::make(new IndexedRegister(1, board)).name("IMR").address(0x40+0x2C).type(IRegister::BITS).getter(*this, &TC::getIMR1));
  
	// Channel 2
	add(HardRegister32::make(new IndexedRegister(2, board)).name("CCR").address(2*0x40+0x00).type(IRegister::BITS).setter(*this, &TC::setCCR2));
	add(HardRegister32::make(new IndexedRegister(2, board)).name("CMR").address(2*0x40+0x04).type(IRegister::BITS).setter(*this, &TC::setCMR2));
	add(HardRegister32::make(new IndexedRegister(2, board)).name("CV").address(2*0x40+0x10).type(IRegister::BITS).getter(*this, &TC::getCV2));
	add(HardRegister32::make(new IndexedRegister(2, board)).name("RA").address(2*0x40+0x14).type(IRegister::BITS).setter(*this, &TC::setRA2).getter(*this, &TC::getRA2));
	add(HardRegister32::make(new IndexedRegister(2, board)).name("RB").address(2*0x40+0x18).type(IRegister::BITS).setter(*this, &TC::setRB2).getter(*this, &TC::getRB2));
	add(HardRegister32::make(new IndexedRegister(2, board)).name("RC").address(2*0x40+0x1C).type(IRegister::BITS).setter(*this, &TC::setRC2).getter(*this, &TC::getRC2));
	add(HardRegister32WithSoftIO::make(new StatusRegister(2, board)).name("SR").address(2*0x40+0x20).type(IRegister::BITS).getter(*this, &TC::getSR2IO, &TC::getSR2));
	add(HardRegister32::make(new IndexedRegister(2, board)).name("IER").address(2*0x40+0x24).type(IRegister::BITS).setter(*this, &TC::setIER2));
	add(HardRegister32::make(new IndexedRegister(2, board)).name("IDR").address(2*0x40+0x28).type(IRegister::BITS).setter(*this, &TC::setIDR2));
	add(HardRegister32::make(new IndexedRegister(2, board)).name("IMR").address(2*0x40+0x2C).type(IRegister::BITS).getter(*this, &TC::getIMR2));
  
	// BCR and BMR act upon the 3 CHANNELS
	add(HardRegister32::make(board).name("BCR").address(0xC0).type(IRegister::BITS).setter(*this, &TC::setBCR));
	add(HardRegister32::make(board).name("BMR").address(0xC4).type(IRegister::BITS).setter(*this, &TC::setBMR));
    
	// debug 
	info(QString("initialized (base=%1)").arg(address(), 8, 16, QChar('0')));
}


/**
 */
TC::~TC(void) { 
}

/**
 */
void TC::makeVisual(VisualGraph& graph) {

		// configure the component
		VisualComponent *vcomp = new VisualComponent(this);
		vcomp->w = 55;
		vcomp->h = 120;
		
		for(int i = 0; i <= 2; i++) 
		{
		
			// external clock input
			vcomp->add(new VisualPin(*vcomp, tclk[i], Visual::EAST, (10+ i*10)));             
	
			// I/O Line A
			vcomp->add(new VisualPin(*vcomp, tioa[i], Visual::EAST, (50 + i*10)));
		
			// I/O Line B
			vcomp->add(new VisualPin(*vcomp, tiob[i], Visual::EAST, (90 + i*10)));   
	
		}	
			
		vcomp->add(new VisualPin(*vcomp, INT0, Visual::WEST, 20)); 
		vcomp->add(new VisualPin(*vcomp, INT1, Visual::WEST, 30));
		vcomp->add(new VisualPin(*vcomp, INT2, Visual::WEST, 40));
		
		
		vcomp->add(new VisualPin(*vcomp, sync, Visual::WEST, 100)); 
				
		vcomp->parse(configuration());

		// prepare UI
		vcomp->make(name());
		vcomp->makePins();
		graph.add(vcomp);
}


/**
 * Get the divider of the given channel.
 * @param i		Channel to get divider for.
 * @return		Current divider or 0 if the timer is not periodic.
 */
int TC::div(int i) {
	uint32_t cmr;
	switch(i) {
	case 0:		cmr = CMR0; break;
	case 1:		cmr = CMR1; break;
	case 2:		cmr = CMR2; break;
	default:	assert(false); return 0;
	}

	switch(bitf(cmr, 0, 3)) {
    case 0:		return 2;
	case 1:		return 8;
	case 2: 	return 32;
	case 3: 	return 128;
	case 4: 	return 1024;
	case 5: 	return 0;
	case 6: 	return 0;
	case 7: 	return 0;
	default:	assert(false); return 0;
	}
}


/**
 * Get the counting period (time between counter reset) for the given timer.
 * @param i		Channel to get period for.
 * @return		Period (in MCK ticks) or 0 if the TC is not periodic.
 */
time_t TC::period(int i) {
	// !!TODO!! fix. Depend if CPCTRG is set or not.
	switch(i) {
	case 0:		return RC0;
	case 1:		return RC1;
	case 2:		return RC2;
	default:	assert(false); return 0;
	}
}


/**
 * starts the counting. The choice of the counting clock is based on the 5 internal clocks.
 * 
 * @param channel the number of the channel 0..2 .
 * @param cmr the content of the register CMR.
 * @param sr the content of the register SR. 
 */
void TC::startCLK(int channel,uint32_t cmr, uint32_t sr ) 
{
	
	switch (channel)
	{
			case 0: 
			{				    		          
		        if(!bit(cmr,15))                          // verify if it's in capture mode               
		        {     
					 if (bit(sr,16))                       // verify if clk is enabled
                     {    
				         time_t rctime=RC0; 
						 T0[0]= board.scheduler()->time(); 
		         
		                if(RC0!=0)                        
		                {		     
					        switch(bitf(cmr, 0, 3))      // TCCLKS
					       {     
			                  
						        case 0: 
								{  
								    rc_event0->resched(board.scheduler()->time()+rctime*2,0);                             /// time MCK/2      										                  
							    	break;
							    }                   
								case 1: 
								{   	 
									rc_event0->resched(board.scheduler()->time()+rctime*8,0);                              /// time MCK/8                           
									break;
								} 
								case 2:  
								{	   	 
									rc_event0->resched(board.scheduler()->time()+rctime*32,0);    /// time MCK/32                           
									break;
								} 
								case 3:   
								{   	 
									rc_event0->resched(board.scheduler()->time()+rctime*128,0);    /// time MCK/128                           
									break;
								} 
								case 4:  
								{   	 
									rc_event0->resched(board.scheduler()->time()+rctime*1024,0);    /// time MCK/1024                           
									break;
								} 
								case 5:   qDebug() << "CLOCK: XC0 " << endl;     break;  
								case 6:   qDebug() << "CLOCK: XC1 " << endl;     break;   
								case 7:   qDebug() << "CLOCK: XC2 " << endl;     break;    
							}					
						}
						else         
								tc_event0->resched(board.scheduler()->time()+ 0xffff,0);       
			        }
		        }
		        break;
		    }		                     
			case 1:
		    {  		        
		        if(!bit(cmr,15))                  
		        {     
					 if (bit(sr,16))             
                     {    
				        time_t rctime=RC1; 
				        T0[1]= board.scheduler()->time(); 
				   
		                if(RC1!=0)
		                {		
							switch (bitf(cmr, 0, 3))
							{
						        case 0: 
								{  
								    rc_event1->resched(board.scheduler()->time()+rctime*2,1);                             /// time MCK/2      										                  
							    	break;
							    }                   
								case 1: 
								{   	 
									rc_event1->resched(board.scheduler()->time()+rctime*8,1);                              /// time MCK/8                           
									break;
								} 
								case 2:  
								{	   	 
									rc_event1->resched(board.scheduler()->time()+rctime*32,1);    /// time MCK/32                           
									break;
								} 
								case 3:   
								{   	 
									rc_event1->resched(board.scheduler()->time()+rctime*128,1);    /// time MCK/128                           
									break;
								} 
								case 4:  
								{   	 
									rc_event1->resched(board.scheduler()->time()+rctime*1024,1);    /// time MCK/1024                           
									break;
								} 
								case 5:   qDebug() << "CLOCK: XC0 " << endl;     break;  
								case 6:   qDebug() << "CLOCK: XC1 " << endl;     break;   
								case 7:   qDebug() << "CLOCK: XC2 " << endl;     break; 
							}
						}
						else         
							tc_event1->resched(board.scheduler()->time()+ 0xffff,1);
					}
			   }
							 	
			   break;    
			} 
			case 2:
		    {   
		        
		         if(!bit(cmr,15))
		         {     
					 if (bit(sr,16))
                     {  

						time_t rctime=RC2; 
						T0[2]= board.scheduler()->time(); 
				  
						if(RC2!=0)
						{		
							switch(bitf(cmr, 0, 3))
							{      
						        case 0: 
								{  
								    rc_event2->resched(board.scheduler()->time()+rctime*2,2);                             /// time MCK/2      										                  
							    	break;
							    }                   
								case 1: 
								{   	 
									rc_event2->resched(board.scheduler()->time()+rctime*8,2);                              /// time MCK/8                           
									break;
								} 
								case 2:  
								{	   	 
									rc_event2->resched(board.scheduler()->time()+rctime*32,2);    /// time MCK/32                           
									break;
								} 
								case 3:   
								{   	 
									rc_event2->resched(board.scheduler()->time()+rctime*128,2);    /// time MCK/128                           
									break;
								} 
								case 4:  
								{   	 
									rc_event2->resched(board.scheduler()->time()+rctime*1024,2);    /// time MCK/1024                           
									break;
								} 
								case 5:   qDebug() << "CLOCK: XC0 " << endl;     break;  
								case 6:   qDebug() << "CLOCK: XC1 " << endl;     break;   
								case 7:   qDebug() << "CLOCK: XC2 " << endl;     break; 
						   }		
						}
						else         
							tc_event2->resched(board.scheduler()->time()+ 0xffff,2);       
					}
				 }
			      break; 
		     } 
	}
	
}

/**
 * reserts the counters value to 0.
 * @param channel the number of the channel 0..2.
 * @param all according to its value one or all counters are reset.
 */
void TC::resetCLK(int channel, bool all)
{ 
	    if(all)                       
	    {
	          	setf(CV0,0,15,0x0000);  T0[0]=0;
	          	setf(CV1,0,15,0x0000);  T0[1]=0;
	          	setf(CV2,0,15,0x0000);  T0[2]=0;
	    }
	    else
	    {
			switch (channel)
			{
				case 0: {  setf(CV0,0,15,0x0000);    T0[0]=0;   break;    }                  
				case 1: {  setf(CV1,0,15,0x0000);    T0[1]=0;   break;    }
				case 2: {  setf(CV2,0,15,0x0000);    T0[2]=0;   break;    }
			}
	    }
}

/**
 * Changes the output pin according to the given mask.
 * @param changes	Mask with a 1 for each pin whose state changes.
 * @param channel   the number of the channel 0..2.
 */
void TC::changeOut(uint32_t changes, int channel) {
	for(int i = 0; i < 32; i++) {
		uint32_t mask = 1 << i;
		if(changes & mask)
	    {  if(channel==0)  INT0->change(board.scheduler()->time(), true);
	       if(channel==1)  INT1->change(board.scheduler()->time(), true); 
	       if(channel==2)  INT2->change(board.scheduler()->time(), true);  
	    }
	}
}

/**
 * verifies if there is an interrupt to be asserted on the pin INT0.  
 */
uint32_t TC::it0() { return   getSR0() & IMR0; }   
/**
 * verifies if there is an interrupt to be asserted on the pin INT1.  
 */       
uint32_t TC::it1() { return   getSR1() & IMR1; }
/**
 * verifies if there is an interrupt to be asserted on the pin INT2.  
 */
uint32_t TC::it2() { return   getSR2() & IMR2; }

uint32_t TC:: getSR0()
{   			   
    uint32_t val= SR0;
    setf(SR0,0,7,0);
    return val;       
}

uint32_t TC:: getSR0IO()
{  
	return SR0;
}	

uint32_t TC:: getCV0() {
	if((bit(CCR0, 0)) && (T0[0] != 0)) {
		time_t d = div(0);
		if(d == 0)
			return CV0;
		else
			return ((board.scheduler()->time() - T0[0]) / d) % period(0);
	}
	else
		return CV0;
}


void TC::setCCR0(uint32_t v) {

	        setf(CCR0,0,31,v);
	               										 
			 if (bit(CCR0,0) && !(bit(CCR0,1)))                
			            {  
							set(SR0,16);
					    }
						                     
			 if(bit(CCR0,1)) 
			           {                                      
						    clear(SR0,16); 						    
						 	rc_event0->cancel(); 
						 	tc_event0->cancel();                   							 			 
						    T0[0]=0;                         
				       } 
			 if(bit(CCR0,2))                                  
					{
							T0[0]= board.scheduler()->time(); resetCLK(0, false); startCLK(0,CMR0, SR0); 
					}   
					
}

void TC::setCMR0(uint32_t v) {
  setf(CMR0,0,31,v);
}
    
void TC::setIER0(uint32_t v) {
	IMR0|= v;
}
    
void TC::setIDR0(uint32_t v) {
	IMR0 &= ~v;
}    
   
      
uint32_t TC:: getSR1()
{   			   
    uint32_t val= SR1;
    setf(SR1,0,7, 0x00 );
    return val; 
         
}

uint32_t TC:: getSR1IO()
{  
 return SR1;
}

uint32_t TC:: getCV1() {
	if((bit(CCR1, 0)) && (T0[1] != 0)) {
		time_t d = div(1);
		if(d == 0)
			return CV1;
		else
			return ((board.scheduler()->time() - T0[1]) / d) % period(1);
	}
	else
		return CV1;
}


void TC::setCCR1(uint32_t v) {

	        setf(CCR1,0,31,v);
	               										 
			 if (bit(CCR1,0) && !(bit(CCR1,1)))                      
			            {  
							set(SR1,16);
					    }
						                     
			 if(bit(CCR1,1)) 
			           {                                             
						    clear(SR1,16); 						    
						 	rc_event1->cancel(); 
						 	tc_event1->cancel();                   							 			 
						    T0[1]=0;                         
				       } 
			 if(bit(CCR1,2))                                       
					{
							T0[1]= board.scheduler()->time(); resetCLK(1, false); startCLK(1,CMR1, SR1); 
					}   
					
}

void TC::setCMR1(uint32_t v) {
  setf(CMR1,0,31,v);
}
    
void TC::setIER1(uint32_t v) {
	IMR1|= v;
}
    
void TC::setIDR1(uint32_t v) {
	IMR1 &= ~v;
}    
   
uint32_t TC:: getSR2()
{   			   
    uint32_t val= SR2;
    setf(SR2,0,7, 0x00 );
    return val; 
         
}

uint32_t TC:: getSR2IO()
{  
 return SR2;
}

uint32_t TC:: getCV2() {
	if((bit(CCR2, 0)) && (T0[2] != 0)) {
		time_t d = div(2);
		if(d == 0)
			return CV2;
		else
			return ((board.scheduler()->time() - T0[2]) / d) % period(2);
	}
	else
		return CV2;
}


void TC::setCCR2(uint32_t v) {

	        setf(CCR2,0,31,v);
	               										 
			 if (bit(CCR0,0) && !(bit(CCR2,1)))                         
			            {  
							set(SR2,16);
					    }
						                     
			 if(bit(CCR2,1)) 
			           {                                     
						    clear(SR2,16); 						    
						 	rc_event2->cancel(); 
						 	tc_event2->cancel();                    							 			 
						    T0[2]=0;                         
				       } 
			 if(bit(CCR2,2))                                            
					{
							T0[2]= board.scheduler()->time(); resetCLK(2, false); startCLK(2,CMR2, SR2); 
					}   					
}

void TC::setCMR2(uint32_t v) {
  setf(CMR2,0,31,v);
}
    
void TC::setIER2(uint32_t v) {
	IMR2|= v;
}
    
void TC::setIDR2(uint32_t v) {
	IMR2 &= ~v;
}    
   
void TC::setBCR(uint32_t v) {

	 if(bit(v,0))  
	 {
		  resetCLK(0, true);	  
     }
	 setf(BCR,0,31,v);	
}

void TC::setBMR(uint32_t v) {
  
  	setf(BMR,0,31,v);
  switch(bitf(v, 0, 1))
  {
    case 0:   qDebug() << "TCLK0 " << endl;     break;  
	case 1:   qDebug() << "NO EXTERNAL SIGNAL WAS CHOSEN! " << endl;     break;   
	case 2:   qDebug() << "TIOA1 " << endl;     break;
	case 3:   qDebug() << "TIOA2 " << endl;     break;    
   }
   
   switch(bitf(v, 2, 3))
  {
    case 0:   qDebug() << "TCLK1 " << endl;     break;  
	case 1:   qDebug() << "NO EXTERNAL SIGNAL WAS CHOSEN! " << endl;     break;   
	case 2:   qDebug() << "TIOA0 " << endl;     break;
	case 3:   qDebug() << "TIOA2 " << endl;     break;    
   }
   
   switch(bitf(v, 4, 5))
  {
    case 0:   qDebug() << "TCLK2 " << endl;     break;  
	case 1:   qDebug() << "NO EXTERNAL SIGNAL WAS CHOSEN! " << endl;     break;   
	case 2:   qDebug() << "TIOA0 " << endl;     break;
	case 3:   qDebug() << "TIOA1 " << endl;     break;    
   }	
}


/**
 */
void TC::getStaticConstants(QVector<Constant>& csts) const {
	
	// TC_BCR
	csts.append(Constant::mask("SYNC",	0));
	
	// TC_BMR
	csts.append(Constant::field("TC0XC0S_TCLK0", 0b00, 0));
	csts.append(Constant::field("TC0XC0S_none",  0b01, 0));
	csts.append(Constant::field("TC0XC0S_TIOA1", 0b10, 0));
	csts.append(Constant::field("TC0XC0S_TIOA2", 0b11, 0));
	csts.append(Constant::field("TC1XC1S_TCLK1", 0b00, 2));
	csts.append(Constant::field("TC1XC1S_none",  0b01, 2));
	csts.append(Constant::field("TC1XC1S_TIOA0", 0b10, 2));
	csts.append(Constant::field("TC1XC1S_TIOA2", 0b11, 2));
	csts.append(Constant::field("TC2XC2S_TCLK2", 0b00, 4));
	csts.append(Constant::field("TC2XC2S_none",  0b01, 4));
	csts.append(Constant::field("TC2XC2S_TIOA0", 0b10, 4));
	csts.append(Constant::field("TC2XC2S_TIOA1", 0b11, 4));
	
	// TC_CCR
	csts.append(Constant::mask("CLKEN", 	0));
	csts.append(Constant::mask("CLKDIS",	1));
	csts.append(Constant::mask("SWTRG",		2));
	
	// TC_CMR
	csts.append(Constant::field("TCCLKS_CLOCK1", 	0b000,	 0));
	csts.append(Constant::field("TCCLKS_CLOCK2", 	0b001,	 0));
	csts.append(Constant::field("TCCLKS_CLOCK3", 	0b010,	 0));
	csts.append(Constant::field("TCCLKS_CLOCK4", 	0b011,	 0));
	csts.append(Constant::field("TCCLKS_CLOCK5", 	0b100,	 0));
	csts.append(Constant::field("TCCLKS_XC0",		0b101,	 0));
	csts.append(Constant::field("TCCLKS_XC1", 		0b110,	 0));
	csts.append(Constant::field("TCCLKS_XC2", 		0b111,	 0));
	csts.append(Constant::mask("CLKI",						 3));
	csts.append(Constant::field("BURST_NONE",		0b00,	 4));
	csts.append(Constant::field("BURST_XC0",		0b01,	 4));
	csts.append(Constant::field("BURST_XC1",		0b10,	 4));
	csts.append(Constant::field("BURST_XC2",		0b11,	 4));

	// WAVE = 0
	csts.append(Constant::mask("LDBSTOP",					 6));
	csts.append(Constant::mask("LDBDIS",					 7));
	csts.append(Constant::field("ETRGEDG_NONE",		0b00,	 8));
	csts.append(Constant::field("ETRGEDG_RISE",		0b01,	 8));
	csts.append(Constant::field("ETRGEDG_FALL",		0b10,	 8));
	csts.append(Constant::field("ETRGEDG_BOTH",		0b11,	 8));
	csts.append(Constant::mask("ABETRG",					10));
	csts.append(Constant::mask("CPCTRG",					14));
	csts.append(Constant::mask("WAVE",						15));
	csts.append(Constant::field("LDRA_NONE",		0b00,	16));
	csts.append(Constant::field("LDRA_RISE",		0b01,	16));
	csts.append(Constant::field("LDRA_FALL",		0b10,	16));
	csts.append(Constant::field("LDRA_BOTH",		0b11,	16));
	csts.append(Constant::field("LDRB_NONE",		0b00,	18));
	csts.append(Constant::field("LDRB_RISE",		0b01,	18));
	csts.append(Constant::field("LDRB_FALL",		0b10,	18));
	csts.append(Constant::field("LDRB_BOTH",		0b11,	18));
	
	// WAVE = 1
	csts.append(Constant::mask("CPCSTOP",					 6));
	csts.append(Constant::mask("CPCDIS",					 7));
	csts.append(Constant::field("EEVTEDG_NONE",		0b00,	 8));
	csts.append(Constant::field("EEVTEDG_RISE",		0b01,	 8));
	csts.append(Constant::field("EEVTEDG_FALL",		0b10,	 8));
	csts.append(Constant::field("EEVTEDG_BOTH",		0b11,	 8));
	csts.append(Constant::field("EEVT_TIOB",		0b00,	10));
	csts.append(Constant::field("EEVT_XC0",			0b01,	10));
	csts.append(Constant::field("EEVT_XC1",			0b10,	10));
	csts.append(Constant::field("EEVT_XC2",			0b11,	10));
	csts.append(Constant::mask("ENETRG",					12));
	csts.append(Constant::field("WAVSEL_UP_WO",		0b00,	13));
	csts.append(Constant::field("WAVSEL_UP_WI",		0b01,	13));
	csts.append(Constant::field("WAVSEL_UPDOWN_WO",	0b10,	13));
	csts.append(Constant::field("WAVSEL_UPDWON_WI",	0b11,	13));
	csts.append(Constant::field("ACPA_NONE",		0b00,	16));
	csts.append(Constant::field("ACPA_SET",			0b01,	16));
	csts.append(Constant::field("ACPA_CLEAR",		0b10,	16));
	csts.append(Constant::field("ACPA_TOGGLE",		0b11,	16));
	csts.append(Constant::field("ACPC_NONE",		0b00,	18));
	csts.append(Constant::field("ACPC_SET",			0b01,	18));
	csts.append(Constant::field("ACPC_CLEAR",		0b10,	18));
	csts.append(Constant::field("ACPC_TOGGLE",		0b11,	18));
	csts.append(Constant::field("AEEVT_NONE",		0b00,	20));
	csts.append(Constant::field("AEEVT_SET",		0b01,	20));
	csts.append(Constant::field("AEEVT_CLEAR",		0b10,	20));
	csts.append(Constant::field("AEEVT_TOGGLE",		0b11,	20));
	csts.append(Constant::field("ASWTRG_NONE",		0b00,	22));
	csts.append(Constant::field("ASWTRG_SET",		0b01,	22));
	csts.append(Constant::field("ASWTRG_CLEAR",		0b10,	22));
	csts.append(Constant::field("ASWTRG_TOGGLE",	0b11,	22));
	csts.append(Constant::field("BCPA_NONE",		0b00,	16));
	csts.append(Constant::field("BCPA_SET",			0b01,	16));
	csts.append(Constant::field("BCPA_CLEAR",		0b10,	16));
	csts.append(Constant::field("BCPA_TOGGLE",		0b11,	16));
	csts.append(Constant::field("BCPC_NONE",		0b00,	18));
	csts.append(Constant::field("BCPC_SET",			0b01,	18));
	csts.append(Constant::field("BCPC_CLEAR",		0b10,	18));
	csts.append(Constant::field("BCPC_TOGGLE",		0b11,	18));
	csts.append(Constant::field("BEEVT_NONE",		0b00,	20));
	csts.append(Constant::field("BEEVT_SET",		0b01,	20));
	csts.append(Constant::field("BEEVT_CLEAR",		0b10,	20));
	csts.append(Constant::field("BEEVT_TOGGLE",		0b11,	20));
	csts.append(Constant::field("BSWTRG_NONE",		0b00,	22));
	csts.append(Constant::field("BSWTRG_SET",		0b01,	22));
	csts.append(Constant::field("BSWTRG_CLEAR",		0b10,	22));
	csts.append(Constant::field("BSWTRG_TOGGLE",	0b11,	22));
	
	// TC_SR / TC_IER / TC_IDR / TC_IMR
	csts.append(Constant::mask("COVFS",	 0));
	csts.append(Constant::mask("LOVRS",	 1));
	csts.append(Constant::mask("CPAS",	 2));
	csts.append(Constant::mask("CPBS",	 3));
	csts.append(Constant::mask("CPCS",	 4));
	csts.append(Constant::mask("LDRAS",	 5));
	csts.append(Constant::mask("LDRBS",	 6));
	csts.append(Constant::mask("ETRGS",	 7));
	csts.append(Constant::mask("CLKSTA",16));
	csts.append(Constant::mask("MTIOA",	17));
	csts.append(Constant::mask("MTIOB",	18));
	
	csts.append(Constant::mask("CPCS",		4));
	csts.append(Constant::mask("COVFS",		0));
}


/**
 * Called to build a new TC.
*/
extern "C" IComponent* init(Board& board,QDomElement xml, QString name) {
	return new TC(board, xml, name);
}


} 	// bsim



