/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
#ifndef BSIM_PWM_HPP
#define BSIM_PWM_HPP

#include <stdint.h>

#include <QDomElement>

#include <Board.hpp>
#include <IComponent.hpp>
#include <Scheduler.hpp>
#include <UInt32Register.hpp>

namespace bsim {
     class outPin;          
     class PWMEvent;
	 class DUTYEvent;
	 class CENTEREvent;
	  
class PWM: public IComponent {
	  friend class outPin;
	  friend class PWMEvent;
	  friend class DUTYEvent;
	  friend class CENTEREvent;
public:
	static const int cnum = 4;
 
	PWM(Board& b, QDomElement dom, QString name);
	~PWM(void);

	virtual void makeVisual(VisualGraph& graph);    
	virtual void genHeader(QTextStream& out); 
		  
	void setMR (uint32_t v) ;
	void setENA(uint32_t v) ;               
	void setDIS(uint32_t v) ; 
	inline uint32_t getSR(void) { return SR; }
	void setIER(uint32_t v) ;  
	void setIDR(uint32_t v) ;
	inline uint32_t getIMR(void) { return IMR; }
	uint32_t getISR();
	uint32_t getISRIO();  
		
	void setCMR(int i, uint32_t v);
	 
	void setCDTY(int i, uint32_t v) ;  
	void setCPRD(int i, uint32_t v) ;
	uint32_t getCCNT(int i);
	void setCUPD(int i, uint32_t v) ;  

	uint32_t it(void);
	void startCounter(int channel);
	void changeOUT(uint32_t val);
	void createEvent(int channel,bool value, time_t time);
	void clkAB(int channel, bool clka);

	inline void resetCLK(int i)  {	
		assert(0 <= i && i < cnum);
		CCNT[i] = 0;
		T0[i] = 0;
	} 
    
private:

	time_t T0[4];
	
	class MRClass: public UInt32Register {
	public:
		MRClass(IComponent *comp, const Make& make);
		BitField DIVA;
		BitEnum PREA;
		BitField DIVB;
		BitEnum PREB;
	} MR;
	
	UInt32ControlRegister ENA, DIS;
	
	class SRClass: public UInt32Register {
	public:
		SRClass(IComponent *comp, const Make& make);
		BitArray CHID;
	} SR;

	UInt32ControlRegister IER, IDR;
	SRClass IMR, ISR;
	class CMRClass: public UInt32Array {
	public:
		CMRClass(IComponent *comp, const Make& make);
		BitEnum CPRE;
		Bit CALG;
		Bit CPOL;
		Bit CPD;
	} CMR;
	UInt32Array CDTY, CPRD, CCNT, CUPD;

	Pin *OUT[cnum];
	Pin *INT;

	int clknum[4];

	PWMEvent *pwm_event[cnum];
	DUTYEvent *duty_event[cnum];
	CENTEREvent *center_event[cnum];

	uint32_t
		periodeA[cnum],
		periodeB[cnum],
		duty_A[cnum],
		duty_B[cnum];
	bool update[cnum];
};
	
}	// bsim

#endif
