/**
 * This file is part of BoardSim.
 * Copyright (c) 2017, University of Toulouse
 * 
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */
 
#include <QLabel>
#include <QDebug>
#include <Display.hpp>
#include <IRegister.hpp>
#include <Scheduler.hpp>
#include <Board.hpp>
#include "PWM.hpp"

//#define TRACE_PWM
#ifdef TRACE_PWM
#	define TRACE(c)		c
#else
#	define TRACE(c)
#endif


namespace bsim{
	
/**
 * @class PWM
 * 
 * @li Four channels, one 16-bit counter per channel
 * @li Common clock generator, providing thirteen different clocks
 *    - One Mudulo n counter providing eleven clocks
 *    - Two independent linear dividers working on modulo n counter outputs
 * @li Independent channel programming
 *    - Independent enable/disable commands 
 *    - Independent clock selection
 *    - Programmable selection of the output waveform polarity
 *    - Programmable center or left aligned waveform 
 * 
 * @par Pins
 * @li @c PWM0 -- output pin,
 * @li @c PWM1 -- output pin,
 * @li @c PWM2 -- output pin,
 * @li @c PWM3 -- output pin,
 * @li @c INT  -- output pin.
 * 
 * @par Configuration
 * No configuration.
 * 
 * @par Registers
 * The following registers are 32-bits wide and each bit is associated to a line of the same bit number.
 * @li @c MR --   configurates the clock generator(read/write),
 * @li @c ENA --  enables the PWMx outputs for the channels  by writing 1 on the corresponding bit in SR(write),
 * @li @c DIS  -- disables the PWMx outputs for the channels  by writing 0 on the corresponding bit in SR  (write),
 * @li @c SR  --  whether the PWMx outputs are enabled for the channels or not (read),
 * @li @c IER  -- enables interrupts for PWMx channel by writing 1 on the corresponding bit in IMR (write),
 * @li @c IDR  -- disables interrupts for PWMx channel by writing 0 on the corresponding bit in IMR  (write),
 * @li @c IMR  -- the state of the interrupts, enabled/ disabled (read), 
 * @li @c ISR --  whether a new channel period was occured since its last read or not(read),
 * @li @c CMR --  configurates the channel alignment, polarity and the period update (read/write),
 * @li @c CDTY -- defines the waveform duty cycle (read/write),
 * @li @c CPRD -- defines the waveform period (read/write),
 * @li @c CCNT -- contains the internal counter value (read),
 * @li @c CUPD -- acts as a double buffer for the period or the duty cycle (write),
 * 
 * @ingroup components
 */

/**
 * Constructor.
 * @param board		Current board.
 * @param xml		Configuration.
 * @param n			Component logic name.
 */
	                            	
class outPin: public Pin {
public:
	outPin (PWM& _pwm, QString name): Pin(&_pwm, name), pwm(_pwm) {
     }	
	void onChange(uint64_t time, bool value) override {	}
	
private:
	PWM& pwm;
};

 // Internal counter
class PWMEvent: public Scheduler::Event {
	 public:
	 PWMEvent (Board& _board, time_t tm, PWM& _pwm, int chl)
	 :	Scheduler::Event(_board, tm, "PWM::PeriodEvent"),
		pwm(_pwm),
		time(tm),
		channel(chl)
	 { } 
	 
	void run(void) {
		pwm.ISR.CHID[channel] = 1;
		if(pwm.IMR.CHID[channel])
			pwm.changeOUT(pwm.it());            				 						
		if(pwm.update[channel]) {
			pwm.update[channel] = false;
			if(!pwm.CMR.CPD[channel])
				pwm.setCDTY(channel, pwm.CUPD[channel]);
			else
				pwm.setCPRD(channel, pwm.CUPD[channel]);            								                                             
		}
		pwm.resetCLK(channel);
		pwm.startCounter(channel);
    }
    
	void resched(time_t t, int chl) {
		reschedule(t);
		channel = chl;    
	}
    
private:
	int channel;
	time_t time;
	PWM& pwm;
};

// Duty Cycle first event
class DUTYEvent: public Scheduler::Event {
public:
	 DUTYEvent (Board& _board, time_t tm, PWM& _pwm, int chl, bool cpol, int clknum)
	 :	Scheduler::Event(_board, tm,  "PWM::DutyCycleFirstEvent"),
		pwm(_pwm),
		time(tm),
		channel(chl),
		CPOL(cpol),
		clkNumber(clknum)
	 { } 
	 
	void run(void) {
		
		// high-level start
		if(CPOL) {
			pwm.OUT[channel]->change(pwm.board().scheduler()->time(), false);
			if(pwm.CMR.CALG[channel]) { 
				uint32_t cprd = pwm.CPRD[channel];
				uint32_t cdty = pwm.CDTY[channel];
				time_t tm = 2 * (cprd - cdty);
				pwm.createEvent(channel, true, tm * clkNumber);
			}	
		}
		
		// low-level start
		else { 
			pwm.OUT[channel]->change(pwm.board().scheduler()->time(), true);
			if(pwm.CMR.CALG[channel]) { 
				uint32_t cprd = pwm.CPRD[channel];  
				uint32_t cdty = pwm.CDTY[channel];
				time_t tm = 2 * (cprd - cdty);
				pwm.createEvent(channel, false, tm * clkNumber);
			}
		}
	}
    
	void resched(time_t t, int ch, bool cp, int clkn) {
		reschedule(t);
		channel = ch;
		CPOL = cp;
		clkNumber = clkn;          
	}  
    
private:
	time_t time;
	int channel;
	int clkNumber;
	bool CPOL;
	PWM& pwm;
};


// Duty cycle second event (center aligned waveform case)
class CENTEREvent: public Scheduler::Event {
	 public:
	 CENTEREvent (Board& _board, time_t tm, PWM& _pwm, int chl, bool val)
	 :	Scheduler::Event(_board, tm,  "PWM::DutyCycleCenterEvent"),
		pwm(_pwm),
		time(tm),
		channel(chl),
		value(val)
		{ } 
	 
	void run(void) {
		pwm.OUT[channel]->change(pwm.board().scheduler()->time(), value);
	}
	
	void resched(time_t t, int ch, bool v) {
		reschedule(t);
		channel = ch;
		value = v;         
	}  
    
private:
	time_t time;
	int channel;
	bool value;
	PWM& pwm;
};

PWM::PWM(Board& board, QDomElement xml, QString n)
:	IComponent(board, xml),
	MR (this, 	UInt32::make()			.name("MR")  .offset(0x000).set(this, &PWM::setMR)),
	ENA(this,	UInt32::make()			.name("ENA") .offset(0x004).set(this, &PWM::setENA).write()),
	DIS(this, 	UInt32::make()			.name("DIS") .offset(0x008).set(this, &PWM::setDIS).write()),
	SR (this,	UInt32::make()			.name("SR")  .offset(0x00c).get(this, &PWM::getSR).read()),
	IER(this,	UInt32::make()			.name("IER") .offset(0x010).set(this, &PWM::setIER).write()),
	IDR(this, 	UInt32::make()			.name("IDR") .offset(0x014).set(this, &PWM::setIDR).write()),
	IMR(this,	UInt32::make()			.name("IMR") .offset(0x018).get(this, &PWM::getIMR).read()),
	ISR(this,	UInt32::make()			.name("ISR") .offset(0x01C).get(this, &PWM::getISR).read()),
	CMR(this,	UInt32Array::make(cnum)	.name("CMR") .offset(0x200).stride(0x20).set(CMR,  this, &PWM::setCMR)),
	CDTY(this,	UInt32Array::make(cnum)	.name("CDTY").offset(0x204).stride(0x20).set(CDTY, this, &PWM::setCDTY)),
	CPRD(this,	UInt32Array::make(cnum)	.name("CPRD").offset(0x208).stride(0x20).set(CPRD, this, &PWM::setCPRD)),
	CCNT(this,	UInt32Array::make(cnum)	.name("CCNT").offset(0x20C).stride(0x20).get(CCNT, this, &PWM::getCCNT).read()),
	CUPD(this,	UInt32Array::make(cnum)	.name("CUPD").offset(0x210).stride(0x20).set(CUPD, this, &PWM::setCUPD).write())
{

    // initialize registers
	setAddress(0xFFFCC000);
	*MR  = 0;
	*SR  = 0;
	*IMR = 0;
	*ISR = 0;
	for(int i = 0; i < cnum; i++) {	
		CMR[i]  = 0;
		CDTY[i] = 0;
		CPRD[i] = 0;
		CCNT[i] = 0;
		CUPD[i] = 0;
	}
     
	// initialize clocks
	for(int i = 0; i < cnum; i++) {
		clknum[i] = 0;
		T0[i] = 0;
	}       

	// initialzize pins
    add(INT = new outPin(*this, "INT"));
    for(int i = 0; i < cnum; i++) 
		add(OUT[i] = new outPin(*this, QString("PWM%1").arg(i)));
    
    // initialize events
    for(int i = 0; i < cnum; i++) {
		duty_event[i] 	= new DUTYEvent(board, 0, *this, i, false, 0);
		pwm_event[i] 	= new PWMEvent(board, 0, *this, i); 
		center_event[i] = new CENTEREvent(board, 0, *this, i, false); 
	}
     
	// configure from XML
	configure(xml);
		
	// debug 
	info(QString("initialized (base=%1)").arg(address(), 8, 16, QChar('0')));	
}

/**
 */
PWM::~PWM(void) {
	// TODO	free pins and events
}

/**
 */
void PWM::makeVisual(VisualGraph& graph) {

		// configure the component
		VisualComponent *vcomp = new VisualComponent(this);
		vcomp->w = 100;
		vcomp->h = 120;
		
		// put the pints
		int top = (120 - (cnum - 1) * 15) / 2; 
		for(int i = 0; i < cnum; i++)
			vcomp->add(new VisualPin(*vcomp, OUT[i], Visual::EAST, top + i * 15));
        vcomp->add(new VisualPin(*vcomp, INT, Visual::WEST, (15)));		
		vcomp->parse(configuration());

		// prepare UI
		vcomp->make(name());
		vcomp->makePins();
		graph.add(vcomp);
}	

/**
 * Adds the second duty cycle event (go from signal = o to 1 and vice versa) in the case of a center aligned waveform .
 * @param channel  the number of the channel 0..3.
 * @param value  the value of the signal.
 * @param time  the time of the event.
 */
void PWM ::createEvent(int channel, bool value, time_t time) {
	center_event[channel]->resched(board().scheduler()->time()+ time, channel, value);
}

/**
 * Changes the output pin according to the given mask.
 * @param changes	Mask with a 1 for each pin whose state changes.
 */
void PWM ::changeOUT(uint32_t changes)
{
		for(int i = 0; i < 4; i++) {
			uint32_t mask = 1 << i;
			if(changes & mask) {
				INT->change(board().scheduler()->time(), true);
			}
		}
}

/**
 * verifies if there is an interrupt to be asserted.  
 */
uint32_t PWM ::it() { 
	return   getISR() & IMR;  
} 
/**
 * calculates the period and the duty cycle according to the chosen clock (CLKA or CLKB).
 * @param channel the number of the channel 0..3.
 * @param clka   whether CLKA is chosen or CLKB.
 */
void PWM::clkAB(int channel, bool clka) {	
	
	// get period and duty
	uint32_t cprd, cdty;
	cprd = CPRD[channel];
	cdty = CDTY[channel];
	
	// compute DIV and PRE
	uint32_t DIV, PRE;
	if(clka) {
		DIV = MR.DIVA;
		PRE = MR.PREA;
	}
	else {
		DIV = MR.DIVB;
		PRE = MR.PREB;
	}
	
	// compute period and duty
	uint32_t  periode, duty_cycle;
	if(DIV == 0 || PRE > 0b1010) {
		periode = 0;
		duty_cycle = 0;
	}
	else if(PRE > 0b1010) {
		periode = (cprd << PRE) * DIV;
		duty_cycle = (cdty << PRE) * DIV;		
	}
	
	// assign period and duty
	if(clka)
		for(int i = 0; i < cnum; i++) {
			periodeA[i] = periode;
			duty_A[i] = duty_cycle;
		}
	else
		for(int i = 0; i < cnum; i++) {
			periodeB[i] = periode;
			duty_B[i] = duty_cycle;
		}
} 

/**
 * Starts the counting.
 * @param channel the number of the channel 0..3.
 */
void PWM::startCounter(int channel) {
	
	// prepare data
	T0[channel] = board().scheduler()->time();
	time_t cprd = CPRD[channel];  
	time_t cdty = CDTY[channel];
	TRACE(qDebug() << "PWM: ch=" << channel << ", cdty=" << cdty << ", cprd=" << cprd);

	// compute times
	time_t periode0, duty_cycle0;
	if(CMR.CPRE[channel] < 0b1011) {
		periode0 = cprd << CMR.CPRE[channel];
		duty_cycle0 = cdty << CMR.CPRE[channel];
		clknum[channel] = 1 << CMR.CPRE[channel];
	}
	else if(CMR.CPRE[channel] == 0b1011) {
		clkAB(channel, true);
		periode0 = periodeA[channel];
		duty_cycle0 = duty_A[channel];
	}
	else if(CMR.CPRE[channel] == 0b1100) {
		clkAB(channel, false);
		periode0 = periodeB[channel];
		duty_cycle0 = duty_B[channel];
	}
	else
		return;
	
	// high-level start
	if(CMR.CPOL[channel]) {																
		if(cprd==cdty)
			OUT[channel]->change(board().scheduler()->time(), true);
		else {	
			if(cdty == 0)
				OUT[channel]->change(board().scheduler()->time(), false);
			else {
				OUT[channel]->change(board().scheduler()->time(), true);
				if(CMR.CALG[channel])
					periode0 = periode0 * 2;
				duty_event[channel]->resched(board().scheduler()->time() + duty_cycle0, channel, true, clknum[channel]); 				
			}
		}
	}
	
	// low-level start
	else {
		if(cprd == cdty)
			OUT[channel]->change(board().scheduler()->time(), false);
		else {	
			if(cdty == 0)
				OUT[channel]->change(board().scheduler()->time(), true);
			else {
				OUT[channel]->change(board().scheduler()->time(), false);           ////
				if(CMR.CALG[channel])
					periode0 = periode0 * 2;                           // if the waveform is center aligned
				duty_event[channel]->resched(board().scheduler()->time() + duty_cycle0, channel, false, clknum[channel]); 				
			}
		}	
	}
	
	// start event
	if(periode0)
		pwm_event[channel]->resched((board().scheduler()->time() + periode0), channel);	
			
}

/**
 */
uint32_t PWM::getISR(void) { 
	uint32_t val = ISR;
	*ISR = 0;
	return val; 
}

/**
 */
void PWM::setMR (uint32_t v) {
	*MR = v;
}

/**
 */
void PWM::setENA(uint32_t v) {
    SR |= v; 
    for(int i = 0; i < cnum; i++)
		if(uint32::bit(v, i)) {
			resetCLK(i);
			startCounter(i);
			update[i] = false;
		}
}              

/**
 */
void PWM::setDIS(uint32_t v) {
	SR &= ~v;
	for(int i = 0; i < cnum; i++)
		if(uint32::bit(v, i)) {
			pwm_event[i]->cancel();
			duty_event[i]->cancel();
			center_event[i]->cancel();
		}
} 

/**
 */
void PWM::setIER(uint32_t v) {
		IMR |= v;
} 

/**
 */
void PWM::setIDR(uint32_t v) {
	IMR &= ~v;
}

/**
 */
uint32_t PWM:: getCCNT(int i) {
	assert(0 <= i && i < cnum);          
	if(SR.CHID[i] && T0[i] != 0) {
		time_t	t = board().scheduler()->time();  
		t = t - T0[0];	// TODO seems dubious
		return t;	
	}
	else
		return CCNT[i];
}   

/**
 */
void PWM::setCMR(int i, uint32_t v) {
	assert(0 <= i && i < CMR.count());
	if(SR.CHID[i])
		setb(CMR[i], 10, bit(v,10));
	else
		CMR[i] = v;
}

/**
 */
void PWM ::setCDTY(int i, uint32_t v) {
	assert(0 <= i && i < CMR.count());
	if(v <= CPRD[i])
		CDTY[i] = v;
	// TODO dubious: maybe we have to recompute events
}  

/**
 */
void PWM::setCPRD(int i, uint32_t v) {	
	assert(0 <= i && i < CMR.count());
	if(v >= CDTY[i])
		CPRD[i] = v;
	// TODO dubious: maybe we have to recompute events
} 

/**
 */
void PWM::setCUPD(int i, uint32_t v) {
	CUPD[i] = v;
	update[i] = true;
	TRACE(qDebug() << "PWM: CUPD[" << i << "]=" << v);
}  

/**
 */	
void PWM::genHeader(QTextStream& out) {  		
	out << "	.equ PWM_CMR_CALG, 	(1 << 8)\n";
	out << "	.equ PWM_CMR_CPOL, 	(1 << 9)\n";
	out << "	.equ PWM_CMR_CPDON, 	(1 << 10)\n";
	out << "	.equ PWM_CMR_CPDOFF, 	(0 << 10)\n";
}

/**
 */
PWM::MRClass::MRClass(IComponent *comp, const Make& make)
:	UInt32Register(comp, make),
	DIVA(*this, "DIVA",  7,  0),
	PREA(*this, "PREA", 11,  8, 0),
	DIVB(*this, "DIVB", 23, 16),
	PREB(*this, "PREB", 27, 24, 0)
{
}

/**
 */
PWM::SRClass::SRClass(IComponent *comp, const Make& make)
:	UInt32Register(comp, make),
	CHID(*this,	"CHID",	 3,  0)
{
}


/**
 */
PWM::CMRClass::CMRClass(IComponent *comp, const Make& make)
:	UInt32Array(comp, make),
	CPRE(*this,	"CPRE",	 3,  0, _
		& val("MCK",		0b0000)
		& val("MCK_2",		0b0001)
		& val("MCK_4",		0b0010)
		& val("MCK_8",		0b0011)
		& val("MCK_16",		0b0100)
		& val("MCK_32",		0b0101)
		& val("MCK_64",		0b0110)
		& val("MCK_128",	0b0111)
		& val("MCK_256",	0b1000)
		& val("MCK_512",	0b1001)
		& val("MCK_1024",	0b1010)
		& val("CLKA",		0b1011)
		& val("CLKB",		0b1100)),
	CALG(*this,	"CALG",	 	 8),
	CPOL(*this,	"CPOL",  	 9),
	CPD	(*this,	"CPD",  	10)
{ }

	
/**
 * Called to build a new PWM.
 */
extern "C" IComponent* init(Board& board,QDomElement xml, QString name) {
	return new PWM (board, xml, name);	
}
	
}  // bsim
