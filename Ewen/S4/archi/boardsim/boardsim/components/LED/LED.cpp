/**
 * This file is part of BoardSim.
 *
 * BoardSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BoarSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BoardSim.  If not, see <http://www.gnu.org/licenses/>
 */

#define BSIM_LED_SVG

#include <QDebug>
#include <QPainter>
#include <QGraphicsRectItem>
#include <MainWindow.hpp>
#include <Display.hpp>
#ifdef BSIM_LED_SVG
#	include <QGraphicsSvgItem>
#	include <QGraphicsColorizeEffect>
#endif

//#include "LED.hpp"
#include "Board.hpp"
#include "Display.hpp"


namespace bsim {

/**
 * @class LED
 * Represents a simple LED.
 *
 * @par Pins
 * @li @c in - the input of the LED (a 1 switches the LED on, a 0 off)
 *
 * @par Configuration
 * @li attribute @c negative = "yes" - causes the LED to work in negative logic (0 for switch on)
 * @li attribute @c color = "color" - switched-on color (in HTML color format)
 * @li attribute @c off_color = "color" - switched-off color (in HTML color format)
 * @li attribute @c border = "color" - border color (in HTML color format)
 *
 * @ingroup components
 */
class LED: public IComponent {
public :
	const static int w = 20, h = 30;

	/**
	 * Build a LED.
	 * @param board		Container board.
	 * @param xml		Configuration XML node.
	 * @param n			Name of the component.
	 */
    LED(Board& board, QDomElement xml, QString name)
	:	IComponent(board, xml),
		rect(nullptr),
		off(QColor::fromRgb(248, 248, 255)),
		on(QColor::fromRgb(173, 255, 47)),
		border(QColor::fromRgb(196, 196, 196)),
		in(*this),
		neg(false),
		val(false)
#		ifdef BSIM_LED_SVG
			,
			pix(nullptr),
			effect(nullptr)
#		endif
	{
		add(&in);
		IComponent::configure(xml);
		info("succesful initialization");
	}

	virtual ~LED(void) {
	}

	void makeVisual(VisualGraph& graph) override {
		VisualComponent *vcomp = new VisualComponent(this);

		// configure it
		vcomp->fill_color = on;
		vcomp->w = w;
		vcomp->h = h;
		vcomp->lab_ypos = Visual::ABOVE;
		vcomp->lab_dir = Visual::VERTICAL;
		vcomp->add(new VisualPin(*vcomp, &in, Visual::SOUTH, w / 2, false));
		vcomp->parse(configuration());
		on = vcomp->fill_color;

		// set the display
#		ifdef BSIM_LED_SVG
			pix = new QGraphicsSvgItem(board().prefix() + "/led.svg");
			pix->setPos(vcomp->x, vcomp->y);
			effect = new QGraphicsColorizeEffect();
			effect->setColor(on);
			effect->setStrength(0);
			pix->setGraphicsEffect(effect);
			vcomp->addToGroup(pix);
#		else
			rect = new QGraphicsRectItem(vcomp->x, vcomp->y, vcomp->w, vcomp->h);
			rect->setBrush(QBrush(off));
			rect->setPen(QPen(border));
			vcomp->addToGroup(rect);
#		endif
			vcomp->makeLabel(name());
			vcomp->makePins();

		// update the display
		graph.add(vcomp);
		doUpdate();
	}

	void configure(Configuration &conf, IMonitor &monitor) override {
		neg = conf.get("negative", neg);
		off = conf.get("off_color", off);
		on = conf.get("color", on);
		border = conf.get("border", border);
	}

private:
	bool val;
	bool neg;
	QGraphicsRectItem *rect;
	QColor off, on, border;

	/**
	 * Update the display of the LED.
	 */
	void doUpdate(void) {
#		ifdef BSIM_LED_SVG
			effect->setStrength((val ^ neg) ? 1 : 0);
#		else
			rect->setBrush((val ^ neg) ? on : off);
#		endif
	}

	class InPin: public Pin {
	public:

		InPin(LED& led): Pin(&led, "in"), _led(led) {
		}

		void onChange(uint64_t time, bool value) override {
			_led.val = value;
			_led.doUpdate();
		}

	private:
		LED& _led;
	};

	InPin in;

#	ifdef BSIM_LED_SVG
		QGraphicsSvgItem *pix;
		QGraphicsColorizeEffect *effect;
#	endif
};


extern "C" IComponent* init (Board& board, QDomElement xml, QString name) {
	return new LED(board,xml,name) ;
}

}	// bsim

