#ifndef __PREPA1_H__
#define __PREPA1_H__

#define _POSIC_C_SOURCE 1

#include <stdio.h>
#include <stdlib.h>

#define NULL_POINTER_EXCEPTION NULL

typedef struct node
{
	int val;
	struct node* next;
	struct node* prev;
} node;

typedef struct node* ptr_node;

node* construct_node(node * n, int value);
node* add_node(node* n, node* new);
node* next_node(node* n, int rank);



#endif