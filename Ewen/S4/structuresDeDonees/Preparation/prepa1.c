#include "prepa1.h"

node* construct_node(node * n, int value){
	n = malloc(sizeof(struct node));
	n->prev = NULL;
	n->next = NULL;
	n->val = value;
	return n;
}
node* add_node(node* n, node* new)
{
	new->prev = n;
	n->next = new;
	return n;
}

node* next_node(node* n, int rank)
{
	node* tmp = n;
	for(int i = 1; i<rank;i++)
	{
		if(tmp->next != NULL)
		tmp = tmp->next;
		else
		{
			fprintf(stderr, "ERROR at node*->next NULL pointer exception in function node* next_node()\n", NULL);
			return NULL_POINTER_EXCEPTION;
		}
	}
	return tmp;
}




int main(void)
{	
	node* n;
	n = construct_node(n, 1);
	node* tmp1;
	tmp1 = construct_node(tmp1, 2);
	node* tmp2 = construct_node(tmp2, 3);
	tmp1 = add_node(tmp1, tmp2);
	n = add_node(n, tmp1);
	node* test;
	if((test = next_node(n, 4))== NULL)
	{
		free(n);
		free(tmp1);
		free(tmp2);
		return 2;
	}
	printf("%d\n", test->val);
	free(n);
	free(tmp1);
	free(tmp2);
	free(test);

	return 0;
}