// Need this to use the getline C function on Linux. Works without this on MacOs. Not tested on Windows.
#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#include "token.h"
#include "queue.h"
#include "stack.h"

bool isSymbol(char c)
{
	switch(c)
	{
		case '+':
			return 1;
		case '-':
			return 1;
		case '*':
			return 1;
		case '/':
			return 1;
		case 'A': // le symbole ∧ est considéré comme plusieurs caractères
			return 1;
		case '(':
			return 1;
		case ')':
			return 1;
		case '^':
			return 1;
	}
	return 0;
}

Queue* stringToTokenQueue(char *expression)
{
	Queue* q = createQueue();
	char* curpos = expression;
	int pos = 0;
	int save = 0;
	int cpt = 0;
	char c = curpos[pos];
	while(c != '\0')
	{
		if(c == ' ' ||c =='\n')
		{
			if(cpt > 0)
			{
						Token* t = createTokenFromString(&curpos[save], cpt-1);
			 			q = queuePush(q, t);
			}
			pos++;
			c = curpos[pos];
			cpt = 0;
			save = 0;
		}
		else{
			save = pos;
			if(isSymbol(c))
			{
			if(cpt>0)
			{

			 			Token* t = createTokenFromString(&curpos[(save-1)], cpt);
			 			q = queuePush(q, t);
			 			cpt = 0;
			}
					Token* ts = createTokenFromString(&curpos[save], cpt);
			 		q = queuePush(q, ts);
			}
			else
			{
				cpt++;
			}
			pos++;
			c = curpos[pos];
			}
		
	}
	return q;
}

void printToken(FILE* f, void* e)
{
	Token* t = (Token*) e;
	tokenDump(f, t);
	free(t);
}

void computeExpressions(FILE* f)
{

	char* buffer;
	FILE* tmp = stdout;
	size_t bufferSize = 255;
	buffer = (char*)malloc(bufferSize*sizeof(char*));
	while(getline(&buffer, &bufferSize, f)!=EOF)
	{
		if(strcmp(buffer, "\n"))
		{	
			printf("Input\t:\t%s", buffer);
			Queue* qBis = stringToTokenQueue(buffer);
			printf("Infix\t:\t");
			queueDump(tmp, qBis, &printToken);
			printf("\n\n\n");
			deleteQueue(&qBis);
		}

	}
		free(buffer);
}
Queue *shuntingYard(Queue* infix){

	Queue *shuntingYard = createQueue();
	Stack *operators = createStack(0);
	Token *tmp;
	while(!queueEmpty(infix)){
		tmp = queueTop(infix);
		if(tokenIsNumber(tmp))
			queuePush(shuntingYard,tmp);
		if(tokenIsOperator(tmp)){
			while((tokenGetOperatorPriority(queueTop(infix))>tokenGetOperatorPriority(tmp)) || (tokenGetOperatorPriority(queueTop(infix)) == tokenGetOperatorPriority(infix) && tokenOperatorIsLeftAssociative(queueTop(infix))) && !tokenIsParenthesis(queueTop(infix)) ){
				queuePush(shuntingYard,queueTop(infix);
				queuePop(infix);
			}
			stackPush(operators,tmp);
		}
	}
	}
}

/** Main function for testing.
 * The main function expects one parameter that is the file where expressions to translate are
 * to be read.
 *
 * This file must contain a valid expression on each line
 *
 */

int main(int argc, char **argv){
	FILE *input;
	
	if (argc<2) {
		fprintf(stderr,"usage : %s filename\n", argv[0]);
		return 1;
	}
	
	input = fopen(argv[1], "r");

	if ( !input ) {
		perror(argv[1]);
		return 1;
	}
	computeExpressions(input);


	fclose(input);
	return 0;
}
 
