#ifndef _QUEUE_H_
#define _QUEUE_H_

#define ELEMENT int
#define QUEUE_SIZE 8

#include <stdbool.h>
#include <stdio.h>

typedef struct queue_s queue_t;

bool empty( const queue_t *q);

bool full( const queue_t *q);

queue_t* create_queue();

queue_t* push( queue_t *q, ELEMENT e);

void pop( queue_t *q);

ELEMENT top( const queue_t *q);


#endif
