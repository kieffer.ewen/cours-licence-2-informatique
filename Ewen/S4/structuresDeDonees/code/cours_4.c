/*
4.3 TAD list
Collection ordonée d'élements
Accès, ajout, suppr à n'importe quelle position valide
Liste fonctions :
#### OP CONSTRUCTEUR ####
list -> list
push_back
#### OP ####
push_front 
pop_front
pop_back
insert_at
remove_at
front
back
getElem
is_empty -> vide = size = 0
size -> nbElem ajoutés 

#### préconditions ####

pop_back/front(l) ssi !isEmpty(l)
front et back ssi !is_empty(l)
remove_at et insert_at ssi pos<size(l)
remove_at ssi l != new list() ET pos < size(l)
ith ssi !is_empty(l)

#### AXIOMES ####
push_front(list(), e) = push_back(list(), e)
push_front(push_back(l, e1), e2) = push_front(push_back(l, e2), e1)    
pop_back(push_back(l, e)) = l
pop_front(push_back(l, e)) = l
front(push_back(l, e)) = front(l)
front(push_back(list(), e)) = e
back(push_back(l, e)) = e;
is_empty(list()) = TRUE
is_empty(push_back(list(), e) = FALSE;
is_empty(l) = false ssi l != list()
size(list()) = 0;
size(push_back(l, e)) = size(l)+1
size(l) = nbElement ssi l != list()  
ith(l , pos) = e
ith(push_back(list(), e), pos) = l 

insert_at(new list(), pos, e) = push_back(l, e) ssi pos = 0
insert_at(push_back(l, e1),pos, e2 ) = pushback(insert_at(l, pos, e2), e1) ssi  0<= pos < size(push_back(l,e))
insert_at(push_back(list(),e1), pos, e2) = push_back(push_back(l, e1)l, e2) ssi pos = size(push_back(l,e))

<<<<<<< HEAD
remove_at(push_back(l,e), pos) = l  pos = size(push_back(l,e)-1)
remove_at(push_back(l, e1), pos) = push_back(l,e1) ssi pos = size(push_back(l ,e))
remove_at(push_back(l, e1), pos) = push_back(remove_at(l, pos), e1) ssi pos<=size(push_back(l,e)-1)
=======
remove_at(push_back(list(),e), pos, e) = list() ssi pos = 0
remove_at(push_back(l, e1), pos, e2) = push_back(l,e1) ssi pos = size(push_back(l ,e))
remove_at(push_back(l, e1), pos, e2) = push_back(remove_at(l, pos, e2), e1) ssi 0<pos<=size(push_back(l,e))

>>>>>>> 77e4ea4108119dbb8a7f74c05ea09eacf16cba5a

JAVADOC :O
1.  Ajout d’un  element en tête de liste :											pushfront: List×Item→List
2.  Suppression d’un element en tête de liste :										popfront:  List→List
3.  Ajout d’un  element en fin de liste :											pushback:  List×Item→List
4.  Suppression d’un element en fin de liste :										popback:   List→List
5.  Ajout d’un  element à une position donnee de la liste :							insertat:  List×int×Item→List
6.  Suppression d’un element à une position donnee de la liste :					removeat:  List×int×Item→List
7.  Accès à la valeur de la tête de liste :											front:     List→Item
8.  Accès à la valeur de la queue de liste :										back:      List→Item
9.  Accès à la valeur du i`eme  element de la liste :								ith:       List×int→Item
*/
#include <stdio.h>
#include <stdlib.h>

#define ELEM int
#define MAXSIZE 10; 

typedef struct node {
int value;
struct _DequeElement *previous;
struct _DequeElement *next;
} *node;

typedef struct _Deque {
	DequeElement head;
	DequeElement tail;
	int size;
}DequeInternal;


dequeue_t* dequeue_create()
{
	dequeue_t* ret = malloc(sizeof(dequeue_t));
	ret->size = 0;
	ret->head = NULL;
	ret->tail = NULL;
	return ret;
}

dequeue_t push(dequeue_t q, inv v)
{
	Node **n = (l->size ? &(l->head)->previous) : &(l->tail);
	*n = 
}

int main(int argc, char** argv)
{
	return 0;
}
