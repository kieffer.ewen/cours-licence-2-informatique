#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define ELEM int

typedef struct elem_s
{
	int value;
	struct elem_s* next;
}*elem_t;

typedef struct stack_s
{
	elem_t top;
	
}stack_t;

void error(char* message, int codeErreur)
{
	fprintf(stderr,message);
	exit(codeErreur);
}

stack_t* initStack(void)
{	
	stack_t* s = malloc(sizeof(struct stack_s));
	s->top = NULL;
	return s;
}

int isEmpty(const stack_t s)
{
	return(s.top == NULL);
}

ELEM top(const stack_t s)
{
	assert(!isEmpty(s));
		return s.top->value;
}

void push(stack_t* s, ELEM e)
{
	elem_t tmp = malloc(sizeof(struct elem_s));
	tmp->value = e;
	tmp->next = NULL;
	s->top = tmp;
}

void pop(stack_t* s)
{
	elem_t e = s->top;
	s->top = e->next;
	free(e);
}

void afficherStack(const stack_t* s)
{
	elem_t tmp = s->top;
	while(tmp != NULL)
	{
		printf("Value top : %d\n", tmp->value);	
		tmp = tmp->next;
	}

}
int main(int argc, char** argv)
{
	stack_t* s = initStack();
	push(s, 2);
	push(s, 3);
	push(s, 6);
	afficherStack(s);
	pop(s);
	free(s);
	return 0;
}
