#include "queue_cour.h"

#include <stdbool.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#define ELEM int
typedef struct queue_s{
  ELEMENT data[QUEUE_SIZE];
  int head;
  int tail;
} queue_t;

typedef struct node_s{
  int value;
  struct node_s* next;
}node_t;

queue_t* createQueue(){
  queue_t *q malloc(sizeof(queue_t));
  q->head = NULL;
  q->tail = NULL;

}

queue_t* push(queue_t q, ELEM e)
{
  node_t* n = malloc(sizeof(node_t))
  n->value = e;
  n->next = NULL;

  if(q->tail == NULL)
    q->tail = q->head = n;
  else
  {
    q->tail->next = n;
    q->tail = n;
  }
}

queue_t* pop(queue_t* q)
{
  Queue_t* tmp = q->head;
  q->head = q->head->next;
  q->size--;
  free (tmp);
  return (q);
}
//FAIRE etat de la mémoire par étapes