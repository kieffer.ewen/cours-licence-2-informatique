#include <stdio.h>
#include <stdlib.h>

#define STACK_SIZE 512
#define ELEM int

typedef struct stack_s
{
	ELEM data[STACK_SIZE];
	int top;
}stack_t;

void error(char* message, int codeErreur)
{
	fprintf(stderr,message);
	exit(codeErreur);
}

stack_t initStack()
{
	stack_t s;
	for(int i = 0; i<STACK_SIZE; i++)
		{s.data[i] = 0;}
	s.top = -1;
	return s;
}


int isFull(const stack_t s)
{
	if(s.top == STACK_SIZE-1)
		return 1;
	return 0;
}

int isEmpty(const stack_t s)
{
	if(s.top == -1)
		return 1;
	return 0;
}

ELEM top(const stack_t s)
{
	if(!isEmpty(s))
		return s.top;
	return 0;
}

void push(stack_t* s, ELEM e)
{
	if(!isFull(*s)){
		s->data[s->top+1] = e;
		s->top += 1;
	}else
		error("Stack overflow", 255);
}

void pop(stack_t* s)
{
	if(!isEmpty(*s)){
		s->data[s->top] = 0;
		s->top -= 1;
	}
	else{
		error("Array out of bound", 255);
	}
}

void afficherStack(const stack_t s)
{
	for(int i = 0; i<s.top;i++)
		printf("data[%d] = %d\n", i+1, s.data[i]);
}

int main(int argc, char** argv)
{
	stack_t s = initStack();
	printf("La pile est vide ? %d\n", isEmpty(s));
	for(int i = 0; i<10;i++)
	{
		push(&s, i);
	}
	pop(&s);
	afficherStack(s);
	return 0;
}