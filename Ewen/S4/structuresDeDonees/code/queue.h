#ifndef _QUEUE_H
#define _QUEUE_H

#define ELEM int


typedef struct queue_s queue_t;

void createQueue(queue_t* q);


void push(queue_t* q, ELEM e);

int isEmpty(const queue_t q);


void pop(queue_t* q);

void afficherQueue(const queue_t q);

int top(const queue_t q);


#endif
