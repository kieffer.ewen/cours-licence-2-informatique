#include "queue.h"
#include <stdio.h>
#include <assert.h>

#define MAXSIZE 8

typedef struct queue_s{
	
	ELEM tab[MAXSIZE];
	int top;
	
}queue_t;


void createQueue(queue_t* q)
{
	for(int i = 0; i<MAXSIZE;i++)
	{
		q->tab[i] = 0;
	}
	q->top = 0;
} 

void push(queue_t* q, ELEM e)
{	
	if(q->top<MAXSIZE){
		q->tab[q->top] = e;
		q->top++;
	}
}

int isEmpty(const queue_t q)
{
	return (q.top == 0);
}

void pop(queue_t* q)
{
	assert(!isEmpty(*q));
	for(int i = 0; i<q->top; i++)
	{
		q->tab[i] = q->tab[i+1];
	}
	q->top -= 1;
	q->tab[MAXSIZE-1] = 0;
}
void afficherQueue(const queue_t q)
{
	for(int i= 0;i<MAXSIZE;i++)
	{
		printf("tab[%d] = %d\n", i, q.tab[i]);
	}
}

int top(const queue_t q)
{
	return q.top;
}

int main(void)
{
	queue_t q;
	createQueue(&q);
	for(int i = 0;i < MAXSIZE;i++)
	{
		push(&q, i);
	}
	
	pop(&q);
	pop(&q);
	pop(&q);

	push(&q, 520);	
	
	afficherQueue(q);
	return 0;
}
