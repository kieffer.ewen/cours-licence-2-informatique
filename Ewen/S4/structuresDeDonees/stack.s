	.file	"stack.c"
	.text
	.globl	error
	.type	error, @function
error:
.LFB6:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movq	stderr(%rip), %rax
	movq	-8(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf@PLT
	movl	-12(%rbp), %eax
	movl	%eax, %edi
	call	exit@PLT
	.cfi_endproc
.LFE6:
	.size	error, .-error
	.globl	initStack
	.type	initStack, @function
initStack:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$1952, %rsp
	movq	%rdi, -2072(%rbp)
	movl	$0, -4(%rbp)
	jmp	.L3
.L4:
	movl	-4(%rbp), %eax
	cltq
	movl	$0, -2064(%rbp,%rax,4)
	addl	$1, -4(%rbp)
.L3:
	cmpl	$511, -4(%rbp)
	jle	.L4
	movl	$-1, -16(%rbp)
	movq	-2072(%rbp), %rax
	movq	%rax, %rdx
	leaq	-2064(%rbp), %rax
	movl	$2052, %ecx
	movq	(%rax), %rsi
	movq	%rsi, (%rdx)
	movl	%ecx, %esi
	addq	%rdx, %rsi
	leaq	8(%rsi), %rdi
	movl	%ecx, %esi
	addq	%rax, %rsi
	addq	$8, %rsi
	movq	-16(%rsi), %rsi
	movq	%rsi, -16(%rdi)
	leaq	8(%rdx), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rdx
	subq	%rdx, %rax
	addl	%edx, %ecx
	andl	$-8, %ecx
	shrl	$3, %ecx
	movl	%ecx, %edx
	movl	%edx, %edx
	movq	%rax, %rsi
	movq	%rdx, %rcx
	rep movsq
	movq	-2072(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	initStack, .-initStack
	.globl	isFull
	.type	isFull, @function
isFull:
.LFB8:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	2064(%rbp), %eax
	cmpl	$511, %eax
	jne	.L7
	movl	$1, %eax
	jmp	.L8
.L7:
	movl	$0, %eax
.L8:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8:
	.size	isFull, .-isFull
	.globl	isEmpty
	.type	isEmpty, @function
isEmpty:
.LFB9:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	2064(%rbp), %eax
	cmpl	$-1, %eax
	jne	.L10
	movl	$1, %eax
	jmp	.L11
.L10:
	movl	$0, %eax
.L11:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9:
	.size	isEmpty, .-isEmpty
	.globl	top
	.type	top, @function
top:
.LFB10:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$2056, %rsp
	movq	%rsp, %rax
	movq	%rax, %rdx
	leaq	16(%rbp), %rax
	movl	$256, %ecx
	movq	%rdx, %rdi
	movq	%rax, %rsi
	rep movsq
	movq	%rsi, %rax
	movq	%rdi, %rdx
	movl	(%rax), %ecx
	movl	%ecx, (%rdx)
	leaq	4(%rdx), %rdx
	leaq	4(%rax), %rax
	call	isEmpty
	addq	$2056, %rsp
	testl	%eax, %eax
	jne	.L13
	movl	2064(%rbp), %eax
	jmp	.L14
.L13:
	movl	$0, %eax
.L14:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10:
	.size	top, .-top
	.section	.rodata
.LC0:
	.string	"Stack overflow"
	.text
	.globl	push
	.type	push, @function
push:
.LFB11:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movq	-8(%rbp), %rax
	subq	$2056, %rsp
	movq	%rsp, %rdx
	movl	$256, %ecx
	movq	%rdx, %rdi
	movq	%rax, %rsi
	rep movsq
	movq	%rsi, %rax
	movq	%rdi, %rdx
	movl	(%rax), %ecx
	movl	%ecx, (%rdx)
	leaq	4(%rdx), %rdx
	leaq	4(%rax), %rax
	call	isFull
	addq	$2056, %rsp
	testl	%eax, %eax
	jne	.L16
	movq	-8(%rbp), %rax
	movl	2048(%rax), %eax
	leal	1(%rax), %edx
	movq	-8(%rbp), %rax
	movslq	%edx, %rdx
	movl	-12(%rbp), %ecx
	movl	%ecx, (%rax,%rdx,4)
	movq	-8(%rbp), %rax
	movl	2048(%rax), %eax
	leal	1(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, 2048(%rax)
	jmp	.L18
.L16:
	movl	$255, %esi
	leaq	.LC0(%rip), %rdi
	call	error
.L18:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11:
	.size	push, .-push
	.section	.rodata
.LC1:
	.string	"Array out of bound"
	.text
	.globl	pop
	.type	pop, @function
pop:
.LFB12:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	subq	$2056, %rsp
	movq	%rsp, %rdx
	movl	$256, %ecx
	movq	%rdx, %rdi
	movq	%rax, %rsi
	rep movsq
	movq	%rsi, %rax
	movq	%rdi, %rdx
	movl	(%rax), %ecx
	movl	%ecx, (%rdx)
	leaq	4(%rdx), %rdx
	leaq	4(%rax), %rax
	call	isEmpty
	addq	$2056, %rsp
	testl	%eax, %eax
	jne	.L20
	movq	-8(%rbp), %rax
	movl	2048(%rax), %edx
	movq	-8(%rbp), %rax
	movslq	%edx, %rdx
	movl	$0, (%rax,%rdx,4)
	movq	-8(%rbp), %rax
	movl	2048(%rax), %eax
	leal	-1(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, 2048(%rax)
	jmp	.L22
.L20:
	movl	$255, %esi
	leaq	.LC1(%rip), %rdi
	call	error
.L22:
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12:
	.size	pop, .-pop
	.section	.rodata
.LC2:
	.string	"data[%d] = %d\n"
	.text
	.globl	afficherStack
	.type	afficherStack, @function
afficherStack:
.LFB13:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$0, -4(%rbp)
	jmp	.L24
.L25:
	movl	-4(%rbp), %eax
	cltq
	movl	16(%rbp,%rax,4), %eax
	movl	-4(%rbp), %edx
	leal	1(%rdx), %ecx
	movl	%eax, %edx
	movl	%ecx, %esi
	leaq	.LC2(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	addl	$1, -4(%rbp)
.L24:
	movl	2064(%rbp), %eax
	cmpl	%eax, -4(%rbp)
	jl	.L25
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13:
	.size	afficherStack, .-afficherStack
	.section	.rodata
.LC3:
	.string	"La pile est vide ? %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB14:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$2080, %rsp
	movl	%edi, -2068(%rbp)
	movq	%rsi, -2080(%rbp)
	leaq	-2064(%rbp), %rax
	movq	%rax, %rdi
	movl	$0, %eax
	call	initStack
	subq	$2056, %rsp
	movq	%rsp, %rax
	movq	%rax, %rdx
	leaq	-2064(%rbp), %rax
	movl	$256, %ecx
	movq	%rdx, %rdi
	movq	%rax, %rsi
	rep movsq
	movq	%rsi, %rax
	movq	%rdi, %rdx
	movl	(%rax), %ecx
	movl	%ecx, (%rdx)
	leaq	4(%rdx), %rdx
	leaq	4(%rax), %rax
	call	isEmpty
	addq	$2056, %rsp
	movl	%eax, %esi
	leaq	.LC3(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$0, -4(%rbp)
	jmp	.L27
.L28:
	movl	-4(%rbp), %edx
	leaq	-2064(%rbp), %rax
	movl	%edx, %esi
	movq	%rax, %rdi
	call	push
	addl	$1, -4(%rbp)
.L27:
	cmpl	$9, -4(%rbp)
	jle	.L28
	leaq	-2064(%rbp), %rax
	movq	%rax, %rdi
	call	pop
	subq	$8, %rsp
	subq	$2056, %rsp
	movq	%rsp, %rax
	movq	%rax, %rdx
	leaq	-2064(%rbp), %rax
	movl	$256, %ecx
	movq	%rdx, %rdi
	movq	%rax, %rsi
	rep movsq
	movq	%rsi, %rax
	movq	%rdi, %rdx
	movl	(%rax), %ecx
	movl	%ecx, (%rdx)
	leaq	4(%rdx), %rdx
	leaq	4(%rax), %rax
	call	afficherStack
	addq	$2064, %rsp
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14:
	.size	main, .-main
	.ident	"GCC: (Debian 8.3.0-6) 8.3.0"
	.section	.note.GNU-stack,"",@progbits
