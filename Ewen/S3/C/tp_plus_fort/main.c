#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAXSIZE 9

int f1(int tab[], int size)
{
	if(size<1)
	{
	fprintf(stderr, "ERROR : array in parameter need 1 column at least\n")	;
	}
	int max = 0;
	for(int deb = 0;deb<size;deb++)
		for(int fin = deb;fin<size;fin++)
		{
			int tmp = 0; 
			for(int j =  deb; j<=fin;j++)
			{
				tmp+= tab[j];			
			}
			//printf("%d > %d\n", tmp, max);
			if(tmp > max)
				max = tmp;
		}
		return max;
}


int f2(int tab[], int size)
{	
	srand(time(NULL));
	
	if(size<1)
	{
	fprintf(stderr, "ERROR : array in parameter need 1 column at least")	;
	}
	int max = 0;
	for(int deb = 0;deb<size;deb++)
	{
		int tmp = 0; 		
		for(int fin = deb;fin<size;fin++)
		{
			tmp+= tab[fin];
			//printf("%d > %d\n", tmp, max);
			if(tmp > max)
				max = tmp;
		}
	}
		return max;
}

int f3(int tab[], int size)
{
	int res, res_i;
	res =  tab[0];
	res_i = tab[0];
	for(int deb=0; deb<size;deb++)
	{
		if(res_i > 0)
		{
			res_i = res_i + tab[deb];	
		}
		else{
			res_i = tab[deb];		
		}
		if(res_i > res)
			res = res_i;
	}

	printf("res = %d \t| res_i = %d\n", res, res_i);
	return res;
}

int main(int argc, char** argv)
{
	int size = atoi(argv[1]);

	int tab[size];
	for(int i =0;i<size;i++)
	{
		tab[i]= rand()%21-10;	
	}
	int tab2[9]={2,-1,0,-2,3,-4,8,-1,2};
	//O(n^3)
	clock_t debut = clock();
	int res = f1(tab, size);
	clock_t fin = clock();
	printf("############ FONCTION O(n^3)############\n\n");
	printf("temps CPU : %2.f secondes \tRes = %d\n\n", (double)(fin-debut)/CLOCKS_PER_SEC, res);
	printf("########################################\n\n");
	
	//O(n^2)
	debut = clock();
	int res2 = f2(tab, size);
	fin = clock();
	printf("############ FONCTION O(n^2)############\n\n");
	printf("temps CPU : %2.f secondes \tRes = %d\n\n", (double)(fin-debut)/CLOCKS_PER_SEC, res2);
	printf("########################################\n\n");
	//O(n)
	debut = clock();
	int res3 = f3(tab, size);
	fin = clock();
	printf("############ FONCTION O(n)############\n\n");
	printf("temps CPU : %2.f secondes \tRes = %d\n\n", (double)(fin-debut)/CLOCKS_PER_SEC, res3);
	printf("########################################\n\n");
	return 0;
}
