#include "sudoku.h"
#include <stdlib.h>
#include <stdio.h>



/*	#########################################################################
	#########################################################################
							FONCTIONS A RAJOUTER DANS SUDOKU.H	
	#########################################################################
	#########################################################################

	int rechercherValeurColonne(T_sudoku s, int candidat, int indice);
	int rechercherValeurLigne(T_sudoku s, int candidat, int indice);
	int rechercherValeurLigne(T_sudoku s, int candidat, int indice);

*/

/* Implanter les fonctions de sudoku.h ici */
/* Traduction indice/coordonnees */
T_coordonnees obtenirCoords(int indice){
	T_coordonnees coordonnees;
	coordonnees.ligne = indice/9;
	coordonnees.colonne = indice%9;
	return coordonnees;
}
/* Retourne les coordonnees de la case a partir de son indice dans le sudoku */


int obtenirIndice(T_coordonnees coords){
	return 9*coords.ligne + coords.colonne;
}
/* Retourne l'indice d'une case dans le sudoku a partir de ses coordonnees */


T_coordonnees debutRegion(int indiceRegion){
	T_coordonnees coordonnees;
	coordonnees.ligne = 3*(indiceRegion/3);
	coordonnees.colonne = 3*(indiceRegion%3);
	return coordonnees;
}
/* Retourne les coordonnees de la premiere case (en haut a gauche) d'une region a partir de l'indice de cette region */

int estCandidat(T_sudoku s, int candidat, int indice){
	T_coordonnees debut = debutRegion(indiceRegion(obtenirCoords(indice)));
	int debut_col = debut.colonne;
	for(int i = 0; i < 3; i++){
		debut.colonne = debut_col;
		for(int j = 0; j < 3; j++){
			if(candidat == s.grille[obtenirIndice(debut)].val)
				return 1;
			debut.colonne ++;
		}
		debut.ligne ++;
	}
	T_coordonnees coord_case = obtenirCoords(indice);
	T_coordonnees it;
	T_coordonnees at;
	it.ligne = coord_case.ligne;
	it.colonne = 0;
	at.colonne = coord_case.colonne;
	at.ligne = 0;
	for(int i = 1; i < 10; i ++){
		if(candidat == s.grille[obtenirIndice(it)].val)
			return 1;
		it.colonne ++;	

	}
	for(int i = 1; i < 10; i ++){
		if(candidat == s.grille[obtenirIndice(at)].val)
			return 1;
		at.ligne ++;

	}
	return 0;

}
int indiceRegion(T_coordonnees coords){
	return (coords.ligne/3)*3 + coords.colonne/3;
}/* Retourne l'indice de la region a laquelle appartient la case ayant ces coordonnees*/

/* Lire une grille et initialiser un sudoku */
T_sudoku lireSudoku(char* chemin){
	FILE* fic=NULL;
	T_sudoku sudoku;
	fic = fopen(chemin, "r");
	if(fic == NULL)
		{
			fprintf(stderr, "Erreur à l'ouverture du fichier : %s\n", chemin);
			exit(1);
		}
	else
	{
		int cpt = 0;
		int caractere;
		while(fscanf(fic,"%d", &caractere) != EOF){
			sudoku.grille[cpt].val = caractere;
			cpt++;
		}

	}
	fclose(fic);
	return sudoku;
}
void initialiserSudoku(T_sudoku* s){
	for (int i = 0; i < 81; i ++) {
		if(s->grille[i].val == 0) {
			s->grille[i].n_candidats = 9;
		for(int k = 0; k < 9; k ++){
			s->grille[i].candidats[k] = k + 1;
		}
		}else{
			s->grille[i].n_candidats = 0;
		for(int j = 0; j<9;j++){
			s->grille[i].candidats[j] = 0;
		}

		}
	}
}



/* Affichage de grilles */
void afficherSudoku(T_sudoku s){
	T_coordonnees cases;
	printf("\n");
	for (int i = 0; i < 9; i++){
		for (int j = 0; j < 9; j++) {
			cases.ligne = i;
			cases.colonne = j;
			printf("%d", s.grille[obtenirIndice(cases)].val);
		}
	}
} /* Afficher un sudoku: 9 lignes de 9 entiers separes par des tabulations (\t)*/

/* Recherche et suppression de candidats dans une case */
/* Renvoie l'indice de val dans le tableau de candidats de c. Si l'indice n'est pas present, renvoie c.n_candidats */
int rechercherValeur(int val, T_case c){
	for(int i = 0; i < c.n_candidats; i++) {
		if(c.candidats[i] == val)
			return i;
	}
	return c.n_candidats;
}

/* ival est l'indice de la valeur a supprimer, on considere que la valeur a supprimer est presente dans les candidats de la case */
void supprimerValeur(int ival, T_case* pc){
	for (int i = 0; i < pc->n_candidats; i++) {
		if(ival == pc->candidats[i]){
			pc->n_candidats --;
			pc->candidats[i] = 0;
		}
	}
	for (int i = 0; i < 8; i ++) {
		if(pc->candidats[i] == 0) {
			pc->candidats[i] = pc->candidats[i+1];
			pc->candidats[i+1] = 0;
		}
	}
}

/* Implementation des regles */
/* Applique R1 sur la case */
int R1_case(T_case* pc){
	if(pc->n_candidats == 1)
	{
		pc->val = pc->candidats[0];
		for (int i = 0; i < 9; i ++)
		 	pc->candidats[i] = 0;
		pc->n_candidats = 0;
		return 1;
	}
	else 
		return 0;
}

/* Applique R2 sur la case. */
int R1_sudoku(T_sudoku* ps){
	int cpt = 0;
	for(int i = 0; i<81 ;i++)
	{
		if(R1_case(&ps->grille[i])==1)
		{
				int k = ps->grille[i].val;
				T_coordonnees debut = debutRegion(indiceRegion(obtenirCoords(i)));
				int debut_col = debut.colonne;
				T_coordonnees t;
				for(int f = 0; f < 3; f++){
					debut.colonne = debut_col;
					for(int j = 0; j < 3; j++){
						t.ligne = debut.ligne;
						t.colonne = debut.colonne;
						supprimerValeur(k, &ps->grille[obtenirIndice(t)]);										
						debut.colonne ++;
					}
					debut.ligne ++;
				}

				T_coordonnees caze = obtenirCoords(i);
				t.ligne = caze.ligne;
				for (int j = 0; j < 9; j ++) {
					t.colonne = j;
					supprimerValeur(k, &ps->grille[obtenirIndice(t)]);				
				}
				t.colonne = caze.colonne;
				for (int j = 0; j < 9; j ++) {
					t.ligne = j;
					supprimerValeur(k, &ps->grille[obtenirIndice(t)]);					
				}

			cpt++;
		}
	}
	if(cpt > 0)
		return 1;
	return 0;
}

 /* Applique R1 sur toutes les cases du sudoku, renvoie 1 si une modification a eu lieu, 0 sinon */
int R2_case(int indCase, T_sudoku* ps){
	int cpt = 0;
	if(ps->grille[indCase].val != 0) 
		return 0;
	for(int k = 1; k < 10; k ++){
		if(estCandidat(*ps,k,indCase) == 1 && (ps->grille[indCase].candidats[rechercherValeur(k, ps->grille[indCase])] == k)){
			supprimerValeur(k, &ps->grille[indCase]);
			cpt ++;
		}
	}
	if (cpt > 0){
		return 1;
	}
	return 0;

}

/* Applique R2 sur toutes les cases du sudoku, renvoie 1 si une modification a eu lieu, 0 sinon */
int R2_sudoku(T_sudoku* ps){
	int cpt = 0;
	for(int i = 0; i < 81; i++){
		if(R2_case(i, ps) == 1){
			cpt ++;
		}
	}
	if(cpt > 0){
		return 1;
	}
	return 0;
}

int rechercherValeurRegion(T_sudoku s, int candidat, int indice)
{
	T_coordonnees debut = debutRegion(indiceRegion(obtenirCoords(indice)));
	int debut_col = debut.colonne;
	for(int i = 0; i < 3; i++)
		{
		debut.colonne = debut_col;
		for(int j = 0; j < 3; j++)
		{
			if(candidat == s.grille[obtenirIndice(debut)].candidats[rechercherValeur(candidat, s.grille[obtenirIndice(debut)])] && obtenirIndice(debut) != indice)
				return 1;
			debut.colonne ++;
		}
		debut.ligne ++;
	}
	return 0;
}
int rechercherValeurLigne(T_sudoku s, int candidat, int indice)
{
	T_coordonnees coord_case = obtenirCoords(indice);
	T_coordonnees at;
	at.colonne = coord_case.colonne;
	at.ligne = 0;
	for(int i = 0; i < 9; i ++)
	{
			if(candidat == s.grille[obtenirIndice(at)].candidats[rechercherValeur(candidat, s.grille[obtenirIndice(at)])] && obtenirIndice(at) != indice)
				return 1;
		at.ligne ++;
	}
	return 0;

}
int rechercherValeurColonne(T_sudoku s, int candidat, int indice)
{
	T_coordonnees coord_case = obtenirCoords(indice);
	T_coordonnees it;
	it.ligne = coord_case.ligne;
	it.colonne = 0;
	for(int i = 0; i < 9; i ++)
	{
			if(candidat == s.grille[obtenirIndice(it)].candidats[rechercherValeur(candidat, s.grille[obtenirIndice(it)])] && obtenirIndice(it) != indice)
				return 1;
		it.colonne ++;	
	}
	return 0;
}

/* Applique R3 sur la case. */
int R3_case(int indCase, T_sudoku* ps)
{
		if(ps->grille[indCase].val != 0){
			return 0;
		}
		T_coordonnees debut = debutRegion(indiceRegion(obtenirCoords(indCase)));
		int debut_col = debut.colonne;
		T_coordonnees t;
		for(int i = 0; i < ps->grille[indCase].n_candidats; i++) 
		{
			if(rechercherValeurRegion(*ps, ps->grille[indCase].candidats[i], indCase) == 0)
			{
				ps->grille[indCase].val = ps->grille[indCase].candidats[i];
				ps->grille[indCase].n_candidats = 0;
				for (int k = 0; k < 9; k++)
				{
					ps->grille[indCase].candidats[k] = 0;
				}				
				return 1;
			}
			if(rechercherValeurLigne(*ps, ps->grille[indCase].candidats[i], indCase) == 0)
			{
				ps->grille[indCase].val = ps->grille[indCase].candidats[i];
				ps->grille[indCase].n_candidats = 0;
				for (int k = 0; k < 9; k++){
					ps->grille[indCase].candidats[k] = 0;
				}
				return 1;
			}
			if(rechercherValeurColonne(*ps, ps->grille[indCase].candidats[i], indCase) == 0) 
			{
				ps->grille[indCase].val = ps->grille[indCase].candidats[i];
				ps->grille[indCase].n_candidats = 0;
				for (int k = 0; k < 9; k++)
				{
					ps->grille[indCase].candidats[k] = 0;
				}
				return 1;
			}
		}
		return 0;
}
/* Applique R3 sur toutes les cases du sudoku, renvoie 1 si une modification a eu lieu, 0 sinon */
int R3_sudoku(T_sudoku* ps)
{
	int cpt = 0;
	for(int i = 0; i<81 ;i++)
	{
		if(R3_case(i,ps)==1)
		{
				int k = ps->grille[i].val;
				T_coordonnees debut = debutRegion(indiceRegion(obtenirCoords(i)));
				int debut_col = debut.colonne;
				T_coordonnees t;
				for(int f = 0; f < 3; f++)
				{
					debut.colonne = debut_col;
					for(int j = 0; j < 3; j++)
					{
						t.ligne = debut.ligne;
						t.colonne = debut.colonne;
						supprimerValeur(k, &ps->grille[obtenirIndice(t)]);										
						debut.colonne ++;
					}
					debut.ligne ++;
				}

				T_coordonnees caze = obtenirCoords(i);
				t.ligne = caze.ligne;
				for (int j = 0; j < 9; j ++)
				{
					t.colonne = j;
					supprimerValeur(k, &ps->grille[obtenirIndice(t)]);				
				}
				t.colonne = caze.colonne;
				for (int j = 0; j < 9; j ++) {
					t.ligne = j;
					supprimerValeur(k, &ps->grille[obtenirIndice(t)]);					
				}

			cpt++;
		}
	}
	if(cpt > 0)
		return 1;
	return 0;

}
int bruteForce(T_sudoku* ps)
{
	
}
/* Bonus */

/* Verification du resultat */
int valide(T_sudoku s)
{

} /* Renvoie 1 si la grille est complete et valide */
int verifResultat(T_sudoku initialS,T_sudoku s)
{
	
} /* Renvoie 1 si le sudoku est correct, et correspond bien au probleme initial */


/* Resolution */
void resoudreSudoku(char* chemin)
{

}
void resoudreSudokuCharge(int** ps,int nb)
{

}
