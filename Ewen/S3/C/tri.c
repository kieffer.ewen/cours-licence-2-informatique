/**
 * \brief Tests sur les tris avec mesure du temps
 * \brief usage : nom_du_prog nom_du_fichier [limite] o� limite dit de ne consid�rer que les limite premiers nombres du fichier �r d�faut limite = 100
 *
 * \author Jean-Paul Bahsoun, Vincent Dugat, Pierre Piccinini
 * \date septembre 2016, septembre 2017
 */

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
#include "pile.h"

#define TMAX 300000 // dimension maximale des tableaux

typedef int tableau[TMAX];

void echanger(int *a,int *b) // swap
{
    int aux=*a;
    *a=*b;
    *b=aux;
}

void afficher_tab( tableau t , int dim)
{
    int i;
    for (i=0;i<dim;i++)
        printf("%4d",t[i]);
    printf("\n");
}

/* remplit le tableau avec les dim entiers d'un fichier
 s'il n'y a pas assez d'entiers dans le fichier :  le programme stoppe
*/ 
void tab_fichier(tableau t,int dim, char *nom)
{
    int i;
    FILE * f_in;
    f_in = fopen(nom,"r") ; 
    if (f_in == NULL)
    {  	
			fprintf(stderr,"\n Erreur : Impossible d ouvrir le fichier %s\n",nom) ;    
			exit(EXIT_FAILURE) ;}
     for (i=0;i<dim;i++) 
    {
 	     fscanf(f_in,"%d",&t[i]) ;
 	     if (feof(f_in))
 	     {
			printf("Erreur: Pas assez d'entiers dans le fichier %s\n",nom);  
			exit(EXIT_FAILURE);}
    }     
     fclose(f_in);
}

// copie de dim �l�ments de T1 dans t2
void initCopieTab(tableau t1, tableau t2, int dim){
    int i;
    for(i=0;i<dim;i++) t2[i]=t1[i]; // copie pour un autre tri
    }
    
// fonctions et proc�dures de tri

/*renvoie un booleen vrai ssi t[deb1,fin1]<=val*/
bool inf_eq(tableau t, int deb1, int fin1, int val)
{
  int i;  
  for (i=deb1;i<=fin1;++i)
          if (t[i]>val)
              return false;
  return true;        
}  

/*renvoie un bool�en vrai ssi val<t[deb1,fin1]l*/
bool sup(tableau t, int deb1, int fin1, int val)
{
  int i;  
  for (i=deb1;i<=fin1;++i)
         if (t[i]<=val)
              return false;
  return true;        
}   

int partitionnerInv(tableau tab, int ind_premier, int ind_dernier){
		int ind_pivot = ind_premier;
		int j= ind_dernier;
		//assert( tab[ind_premier] <= tab[ind_pivot] < tab[j]<= tab[ind_dernier] && ind_premier <= ind_pivot && ind_pivot < j <= ind_dernier);


		//assert(ind_premier <= ind_pivot && ind_pivot <= j && j <= ind_dernier);
		while(ind_pivot != j)
		{
		//assert(tab[ind_premier] <= tab[ind_pivot] <= tab[j] && ind_pivot < j <= ind_dernier);
			if(tab[ind_premier]<= tab[ind_pivot])
			{
				echanger(&tab[ind_premier], &tab[ind_pivot]);
				ind_pivot = ind_premier;
				ind_premier++;
			}else
			{
				echanger(&tab[ind_premier], &tab[j]);
				j--;
			}
		//assert(tab[ind_premier] <= tab[ind_pivot] < tab[j] <= tab[ind_dernier]);
		}
		//assert(tab[ind_premier] <= tab[ind_pivot] < tab[j] <= tab[ind_dernier] && ind_pivot == j);
		return ind_pivot;
}

/* E: tab tableau de N entiers 
    0<=d<N  0<=f<N 
 tri rapide 
S:  slice tab[d,f] tri�
*/ 
void triRapide(tableau tab, int d, int f){
    int ind_pivot;
    if (d<f) {
        ind_pivot = partitionnerInv(tab, d, f);       
        triRapide(tab, d, ind_pivot-1);
        triRapide(tab, ind_pivot+1, f);
    }
}

void triRapideNR(tableau tab, int d, int f){
    pile_t pil;
	init_pile(&pil);
	empiler(&pil, d);
	empiler(&pil, f);
	while(!pile_vide(&pil))
	{
		f = sommet(&pil); depiler(&pil);
		d  = sommet(&pil); depiler(&pil);
		int p = partitionnerInv(tab, d, f);
		if(p-1 > d)
		{
			empiler(&pil, d);
			empiler(&pil, p-1);
		}
		if(p+1 < f)
		{
			empiler(&pil, p+1);
			empiler(&pil, f);
		}
	}
}
// tri bulle : nb de comparaisons de l'ordre de dim^2
void tri_bulle(tableau t,int dim)
{
	int i,j;
	for (i=0;i<dim;i++)
		for(j=i+1;j<dim;j++)
			if (t[i]>t[j])
            	echanger(&t[i],&t[j]);
}

void fusionner(int T[], int p1, int d1, int p2, int d2, int M[])
{
        int cpt1=p1;
        int cpt2=p2;
        int i;
        
        //on recopie les éléments du début du tableau
        for(i=p1;i<=d1;i++)
            {
            M[i-p1]=T[i];
            }
                        
        for(i=p1;i<=d2;i++)
            {        
            if (cpt1==p2) //c'est que tous les éléments du premier tableau ont été utilisés
                {
                break; //tous les éléments ont donc été classés
                }
            else if (cpt2==(d2+1)) //c'est que tous les éléments du second tableau ont été utilisés
                {
                T[i]=M[cpt1-p1]; //on ajoute les éléments restants du premier tableau
                cpt1++;
                }
            else if (M[cpt1-p1]<T[cpt2])
                {
                T[i]=M[cpt1-p1]; //on ajoute un élément du premier tableau
                cpt1++;
                }
            else
                {
                T[i]=T[cpt2]; //on ajoute un élément du second tableau
                cpt2++;
                }
            }
}
void tri_fusion(int T[], int prem, int der, int M[])
{
	int milieu;
	if(prem<der)
	{
		milieu = (prem+der)/2;
		tri_fusion(T, prem, milieu, M);
		tri_fusion(T, milieu+1, der, M);
		fusionner(T, prem, milieu, milieu+1, der, M);
	}
}

void tri_fusionNR(int T[], int prem, int der, int M[])
{
	pile_t pile;
	init_pile(&pile);
	int milieu = 0;
	if(prem<der)
	{	
		//A IMPLEMENTER
	}
	/*if(prem<der)
	{
		milieu = (prem+der)/2;
		tri_fusion(T, prem, milieu, M);
		tri_fusion(T, milieu+1, der, M);
		fusionner(T, prem, milieu, milieu+1, der, M);
	}*/
}

// =================== MAIN =================
/* 2 arguments:     le nom du fichier ,   le nombre d'entiers du fichier trait�s*/

int main(int argc,char **argv)
{
    int dim;
    tableau t,t2,M; 
    clock_t now,end; // d�claration de timers
    switch (argc)
       {
           case 1: printf("Usage : commande fichier [dim]\n");return 1;break;
        case 2: dim=100;break;
        case 3: dim=atoi(argv[2]);break;
        default: printf("Erreur ligne de commande: trop d'arguments\n");return 1;break;
       }
    printf("Dim=%d TMAX=%d\n",dim,TMAX);
    tab_fichier(t,dim, argv[1]); // on lit les donn�es dans le 2�me param�tre de la ligne de commande
    //printf("tab 1:");ecriture_tab(t,dim);
    
    initCopieTab(t,t2,dim); // les tris se feront toujours sur t2 qui sera r�initialis� � chaque fois
    
    printf("Tri bulles\n");
    now = clock(); // d�part chrono
    tri_bulle(t2,dim);
    end=clock();
    printf("\nTemps CPU : %.2f secondes \n\n",(double) (end-now)/CLOCKS_PER_SEC); // fin chrono
    //printf("tab 1 trie' ");afficher_tab(t2,dim); // pour v�rifier sur un petit vecteur
    printf("-------------------------------\n");
    
    initCopieTab(t,t2,dim); // r�initialisation de t2
 
    printf("Tri rapide récursif\n");
    now = clock(); // d�part chrono
    triRapideNR(t2,0,dim-1); 
    end =clock();
    printf("\nTemps CPU : %.2f secondes mais l'avez-vous ajoute' ? \n\n",(double) (end - now) / CLOCKS_PER_SEC); // fin chrono
   //printf("tab 1 trie' ");afficher_tab(t2,dim); // pour v�rifier sur un petit vecteur
    printf("-------------------------------\n");
    
// M�me principe pour le tri fusion
    printf("-------------------------------\n");
    
    initCopieTab(t,t2,dim); // r�initialisation de t2
 	
    printf("Tri Fusion\n");
    now = clock(); // d�part chrono
    tri_fusion(t2, 0, dim-1,M); 
    end =clock();
    printf("\nTemps CPU : %.2f secondes mais l'avez-vous ajoute' ? \n\n",(double) (end - now) / CLOCKS_PER_SEC); // fin chrono
   	//printf("tab 1 trie' ");afficher_tab(t2,dim); // pour v�rifier sur un petit vecteur
    printf("-------------------------------\n");
    
// M�me principe pour le tri fusion
    return 0;
}


