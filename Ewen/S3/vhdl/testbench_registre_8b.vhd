LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY test IS
END ENTITY;

ARCHITECTURE test_reg8 OF test IS
	SIGNAL clk : STD_LOGIC :='0';
	SIGNAL rst, wr,rd : STD_LOGIC;
	SIGNAL I : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL O : STD_LOGIC_VECTOR(7 DOWNTO 0);

BEGIN
	reg : ENTITY work.registre_8b PORT MAP(clk, rst, wr,rd, I, O);
PROCESS IS
    BEGIN
       FOR i IN 1 to 10 LOOP
            clk <= NOT clk;
			WAIT FOR 10 ns;
        END LOOP;
        WAIT;
    END PROCESS;
	I <= "11001001";
	rst <='0', '1' AFTER 50 ns;
	wr <= '1' AFTER 20 ns;
	rd <= '0', '1' AFTER 30 ns;
END ARCHITECTURE;
