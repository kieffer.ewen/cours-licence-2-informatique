LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
 
ENTITY dr_registre IS
    PORT(   D : IN STD_LOGIC;
        	Q : INOUT STD_LOGIC_VECTOR(7 downto 0);
        	ESD,  WR, SET : IN STD_LOGIC
		);
END ENTITY dr_registre;
 
ARCHITECTURE comp OF dr_registre IS
BEGIN
    PROCESS(CLR, WR)
    BEGIN
        ELSIF CLR = '1' THEN
            Q <= (OTHERS => '0');          
        ELSIF WR = '1' THEN
            Q <= ESD & Q(7 downto 1);
        END IF;
    END PROCESS;
END ARCHITECTURE comp;
