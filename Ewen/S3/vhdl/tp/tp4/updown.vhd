LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.numeric_std.all;
ENTITY compteur IS
	PORT(	clk, updown	: IN STD_LOGIC;
		S 		: OUT STD_LOGIC_VECTOR(2 DOWNTO 0)

	);
END ENTITY compteur;

ARCHITECTURE cpt OF compteur IS
BEGIN
	PROCESS(clk)
	VARIABLE c : INTEGER := 0;
	BEGIN
		IF RISING_EDGE(clk) THEN
			IF updown = '0' THEN		
				c := c + 1;
				IF c = 5 THEN
				 c := 0;
				END IF;
			ELSE 
				c := c - 1;
				IF c = -1 THEN
				 c := 4;
				END IF;
			END IF;	
		END IF;
		S <=  STD_LOGIC_VECTOR(to_unsigned(c, S'LENGTH));
	END PROCESS;
END ARCHITECTURE;

