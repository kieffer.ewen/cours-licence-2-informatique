ENTITY reg8b IS
	PORT(
		datain : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		rw, rd : IN STD_LOGIC;
		dataout : INOUT STD_LOGIC(7 DOWNTO 0)
	);
END ENTITY reg8b;

ARCHITECTURE arcreg OF reg8b IS
BEGIN

	PROCESS(rw) IS
	BEGIN
		IF RISING_EDGE(rw) THEN
		dataout <= datain,
		ELSIF FALLING(ed) THEN
		dataout <= dataout;	
		END IF;
	END PROCESS;

END ARCHITECTURE;
