LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY testcirc IS
END ENTITY;

ARCHITECTURE testbench OF testcirc IS
	SIGNAL clk, ecrDef : STD_LOGIC := '0';
	SIGNAL clk_en : STD_LOGIC := '1';
	SIGNAL S : STD_LOGIC_VECTOR(7 DOWNTO 0);
	CONSTANT period : TIME := 10 ns;
BEGIN
	composant : ENTITY WORK.circ2 PORT MAP(clk, ecrDef,S);
	clk <= (NOT clk) AND clk_en AFTER period/2;

	ecrDef <= '1' after 50 ns;
	clk_en <= '0' after 120 ns;
END ARCHITECTURE;
