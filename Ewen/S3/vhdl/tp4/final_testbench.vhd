LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY final_test IS
END ENTITY;

ARCHITECTURE test OF final_test IS
	SIGNAL	D :  STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL	ecrDef : STD_LOGIC := '1'; 
	SIGNAL	S : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL clk : STD_LOGIC := '0';
	SIGNAL clk_en : STD_LOGIC := '1';
	CONSTANT periode : TIME := 10 ns;
	CONSTANT tmp : TIME := 7 ns;
	
	SIGNAL tmp_en : STD_LOGIC := '1';

BEGIN
	reg : ENTITY work.final PORT MAP(D, clk, ecrDef,S);

	clk <= (NOT clk) AND clk_en AFTER periode/2;
	clk_en <= '0' after 200 ns;
	tmp_en <= '0' after 200 ns;
	
	D<= "11111111", "11010110" after 15 ns, "11001100" AFTER 30 ns, "00001111" AFTER 60 ns;
	ecrDef <= (not ecrDef) AND tmp_en after tmp;

END ARCHITECTURE;
