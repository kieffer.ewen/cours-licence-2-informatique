LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY decoder IS
   PORT
   (
      sel : IN std_logic_vector(2 DOWNTO 0);

      s : out std_logic_vector(7 DOWNTO 0)
   );
end decoder;


ARCHITECTURE dec OF Decoder IS
BEGIN
   s <= "00000001" WHEN sel="000" ELSE
        "00000010" WHEN sel="001" ELSE
        "00000100" WHEN sel="010" ELSE
        "00001000" WHEN sel="011" ELSE
        "00010000" WHEN sel="100" ELSE
        "00100000" WHEN sel="101" ELSE
        "01000000" WHEN sel="110" ELSE
        "10000000";

	
END ARCHITECTURE dec;
