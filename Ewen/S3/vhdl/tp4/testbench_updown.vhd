LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY test_updown is
END ENTITY test_updown;

ARCHITECTURE tb of test_updown IS 
SIGNAL clk, updown : STD_LOGIC := '0';
SIGNAL clk_en : STD_LOGIC := '1'; 
SIGNAL S : STD_LOGIC_VECTOR(2 DOWNTO 0);
CONSTANT periode : TIME := 10 ns;
BEGIN
	compteur : ENTITY WORK.compteur PORT MAP(clk, updown, S);

	clk <= (NOT clk) AND clk_en AFTER periode/2;
	clk_en <= '0' AFTER 120 ns;
	updown <='1' AFTER 60 ns;
END ARCHITECTURE;
