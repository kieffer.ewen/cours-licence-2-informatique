LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY registre_8b IS
	PORT( wr, rd: IN STD_LOGIC;
			dataIn : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			dataOut : INOUT STD_LOGIC_VECTOR(7 DOWNTO 0));
END ENTITY registre_8b;

ARCHITECTURE rtl OF registre_8b IS
BEGIN
	PROCESS(wr, rd) IS
	BEGIN
			IF wr = '1' THEN
				dataOut <= dataIn;
			ELSIF rd = '1' THEN
				dataOut <= (OTHERS =>'Z');
			ELSIF rd = '0' THEN
				dataOut <= dataOut;
			ELSIF wr = '0' THEN
				dataOut <= dataOut;
			END IF;				
	END PROCESS;
END ARCHITECTURE;
