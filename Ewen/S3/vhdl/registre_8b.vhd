LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY registre_8b IS
	PORT(	clk, rst, wr, rd : IN STD_LOGIC;
			dataIn : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			dataOut : INOUT STD_LOGIC_VECTOR(7 DOWNTO 0));
END ENTITY registre_8b;

ARCHITECTURE rtl OF registre_8b IS
BEGIN
	PROCESS(clk)
	BEGIN
		IF RISING_EDGE(clk) THEN
			IF rst = '1' THEN
				dataOut <= (OTHERS => '0');
			ELSIF wr = '1' THEN
				dataOut <= dataIn;						
			END IF;			
			
			IF rd = '1'THEN
				dataOut <= (OTHERS =>'Z');
			ELSE 
				dataOut <= dataOut;
			END IF;
		END IF;
	END PROCESS;
END ARCHITECTURE;
