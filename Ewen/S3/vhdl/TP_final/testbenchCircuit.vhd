LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY test1 IS
END ENTITY;

ARCHITECTURE test OF test1 IS
	SIGNAL rst, wr,rd : STD_LOGIC := '0';
	SIGNAL A , B : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL O : STD_LOGIC_VECTOR(7 DOWNTO 0);
	SIGNAL clk : STD_LOGIC := '0';
	SIGNAL clk_en : STD_LOGIC := '1';
	CONSTANT periode : TIME := 10 ns;

BEGIN
	reg : ENTITY work.top PORT MAP(A, B, wr,rd, clk, O);

	clk <= (NOT clk) AND clk_en AFTER periode/2;
	A <= "11110000";
	B <= "00001111";	
	wr <= '1' AFTER 5 ns;
	rd <= '1' AFTER 30 ns;
	clk_en <= '0' after 60 ns;
END ARCHITECTURE;
