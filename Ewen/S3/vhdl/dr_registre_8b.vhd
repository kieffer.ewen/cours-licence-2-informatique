LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
 
ENTITY dr_registre IS
    PORT(   D : IN STD_LOGIC_VECTOR(7 downto 0);
        	Q : INOUT STD_LOGIC_VECTOR(7 downto 0);
        	ESD,  WR, RD, CLR, SET : IN STD_LOGIC
		);
END ENTITY dr_registre;
 
ARCHITECTURE comp OF dr_registre IS
BEGIN
    PROCESS(CLR, SET, WR, RD)
    BEGIN
        IF SET = '1' THEN
            Q <= D;
        ELSIF CLR = '1' THEN
            Q <= (OTHERS => '0');          
        ELSIF WR = '1' THEN
            Q <= ESD & Q(7 downto 1);
        ELSIF RD = '1' THEN
            Q <= Q(0) & Q(7 downto 1);
        END IF;
    END PROCESS;
END ARCHITECTURE comp;
