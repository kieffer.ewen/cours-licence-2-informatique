if [ $# -ne 1 ]
then

	echo "ERROR 1 : parameters not valid" >&2;
	exit 1;
fi


for i in `ls -1 "$1"`
do
	echo "$i"
	if test -d "$1/$i" 
	then
		sh arbre.sh "$1$i"
	fi
done 
