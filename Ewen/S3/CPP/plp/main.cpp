#include <iostream>
#include <stdlib.h>
#include <vector>
#include "main.hpp"
int Objet::afficher(std::string str)
{
    if(str == "")
    {
        std::cerr<<"ERROR : fonction afficher(std::string) la chaine rentrée en paramètre est vide"<<std::endl;
        exit(-1);
    }
    std::cout<<str<<std::endl;
    return 0;
}
  void Objet::setName_(std::string str ){
      name_ = str;
  }
  
  std::string Objet::getName_(){
      return name_;
  }

int main(){
    Objet m;
    //int nbLigne;
    
    m.setName_("Coucou");
    m.afficher(m.getName_());
 


    return 0;
}
