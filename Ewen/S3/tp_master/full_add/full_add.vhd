IBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;  
USE IEEE.NUMERIC_STD.ALL;
 
ENTITY full_adder IS
PORT(
    A, B, C : IN STD_LOGIC;
    S,R : OUT STD_LOGIC
);
END ENTITY;
 
ARCHITECTURE fdd_full_adder OF full_adder IS
SIGNAL resultat : unsigned(1 downto 0);
BEGIN
    resultat <=('0' & A) + ('0' & B) + ('0' & C);
    S <= resultat(0);      
    R <= resultat(1);
END ARCHITECTURE;
