#include <stdio.h>
#include <stdlib.h>
#include <time.h>
typedef struct Tab
	{
		int* tab;
		int n;
	}Tab;


int int_random(int mod)
{
	return rand() % 100;
}	

Tab remove_multiple(Tab* tableau,Tab* dest, int value)
{
	// si value est égal à 0 on quitte la fonction car division par 0 impossible
	if(value==0)
		exit(2);
	Tab* t = tableau;
	Tab * t2 = dest;
	int count = 0;
	for(int i=0;i<t->n;i++)
	{
		if((t->tab[i]%value)==0){
		count++;
		}
	}

	t2->tab = malloc(count*sizeof(int));
	if(t->tab==NULL)
		printf("Erreur malloc");

	int j = 0;
	t2->n = count;
	for(int i=0; i<t->n && j<t2->n;i++)
	{
		if((t->tab[i]%value)==0){
			t2->tab[j]=t->tab[i];
			printf("pour j:%d et i:%d = %d",j,i,t->tab[i]);
			j++;
		}
	}
	return *t2;
}

int main(int argc, char** argv)
{
	srand(time(NULL));

	Tab t;
	Tab tab2;
	printf("Nombre d'elements :");
	scanf("%d",&t.n);

	t.tab = malloc(t.n*sizeof(int));
	if(t.tab==NULL)
		printf("Erreur malloc");

	for(int i=0;i<t.n;i++)
	{
		t.tab[i] = int_random(100);
	}	
	
	tab2 = remove_multiple(&t, &tab2, 7);
	
	printf("####### TAB INIT #######");	

	for(int i=0;i<t.n;i++)
		{
			printf("%d\n",t.tab[i]);
		}
	printf("####### TAB MULTIPLE #######");	
	for(int i=0;i<tab2.n;i++)
		{
			printf("%d\n",tab2.tab[i]);
		}	

	free(tab2.tab);
	free(t.tab);				
	return 0;
}
