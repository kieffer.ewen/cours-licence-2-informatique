#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int int_random(int mod)
{
	return rand() % mod;
}	
//Initialisation d'une matric
int** init_matrice(int x, int y)
{
	//notre matrice T
	int** T = malloc(x*sizeof(int*));
	if(T==NULL)
		printf("Erreur malloc T");
    
	for(int n=0;n<y;n++)
    {
        T[n] = malloc(y*sizeof(int));
	if(T[n]==NULL)
			printf("Erreur malloc T[%d]",n);
    }
   
 for(int j=0;j<y;j++) 
    {
        for(int i=0;i<x;i++) 
        {
            T[j][i]=int_random(100); // On initialise chaque champs du tableau à une valeur au hasard
        }
    }
	return T;
}

//affiche une matrice
void afficher_matrice(int** matrice,int x,int y)
{
	printf("\nLa matrice que vous avez rentré est : \n");
	  for(int i=0;i<y;i++){
        for(int j=0;j<x;j++){
            printf("%d  ",matrice[i][j]);
        }
		printf("\n");
    }
}

//fonction principale
int main(int argc, char **argv)
{
	srand(time(NULL));
  	int x = atoi(argv[1]);
	int y = atoi(argv[2]);
    int** m1 = init_matrice(x,y);    
	afficher_matrice(m1,x,y);	
	free(m1);
	
 	return 0; 
}
