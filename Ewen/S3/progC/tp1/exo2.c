#include <stdio.h>

int isPalindrome(int tab[], const int n)
{
	for(int i=0; i<n;i++)
	{
		if(tab[i]!=tab[n-1-i])
			return 0;
	}
	return 1;
}

void init_tab(int tab[], const int n)
{
	for(int i=0;i<n;i++)
	{
		printf("affectez une valeur a tab[%d] :",i);
		scanf("%d", &	tab[i]);
	}
}

int main()
{	
	int tab_size;	
	printf("Donnez la dimension du tableau : ");
	scanf("%d",&tab_size);
	int tab[tab_size];
	init_tab(tab,tab_size);
	printf("%d\n",isPalindrome(tab,10));
	return 0;
}
