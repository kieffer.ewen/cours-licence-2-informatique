#include <stdio.h>

int puissance(int x, int n)
{
	int tmp = x;
	for(int i = 0;i<n-1;i++)
	{
		tmp=tmp*x;
	}
	return tmp;	
}

int puissanceRec(int x, int n,int prec)
{	
	if(n==1)
	{
		return x;
	}
	return puissanceRec(x*prec,n-1,prec);
}

int main()
{
	int t = puissance(2,3);
	printf("%d\net : %d\n", t, puissanceRec(2,3,2));
	return 0;
}
