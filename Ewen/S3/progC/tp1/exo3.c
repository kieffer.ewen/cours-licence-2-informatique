#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <time.h>

#define N_MAX 1000

typedef struct{
	int tab[N_MAX]; //tableau statique
	int n; //nombre de valeur 
} Tab;

//génere un nombre aléatoire
int int_random()
{
	srand(time(NULL));
	return rand();
}	

//initialise nb_val elements dans un tableau avec des valeurs aléatoires
Tab init_tab(int nb_val)
{	
	//si le nombre de valeur à initialiser est supérieur à N_MAX on sort de la fonction
	if(nb_val > N_MAX)
		exit(1);
	Tab t;	
	int t[nb_val];
	t.tab = t;
	printf("%ld", sizeof(t.tab)/sizeof(int));
	t.n = nb_val;

	for(int i=0;i<t.n;i++)
	{
		t.tab[i] = random();
	}
	return t;
}

//supprime les multiples de value dans un tableau 
Tab remove_multiple(Tab* tableau,int value)
{
	// si value est égal à 0 on quitte la fonction car division par 0 impossible
	if(value==0)
		exit(2);
	Tab* t = tableau;
	for(int i=0;i<t->n;i++)
	{
		if((t->tab[i]%value)==0)
			t->tab[i]=0;
	}
	return *t;
}

//fonction principale
int main()
{
	//initialisation de t avec 10 valeurs aléatoirie
	Tab t = init_tab(10);
	t.tab[5] = 7;
	t.tab[0] = 14;
	//supprime les multiples de 7
	t = remove_multiple(&t,7);

	for(int i=0;i<t.n;i++)
	{
		printf("Tab[%d] : %d\n",i, t.tab[i]);
	}
	return 0;
}
