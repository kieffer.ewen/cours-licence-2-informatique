<?php
	require("includes/header.php");
	require("php/navbar.php");

?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Foundation Starter Template</title>
  </head>
  <body>
    <h1>Hello, world!</h1>

    <script>
      $(document).foundation();
    </script>

  </body>
</html>
